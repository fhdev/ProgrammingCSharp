﻿using System;

namespace BemenetKimenet
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Program céljának leírása. */
            Console.WriteLine("Ez a program gyakorlás a konzolos be- és kimenetet használatához.");
            /* Szöveg beolvasása korábban létrehozott változóba. */
            Console.WriteLine("Kérem írjon be egy szöveget!");
            string bemenet1;
            bemenet1 = Console.ReadLine();
            Console.WriteLine("A beolvasott szöveg: " + bemenet1);
            /* Szöveg beolvasása itt létrehozott változóba. */
            Console.WriteLine("Kérem írjon be egy szöveget!");
            string bemenet2 = Console.ReadLine();
            Console.WriteLine("A beolvasott szöveg: " + bemenet2);
            /* Lenyomott billentyű beolvasása. */
            Console.Write("Kérem nyomjon le egy billentyűt! ");
            ConsoleKeyInfo billentyu = Console.ReadKey();
            Console.WriteLine();
            Console.WriteLine("A lenyomott billentyű: " + billentyu.Key.ToString());
            /* Egész szám beolvasása (Convert osztály). */
            Console.WriteLine("Kérem adjon meg egy egész számot!");
            bemenet1 = Console.ReadLine();
            int szam1 = Convert.ToInt32(bemenet1);
            Console.WriteLine("A beolvasott szám: " + szam1);
            /* Egész szám beolvasása (Parse statikus metódus). */
            Console.WriteLine("Kérem adjon meg még egy egész számot!");
            bemenet1 = Console.ReadLine();
            double szam2 = double.Parse(bemenet1);
            Console.WriteLine("Az eddig beolvasott számok: {0:D} és {1:F3}", szam1, szam2);
            /* Egész szám beolvasása (TryParse statikus metódus). */
            Console.WriteLine("Kérem adjon meg egy harmadik egész számot!");
            bemenet1 = Console.ReadLine();
            int szam3;
            bool siker = int.TryParse(bemenet1, out szam3);
            Console.WriteLine("A beolvasás sikeres: " + siker.ToString());
            Console.WriteLine("A beolvasott szám: {0:D}", szam3);
            Console.Write("Kérem nyomjon le egy billentyűt a kilépéshez...");
            Console.ReadKey();
        }
    }
}
