﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hf10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Kérem adja meg a mátrix méretét!");
            Console.Write("n = ");
            int n;
            int.TryParse(Console.ReadLine(), out n);

            Console.WriteLine("Kérem adja meg a mátrix elemeit!");
            int[,] matrix = new int[n, n];
            for (int sor = 0; sor < n; sor++)
            {
                for (int oszlop = 0; oszlop < n; oszlop++)
                {
                    Console.Write("M[{0},{1}] = ", sor, oszlop);
                    int.TryParse(Console.ReadLine(), out matrix[sor, oszlop]);
                }
            }

            Console.WriteLine("Eredmények:");
            for (int sor = 0; sor < n; sor++)
            {
                int paros7Oszthato = 0;
                for (int oszlop = 0; oszlop < n; oszlop++)
                {
                    if(((matrix[sor,oszlop] % 7) == 0) || ((matrix[sor, oszlop] % 2) == 0))
                    {
                        paros7Oszthato++;
                    }
                }
                Console.WriteLine("{0}", paros7Oszthato);
            }
        }
    }
}
