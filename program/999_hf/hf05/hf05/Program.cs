﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hf05
{
    class Program
    {
        static void Main(string[] args)
        {
            double szam1 = 0, szam2 = 0;
            Console.Write("szam1 = ");
            double.TryParse(Console.ReadLine(), out szam1);
            Console.Write("szam2 = ");
            double.TryParse(Console.ReadLine(), out szam2);
            Console.WriteLine("Eredmény:");
			Console.Write("A nagyobb és kisebb szám hányadosa:");
            if (szam1 > szam2)
            {
                Console.WriteLine(szam1 / szam2);
            }
            else
            {
                Console.WriteLine(szam2 / szam1);
            }

        }
    }
}
