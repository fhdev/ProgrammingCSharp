﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hf11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Kérem írjon be egy szöveget!");
            string szoveg = "";
            string sor;
            while (!string.IsNullOrEmpty(sor = Console.ReadLine()))
            {
                szoveg += sor;
            }

            szoveg = szoveg.ToLower();
            int szamlalo = 0;
            for (int i = 0; i < szoveg.Length - 1; i++)
            {
                if (char.IsLetter(szoveg[i + 1]) && char.IsLetter(szoveg[i]) && (szoveg[i + 1] - szoveg[i]) == 1)
                    szamlalo++;
            }
            Console.WriteLine("Eredmény:");
            Console.WriteLine(szamlalo);
        }
    }
}
