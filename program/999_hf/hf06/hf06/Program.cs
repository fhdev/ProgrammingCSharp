﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hf06
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Adja meg, hogy soros (S) vagy párhuzamos (P) kapcsolás eredőjét szeretné kiszámítani.");
            string opcio = Console.ReadLine().ToUpper();
            double eredo = 0;
            double ertek = 1;
            while (ertek > 0)
            {
                Console.Write("R = ");
                double.TryParse(Console.ReadLine(), out ertek);
                if (ertek > 0)
                {
                    if (opcio == "S")
                    {
                        eredo += ertek;
                    }
                    else if (opcio == "P")
                    {
                        eredo += 1 / ertek;
                    }
                    else
                    {
                        Console.WriteLine("Ilyen opció nem létezik!");
                        break;
                    }
                }
            }
            if (opcio == "P")
            {
                eredo = 1 / eredo;
            }
			Console.WriteLine("Eredmény:");
            Console.WriteLine("Re = {0}", eredo);
        }
    }
}
