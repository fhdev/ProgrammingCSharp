﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hf08
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Adja meg a tömb méretét!");
            Console.Write("n = ");
            int n;
            int.TryParse(Console.ReadLine(), out n);

            Console.WriteLine("Adja meg a tömb elemeit!");
            double[] tomb = new double[n];
            for (int i = 0; i < n; i++)
            {
                Console.Write("x[{0}] = ", i);
                double.TryParse(Console.ReadLine(), out tomb[i]);
            }

            double atlag = 0;
            for (int i = 0; i < n; i++)
            {
                atlag += tomb[i];
            }
            atlag /= n;

            int nagyobbak = 0;
            int kisebbek = 0;
            for (int i = 0; i < n; i++)
            {
                if (tomb[i] > atlag)
                    nagyobbak++;
                else if (tomb[i] < atlag)
                    kisebbek++;
            }
            Console.WriteLine("Eredmények:");
            Console.WriteLine("átlagnál nagyobb elemek száma = {0}", nagyobbak);
            Console.WriteLine("átlagnál kisebb elemek száma = {0}", kisebbek);
        }
    }
}
