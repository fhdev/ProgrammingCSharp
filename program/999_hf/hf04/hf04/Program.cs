﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hf04
{
    class Program
    {
        static void Main(string[] args)
        {
            
            double szam1 = 0, szam2 = 0, szam3 = 0;
            double legnagyobb = 0;
            Console.Write("Az első szám: ");
            double.TryParse(Console.ReadLine(), out szam1);
            Console.Write("Az második szám: ");
            double.TryParse(Console.ReadLine(), out szam2);
            Console.Write("Az harmadik szám: ");
            double.TryParse(Console.ReadLine(), out szam3);

            if (szam1 > szam2)
            {
                if (szam1 > szam3)
                {
                    legnagyobb = szam1;
                }
                else
                {
                    legnagyobb = szam3;
                }
            }
            else
            {
                if (szam2 > szam3)
                {
                    legnagyobb = szam2;
                }
                else
                {
                    legnagyobb = szam3;
                }
            }
			Console.WriteLine("Eredmény:");
            Console.WriteLine("A legnagyobb szám: {0:G}", legnagyobb);
        }
    }
}
