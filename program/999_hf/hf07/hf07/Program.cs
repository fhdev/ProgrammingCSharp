﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hf07
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Adja meg a maximális lépésszámot!");
            Console.Write("n = ");
            int n;
            int.TryParse(Console.ReadLine(), out n);

            Console.WriteLine("Adja meg a pontosságot!");
            Console.Write("eps = ");
            double eps;
            double.TryParse(Console.ReadLine(), out eps);

            double exp = 1;
            double fact = 1;
            double inc = 0;

            for(int i = 1; i <= n; i++)
            {
                fact *= i;
                inc = 1 / fact;
                if (Math.Abs(inc) < eps)
                    break;
                exp += inc;  
            }
			Console.WriteLine("Eredmény:");
            Console.WriteLine("e = {0}", exp);
        }
    }
}
