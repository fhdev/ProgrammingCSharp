﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hf08
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Adja meg a tömb méretét!");
            Console.Write("n = ");
            int n;
            int.TryParse(Console.ReadLine(), out n);

            Console.WriteLine("Adja meg a tömb elemeit!");
            int[] tomb = new int[n];
            for (int i = 0; i < n; i++)
            {
                Console.Write("x[{0}] = ", i);
                int.TryParse(Console.ReadLine(), out tomb[i]);
            }

            int legnagyobb = tomb[0];
            int iLegnagyobb = 0;
            int legkisebb = tomb[0];
            int iLegkisebb = 0;
            for (int i = 1; i < n; i++)
            {
                if (tomb[i] > legnagyobb)
                {
                    legnagyobb = tomb[i];
                    iLegnagyobb = i;
                }
                if(tomb[i] < legkisebb)
                {
                    legkisebb = tomb[i];
                    iLegkisebb = i;
                }
            }
            Console.WriteLine("Eredmények:");
            Console.WriteLine("legnagyobb = {0}", legnagyobb);
            Console.WriteLine("legnagyobb index = {0}", iLegnagyobb);
            Console.WriteLine("legkisebb = {0}", legkisebb);
            Console.WriteLine("legkisebb index = {0}", iLegkisebb);
        }
    }
}
