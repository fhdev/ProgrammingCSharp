﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nagyobb
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Két szám közül a nagyobbik kiválasztása.");
            /* Első szám beolvasása. */
            Console.Write("Kérlek írj be egy egész számot. a = ");
            string bemenet = Console.ReadLine();
            int a;
            int.TryParse(bemenet, out a);
            /* Második szám beolvasása. */
            Console.Write("Kérlek írj be még egy egész számot. b = ");
            bemenet = Console.ReadLine();
            int b;
            int.TryParse(bemenet, out b);
            /* Reláció megállapítása és eredmény kiírása. */
            if(a > b)
            {
                Console.WriteLine("Az első szám a nagyobb. {0:D} > {1:D}", a, b);
            }
            else if(a == b)
            {
                Console.WriteLine("A két szám egyenlő. {0:D} = {1:D}", a, b);
            }
            else
            {
                Console.WriteLine("A második szám a nagyobb. {0:D} < {1:D}", a, b);
            }
            Console.ReadKey();
        }
    }
}
