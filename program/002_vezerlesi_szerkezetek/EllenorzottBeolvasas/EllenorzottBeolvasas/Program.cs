﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EllenorzottBeolvasas
{
    class Program
    {
        static void Main(string[] args)
        {
            /* A program céljának leírása. */
            Console.WriteLine("Egész szám ellenőrzött beolvasása.");
            Console.WriteLine("Konverzió sikerességének vizsgálata.");
            /* Egész szám beolvasása */
            Console.Write("Kérem írjon be egy egész számot! a = ");
            string bemenet = Console.ReadLine();
            int szam;
            bool sikerult = int.TryParse(bemenet, out szam);
            /* Tájékoztatjuk a felhasználót hogy sikerült-e a konverzió. */
            if(sikerult)
            {
                Console.WriteLine("A konverzió sikerült. A változó értéke: {0:D}", szam);
            }
            else
            {
                Console.WriteLine("A konverzió nem sikerült. A változó értéke: {0:D}", szam);
            }
            /* Ellenőrzött beolvasás. */
            Console.WriteLine("Ellenőrzött beolvasás.");
            Console.WriteLine("Kérem írjon be egy egész számot!");
            int ujSzam;
            string ujBemenet;
            bool ujSikerult;
            do
            {
                Console.Write("a = ");
                ujBemenet = Console.ReadLine();
                ujSikerult = int.TryParse(ujBemenet, out ujSzam);
            } while (!ujSikerult);
            Console.WriteLine("A beírt szám: {0:D}", ujSzam);
            /* Ellenőrzött beolvasás tömören. */
            Console.WriteLine("Ellenőrzött beolvasás tömören.");
            Console.WriteLine("Kérem írjon be egy egész számot!");
            int legujabbSzam;
            do
            {
                Console.Write("a = ");
            } while (!int.TryParse(Console.ReadLine(), out legujabbSzam));
            Console.WriteLine("A beírt szám: {0:D}", legujabbSzam);
            /* Várunk egy billentyű lenyomására a kilépés előtt. */
            Console.Write("Kérem nyomjon le egy billentyűt a kilépéshez...");
            Console.ReadKey();
        }
    }
}
