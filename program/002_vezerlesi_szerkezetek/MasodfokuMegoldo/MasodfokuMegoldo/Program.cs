﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasodfokuMegoldo
{
    class Program
    {
        static void Main(string[] args)
        {
            /* A program céljának leírása. */
            Console.WriteLine("Az a*x^2+b*x+c=0 másodfokú egyenlet megoldása.");
            /* Az egyenlet együtthatóinak beolvasása. */
            Console.WriteLine("Kérem írja be az egyenlet együtthatóit.");
            Console.Write("a = ");
            string bemenet = Console.ReadLine();
            double a;
            double.TryParse(bemenet, out a);
            Console.Write("b = ");
            bemenet = Console.ReadLine();
            double b;
            double.TryParse(bemenet, out b);
            Console.Write("c = ");
            bemenet = Console.ReadLine();
            double c;
            double.TryParse(bemenet, out c);
            /* A bemeneti adatok ellenőrzése mellett az egyenlet megoldása. */
            /* Az a == 0.0 esetén a megoldóképlet nullával való osztást tartalmazna! */
            if (a == 0.0)
            {
                if (b == 0.0)
                {
                    if (c == 0.0)
                    {
                        Console.WriteLine("A megadott egyenlet azonosságra vezet.");
                    }
                    else
                    {
                        Console.WriteLine("A megadott egyenlet ellentmondásra vezet.");
                    }
                }
                else
                {
                    double x = -c / b;
                    Console.WriteLine("A megadott egyenlet elsőfokú.");
                    Console.WriteLine("A megoldás:  x = {0:F4}", x);
                }
            }
            else
            {
                double D = b * b - 4 * a * c;
                /* A D < 0.0 esetén D értékéből nem vonhatunk gyököt! */
                if (D < 0.0)
                {
                    double valosResz = -b / (2.0 * a);
                    double kepzetesResz = Math.Sqrt(-D) / (2.0 * a);
                    Console.WriteLine("A megadott másodfokú egyenletnek komplex konjugált gyökei vannak.");
                    Console.WriteLine("A megoldás:  x1 = {0:F4} + {1:F4}i  x2 = {0:F4} - {1:F4}i", valosResz, kepzetesResz);
                }
                else
                {
                    double x1 = (-b + Math.Sqrt(D)) / (2.0 * a);
                    double x2 = (-b - Math.Sqrt(D)) / (2.0 * a);
                    if (x1 == x2)
                    {
                        Console.WriteLine("A megadott másodfokú egyenletnek egy kétszeres multiplicitású valós gyöke van.");
                        Console.WriteLine("A megoldás:  x12 = {0:F4}", x1);
                    }
                    else
                    {
                        Console.WriteLine("A megadott másodfokú egyenletnek két valós gyöke van.");
                        Console.WriteLine("A megoldás:  x1 = {0:F4}  x2 = {1:F4}", x1, x2);
                    }
                }
                /* A programból való kilépés előtt várunk egy billentyűzet lenyomására. */
                Console.Write("Kérem nyomjon meg egy billentyűt a kilépéshez... ");
                Console.ReadKey();
            }
        }
    }
}
