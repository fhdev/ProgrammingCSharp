﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EredoEllenallas
{
    class Program
    {
        static void Main(string[] args)
        {
            // Program céljának leírása.
            Console.WriteLine("Eredő ellenállás számítása.");
            // Számítási mód kiválasztása.
            Console.WriteLine("Soros, vagy párhuzamos kapcsolást kíván számítani (S/P)?");
            string valasz = Console.ReadLine();
            // R - aktuális ellenállásérték, RE - eredő ellenállásérték
            double R = 0.0;
            double RE = 0.0;
            // Soros eredő számítása
            if(valasz == "S")
            {
                do
                {
                    Console.WriteLine("Kérem adja meg a következő ellenállás értékét!");
                    Console.Write("R = ");
                    double.TryParse(Console.ReadLine(), out R);
                    if (R > 0.0)
                    {
                        // Összegzés
                        RE += R;
                    }

                } while (R > 0.0);
                Console.WriteLine("Az eredő ellenállás: {0:F4} Ohm.", RE);
            }
            // Párhuzamos eredő számítása
            else if(valasz == "P")
            {
                do
                {
                    Console.WriteLine("Kérem adja meg a következő ellenállás értékét!");
                    Console.Write("R = ");
                    double.TryParse(Console.ReadLine(), out R);
                    if (R > 0.0)
                    {
                        // Reciprok összegzés
                        RE += 1 / R;
                    }

                } while (R > 0.0);
                if (RE > 0.0)
                {
                    // Recpirokösszegek reciproka
                    RE = 1 / RE;
                }
                Console.WriteLine("Az eredő ellenállás: {0:F4} Ohm.", RE);
            }
            else
            {
                Console.WriteLine("Ilyen opció \"" + valasz + "\" nincs.");
            }
        }
    }
}
