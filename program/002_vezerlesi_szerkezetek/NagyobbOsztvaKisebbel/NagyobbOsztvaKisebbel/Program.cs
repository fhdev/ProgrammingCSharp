﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NagyobbOsztvaKisebbel
{
    class Program
    {
        static void Main(string[] args)
        {
            // Program céljának leírása
            Console.WriteLine("Két szám közül a nagyobbik elosztása a kisebbikkel.");
            // Számok beolvasása
            Console.WriteLine("Kérem adja meg az első számot!");
            double nagy;
            double.TryParse(Console.ReadLine(), out nagy);
            Console.WriteLine("Kérem adja meg a második számot!");
            double kicsi;
            double.TryParse(Console.ReadLine(), out kicsi);
            // Nagyobbik szám megtalálása
            if(kicsi > nagy)
            {
                double seged = nagy;
                nagy = kicsi;
                kicsi = seged;
            }
            // Osztás végrehajtása és eredmények kiírása
            if(kicsi != 0)
            {
                Console.WriteLine("Az eredmény: {0:F4} .", nagy / kicsi);
            }
            else
            {
                Console.WriteLine("A nullával való osztás értelmetlen!");
            }
        }
    }
}
