﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            // A program rövid leírása.
            Console.WriteLine("Egyszerű számológép, mely az alapműveletek elvégzésére alkalmas.");
            // Az első szám beolvasása. 
            Console.WriteLine("Kérem írja be az első számot!");
            double a = 0.0;
            do
            {
                Console.Write("\ta = ");
            }
            while (!double.TryParse(Console.ReadLine(), out a));
            // Operátor beolvasása. Csak a megfelelő (+, -, *, /) operátorok elfogadása.
            Console.WriteLine("Kérem írjba be az operátort (+, -, *, /).");
            string op;
            do
            {
                Console.Write("\tMűvelet: ");
                op = Console.ReadLine();
            }
            while ((op != "+") && (op != "-") && (op != "*") && (op != "/"));
            // Második szám beolvasása.
            Console.WriteLine("Kérem adja meg a második számot.");
            double b = 0.0;
            do
            {
                Console.Write("\tb = ");
            }
            while (!double.TryParse(Console.ReadLine(), out b));
            // Művelet eredményének kiszámítása.
            double eredmeny = 0.0;
            switch (op)
            {
                case "+":
                    {
                        eredmeny = a + b;
                        break;
                    }
                case "-":
                    {
                        eredmeny = a - b;
                        break;
                    }
                case "*":
                    {
                        eredmeny = a * b;
                        break;
                    }
                case "/":
                    {
                        eredmeny = a / b;
                        break;
                    }
            }
            // Eredmények kiírása.
            Console.WriteLine("Eredmény:\n\t{0:F5} " + op + " {1:F5} = {2:F5}", a, b, eredmeny);
            // Kilépés előtt egy billentyűlenyomás megvárása.
            Console.Write("Kérem nyomjon meg egy gombot a kilépés előtt...");
            Console.ReadKey();
        }
    }
}