﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TombKeres
{
    class Program
    {
        static void Main(string[] args)
        {
            // A program céljának rövid leírása. 
            Console.WriteLine("Tömb feltöltése random elemekkel, adott elem keresése, páros és" + 
                              " páratlan elemek számának meghatározása.");
            // A tömb méretének beolvasása. 
            Console.WriteLine("\nKérem adja meg a tömb méretét!");
            int n;
            do
            {
                Console.Write("\tn = ");
            }
            while (!int.TryParse(Console.ReadLine(), out n));
            // Tömb létrehozása.
            int[] szamok = new int[n];
            // Random objektum létrehozása a véletlenszámgeneráláshoz.
            Random rnd = new Random();
            // Tömb elemeinek generálása
            Console.WriteLine("\nTömb elemeinek generálása 0 és 100 között véletlenszerűen...");
            for (int i = 0; i < szamok.Length; i++)
            {
                szamok[i] = rnd.Next(0, 101);
                Console.WriteLine("szamok[{0:D}] = {1:D}", i, szamok[i]);
            }
            // Keresni kívánt elem megadása. 
            int keres;
            Console.WriteLine("\nKérem adja meg, melyik számot kívánja a tömbben megkeresni.");
            do
            {
                Console.Write("\tkeres = ");
            }
            while (!int.TryParse(Console.ReadLine(), out keres));
            // Első előfordulás meghatározása lineáris kereséssel. 
            bool talalt = false;
            for (int i = 0; i < szamok.Length; i++)
            {
                // Megtaláltuk a keresett elemet.
                if (szamok[i] == keres)
                {
                    Console.WriteLine("\nA(z) {0:D} szám első előfordulása a tömb {1:D}. eleme.", keres, i + 1);
                    talalt = true;
                    break;
                }
            }
            // Utolsó előfordulás meghatározása lineáris kereséssel.
            for (int i = szamok.Length - 1; i >= 0; i--)
            {
                // Megtaláltuk a keresett elemet.
                if (szamok[i] == keres)
                {
                    Console.WriteLine("\nA(z) {0:D} szám utolsó előfordulása a tömb {1:D}. eleme.", keres, i + 1);
                    talalt = true;
                    break;
                }
            }
            // Ha a keresett szám nem eleme a tömbnek, arról is tájékoztassuk a felhasználót.
            if (!talalt)
            {
                Console.WriteLine("\nA(z) {0:D} szám nem eleme a tömbnek.", keres);
            }
            // Páros és páratlan számok mennyiségének meghatározása.
            int paros = 0;
            int paratlan = 0;
            for (int i = 0; i < szamok.Length; i++)
            {
                // Ha egy szám páros, akkor maradék nélkül osztható kettővel. 
                if((szamok[i] % 2) == 0)
                {
                    paros++;
                }
                else
                {
                    paratlan++;
                }
            }
            Console.WriteLine("A tömbben {0:D} páros és {1:D} páratlan szám van.", paros, paratlan);
        }
    }
}
