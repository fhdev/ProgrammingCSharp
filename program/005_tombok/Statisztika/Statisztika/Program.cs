﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statisztika
{
    class Program
    {
        static void Main(string[] args)
        {
            // Program rövid ismertetése.
            Console.WriteLine("Mérési minták átlagának és szórásának számítása.");
            // A mérési minták számának beolvasása. 
            Console.WriteLine("Kérem adja meg a mérési adatok számát.");
            int n;
            do
            {
                Console.Write("\tn = ");
            }
            while (!int.TryParse(Console.ReadLine(), out n));
            // Mérési adatok tárolására szolgáló tömb létrehozása. 
            double[] adatok = new double[n];
            // Mérési adatok beolvasása.
            Console.WriteLine("Kérem egymás után adja meg a mérési adatokat.");
            for (int i = 0; i < adatok.Length; i++)
            {
                do
                {
                    Console.Write("\t{0:D}. érték = ", i + 1);
                }
                while (!double.TryParse(Console.ReadLine(), out adatok[i]));
            }
            // Értékek átlagának kiszámítása.
            double atlag = 0.0;
            for (int i = 0; i < adatok.GetLength(0); i++)
            {
                atlag += adatok[i];
            }
            atlag /= n;
            // Értékek szórásnégyzetének kiszámítása
            double szorasNegyzet = 0.0;
            for (int i = 0; i < adatok.Length; i++)
            {
                szorasNegyzet += (adatok[i] - atlag) * (adatok[i] - atlag);
            }
            double variancia = szorasNegyzet / n;
            double korrigaltVariancia = szorasNegyzet / (n - 1);
            // Eredmények megjelenítése.
            Console.WriteLine("Eredmények:");
            Console.WriteLine("Átlag:               E(x)    = {0:F5}", atlag);
            Console.WriteLine("Variancia:           Var(x)  = {0:F15}", variancia);
            Console.WriteLine("Korrigált variancia: Var*(x) = {0:F15}", korrigaltVariancia);
        }
    }
}
