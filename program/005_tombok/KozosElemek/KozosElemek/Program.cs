﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KozosElemek
{
    class Program
    {
        static void Main(string[] args)
        {
            // A program céljának rövid ismertetése. 
            Console.WriteLine("Két tömb közös elemeinek meghatározása. A tömbökön belül nincs ismétlődő elem.");
            Console.WriteLine("Kérem adja meg az első 5 elemű tömb elemeit.");
            // Ebben a változóban fogjuk tárolni az első tömböt. (array1 = tömb1) 
            int[] tomb1 = new int[5];
            // Végigmegyünk az új, "üres" tömb minden elemén. 
            for (int i = 0; i < tomb1.Length; i++)
            {
                // Ebben a szöveges változóban fogjuk tárolni a felhasználó által beírt bemeneteket.
                string bemenet;
                // Ennek a logikai változónak az értékével jelezzük, ha a felhasználó által beírt 
                // bemenet érvénytelen volt. Ha az értéke igaz, akkor a bemenet érvénytelen, 
                // ha hamis, akkor érvényes volt. 
                bool ervenytelen = false;
                do
                {
                    // Feltesszük, hogy a bemenet érvényes lesz. 
                    ervenytelen = false;
                    // Bekérjük a tömb aktuális elemét a felhasználótól. 
                    Console.Write("\ttomb1[{0:D}] = ", i);
                    bemenet = Console.ReadLine();
                    // A bevitt szöveget megpróbáljuk egész számmá konvertálni. A TryParse értéke 
                    // igaz, ha a konverzió sikerült, így negáltja akkor igaz, mikor a konverzió nem 
                    // sikerül. Azaz az ervenytelen változó igaz lesz, ha a konverzió sikertelen volt. 
                    ervenytelen = !int.TryParse(bemenet, out tomb1[i]);
                    // Ha a konverzió sikertelen volt tájékoztatjuk a felhasználót, és ugrunk a 
                    // ciklus következő körére, az ez utáni dolgokat már nem hajtjuk végre. 
                    // Ezt csinálja a continue. 
                    if (ervenytelen)
                    {
                        Console.WriteLine("Hiba! A megadott bemenet nem egy egész szám.");
                        continue;
                    }
                    // Ha a felhasználó által beírt bemenet helyesen egy egész szám, akkor megnézzük,
                    // hogy a tömb eddig megadott elemei között szerepel-e már ez a szám. Az i éppen  
                    // az aktuálisan beolvasott szám indexe, tehát a j-vel a régebbi (i-nél kisebb
                    // indexű) elemeken megyünk végig. 
                    for (int j = 0; j < i; j++)
                    {
                        // Az i-edik elem az, amit most szeretnénk beolvasni, a j-edik pedig minden 
                        // lépésben valamelyik korábban beolvasott elem. Ha ezek megegyeznek, akkor 
                        // az azt jelenti, hogy megpróbálunk egy számot másodszorra is beírni, ami 
                        // érvénytelen. 
                        if (tomb1[i] == tomb1[j])
                        {
                            ervenytelen = true;
                            // A vizsgálatot nem is folytatjuk, hiszen ha már beírták ezt a számot, 
                            // mindenképpen másikat kell kérnünk. 
                            break;
                        }
                    }
                    // Ha már szerepelt a megadott szám a tömbben, akkor erről tájékoztatjuk a 
                    // felhasználót, és újat kérünk, hiszen a do-while ciklusból így nem lépünk ki. 
                    if (ervenytelen)
                    {
                        Console.WriteLine("Hiba! Ez a szám már szerepel a tömbben.");
                    }
                } while (ervenytelen); // Ha kilépünk a ciklusból az azt jelenti, hogy az i-edik 
                                       // elemet sikeresenbeolvastuk.
            } // Ha ebből a ciklusból is kilépünk, akkor már az egész tömb megvan. 
            // Második tömb beolvasása ugyanúgy, mint az első esetben. 
            Console.WriteLine("Kérem adja meg az második 5 elemű tömb elemeit.");
            // Ebben a változóban fogjuk tárolni az második tömböt. (array2 = tömb2) 
            int[] array2 = new int[5];
            for (int i = 0; i < array2.Length; i++)
            {
                string bemenet;
                bool ervenytelen = false;
                do
                {
                    Console.Write("\tarray2[{0:D}] = ", i);
                    bemenet = Console.ReadLine();
                    ervenytelen = !int.TryParse(bemenet, out array2[i]);
                    if (ervenytelen)
                    {
                        Console.WriteLine("Hiba! A megadott bemenet nem egy egész szám.");
                        continue;
                    }
                    for (int j = 0; j < i; j++)
                    {
                        if (array2[i] == array2[j])
                        {
                            ervenytelen = true;
                            break;
                        }
                    }
                    if (ervenytelen)
                    {
                        Console.WriteLine("Hiba! Ez a szám már szerepel a tömbben.");
                    }
                } while (ervenytelen);
            }
            // Ebben a változóban fogjuk számolni, hogy a két tömbben hány közös elem van.
            int szamlalo = 0;
            // Az első tömb minden elemén végigmegyünk. 
            for (int i = 0; i < tomb1.Length; i++)
            {
                // Az első tömb minden elemének esetén megvizsgáljuk a második tömb minden elemét is. 
                for (int j = 0; j < array2.Length; j++)
                {
                    // Ha a két elem megegyezik, akkor növeljük a számlálót. 
                    if (tomb1[i] == array2[j])
                    {
                        szamlalo++;
                    }
                }
            }
            // Kiírjuk az eredményeket. 
            Console.WriteLine("Eredmény: a két tömb közös elemeinek száma: {0:D}.", szamlalo);
        }
    }
}
