﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ido
{
    class Program
    {
        // Ez a struktúra alkalmas egy időadat eltárolására. 
        struct Idopont
        {
            // Órák száma. 
            public int Orak;
            // Percek száma. 
            public int Percek;
            // Másodpercek száma. 
            public int Masodpercek;
        }
        static void Main(string[] args)
        {
            // Röviden ismertetjük a program célját. 
            Console.WriteLine("Az éjfél óta eltelt másodpercek alapján a pontos idő meghatározása.\n");
            // Beolvassuk az éjfél óta eltelt másodpercek számát. 
            Console.WriteLine("Kérem adja meg az éjfél óta eltelt másodpercek számát.");
            int masodpercek = EgeszBeolvas("Éjfél óta eltelt másodpercek száma");
            // Ha a felhasználó egy napnál kisebb és nemnegatív érétket ad meg, akkor elvégezzük a 
            // számolást és kiírjuk az eredményt. 
            if ((masodpercek > 0) && (masodpercek < 24 * 60 * 60))
            {
                Idopont most = MasodpercAtvalt(masodpercek);
                IdoKiir(most);
            }
            // Ha a felhasználó negatív vagy egy napnál nagyobb értéket ad meg, akkor hibaüzenetet
            // adunk. 
            else
            {
                Console.WriteLine("A megadott érték negatív, vagy hosszabb egy napnál.");
            }
        }

        // Ez a metódus beolvas egy egész értéket a konzolról. 
        static int EgeszBeolvas(string nev)
        {
            // Ebben a változóban fogjuk eltárolni a beolvasott értéket. 
            int szam = 0;
            // Ennek a változónak az értéke azt fogja jelezni, hogy sikerült-e a beolvasott értéket
            // egész számmá   konvertálni. Ha a konvertálás nem sikerült, a változó értéke igaz 
            // (true), ha sikerült akkor hamis (false) lesz.
            bool ervenytelen = false;
            do
            {
                // Kiírjuk a beolvasni kívánt változó nevét. 
                Console.Write("\t" + nev + " = ");
                // Megpróbáljuk a beírt string értéket egész számra konvertálni. Ha ez sikerül, 
                // akkor az ervenytelen változó értéke hamis lesz. 
                ervenytelen = !int.TryParse(Console.ReadLine(), out szam);
                // Ha a konverzió nem sikerül, akkor azt kiírjuk a felhasználónak. 
                if (ervenytelen)
                {
                    Console.WriteLine("Ez nem egy egész szám. Kérem egész számot adjon meg!");
                }
                // A beolvasást addig ismételjük, amíg a konverzió sikeres nem lesz.
            } while (ervenytelen);  
            // Visszaadjuk a beolvasott értéket. 
            return szam;
        }

        // Ez a metódus az éjfél óta eltelt másodpercek számából megállapítja az aktuális időt, és 
        // azt egy Idopont struktúrában tárolja el.
        static Idopont MasodpercAtvalt(int masodpercek)
        {
            // Ebben a Idopont struktúra típusú változóban tároljuk az eredményt. 
            Idopont ido;
            // Annyi óra telt el, ahányszor a 3600 megvan az eltelt másodpercek számában. 
            ido.Orak = masodpercek / 3600;
            // A következőkben már csak az éjfél óta eltelt másodpercek egész órát el nem érő részét 
            // kell tekinteni. 
            masodpercek %= 3600;
            // Az egy egész órát már el nem érő másodpercekből annyi perc jön ki, ahányszor megvan 
            // az étékben a 60. 
            ido.Percek = masodpercek / 60;
            // Az egy percet már el nem érő másodpercek száma lesz a megjelenítendő másodpercek száma. 
            ido.Masodpercek = masodpercek % 60;
            return ido;
        }

        // Ez a metódus egy Time struktúrában tárolt időértéket a konzolra ír.
        static void IdoKiir(Idopont ido)
        {
            Console.WriteLine("\nAz aktuális idő {0:00}:{1:00}:{2:00}",
                              ido.Orak,
                              ido.Percek,
                              ido.Masodpercek);
        }
    }
}
