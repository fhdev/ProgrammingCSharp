﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lotto
{
    class Program
    {
        const int MIN = 1;
        const int MAX = 90;
        const int DB = 5;

        static Random veletlenSzam = new Random();

        static void Main(string[] args)
        {
            Console.WriteLine("Lottó játék.");
            Console.WriteLine("Kérem adjon meg {0:D} darab számot {1:D} és {2:D} között.", DB, MIN, MAX);
            int[] jatekosSzamai = Beolvas(DB, MIN, MAX);
            Rendez(jatekosSzamai);
            Console.WriteLine("Az Ön számai emelkedő sorrendben:");
            Kiir(jatekosSzamai);
            int[] nyerokSzamok = Veletlen(DB, MIN, MAX);
            Rendez(nyerokSzamok);
            Console.WriteLine("A nyerőszámok emelkedő sorrendben:");
            Kiir(nyerokSzamok);
            int talalatok = TalalatSzamol(jatekosSzamai, nyerokSzamok);
            Console.WriteLine("A találatok száma: {0:D} .", talalatok);
        }

        static int[] Beolvas(int darab, int also, int felso)
        {
            int[] szamok = new int[darab];
            for (int iSzam = 0; iSzam < darab; iSzam++)
            {
                Console.Write("A(z) {0:D} szám: ", iSzam + 1);
                if(!int.TryParse(Console.ReadLine(), out szamok[iSzam]))
                {
                    iSzam--;
                    Console.WriteLine("A megadott érték nem egy egész szám! Kérem próbálja újra!");
                }
                else if ((szamok[iSzam] < also) || (szamok[iSzam] > felso))
                {
                    iSzam--;
                    Console.WriteLine("A megadott egész szám nem {0:D} és {1:D} közé esik! Kérem próbálja újra!", also, felso);
                }
                else
                {
                    for(int iElozo = 0; iElozo < iSzam; iElozo++)
                    {
                        if (szamok[iElozo] == szamok[iSzam])
                        {
                            iSzam--;
                            Console.WriteLine("A megadott egész szám már szerepel! Kérem próbálja újra!");
                            break;
                        }
                    }
                }
            }
            return szamok;
        }

        static int[] Veletlen(int darab, int also, int felso)
        {
            int[] szamok = new int[darab];
            for (int iSzam = 0; iSzam < darab; iSzam++)
            {
                szamok[iSzam] = veletlenSzam.Next(also, felso + 1);
                for (int iElozo = 0; iElozo < iSzam; iElozo++)
                {
                    if(szamok[iElozo] == szamok[iSzam])
                    {
                        iSzam--;
                        break;
                    }
                }
            }
            return szamok;
        }

        static void Kiir(int[] szamok)
        {
            for (int iSzam = 0; iSzam < szamok.Length; iSzam++)
            {
                Console.Write("({0:D2}) ", szamok[iSzam]);
            }
            Console.WriteLine();
        }

        static void Rendez(int[] szamok)
        {
            for(int iUtolso = szamok.Length - 1; iUtolso >= 0; iUtolso--)
            {
                for(int iSzam = 0; iSzam < iUtolso; iSzam++)
                {
                    if(szamok[iSzam] > szamok[iSzam + 1])
                    {
                        int seged = szamok[iSzam];
                        szamok[iSzam] = szamok[iSzam + 1];
                        szamok[iSzam + 1] = seged;
                    }
                }
            }
        }

        static int TalalatSzamol(int[] jatekosSzamai, int[] nyeroSzamok)
        {
            int talalatok = 0;
            for (int iSzam = 0; iSzam < jatekosSzamai.Length; iSzam++)
            {
                for (int iNyero = 0; iNyero < nyeroSzamok.Length; iNyero++)
                {
                    if (jatekosSzamai[iSzam] == nyeroSzamok[iNyero])
                    {
                        talalatok++;
                    }
                }

            }
            return talalatok;
        }
    }
}
