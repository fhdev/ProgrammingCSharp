﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VektorSzorzas
{
    class Program
    {
        static void Main(string[] args)
        {
            // Röviden leírjuk a program célját. 
            Console.WriteLine("Vektorok skaláris, vektoriális és diadikus szorzatának számítása.");
            // Az új függvényünk segítségével beolvasunk két 3 dimenziós vektort. 
            double[] a = TombBeolvas(3, "a");
            double[] b = TombBeolvas(3, "b");
            // Kiszámoljuk a két vektor skaláris szorzatát. 
            double skalarSz = SkalarSzorzat(a, b);
            // Kiszámoljuk a két vektor vektoriális szorzatát. 
            double[] vektorSz = VektorSzorzat(a, b);
            // Kiszámoljuk a két vektor diadikus szorzatát.
            double[,] diadSz = DiadikusSzorzat(a, b);
            // Kiírjuk az eredményeket. 
            Console.WriteLine("a = " + VektorSzovegge(a));
            Console.WriteLine("b = " + VektorSzovegge(b));
            Console.WriteLine("aT . b = {0:F5}", skalarSz);
            Console.WriteLine("a  x b = " + VektorSzovegge(vektorSz));
            Console.WriteLine("a  . bT = ");
            Console.WriteLine(MatrixSzovegge(diadSz));
        }
        // Ez a metótus beolvas egy n dimenziós, nev nevű double vektort a konzolról. 
        static double[] TombBeolvas(int n, string nev)
        {
            // Ebbe a tömbbe fogjuk beolvasni az értékeket, és utána ezt fogjuk visszaadni a 
            // függvény végén. 
            double[] array = new double[n];
            // Megkérjük a felhasználót, hogy írja be a tömb elemeit. 
            Console.WriteLine("Kérem adja meg a(z) \"" + nev + "\" tömb elemeit.");
            // Ez a for ciklus végig megy a még üres (nullákkal feltöltött) tömb elemeit. 
            for (int i = 0; i < n; i++)
            {
                // Ez a változó fogja jelezni, ha a felhasználó által beírt érték nem konvertálható 
                // át egy double értékké.
                bool ervenytelen = false;
                do
                {
                    // Kiírjuk a tömb nevét. és szögletes zárójelek között, hogy épp melyik elemét 
                    // akarjuk beolvasi. 
                    Console.Write("\t" + nev + "[{0:D}] = ", i);
                    // Beolvassuk amit a felhasználó a konzolra ír, és egyidejűleg meg is próbáljuk 
                    // double értékké konvertálni. Ha a konverzió sikeres, akkor az ervenytelen 
                    // változó értéke false lesz, mert a TryParse függvény visszatérési értéke a 
                    // sikeres konverzió esetén true, amit a ! operátorral negálunk.
                    ervenytelen = !double.TryParse(Console.ReadLine(), out array[i]);
                    // Érvénytelen bemenet esetén értesítjük a felhasználót. 
                    if (ervenytelen)
                    {
                        Console.WriteLine("A megadott bemenet nem szám! Kérem számot adjon meg!");
                    }
                // A beolvasást addig ismételjük, míg egy helyes double értéket nem kapunk. 
                } while (ervenytelen); 
            }
            // A függvény végén visszaadjuk a beolvasott tömböt. 
            return array;
        }

        // Ez a metódus az a és b vektorok skaláris szorzatát számítja ki. Az eredmény skalár.
        // Hiba esetén az eredmény 0.
        static double SkalarSzorzat(double[] a, double[] b)
        {
            // Ebben a véltozóban fogjuk tárolni az eredményt. 
            double eredmeny = 0.0;
            // Ha a két átadott vektor hossza nem egyezik meg, akkor nem tudjuk elvégezni a 
            // műveletet. Erről értesítjük a felhasználót is. A végeredmény 0.0 marad. 
            if (a.Length != b.Length)
            {
                Console.WriteLine("A skalárisan szorzandó vektorok hossza nem egyezik meg.");
            }
            else
            {
                // A skaláris szorzat a megfelelő koordináták szorzatainak összege, ezt számolja ki
                // a for ciklus. 
                for (int i = 0; i < a.Length; i++)
                {
                    // Az eredményhez minden egyes lépésben rendre hozzáadjuk az aktuális 
                    // koordináták szorzatait.
                    eredmeny += a[i] * b[i];
                }
            }
            // A függvény végén visszatérünk a kiszámított értékkel. 
            return eredmeny;
        }
        
        // Ez a metódus az a és b vektorok vektoriális szorzatát számítja ki. Az eredmény vektor.
        // Hiba esetén az eredmény egy 3 elemű, csupa 0-kat tartalmazó vektor.
        static double[] VektorSzorzat(double[] a, double[] b)
        {
            // Ebben a változóban fogjuk tárolni az eredményt. A vektoriális szorzat eredménye egy 
            // 3 dimenziós vektor. 
            double[] eredmeny = new double[3];
            // Ha valamelyik vektor hossza nem 3, azaz nem R3 térbeli vektorokról van szó.
            if ((a.Length != 3) || (b.Length != 3))
            {
                Console.WriteLine("A vektoriálisan szorzandó vektorok valamelyike nem 3 dimenziós.");
            }
            // Mindkét vektor térbeli.
            else
            {
                eredmeny[0] =   a[1] * b[2] - a[2] * b[1];
                eredmeny[1] = -(a[0] * b[2] - a[2] * b[0]);
                eredmeny[2] =   a[0] * b[1] - a[1] * b[0];
            }
            // Visszatérünk a kiszámított eredménnyel. 
            return eredmeny;
        }

        // Ez a metótus az a és b vektorok diadikus szorzatát számítja ki, az eredmény mátrix.
        static double[,] DiadikusSzorzat(double[] a, double[] b)
        {
            double[,] eredmeny = new double[a.Length, b.Length];
            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < b.Length; j++)
                {
                    eredmeny[i, j] = a[i] * b[j];
                }
            }
            return eredmeny;
        }

        // Ez a metódus egy szöveges változóba írja ki egy tömb elemeit, majd visszatér vele.
        static string VektorSzovegge(double[] tomb)
        {
            // A vektor [ jellel kezdődik. 
            string szoveg = "[";
            // A tömb minden elemén végigmegyünk
            for (int i = 0; i < tomb.Length; i++)
            {
                // A stringhez hozzáfűzzűk az adott elem szöveges megfelelőjét. 
                szoveg += string.Format("{0:F3} ", tomb[i]);
            }
            // A vektor ] jellel zárul. 
            szoveg += ("\b]");
            // A függvény visszatér a vektor elemeit tartalmazó stringgel. 
            return szoveg;
        }

        // Ez a metódus egy szöveges változóba írja ki egy mátrix elemeit, majd visszatér vele.
        static string MatrixSzovegge(double [,] matrix)
        {
            string szoveg = "";
            int sorokSzama = matrix.GetLength(0);
            int oszlopokSzama = matrix.GetLength(1);
            for (int i = 0; i < sorokSzama; i++)
            {
                // A mátrix minden sora [ jellel kezdődik.
                szoveg += "[";
                for (int j = 0; j < oszlopokSzama; j++)
                {
                    // A szöveghez hozzáfűzzük az adott elemet.
                    szoveg += string.Format("{0,8:#0.000} ", matrix[i, j]);
                }
                // A mátrix minden sora ] jellel zárul.
                szoveg += "]\r\n";
            }
            return szoveg;
        }
    }
}
