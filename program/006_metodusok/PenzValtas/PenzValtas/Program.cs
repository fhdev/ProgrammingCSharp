﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenzValtas
{
    class Program
    {
        static void Main(string[] args)
        {
            // Kiírjuk a felhasználónak a program célját. 
            Console.WriteLine("Megadott pénzösszeg felváltása forint címletekre.\n");
            // Az átváltáshoz szükséges, hogy tudjuk a létező forint címleteket, így ezeket megadjuk. 
            int[] cimletek = { 20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5 };
            // Beolvassuk a felbontani kívánt összeget. 
            Console.WriteLine("Kérem adja meg a felváltani kívánt pénzösszeget.");
            int osszeg = EgeszBeolvas("Pénzösszeg");
            // Felbontjuk a megadott összeget. A changedAmountsForint változó utolsó eleme a maradék
            // összeg lesz. 
            int[] felbontas = Felvalt(osszeg, cimletek);
            // A felbontás eredményét kiírjuk a konzolra. 
            FelvaltKiiras(osszeg, felbontas, cimletek);
        }

        // Ez a metódus beolvas egy egész értéket a konzolról. 
        static int EgeszBeolvas(string nev)
        {
            // Ebben a változóban fogjuk eltárolni a beolvasott értéket. 
            int szam = 0;
            // Ennek a változónak az értéke azt fogja jelezni, hogy sikerült-e a beolvasott értéket
            // egész számmá konvertálni. Ha a konvertálás nem sikerült, a változó értéke igaz, ha
            // sikerült akkor hamis lesz.
            bool ervenytelen = false;
            do
            {
                // Kiírjuk a beolvasni kívánt változó nevét. 
                Console.Write("\t" + nev + " = ");
                // Megpróbáljuk a beírt string értéket egész számra konvertálni. Ha ez sikerül, 
                // akkor az ervenytelen változó értéke hamis lesz. 
                ervenytelen = !int.TryParse(Console.ReadLine(), out szam);
                // Ha a konverzió nem sikerül, akkor azt kiírjuk a felhasználónak. 
                if (ervenytelen)
                {
                    Console.WriteLine("A megadott érték nem egész. Kérem egész értéket adjon meg!");
                }
				// A beolvasást addig ismételjük, amíg a konverzió sikeres nem lesz. 
            } while (ervenytelen); 
            // Visszaadjuk a beolvasott értéket. 
            return szam;
        }
        // Ez a metódus az amount pénzösszeget a notesAndCoins tömbben található címletekre bontja
        // fel.
        static int[] Felvalt(int osszeg, int[] cimletek)
        {
            // Ebben a tömbben fogjuk tárolni, hogy melyik címletből hány darab kell az öszeghez, 
            // mindegyik összeghez a hozzá tartozó indexű darabszám fog tartozni. Ez a tömb egyel 
            // nagyobb, mint amiben a címleteket tároljuk, az utolsó elem lesz az esetleges maradék. 
            int[] felbontas = new int[cimletek.Length + 1];
            // Végigmegyünk az összes címleten és érmén. 
            for (int i = 0; i < cimletek.Length; i++)
            {
                // Az adott címletből annyi darab kell, ahány egész számszor az összegben megvan az 
                // adott címlet. Pl.: 42 356 Ft-hoz 42 356 / 20 000 = 2 -> két darab 20 000-es
                // címlet kell. 
                felbontas[i] = osszeg / cimletek[i];
                // Az összegből le kell vonni azt a részt, amit az adott címlettel már lefedtünk.  
                // Ezt legkönnyebben úgy tehetjük meg, hogy vesszük az adott címlettel való osztás  
                // maradékát, hiszen ennyi összeget kell még szétosztanunk a későbbiekben. 
                // Pl.: 42 356 Ft-hoz veszünk 2 x 20 000 Ft-ot, a maradék szétosztandó összeg pedig 
                // 42 356 % 20 000 = 2 356 Ft lesz. 
                osszeg %= cimletek[i];
            }
            // A tömb utolsó elemébe elhelyezzük a maradékot, hiszen nem biztos, hogy a legkisebb 
            // érmére kerek összeget írt be a felhasználó. Pl.: itt a Forint esetében ha egy összeg
            // nem 5 Ft-ra kerek, akkor lesz maradék. 
            felbontas[cimletek.Length] = osszeg;
            // Visszaadjuk a számítás eredményét. 
            return felbontas;
        }

        // Ez a metódus a pénzváltás eredményét a konzolra iratja.
        static void FelvaltKiiras(int osszeg, int[] felbontas, int[] cimletek)
        {
            // Ebben a string változóban fogjuk eltárolni az eredményt, amit egy lépésben írunk 
            // utána a konzolra. 
            string tartalom = "----------------------------------------\n";
            // Kiírjuk a felváltani kívánt összeget. 
            tartalom += string.Format("{0:D} =", osszeg);
            // Minden címlet értéke mellé beleírjuk a stringbe, hogy mennyi kell az adott címletből. 
            for (int i = 0; i < cimletek.Length; i++)
            {
                tartalom += string.Format("\n{0,5} x {1,3} +", cimletek[i], felbontas[i]);
            }
            // Feltütetjük még a maradék összeget is. 
            tartalom += string.Format("\n{0,5}", felbontas[cimletek.Length]);
            // Kiírjuk a konzolra az összes információt egy lépésben. 
            Console.WriteLine(tartalom);
        }
    }
}
