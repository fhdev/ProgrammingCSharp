﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SorbaRendezes
{
    class Program
    {
        static void Main(string[] args)
        {
            // Program céljának leírása.
            Console.WriteLine("Véletlenszerűen generált tömbök sorbarendezése.");
            // Tömb generálása és lemásolása a különböző algoritmusok teszteléséhez.
            Console.WriteLine("\n\nTÖMB GENERÁLÁSA");
            Folytat();
            int[] random = RandomTomb(100000, 10000);
            int[] masolat1 = new int[random.Length];
            int[] masolat2 = new int[random.Length];
            int[] masolat3 = new int[random.Length];
            Array.Copy(random, masolat1, random.Length);
            Array.Copy(random, masolat2, random.Length);
            Array.Copy(random, masolat3, random.Length);
            TombKiir(random);
            // Stopper objektum létrehozása futási idő méréséhez.
            Stopwatch ora = new Stopwatch();
            // Buborékrendezés tesztje
            Console.WriteLine("\n\nBUBORÉKRENDEZÉS");
            Folytat();
            ora.Restart();
            BuborekRendezes(masolat1);
            ora.Stop();
            TombKiir(masolat1);
            Console.WriteLine("Eltelt idő: {0} ms.", ora.ElapsedMilliseconds);
            // Fejlett buborékrendezés tesztje
            Console.WriteLine("\n\nFEJLETT BUBORÉKRENDEZÉS");
            Folytat();
            ora.Restart();
            BuborekRendezes2(masolat2);
            ora.Stop();
            TombKiir(masolat2);
            Console.WriteLine("Eltelt idő: {0} ms.", ora.ElapsedMilliseconds);
            // Gyorsrendezés tesztje
            Console.WriteLine("\n\nGYORSRENDEZÉS");
            Folytat();
            ora.Restart();
            GyorsRendezes(masolat3, 0, masolat3.Length - 1);
            ora.Stop();
            TombKiir(masolat3);
            Console.WriteLine("Eltelt idő: {0} ms.", ora.ElapsedMilliseconds);
        }

        // Ez a metódus egy billentyűzetleütésre vár a program folytatásához.
        static void Folytat()
        {
            Console.WriteLine("Kérem nyomjon meg egy gombot a folytatáshoz.");
            // A korábban lenyomott gombok kiürítése a bemeneti pufferből.
            while(Console.KeyAvailable)
            {
                Console.ReadKey(true);
            }
            // Várakozás egy gomb lenyomására.
            Console.ReadKey(true);
        }

        // Ez a metódus egy n elemű 0 és max közötti véletlen egész számokból álló tömböt generál. 
        static int[] RandomTomb(int n, int max)
        {
            int[] tomb = new int[n];
            Random rand = new Random();
            for (int i = 0; i < n; i++)
            {
                // A rand.Next metódus a felső határt exkluzívan kéri.
                tomb[i] = rand.Next(max + 1);
            }
            return tomb;
        }

        // Ez a metódus a lehető legjobb képernyőkitöltéssel konzolra írja egy egész számokat 
        // tartalmazó tömb összes elemét. 
        static void TombKiir(int[] tomb)
        {
            // Kiszámoljuk, milyen szélesen kell a számokat kiíratni, hogy egy oszlopba kerüljenek,
            // és legalább egy szóköz is kimaradjon közöttük.
            int maxSzelesseg = Convert.ToInt32(Math.Floor(Math.Log10(tomb.Max()))) + 2;
            // Kiszámoljuk, hogy egy sorba hány szám fog kiférni.
            int egySorba = (Console.BufferWidth - 1) / maxSzelesseg;
            // A tömb minden elemén végigmegyünk.
            for (int i = 0; i < tomb.Length; i++)
            {
                // Kiírjuk a tömb aktuális elemét a kiszámolt szélességgel.
                Console.Write("{0," + maxSzelesseg + "}", tomb[i]);
                // Az egy sorba kiférő számok kiírása után új sort kezdünk.
                if(((i + 1) % egySorba) == 0)
                {
                    Console.WriteLine();
                }
            }
            // Ha az összes szám kiírása után még nem kezdtünk új sort, akkor ezt itt megtesszzük.
            if ((tomb.Length % egySorba) != 0)
            {
                Console.WriteLine();
            }
        }

        // Ez a metódus sorbarendezi a tomb tömböt a BubbleSort algoritmussal.
        static void BuborekRendezes(int[] tomb)
        {
            // Minden vizsgálati körben egy elem biztosan a helyére kerül. Ezeket a rendezett
            // elemeket már nem lesz érdemes a további körökben vizsgálni.
            for (int utolso = tomb.Length - 1; utolso > 0; utolso--)
            {
                // A tömb szomszédos elemein végigmenve kicseréljük a rossz sorrendben lévő
                // elemeket.
                for (int aktualis = 0; aktualis < utolso; aktualis++)
                {
                    if (tomb[aktualis] > tomb[aktualis + 1])
                    {
                        Csere(ref tomb[aktualis], ref tomb[aktualis + 1]);
                    }
                }
            }
        }

        // Ez a metódus sorbarendezi a tomb tömböt a továbbfejlesztett BubbleSort algoritmussal.
        static void BuborekRendezes2(int[] tomb)
        {
            // Ebben a változóban tároljuk, hogy milyen indexig vizsgáljuk a tömbben az elemeket.
            // Csak addig érdemes vizsgálni az elemek közötti kapcsolatokat, amíg a legutóbbi
            // körben csere történt. Utána az elemek már biztosan sorrendben vannak.
            int utolso = tomb.Length - 1;
            // Amíg volt csere az előző körben.
            while (utolso > 0)
            {
                // Keressük az adott vizsgálati ciklusban az utolsó csere helyét. 
                int csere = 0;
                // Végigmegyünk a tömb még rendezetlen részén, és a nem megfelelően álló szomszédos
                // elemeket kicseréljük.
                for (int aktualis = 0; aktualis < utolso; aktualis++)
                {
                    if (tomb[aktualis] > tomb[aktualis + 1])
                    {
                        Csere(ref tomb[aktualis], ref tomb[aktualis + 1]);
                        csere = aktualis;
                    }
                }
                // Ha az adott vizsgálati ciklusban történt csere, akkor a következő körben már csak
                // a tömb a legutóbb történt csere előtti részét nézzük át, hiszen utána már 
                // biztosan sorba vannak rendezve az elemek.
                utolso = csere;
            }
        }

        // Ez a metódus sorba rendezi a tomb tömb elso és utolso indexű eleme közötti részét a 
        // QuickSort algoritmussal.
        static void GyorsRendezes(int[] tomb, int elso, int utolso)
        {
            // Ha értelmes tartományt kaptunk bemenetnek.
            if(elso < utolso)
            {
                // A pivot biztosan a helyére került.
                int pivotHelye = Particionalas(tomb, elso, utolso);
                // Pivot előtti részek rendezése.
                GyorsRendezes(tomb, elso, pivotHelye - 1);
                // Pivot utáni részek rendezése.
                GyorsRendezes(tomb, pivotHelye + 1, utolso);
            }
        }

        // Ez a metódus a GyorsRendezés algoritmusának része. Kiválasztja a tomb[utolso] elemet,
        // majd úgy rendezi a tömböt, hogy a kiválasztott elem a helyére kerüljön, tehát az övénél 
        // kisebb indexeken csak nála kisebb, az övénél nagyobb indexeken pedig csak nála nagyobb
        // elemek álljanak. 
        static int Particionalas(int[] tomb, int elso, int utolso)
        {
            // Minden lépésben tomb[utolso] a pivot elem. Minden nála kisebb elemet a tömb elejére
            // rakunk át.
            int i = elso;
            for (int j = elso; j < utolso; j++)
            {
                if(tomb[j] < tomb[utolso])
                {
                    Csere(ref tomb[i], ref tomb[j]);
                    i++;
                }
            }
            // Magát a pivot elemet áthelyezzük a nála biztosan nagyobb elemek elé.
            if(tomb[utolso] < tomb[i])
            {
                Csere(ref tomb[utolso], ref tomb[i]);
            }
            return i;
        }

        // Ez a metódus a neki átadott két változó értékét felcseréli. 
        static void Csere(ref int a, ref int b)
        {
            int puffer = a;
            a = b;
            b = puffer;
        }
    }
}
