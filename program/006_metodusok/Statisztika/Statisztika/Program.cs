﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statisztika
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Átlag és szórás számítása.");
            Console.WriteLine("Kérem adja meg az értékek számát.");
            int meret = EgeszBeolvasas("értékek száma");
            Console.WriteLine("Kérem adja meg az értékeket egymás után.");
            double[] meresek = TombBeolvasas("érték", meret);
            double atlag = Atlag(meresek);
            Console.WriteLine("A mérési értékek átlaga: {0:F5}", atlag);
            double szorasNegyzet = SzorasNegyzet(meresek);
            Console.WriteLine("A mérési értékek szórása: {0:F5}", Math.Sqrt(szorasNegyzet));
        }
        // Ez a metódus beolvas egy int számot a konzolról.
        static int EgeszBeolvasas(string nev)
        {
            int szam;
            do
            {
                Console.Write("\t" + nev + " = ");
            } while (!int.TryParse(Console.ReadLine(), out szam));
            return szam;
        }

        // Ez a metódus beolvas egy double számokból álló tömböt a konzolról.
        static double[] TombBeolvasas(string nev, int hossz)
        {
            double[] szamok = new double[hossz];
            for(int i = 0; i < hossz; i++)
            {
                do
                {
                    Console.Write("\t{0:D}. " + nev + " = ", i + 1);
                } while (!double.TryParse(Console.ReadLine(), out szamok[i]));
            }
            return szamok;
        }

        // Ez a metódus kiszámolja egy double tömbben található értékek átlagát (számtani közép).
        static double Atlag(double[] szamok)
        {
            double atlag = 0.0;
            /* Az értékek össeadása. */
            for(int i = 0; i < szamok.Length; i++)
            {
                atlag += szamok[i];
            }
            /* Az értékek összegének osztása az értékek számával. */
            atlag /= szamok.Length;
            return atlag;
        }

        // Ez a metódus kiszámolja egy double tömbben található értékek szórásnégyzetét.
        static double SzorasNegyzet(double[] szamok)
        {
            /* Átlag számítása az erre írt metódus segítségével. */
            double atlag = Atlag(szamok);
            double szorasNegyzet = 0.0;
            /* Az egyes értékek és az átlag különbségének négyzetösszegének számítása. */
            for (int i = 0; i < szamok.Length; i++)
            {
                szorasNegyzet += (szamok[i] - atlag) * (szamok[i] - atlag);
            }
            /* Az értékek és az átlag különbségének négyzetösszegének osztása az értékek számával. */
            szorasNegyzet /= szamok.Length;
            return szorasNegyzet;
        }
    }
}
