﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            // Röviden leírjuk a program célját. 
            Console.WriteLine("Véletlenszerűen generált mátrix determinánsának számítása.");
            // Generálunk egy 10 x 10-es véletlen mátrixot melynek elemei -10 és 10 között lesznek. 
            double[,] a = RandomMatrix(10, 10, -10.0, 10.0);
            // Generálunk egy 10 x 10-es egységmátrixot. 
            double[,] i = EgysegMatrix(10);
            // Definiálunk egy 3 x 3-as próba mátrixot a determináns számítás ellenőrzéséhez. 
            double[,] n = { { 1.0, 7.0, 3.0 }, { 2.0, 8.0, 6.0 }, { -12.0, 9.0, 6.0 } };
            // Kiíratjuk a képernyöre a random mátrixot. 
            Console.WriteLine("\nA véletlenszerűen generált mátrix:\n");
            Console.WriteLine(MatrixToString(a));
            // Kiszámítjuk a véletlen mátrix determinánsát és kiírjuk a konzolra. 
            double d = Determinans(a);
            Console.WriteLine("\nA mátrix determinánsa: {0:F5}", d);
            // Kiíratjuk a képernyőre az egységmátrixot. 
            Console.WriteLine("\nAz egységmátrix:\n");
            Console.WriteLine(MatrixToString(i));
            // Kiszámítjuk az egységmátrix determinánsát és kiírjuk a konzolra. 
            d = Determinans(i);
            Console.WriteLine("\nAz egységmátrix determinánsa: {0:F5}", d);
        }

        // Ez a metódus generál egy n x m -es véletlen számokat tartalmazó mátrixot.
        // A mátrix elemei double típusúak és also alsó és felso felső korlátok között generálódnak.
        static double[,] RandomMatrix(int n, int m, double also, double felso)
        {
            double[,] matrix = null;
            // Ha a sorok vagy oszlopok számára negatív vagy 0 értéket adtak meg a híváskor, 
            // null értékkel térünk majd vissza.
            if ((n > 0) && (m > 0))
            {
                // Ebben a változóban tároljuk a véletlen mátrixot. 
                matrix = new double[n, m];
                // Ha a megadott alsó határ nem kisebb mint a megadott felső határ, akkor egy csupa
                // 0 tömbbel térünk majd vissza. 
                if (also < felso)
                {
                    // Ez a Random típusú változó alkalmas a véletlenszám generálásra. 
                    Random rand = new Random();
                    // Végigmegyünk a mátrix sorain és oszlopain, így minden egyes elemének értéket 
                    // adunk. 
                    for (int sor = 0; sor < n; sor++)
                    {
                        for (int oszlop = 0; oszlop < m; oszlop++)
                        {
                            // A mátrix adott eleme az lb és az ub közé eső tizedestört lesz, mivel 
                            // a rand.NextDouble() értéke 0.0 és 1.0 közé esik. 
                            matrix[sor, oszlop] = also + rand.NextDouble() * (felso - also);
                        }
                    }
                }
            }
            // Visszatérünk a mátrix-szal 
            return matrix;
        }

        // Ez a metódus egy double típusú elemekből álló mátrixot a képernyőre kiíratható formátumú 
        // string változóba ment.
        static string MatrixToString(double[,] matrix)
        {
            // Ebben a változóban fogjuk tárolni a mátrix szöveges megfelelőjét. 
            string result = "";
            // Lekérdezzük a mátrix sorainak számát. A GetLegnth() metódus paramétere a kiívánt 
            // dimenzió száma, ami mentén a tömb hosszára kíváncsiak vagyunk. Az indexelés itt is 
            // 0-val kezdődik, így az első dimenzió indexe 0. 
            int n = matrix.GetLength(0);
            // Lekérdezzük a mátrix oszlopainak számát. 
            int m = matrix.GetLength(1);
            // Végigmegyünk a mátrix sorain. 
            for (int sor = 0; sor < n; sor++)
            {
                // Ha az első sorban vagyunk, egy ┌ jellel megkezdjük a mátrix bal oldali szögletes 
                // zárójelének rajzolását. 
                if (sor == 0)
                {
                    result += "┌ ";
                }
                // Ha az utolsó sorban vagyunk, a └ jellel befejezzük a mátrix bal oldali szögletes
                //  zárójelének rajzolását. 
                else if (sor == n - 1)
                {
                    result += "└ ";
                }
                // Ha valamelyik köztes sorban vagyunk, akkor a │ jellel rajzoljuk a mátrix bal 
                // oldali szögletes zárójelét. 
                else
                {
                    result += "│ ";
                }
                // Végigmegyünk a mátrix adott sorának minden elemén, és beleírjuk a string-be. 
                for (int column = 0; column < m; column++)
                {
                    result += string.Format("{0,10:#0.000} ", matrix[sor, column]);
                }
                // Ha az első sorban vagyunk, egy ┐ jellel megkezdjük a mátrix jobb oldali szögletes 
                //  zárójelének rajzolását. Ez után ugrunk a következő sorra. 
                if (sor == 0)
                {
                    result += "┐\n";
                }
                // Ha az utolsó sorban vagyunk, a ┘ jellel befejezzük a mátrix jobb oldali szögletes 
                // zárójelének rajzolását. 
                else if (sor == n - 1)
                {
                    result += "┘";
                }
                // Ha valamelyik köztes sorban vagyunk, akkor a │ jellel rajzoljuk a mátrix jobb 
                // oldali szögletes zárójelét. Ez után ugrunk a következő sorra.
                else
                {
                    result += "│\n";
                }
            }
            // A metódus végén visszaadjuk az elkészült string-et. 
            return result;
        }

        // Ez a metódus generál egy n x n -es egységmátrixot.
        static double[,] EgysegMatrix(int n)
        {
            // Ebben a változóban fogjuk tárolni az egységmátrixot. 
            double[,] egyseg = new double[n, n];
            // Végigmegyünk a mátrix minden során. 
            for (int sor = 0; sor < n; sor++)
            {
                // A mátrix adott sorában végigmegyünk a mátrix minden elemén. 
                for (int oszlop = 0; oszlop < n; oszlop++)
                {
                    // Ha a főátlóban vagyunk, azaz a sor és oszlop indexek megegyeznek, akkor az 
                    // aktuális elem értéke 1.0. 
                    if (sor == oszlop)
                    {
                        egyseg[sor, oszlop] = 1.0;
                    }
                }
            }
            // A metódus végén visszaadjuk a generált egységmátrixot. 
            return egyseg;
        }

        // Ez a metódus kiszámolja egy négyzetes mátrix determinánsát. 
        // A metódus rekurzív, hiszen meghívja saját magát.
        static double Determinans(double[,] matrix)
        {
            // Ebben a változóban fogjuk tárolni a kiszámolt determinánst. 
            double det = Double.NaN;
            // Lekérdezzük a mátrix sorainak és oszlopainak a számát. 
            int n = matrix.GetLength(0);
            int m = matrix.GetLength(1);
            // Ha a sorok és oszlopok száma nem egyezik meg, akkor a mátrixnak nincsen determinánsa. 
            if (n == m)
            {
                // A determinánst összegként fogjuk számítani, ezért kinullázzuk a kezdeti értéket.
                det = 0;
                // Ha a márix 2 x 2 akkor számoljuk a determinánst a definíció szerint. 
                if (n == 2)
                {
                    det = matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
                }
                // Ha a mátrix nagyobb mint 2 x 2 -es akkor végigmegyünk az első sor elemein és  
                // számítjuk az egyes aldeterminánsokat. 
                else
                {
                    // Végigmegyünk a mátrix első során. 
                    for (int oszlopElsoSorban = 0; oszlopElsoSorban < n; oszlopElsoSorban++)
                    {
                        // Az előjel szabály szerint számítjuk az előjelet (+ - + - + -). 
                        double sgn = 1.0;
                        if (oszlopElsoSorban % 2 != 0)
                        {
                            sgn = -1.0;
                        }
                        // Ebben a tömbben fogjuk tárolni az aktuális első sorbeli elemhez tartozó
                        // egyel kisebb dimenziójúmátrixot, ennek a neve minor. 
                        double[, ] minor = new double[n - 1, m - 1];
                        // Ebben a változóban fogjuk tárolni a minor mátrix aktuális oszlopának 
                        // indexét. 
                        int oszlopMinor = 0;
                        // Végigmegyünk az eredeti mátrix minden oszlopán. 
                        for (int oszlop = 0; oszlop < m; oszlop++)
                        {
                            // Ha az aktuális oszlop index megegyezik az aktuális elem 
                            // oszlopindexével, akkor ezt az  oszlopot kihagyjuk, hiszen a  
                            // determináns kifejtésekor az aktuális elem sorát és oszlopát 
                            // "letakarjuk". 
                            if (oszlop == oszlopElsoSorban)
                            {
                                continue;
                            }
                            // Végigmegyünk az eredeti mátrix sorain, de csak a második sortól 
                            // kezdve, mert az első sort  mindig ki kell hagynunk, hiszen abban van 
                            // az aktuális elem, azt "letakarjuk". 
                            for (int sor = 1; sor < n; sor++)
                            {
                                // Ebben a változóban fogjuk tárolni a minor mátrix aktuális sorának 
                                // indexét. Ez mindig eggyel kisebb, mint az eredeti mátrix sorának 
                                // az indexe, hiszen az eredeti mátrixnak az első sorát nem 
                                // használjuk fel. 
                                int sorMinor = sor - 1;
                                // Átmásoljuk a minor mátrixba a megfelelő elemet. 
                                minor[sorMinor, oszlopMinor] = matrix[sor, oszlop];
                            }
                            // A következő lefutáskor már a következő oszlop elemeit akarjuk kiírni, 
                            // ezért növeljük a minor mátrix oszlopindexét. 
                            oszlopMinor++;
                        }
                        // A determináns értékéhez hozzáadjuk az aktuális aldetermináns értékét. 
                        det += sgn * matrix[0, oszlopElsoSorban] * Determinans(minor);
                    }
                }
            }
            // A metódus végén visszatérünk a determináns értékével. 
            return det;
        }

    }
}
