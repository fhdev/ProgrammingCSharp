﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Prim
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Az első 10000 prímszám megkeresése.");
            uint N = 10000;
            // Időmérésre alkalmas objektum.
            Stopwatch stopper = new Stopwatch();
            // Prímszámok generálása a "buta" algoritmussal, és az eltelt idő mérése.
            stopper.Start();
            uint[] primek = PrimKeres(N);
            stopper.Stop();
            long elteltIdo = stopper.ElapsedMilliseconds;
            Kiir(primek);
            // Prímszámok generálása az "okos" algoritmussal, és az eltelt idő mérése.
            stopper.Restart();
            primek = PrimKeresOkos(N);
            stopper.Stop();
            long elteltIdoOkos = stopper.ElapsedMilliseconds;
            Kiir(primek);
            // A két algoritmus futásidejének összehasonlítása 
            Console.WriteLine("Eltelt idő:\n\tegyszerű keresés: {0:D} ms\n\tokos keresés: {1:D} ms",
                              elteltIdo,
                              elteltIdoOkos);
        }

        static void Kiir(uint[] szamok)
        {
            // A tömb minden elemén végigmenve.
            for (int i = 0; i < szamok.Length; i++)
            {
                // Kiírjuk az adott elem értékét.
                Console.Write("{0,10} ", szamok[i]);
                // Minden 10. érték után új sort kezdünk. 
                if(((i + 1) % 10) == 0)
                {
                    Console.WriteLine();
                }
            }
        }

        // Az első mennyit argumentumban megadott számú prím szám keresése. 
        static uint[] PrimKeres(uint mennyit)
        {
            // Tömb a megtalált prímszámok tárolására.
            uint[] primek = new uint[mennyit];
            // Az aktuális szám.
            uint szam = 2;
            // Talált prímszámok mennyisége.
            uint talalt = 0;
            // Amíg nincs meg a megadott számú prím
            while(talalt < mennyit)
            {
                // Feltesszük, hogy az aktuális szám prímszám.
                bool prim = true;
                // Az osztó, amivel az aktuális számot teszteljük.
                uint oszto = 2;
                // Osztó keresése. Osztót a szám négyzetgyökéig érdemes csak keresni.
                while (oszto * oszto <= szam)
                {
                    // Ha az éppen tesztelt osztóval a szám osztható, akkor nem prím.
                    if((szam % oszto) == 0)
                    {
                        prim = false;
                        break;
                    }
                    // Tesztelés a következő lehetséges osztóval.
                    oszto++;
                }
                // Ha az összes lehetséges osztóval leteszteltük a számot, és egyikkel sem volt
                // osztható, akkor megtaláltunk egy új prímszámot.
                if(prim)
                {
                    primek[talalt++] = szam;
                }
                // A következő szám ellenőrzése.
                szam++;
            }
            return primek;
        }

        // Az első mennyit argumentumban megadott számú prím szám keresése (fejlettebb).
        static uint[] PrimKeresOkos(uint mennyit)
        {
            // Tömb a megtalált prímszámok tárolására.
            uint[] primek = new uint[mennyit];
            // Az első prímszám a 2.
            primek[0] = 2;
            // Az aktuális szám. A 2-t már beleírtuk a tömbbe, 3-tól kezdjük a számok vizsgálatát.
            uint szam = 3;
            // Talált prímszámok mennyisége. A 2-t már beleírtuk a tömbbe, 1-ről indul a számláló.
            uint talalt = 1;
            // Amíg nincs meg a megadott számú prím. 
            while (talalt < mennyit)
            {
                // Feltesszük, hogy az aktuális szám prímszám.
                bool prim = true;
                // Osztóként csak az eddig megtalált prímszámokat kell vizsgálni. 
                uint index = 0;
                while (primek[index] * primek[index] <= szam)
                {
                    // Ha valamelyik korábban megtalált prímszám osztója az aktuális számnak, akkor
                    // a szám nem prím. 
                    if ((szam % primek[index]) == 0)
                    {
                        prim = false;
                        break;
                    }
                    // Tesztelés a következő, már korábban megtalált prímszámmal. 
                    index++;
                }
                // Ha az összes lehetséges osztóval leteszteltük a számot, és egyikkel sem volt
                // osztható, akkor megtaláltunk egy új prímszámot.
                if (prim)
                {
                    primek[talalt++] = szam;
                }
                // Következő páratlan szám tesztelése. Páros számokat nem érdemes tesztelni. 
                szam+=2;
            }
            // Talált prímszámok visszaadása.
            return primek;
        }
    }
}
