﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LnkoLkkt
{
    class Program
    {
        static void Main(string[] args)
        {
            // Két szám prímtényezős felbontását, és legnagyobb közös osztóját valamint legkisebb
            // közös többszörösét keressük meg.
            Console.WriteLine("Két szám legkisebb közös többszörösének (lkkt) és legnagyobb közös" +
                              "osztójának (lnko) megtalálása.");
            Console.WriteLine("Kérem adja meg a két számot!");
            Console.Write("szam1 = ");
            uint szam1;
            uint.TryParse(Console.ReadLine(), out szam1);
            Console.Write("szam2 = ");
            uint szam2;
            uint.TryParse(Console.ReadLine(), out szam2);
            uint lnko = Lnko(szam1, szam2);
            uint lkkt = Lkkt(szam1, szam2);
            Console.Write("Az első szám prímtényezős felbontása:\n\tszam1 = ");
            Listaz(PrimTenyezok(szam1));
            Console.Write("A második szám prímtényezős felbontása:\n\tszam2 = ");
            Listaz(PrimTenyezok(szam2));
            Console.WriteLine("Eredények:\n\tlnko({0:D}, {1:D}) = {2:D}\n\tlkkt({0:D}, {1:D}) = " +
                              "{3:D}", szam1, szam2, lnko, lkkt);
        }

        // Prímtényezők kilistázása. 
        static void Listaz(List<uint> tenyezok)
        {
            // Az összes prímtényzőt a konzolra írjuk, egymástól x jellel választjuk el őket.
            for (int i = 0; i < tenyezok.Count; i++)
            {
                Console.Write(tenyezok[i]);
                if(i < tenyezok.Count - 1)
                {
                    Console.Write(" x ");
                }
            }
            Console.WriteLine();
        }

        // Prímtényezős felbontás, a prímtényezők egy listába kerülnek.
        static List<uint> PrimTenyezok(uint szam)
        {
            // Mivel előre nem ismerjük a prímtényezők számát, egy listát alkalmazunk. A lista
            // dinamikusan bővíthető.
            List<uint> tenyezok = new List<uint>();
            // Különböző osztókat próbálunk ki. Akkor állunk le, ha a szám minden osztóját 
            // megtaláltuk.
            for(uint oszto = 2; szam > 1; oszto++)
            {
                // Ha az adott osztó jelölt valóban osztója a számnak, akkor hozzáadjuk a 
                // prímtényezők listájához, és elosztjuk vele a számot.
                while((szam % oszto) == 0)
                {
                    tenyezok.Add(oszto);
                    szam /= oszto;
                }
            }
            return tenyezok;
        }

        // A legnagyobb közös osztó megkeresése az Euklideszi algoritmussal.
        static uint Lnko(uint a, uint b)
        {
            uint temp = 0;
            while(b > 0)
            {
                temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }
        // A legkisebb közös többszörös meghatározása a legnagyobb közös osztó ismeretében.
        static uint Lkkt(uint a, uint b)
        {
            
            uint eredmeny = a * b / Lnko(a, b);
            return eredmeny;
        }
    }
}
