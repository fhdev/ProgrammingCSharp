﻿namespace Proba
{
    partial class FoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Gomb1 = new System.Windows.Forms.Button();
            this.Gomb2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Gomb1
            // 
            this.Gomb1.Location = new System.Drawing.Point(12, 12);
            this.Gomb1.Name = "Gomb1";
            this.Gomb1.Size = new System.Drawing.Size(136, 23);
            this.Gomb1.TabIndex = 0;
            this.Gomb1.Text = "Gomb1";
            this.Gomb1.UseVisualStyleBackColor = true;
            this.Gomb1.Click += new System.EventHandler(this.Gomb1_Click);
            // 
            // Gomb2
            // 
            this.Gomb2.Location = new System.Drawing.Point(12, 50);
            this.Gomb2.Name = "Gomb2";
            this.Gomb2.Size = new System.Drawing.Size(136, 23);
            this.Gomb2.TabIndex = 1;
            this.Gomb2.Text = "Gomb2";
            this.Gomb2.UseVisualStyleBackColor = true;
            this.Gomb2.Click += new System.EventHandler(this.Gomb1_Click);
            // 
            // FoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 261);
            this.Controls.Add(this.Gomb2);
            this.Controls.Add(this.Gomb1);
            this.Name = "FoAblak";
            this.Text = "FoAblak";
            this.Load += new System.EventHandler(this.FoAblak_Load);
            this.Click += new System.EventHandler(this.FoAblak_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Gomb1;
        private System.Windows.Forms.Button Gomb2;
    }
}

