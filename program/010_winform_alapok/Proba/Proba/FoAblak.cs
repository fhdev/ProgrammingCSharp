﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proba
{
    public partial class FoAblak : Form
    {
        private bool hatter;

        public FoAblak()
        {
            InitializeComponent();
        }

        private void FoAblak_Click(object sender, EventArgs e)
        {
            if (hatter)
            {
                this.BackColor = Color.Red;
            }
            else
            {
                this.BackColor = Color.Green;
            }
            hatter = !hatter;
        }

        private void FoAblak_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.Green;
        }

        private void Gomb1_Click(object sender, EventArgs e)
        {
            this.Text = "A " + (sender as Button).Name + " gombot megnyomták!";
            (sender as Button).Text = "Megnyomtak!";
        }
    }
}
