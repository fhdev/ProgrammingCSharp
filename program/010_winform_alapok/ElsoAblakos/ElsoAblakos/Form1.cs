﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace ElsoAblakos
{
    public partial class Form1 : Form
    {
        Stopwatch stopper = new Stopwatch();
        Random veletlen = new Random();
        bool ugorhat = true;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_DoubleClick(object sender, EventArgs e)
        {
            if (this.BackColor == Color.Red) this.BackColor = Color.Green;
            else if (this.BackColor == Color.Green) this.BackColor = Color.Red;
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (stopper.IsRunning)
            {
                stopper.Stop();
                textBox1.Text = stopper.ElapsedMilliseconds + " ms";
            }
            else
            {
                stopper.Restart();
            }
        }

        private void Form1_Click(object sender, EventArgs e)
        {
            if (ugorhat)
            {
                Screen kijelzo = Screen.FromControl(this);
                this.Top = veletlen.Next(0, kijelzo.WorkingArea.Height - this.Height);
                this.Left = veletlen.Next(0, kijelzo.WorkingArea.Width - this.Width);
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Scroll) ugorhat = !ugorhat;
        }
    }
}
