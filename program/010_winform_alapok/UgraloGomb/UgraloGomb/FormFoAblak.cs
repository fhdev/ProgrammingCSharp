﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UgraloGomb
{
    public partial class FormFoAblak : Form
    {
        private static Random veletlen = new Random();
        public FormFoAblak()
        {
            InitializeComponent();
        }

        private void buttonUgro_MouseEnter(object sender, EventArgs e)
        {
            timerMozgat.Start();
        }

        private void timerMozgat_Tick(object sender, EventArgs e)
        {
            buttonUgro.Left = veletlen.Next(0, this.ClientSize.Width - buttonUgro.Width);
            buttonUgro.Top = veletlen.Next(0, this.ClientSize.Height - buttonUgro.Height);
            timerMozgat.Stop();
        }

        private void buttonUgro_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Gratulálok, sikerült elkapnod!");
            timerMozgat.Interval -= 10;
        }
    }
}
