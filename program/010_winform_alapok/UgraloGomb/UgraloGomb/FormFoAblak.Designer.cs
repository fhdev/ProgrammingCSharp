﻿namespace UgraloGomb
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonUgro = new System.Windows.Forms.Button();
            this.timerMozgat = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // buttonUgro
            // 
            this.buttonUgro.BackColor = System.Drawing.Color.Red;
            this.buttonUgro.Location = new System.Drawing.Point(13, 13);
            this.buttonUgro.Margin = new System.Windows.Forms.Padding(0);
            this.buttonUgro.Name = "buttonUgro";
            this.buttonUgro.Size = new System.Drawing.Size(25, 25);
            this.buttonUgro.TabIndex = 0;
            this.buttonUgro.UseVisualStyleBackColor = false;
            this.buttonUgro.Click += new System.EventHandler(this.buttonUgro_Click);
            this.buttonUgro.MouseEnter += new System.EventHandler(this.buttonUgro_MouseEnter);
            // 
            // timerMozgat
            // 
            this.timerMozgat.Interval = 200;
            this.timerMozgat.Tick += new System.EventHandler(this.timerMozgat_Tick);
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.Controls.Add(this.buttonUgro);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFoAblak";
            this.Text = "Kapj el!";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonUgro;
        private System.Windows.Forms.Timer timerMozgat;
    }
}

