﻿namespace FerdeHajitas
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxSebesseg = new System.Windows.Forms.TextBox();
            this.formFoAblakBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textBoxAllasSzog = new System.Windows.Forms.TextBox();
            this.trackBarSebesseg = new System.Windows.Forms.TrackBar();
            this.trackBarAllasSzog = new System.Windows.Forms.TrackBar();
            this.labelSebesseg = new System.Windows.Forms.Label();
            this.labelAllasSzog = new System.Windows.Forms.Label();
            this.panelTabla = new System.Windows.Forms.Panel();
            this.buttonStart = new System.Windows.Forms.Button();
            this.timerMozgat = new System.Windows.Forms.Timer(this.components);
            this.labelIdo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.formFoAblakBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSebesseg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarAllasSzog)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxSebesseg
            // 
            this.textBoxSebesseg.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.formFoAblakBindingSource, "Sebesseg", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxSebesseg.Location = new System.Drawing.Point(220, 12);
            this.textBoxSebesseg.Name = "textBoxSebesseg";
            this.textBoxSebesseg.Size = new System.Drawing.Size(60, 20);
            this.textBoxSebesseg.TabIndex = 1;
            this.textBoxSebesseg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // formFoAblakBindingSource
            // 
            this.formFoAblakBindingSource.DataSource = this;
            this.formFoAblakBindingSource.Position = 0;
            // 
            // textBoxAllasSzog
            // 
            this.textBoxAllasSzog.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.formFoAblakBindingSource, "AllasSzog", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxAllasSzog.Location = new System.Drawing.Point(510, 10);
            this.textBoxAllasSzog.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxAllasSzog.Name = "textBoxAllasSzog";
            this.textBoxAllasSzog.Size = new System.Drawing.Size(60, 20);
            this.textBoxAllasSzog.TabIndex = 3;
            this.textBoxAllasSzog.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // trackBarSebesseg
            // 
            this.trackBarSebesseg.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.formFoAblakBindingSource, "Sebesseg", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.trackBarSebesseg.LargeChange = 2;
            this.trackBarSebesseg.Location = new System.Drawing.Point(64, 10);
            this.trackBarSebesseg.Name = "trackBarSebesseg";
            this.trackBarSebesseg.Size = new System.Drawing.Size(150, 45);
            this.trackBarSebesseg.TabIndex = 0;
            // 
            // trackBarAllasSzog
            // 
            this.trackBarAllasSzog.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.formFoAblakBindingSource, "AllasSzog", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.trackBarAllasSzog.LargeChange = 10;
            this.trackBarAllasSzog.Location = new System.Drawing.Point(352, 10);
            this.trackBarAllasSzog.Maximum = 90;
            this.trackBarAllasSzog.Name = "trackBarAllasSzog";
            this.trackBarAllasSzog.Size = new System.Drawing.Size(150, 45);
            this.trackBarAllasSzog.TabIndex = 2;
            // 
            // labelSebesseg
            // 
            this.labelSebesseg.AutoSize = true;
            this.labelSebesseg.Location = new System.Drawing.Point(14, 14);
            this.labelSebesseg.Margin = new System.Windows.Forms.Padding(5);
            this.labelSebesseg.Name = "labelSebesseg";
            this.labelSebesseg.Size = new System.Drawing.Size(54, 13);
            this.labelSebesseg.TabIndex = 4;
            this.labelSebesseg.Text = "Sebesség";
            // 
            // labelAllasSzog
            // 
            this.labelAllasSzog.AutoSize = true;
            this.labelAllasSzog.Location = new System.Drawing.Point(302, 15);
            this.labelAllasSzog.Margin = new System.Windows.Forms.Padding(5);
            this.labelAllasSzog.Name = "labelAllasSzog";
            this.labelAllasSzog.Size = new System.Drawing.Size(51, 13);
            this.labelAllasSzog.TabIndex = 4;
            this.labelAllasSzog.Text = "Állásszög";
            // 
            // panelTabla
            // 
            this.panelTabla.BackColor = System.Drawing.Color.White;
            this.panelTabla.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTabla.Location = new System.Drawing.Point(17, 83);
            this.panelTabla.Name = "panelTabla";
            this.panelTabla.Size = new System.Drawing.Size(550, 550);
            this.panelTabla.TabIndex = 5;
            this.panelTabla.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTabla_Paint);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(17, 45);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 6;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // timerMozgat
            // 
            this.timerMozgat.Interval = 16;
            this.timerMozgat.Tick += new System.EventHandler(this.timerMozgat_Tick);
            // 
            // labelIdo
            // 
            this.labelIdo.Location = new System.Drawing.Point(434, 45);
            this.labelIdo.Name = "labelIdo";
            this.labelIdo.Size = new System.Drawing.Size(133, 23);
            this.labelIdo.TabIndex = 7;
            this.labelIdo.Text = "0 ms";
            this.labelIdo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 651);
            this.Controls.Add(this.labelIdo);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.panelTabla);
            this.Controls.Add(this.labelAllasSzog);
            this.Controls.Add(this.labelSebesseg);
            this.Controls.Add(this.trackBarAllasSzog);
            this.Controls.Add(this.trackBarSebesseg);
            this.Controls.Add(this.textBoxAllasSzog);
            this.Controls.Add(this.textBoxSebesseg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFoAblak";
            this.Text = "Ferde hajítás";
            ((System.ComponentModel.ISupportInitialize)(this.formFoAblakBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSebesseg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarAllasSzog)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxSebesseg;
        private System.Windows.Forms.TextBox textBoxAllasSzog;
        private System.Windows.Forms.BindingSource formFoAblakBindingSource;
        private System.Windows.Forms.Label labelAllasSzog;
        private System.Windows.Forms.Label labelSebesseg;
        private System.Windows.Forms.TrackBar trackBarAllasSzog;
        private System.Windows.Forms.TrackBar trackBarSebesseg;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Panel panelTabla;
        private System.Windows.Forms.Timer timerMozgat;
        private System.Windows.Forms.Label labelIdo;
    }
}

