﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace FerdeHajitas
{
    class Lovedek
    {
        // Gravitációs gyorsulás [m/s^2]
        private const double g_mps2 = -9.8065;

        // Lövedék átmérője [m]
        private double D_m;
        // Méretarány [pixel/m]
        private int meterbolPixel;
        // Pozíciók [m]
        private double x_m;
        private double y_m;
        // Sebességek [m / s]
        private double vx_mps;
        private double vy_mps;

        // Rajzeszközök
        SolidBrush ecset;
        Pen toll;

        // Pozíciók és méretek rajzoláshoz [pixel]
        private int xbal_p
        {
            get
            {
                return Convert.ToInt32((x_m - D_m / 2) * meterbolPixel);
            }
        }
        private int yfelso_p
        {
            get
            {
                return Convert.ToInt32((y_m + D_m / 2) * meterbolPixel);
            }
        }
        private int D_p
        {
            get
            {
                return Convert.ToInt32(D_m * meterbolPixel);
            }
        }
        // Konstruktor
        public Lovedek(double D_m, double v0_mps, double alpha_0, int meterbolPixel)
        {
            // Adattagok beállítása
            this.D_m = D_m;
            x_m = D_m / 2;
            y_m = D_m / 2;
            // Sebességkomponensek kiszámítása az abszolútérték és szög függvényében
            vx_mps = v0_mps * Math.Cos(alpha_0);
            vy_mps = v0_mps * Math.Sin(alpha_0);
            this.meterbolPixel = meterbolPixel;
            // Rajzeszközök
            ecset = new SolidBrush(Color.Black);
            toll = new Pen(Color.Black);
        }

        // Lövedék mozgatása. Paraméter a mozgató időzítés lépésköze ms mérékegységben.
        public bool Mozog(Panel tabla, int t_ms)
        {
            // Pozíciók és sebességek növelése.
            double t_s = Convert.ToDouble(t_ms) / 1000.0;
            x_m += vx_mps * t_s;
            y_m += vy_mps * t_s;
            vy_mps += g_mps2 * t_s;
            // Ha a labda a következő lépésben már kilépne a pályáról, igaz értékkel térünk vissza.
            double x_m_max = tabla.ClientSize.Width / Convert.ToDouble(meterbolPixel);
            double y_m_max = tabla.ClientSize.Height / Convert.ToDouble(meterbolPixel);
            double x_m_kov = x_m + vx_mps * t_s;
            double y_m_kov = y_m + vy_mps * t_s;
            return ((x_m_kov < D_m / 2) || (x_m_kov > x_m_max - D_m / 2) ||
                    (y_m_kov < D_m / 2) || (y_m_kov > y_m_max - D_m / 2));
        }

        public void Rajzol(Panel tabla, Graphics rajzEszkoz)
        {
            // Méretarány megrajzolása
            int offset = 20;
            Point p1 = new Point(offset, offset);
            Point p2 = new Point(offset + meterbolPixel, offset);
            toll.EndCap = System.Drawing.Drawing2D.LineCap.SquareAnchor;
            toll.StartCap = System.Drawing.Drawing2D.LineCap.SquareAnchor;
            rajzEszkoz.DrawLine(toll, p1, p2);
            // Egy méter felurat megrajzolása
            p1.Y += offset / 5;
            p1.X += offset / 5;
            rajzEszkoz.DrawString("1 méter", new Font("Arial", 8), ecset, p1);
            // Lövedék megrajzolása
            Point balFelsoSarok = new Point(xbal_p, tabla.ClientSize.Height - yfelso_p);
            Size meret = new Size(D_p, D_p);
            Rectangle befoglalo = new Rectangle(balFelsoSarok, meret);
            rajzEszkoz.FillEllipse(ecset, befoglalo);
        }
    }
}
