﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Diagnostics;

namespace FerdeHajitas
{
    
    public partial class FormFoAblak : Form, INotifyPropertyChanged
    {
        // Lövedék mérete és méretarány
        private const double D = 0.2;
        private const int pixelPerMeter = 50;

        // Lövedék objektum
        private Lovedek lovedek;

        // Eltelt idő (ms)
        long elteltIdo;

        // Sebesség és állásszög a csúszkával és szövegdobozzal beállítva.
        private int sebesseg;
        private int allasSzog;
        public int Sebesseg
        {
            get
            {
                return sebesseg;
            }
            set
            {
                if(sebesseg != value)
                {
                    sebesseg = value;
                    // Az adatok kötéséhez informálnunk kell a rendszert, ha a sebesség értéke
                    // megváltozik.
                    NotifyPropertyChanged();
                }
            }
        }
        public int AllasSzog
        {
            get
            {
                return allasSzog;
            }
            set
            {
                if (allasSzog != value)
                {
                    allasSzog = value;
                    // Az adatok kötéséhez informálnunk kell a rendszert, ha az állásszög értéke
                    // megváltozik.
                    NotifyPropertyChanged();
                }
            }
        }
        // Idő mérése
        Stopwatch stopper;
        // Erre az eseményre iratkozik fel a kötésben résztvevő csúszka és szövegdoboz. Innen fogják 
        // tudni, ha a sebesség vagy állásszög értéke megváltozik.
        public event PropertyChangedEventHandler PropertyChanged;
        // Ezzel a metódussal tudatjuk a PropertyChanged eseményre feliratkozókkal (csúszkák,
        // szövegdobozok), hogy egy tulajdonság (sebesség vagy állásszög) értéke megváltozott.
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            // A [CallerMemberName] attribútum hatására a propertyName automatikusan feltöltődik
            // annak a tulajdonságnak a nevével, amin belül a NotifyPropertyChanged metódust hívjuk
            if ( PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        // Főablak konstruktora
        public FormFoAblak()
        {
            InitializeComponent();
            // Vibrálás elkerülése. A DoubleBuffered tulajdonság alapvetően protected, ezért kell
            // így, kerülőúton elérni. 
            typeof(Panel).InvokeMember("DoubleBuffered",
                                       BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                                       null,
                                       panelTabla,
                                       new object[] { true });
            // Alapértékek.
            AllasSzog = 45;
            Sebesseg = 5;
            // Stopper létrehozása
            stopper = new Stopwatch();
            // Új, álló lövedék létrehozása.
            lovedek = new Lovedek(D, 0, 0, pixelPerMeter);
            // Eltelt idő
            elteltIdo = 0;
        }
        // Panel újrarajzolásakor lefutó metódus.
        private void panelTabla_Paint(object sender, PaintEventArgs e)
        {
            // Lövedék kirajzolása
            lovedek.Rajzol(panelTabla, e.Graphics);
        }
        // Ez a metódus a timer ütemezésekor fut le.
        private void timerMozgat_Tick(object sender, EventArgs e)
        {
            // Lövedék mozgatása, és annak megállapítása, hogy elhagyná-e a pályát a következő
            // lépésben
            long ido = stopper.ElapsedMilliseconds;
            bool kint = lovedek.Mozog(panelTabla, (int)(ido - elteltIdo));
            elteltIdo = ido;
            // Panel újrarajzolásának kérése.
            panelTabla.Invalidate();
            // Idő kijelzése
            labelIdo.Text = elteltIdo.ToString() + " ms";
            // Ha a lövedék kimenne a pályáról, a timer megállítása.
            if(kint)
            {
                stopper.Stop();
                timerMozgat.Stop();
                MessageBox.Show("A lövedék a következő lépésben elhagyná a pályát.", "Szimuláció vége", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        // Ez a metódus a start gombra kattintáskor fut le.
        private void buttonStart_Click(object sender, EventArgs e)
        {
            // Új lövedék létrehozása a megadott paraméterek alapján.
            lovedek = new Lovedek(D, Sebesseg, AllasSzog / 180.0 * Math.PI, pixelPerMeter);
            // Mozgatást végző timer indítása.
            timerMozgat.Start();
            // Stopper indítása
            stopper.Restart();
            // Eltelt idő újraindítása
            elteltIdo = 0;
        }
    }
}
