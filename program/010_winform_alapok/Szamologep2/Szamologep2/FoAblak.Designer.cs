﻿namespace Szamologep2
{
    partial class FoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SzovegDobozSzam1 = new System.Windows.Forms.TextBox();
            this.SzovegDobozSzam2 = new System.Windows.Forms.TextBox();
            this.CimkeSzam1 = new System.Windows.Forms.Label();
            this.CimkeSzam2 = new System.Windows.Forms.Label();
            this.SzovegDobozEredmeny = new System.Windows.Forms.TextBox();
            this.CimkeEredmeny = new System.Windows.Forms.Label();
            this.GombOsszeadas = new System.Windows.Forms.Button();
            this.GombKivonas = new System.Windows.Forms.Button();
            this.GombSzorzas = new System.Windows.Forms.Button();
            this.GombOsztas = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SzovegDobozSzam1
            // 
            this.SzovegDobozSzam1.Location = new System.Drawing.Point(120, 13);
            this.SzovegDobozSzam1.Name = "SzovegDobozSzam1";
            this.SzovegDobozSzam1.Size = new System.Drawing.Size(152, 20);
            this.SzovegDobozSzam1.TabIndex = 0;
            this.SzovegDobozSzam1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SzovegDobozSzam2
            // 
            this.SzovegDobozSzam2.Location = new System.Drawing.Point(120, 49);
            this.SzovegDobozSzam2.Name = "SzovegDobozSzam2";
            this.SzovegDobozSzam2.Size = new System.Drawing.Size(152, 20);
            this.SzovegDobozSzam2.TabIndex = 1;
            this.SzovegDobozSzam2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CimkeSzam1
            // 
            this.CimkeSzam1.AutoSize = true;
            this.CimkeSzam1.Location = new System.Drawing.Point(13, 16);
            this.CimkeSzam1.Name = "CimkeSzam1";
            this.CimkeSzam1.Size = new System.Drawing.Size(54, 13);
            this.CimkeSzam1.TabIndex = 1;
            this.CimkeSzam1.Text = "Első szám";
            // 
            // CimkeSzam2
            // 
            this.CimkeSzam2.AutoSize = true;
            this.CimkeSzam2.Location = new System.Drawing.Point(13, 52);
            this.CimkeSzam2.Name = "CimkeSzam2";
            this.CimkeSzam2.Size = new System.Drawing.Size(74, 13);
            this.CimkeSzam2.TabIndex = 1;
            this.CimkeSzam2.Text = "Második szám";
            // 
            // SzovegDobozEredmeny
            // 
            this.SzovegDobozEredmeny.Location = new System.Drawing.Point(120, 85);
            this.SzovegDobozEredmeny.Name = "SzovegDobozEredmeny";
            this.SzovegDobozEredmeny.Size = new System.Drawing.Size(152, 20);
            this.SzovegDobozEredmeny.TabIndex = 7;
            this.SzovegDobozEredmeny.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CimkeEredmeny
            // 
            this.CimkeEredmeny.AutoSize = true;
            this.CimkeEredmeny.Location = new System.Drawing.Point(13, 88);
            this.CimkeEredmeny.Name = "CimkeEredmeny";
            this.CimkeEredmeny.Size = new System.Drawing.Size(54, 13);
            this.CimkeEredmeny.TabIndex = 1;
            this.CimkeEredmeny.Text = "Eredmény";
            // 
            // GombOsszeadas
            // 
            this.GombOsszeadas.Location = new System.Drawing.Point(12, 134);
            this.GombOsszeadas.Name = "GombOsszeadas";
            this.GombOsszeadas.Size = new System.Drawing.Size(55, 23);
            this.GombOsszeadas.TabIndex = 3;
            this.GombOsszeadas.Text = "+";
            this.GombOsszeadas.UseVisualStyleBackColor = true;
            this.GombOsszeadas.Click += new System.EventHandler(this.GombOsszeadas_Click);
            // 
            // GombKivonas
            // 
            this.GombKivonas.Location = new System.Drawing.Point(81, 134);
            this.GombKivonas.Name = "GombKivonas";
            this.GombKivonas.Size = new System.Drawing.Size(55, 23);
            this.GombKivonas.TabIndex = 4;
            this.GombKivonas.Text = "-";
            this.GombKivonas.UseVisualStyleBackColor = true;
            this.GombKivonas.Click += new System.EventHandler(this.GombKivonas_Click);
            // 
            // GombSzorzas
            // 
            this.GombSzorzas.Location = new System.Drawing.Point(150, 134);
            this.GombSzorzas.Name = "GombSzorzas";
            this.GombSzorzas.Size = new System.Drawing.Size(55, 23);
            this.GombSzorzas.TabIndex = 5;
            this.GombSzorzas.Text = "x";
            this.GombSzorzas.UseVisualStyleBackColor = true;
            this.GombSzorzas.Click += new System.EventHandler(this.GombSzorzas_Click);
            // 
            // GombOsztas
            // 
            this.GombOsztas.Location = new System.Drawing.Point(217, 134);
            this.GombOsztas.Name = "GombOsztas";
            this.GombOsztas.Size = new System.Drawing.Size(55, 23);
            this.GombOsztas.TabIndex = 6;
            this.GombOsztas.Text = "/";
            this.GombOsztas.UseVisualStyleBackColor = true;
            this.GombOsztas.Click += new System.EventHandler(this.GombOsztas_Click);
            // 
            // FoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 198);
            this.Controls.Add(this.GombOsztas);
            this.Controls.Add(this.GombSzorzas);
            this.Controls.Add(this.GombKivonas);
            this.Controls.Add(this.GombOsszeadas);
            this.Controls.Add(this.CimkeEredmeny);
            this.Controls.Add(this.CimkeSzam2);
            this.Controls.Add(this.CimkeSzam1);
            this.Controls.Add(this.SzovegDobozEredmeny);
            this.Controls.Add(this.SzovegDobozSzam2);
            this.Controls.Add(this.SzovegDobozSzam1);
            this.Name = "FoAblak";
            this.Text = "Számológép";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox SzovegDobozSzam1;
        private System.Windows.Forms.TextBox SzovegDobozSzam2;
        private System.Windows.Forms.Label CimkeSzam1;
        private System.Windows.Forms.Label CimkeSzam2;
        private System.Windows.Forms.TextBox SzovegDobozEredmeny;
        private System.Windows.Forms.Label CimkeEredmeny;
        private System.Windows.Forms.Button GombOsszeadas;
        private System.Windows.Forms.Button GombKivonas;
        private System.Windows.Forms.Button GombSzorzas;
        private System.Windows.Forms.Button GombOsztas;
    }
}

