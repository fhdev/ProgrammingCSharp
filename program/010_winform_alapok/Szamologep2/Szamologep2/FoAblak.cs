﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Szamologep2
{
    public partial class FoAblak : Form
    {
        private double szam1;
        private double szam2;
        private double eredmeny;

        public FoAblak()
        {
            InitializeComponent();
        }

        private void SzamBeolvas()
        {
            double.TryParse(SzovegDobozSzam1.Text, out szam1);
            double.TryParse(SzovegDobozSzam2.Text, out szam2);
        }

        private void EredmenyKiir()
        {
            SzovegDobozEredmeny.Text = eredmeny.ToString();
        }

        private void GombOsszeadas_Click(object sender, EventArgs e)
        {
            SzamBeolvas();
            eredmeny = szam1 + szam2;
            EredmenyKiir();
        }

        private void GombKivonas_Click(object sender, EventArgs e)
        {
            SzamBeolvas();
            eredmeny = szam1 - szam2;
            EredmenyKiir();
        }

        private void GombSzorzas_Click(object sender, EventArgs e)
        {
            SzamBeolvas();
            eredmeny = szam1 * szam2;
            EredmenyKiir();


        }

        private void GombOsztas_Click(object sender, EventArgs e)
        {
            SzamBeolvas();
            eredmeny = szam1 / szam2;
            EredmenyKiir();
        }
    }
}
