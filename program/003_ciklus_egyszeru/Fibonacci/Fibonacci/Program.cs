﻿using System;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            /* A program céljának leírása. */
            Console.WriteLine("A Fibonacci-sorozat első n elemének felsorolása.");
            Console.WriteLine("A Fibonacci-sorozat első elemeit így értelmezzük: 0 1 1 2 3 ...");
            /* Az n egész szám beolvasása ellenőrzés nélkül. */
            Console.Write("Kérem adja meg n értékét! n = ");
            string bemenet = Console.ReadLine();
            int n;
            int.TryParse(bemenet, out n);
            /* A Fibonacci-sorozat elemeinek kiszámítása. */
            int aktualisElem = 0;
            int kovetkezoElem = 1;
            int hanyadikElem = 1;
            /* A ciklus addig fut, amíg elérünk az n-edik elemig. */
            while(hanyadikElem < n + 1)
            {
                Console.WriteLine("A(z) {0,3:D}. elem: {1,4:D}", hanyadikElem, aktualisElem);
                /* Elmentjük a következő elem jelenlegi értékét. */
                int seged = kovetkezoElem;
                /* Számítjuk a következő elem új értékét, ami az előző két elem összege. 
                 * Tehát a következő elem új értéke az aktuális elemnek és a következő elem jelenlegi értékének
                 * az összege.
                 */
                kovetkezoElem += aktualisElem;
                /* Az aktuális elem új értéke megegyezik a következő elem jelenlegi értékével. */
                aktualisElem = seged;
                /* Léptetjük, hogy hanyadik elemnél tartunk. */
                hanyadikElem++;
            }
            /* A program bezárásával várunk egy billentyű lenyomásáig. */
            Console.Write("Kérem nyomjon meg egy billentyűt a kilépéshez...");
            Console.ReadKey();
        }
    }
}
