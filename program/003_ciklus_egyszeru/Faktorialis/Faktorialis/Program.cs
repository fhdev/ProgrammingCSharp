﻿using System;

namespace Faktorialis
{
    class Program
    {
        static void Main(string[] args)
        {
            /* A program céljának leírása. */
            Console.WriteLine("Az n! (n faktoriális) értékének kiszámítása.");
            /* Egy nemnegatív egész szám ellenőrzött beolvasása. */
            Console.WriteLine("Kérem adja meg n értékét (n>=0)!");
            string bemenet;
            int n;
            bool nErvenyes;
            do
            {
                Console.Write("n = ");
                bemenet = Console.ReadLine();
                nErvenyes = int.TryParse(bemenet, out n);
                /* Ha a beolvasott szám nullánál kisebb, akkor nem érvényes a bemenet. */
                if (n < 0)
                {
                    nErvenyes = false;
                }
                if(!nErvenyes)
                {
                    Console.WriteLine("A megadott érték érvénytelen, kérem próbálja újra!");
                }
            } while (!nErvenyes);
            /* Az n faktoriális számítása while ciklussal, int típusú változóba. */
            Console.WriteLine("Az n! értékének kiszámítása while ciklussal int típusú változóba...");
            Console.WriteLine("Az int típusú változóban tárolható maximális szám: {0:D}", int.MaxValue);
            int aktualisSzamInt = 1;
            int faktorialisInt = 1;
            while(aktualisSzamInt < n + 1)
            {
                faktorialisInt = faktorialisInt * aktualisSzamInt;
                aktualisSzamInt = aktualisSzamInt + 1;
            }
            Console.WriteLine("Az eredmény: {0:D}! = {1:D}", n, faktorialisInt);
            /* Az n faktoriális számítása for ciklussal, double típusú változóba. */
            Console.WriteLine("Az n! értékének kiszámítása for ciklussal long típusú változóba...");
            Console.WriteLine("A long típusú változóban tárolható maximális szám: {0:D}", long.MaxValue);
            long faktorialisLong = 1L;
            for(long aktualisSzamLong = 1L; aktualisSzamLong < n + 1; aktualisSzamLong++)
            {
                faktorialisLong *= aktualisSzamLong;
            }
            Console.WriteLine("Az eredmény: {0:D}! = {1:D}", n, faktorialisLong);
            /* Megnézzük hogy belefér-e a long típusban számított eredmény az int típusú változóba. */
            if(faktorialisLong > int.MaxValue)
            {
                Console.WriteLine("Az {0:D}! értéke nem fér el egy int típusú változóban!", n);
            }
            /* A programból való kilépés előtt várunk egy billentyű lenyomására. */
            Console.Write("Nyomjon meg egy billentyűt a kilépéshez...");
            Console.ReadLine();
        }
    }
}
