﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exponential
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Az e^x értékének közelítése nmax + 1 lépésben.");

            Console.Write("Kérem adja meg a lépések számát! nmax = ");
            int nmax;
            int.TryParse(Console.ReadLine(), out nmax);

            Console.Write("Kérem adja meg x értékét! x = ");
            double x;
            double.TryParse(Console.ReadLine(), out x);

            // Kezdeti értékek n = 0 esetén
            // n! tároló változó
            double fact = 1;
            // x^n tároló változó
            double pow = 1;

            // e^x értékét tároló változó
            double exp = 0;

            for(int n = 0; n <= nmax; n++)
            {
                // A közelítő összeg számítása
                exp += pow / fact;
                // x^n számítása a következő körre
                pow *= x;
                // n! számítása a következő körre
                fact *= (n + 1);     
            }

            Console.WriteLine("Az eredmény: e^{0:G} = {1:G} .", x, exp);
        }
    }
}
