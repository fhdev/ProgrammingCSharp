﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Szamkitalalo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Számkitaláló játék.");
            Console.WriteLine("Gondolok egy egész számra 0 és 100 között!");

            Random random = new Random();
            int gondolt = random.Next(0, 101);

            int hanytipp = 0;
            int tipp = -1;

            while(tipp != gondolt)
            {
                hanytipp++;
                Console.Write("Tippelj! A tipped: ");
                int.TryParse(Console.ReadLine(), out tipp);
                if(tipp > gondolt)
                {
                    Console.WriteLine("Az én számom kisebb!");
                }
                else if(tipp < gondolt)
                {
                    Console.WriteLine("Az én számom nagyobb!");
                }
            }

            Console.WriteLine("A számom valóban a {0:D}. Te {1:D} próbálkozásból találtad ki. ", gondolt, hanytipp);
        }
    }
}
