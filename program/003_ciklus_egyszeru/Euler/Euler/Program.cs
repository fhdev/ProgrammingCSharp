﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler
{
    class Program
    {
        static void Main(string[] args)
        {
            // Program céljának leírása.
            Console.WriteLine("Az Euler-féle szám értékének kiszámítása 0-körüli Taylor-sorának segítségével.");
            Console.WriteLine("A számolás akkor áll le, ha elértük a Taylor-sor megadott indexű tagját.");
            Console.WriteLine("Kérem adja meg a Taylor-sor utolsó figyelembe veendő tagjának indexét.");
            // Egész szám beolvasása ellenőrzötten. 
            int n;
            do
            {
                Console.Write("\tn = ");
            } while (!int.TryParse(Console.ReadLine(), out n));
            // Euler-féle szám közelítése: e ~= 1/0! + 1/1! + 1/2! + 1/3! + 1/n!
            // Az 1 indexű tagot kezdeti értékként adjuk meg! 0! = 1.
            double faktorialis = 1.0;
            // Az 1 indexű tagot kezdeti értékként adjuk meg! 1/0! = 1.
            double euler = 1.0;
            // Szumma számítása.
            for (int i = 1; i < n + 1; i++)
            {
                faktorialis *= i;
                euler += 1.0 / faktorialis;
            }
            // Eredmények kiírása. 
            Console.WriteLine("Az eredmény: e = {0:F10} .", euler);
            Console.WriteLine("Math.E értéke: e = {0:F10} .", Math.E);
        }
    }
}
