﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SzamkitalaloAutomata
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Számkitaláló játék!");
            Console.WriteLine("Gondolj egy egész számra 0 és 100 között, és én kitalálom!");
            Console.WriteLine("Nyomj meg egy billentyűt, ha készen állsz!");
            Console.ReadKey();

            int hanytipp = 0;
            int also = 0;
            int felso = 101;
            int tipp = -1;
            ConsoleKeyInfo billentyu = new ConsoleKeyInfo();

            while (billentyu.Key != ConsoleKey.T)
            {
                hanytipp++;
                tipp = (also + felso) / 2;
                Console.WriteLine("Tippelek! A tippem: {0:D}.", tipp);
                Console.WriteLine("A te számod nagyobb [N], kisebb [K], mint az enyém? Esetleg kitaláltam [T]?");
                billentyu = Console.ReadKey();
                Console.WriteLine();
                switch(billentyu.Key)
                {
                    case ConsoleKey.N:
                    {
                        also = tipp;
                        break;
                    }
                    case ConsoleKey.K:
                    {
                        felso = tipp;
                        break;
                    }
                    case ConsoleKey.T:
                    {
                        break;
                    }
                    default:
                    {
                        Console.WriteLine("Inkább újra megkérdezem...");
                        tippek--;
                        break;
                    }
                }
            }
            Console.WriteLine("A számod a(z) {0:D} volt. Én {1:D} " +
                "próbálkozásból találtam ki.", tipp, hanytipp);
        }
    }
}
