﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Szinusz
{
    class Program
    {
        static void Main(string[] args)
        {
            // Program céljának leírása.
            Console.WriteLine("A cos(alpha) értékének kiszámítása nmax + 1 lépésben vagy epsilon pontossággal.");

            // Utolsó tag indexének beolvasása.
            Console.WriteLine("Kérem adja meg a lépések maximális számát.");
            int nmax;
            do
            {
                Console.Write("nmax [1] = ");
            } while (!int.TryParse(Console.ReadLine(), out nmax));

            // Pontosság beolvasása.
            Console.WriteLine("Kérem adja meg a megkívánt pontosságot.");
            double epsilon;
            do
            {
                Console.Write("epsilon [1] = ");
            } while (!double.TryParse(Console.ReadLine(), out epsilon));

            // Szög beolvasása
            Console.WriteLine("Kérem adja meg az alpha szög értékét [°] mértékegységben.");
            double alpha;
            do
            {
                Console.Write("alpha [°] = ");
            } while (!double.TryParse(Console.ReadLine(), out alpha));

            // Szög átváltása fok-ból radián-ba
            Console.WriteLine("A megadott szög átváltása [rad] mértékegységre...");
            // Egész fordulatok levonása
            double x = alpha - (double)((int)(alpha / 360.0)) * 360.0;
            // Átváltás radián-ba
            x *= Math.PI / 180.0;
            Console.WriteLine("cos({0:G9} [°]) = cos({1:G9} [rad])", alpha, x);

            // Taylor-polinom számítása, a 0 indexű tagot kezdeti értékként számoljuk.
            // A (-1)^n értékét tároló változó
            double elojel = 1.0;
            // A (2n+1)! értékét tároló változó
            double fakt = 1.0;
            // Az x^(2n+1) értékét tároló változó
            double hatvany = 1.0;

            // A cos értékét tároló változó
            double koszinusz = 0.0;

            for (int n = 0; n <= nmax; n++)
            {
                // A közelítõ összeg legújabb tagjának számítása
                double novekmeny = elojel * hatvany / fakt;
                // Pontosság ellenõrzése (fabs és makró nélkül :O)
                if (novekmeny * novekmeny < epsilon * epsilon)
                {
                    Console.WriteLine("A számítás a megadott pontosság elérése miatt az {0:D} " +
                                      "indexű tag után leállt.", n);
                    break;
                }
                // A közelítõ összeg kiszámítása
                koszinusz += novekmeny;

                // (-1)^n kiszámítása a következő körre
                elojel *= -1.0;
                // x^(2.n+1) kiszámítása a következő körre
                hatvany *= x * x;
                // (2.n+1)! kiszámítása a következő körre
                fakt *= (2.0 * (n + 1) - 1.0) * 2.0 * (n + 1);

            }
            // Eredmények kiírása
            Console.WriteLine("Az eredmény: cos({0:G9} [rad]) = {1:G9}", x, koszinusz);
            Console.WriteLine("Az eredmény a Math.Cos fügvénnyel: cos({0:G9} [rad]) = {1:G9}",
                              x, Math.Cos(x));

            // Kilépés megelőzése
            Console.Write("Kérem nyomjon meg egy billentyűt a kilépéshez...");
            Console.ReadKey();
        }
    }
}
