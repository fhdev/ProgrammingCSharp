﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negyzetgyok
{
    class Program
    {
        const double tol = 1e-9;

        static void Main(string[] args)
        {
            Console.WriteLine("Egy szám négyzetgyökének megtalálása.");

            Console.WriteLine("Kérem adjon meg egy pozitív számot!");
            Console.Write("szam = ");
            double szam;
            if(!double.TryParse(Console.ReadLine(), out szam))
            {
                Console.WriteLine("Érvénytelen bemenet! Kilépés...");
            }
            else if(szam < 0)
            {
                Console.WriteLine("A megadott szám negatív! Kilépés...");
            }
            else
            {
                double also, felso, gyok, gyok2;
                if (szam > 1)
                {
                    also = 0;
                    felso = szam;
                }
                else
                {
                    also = szam;
                    felso = 1;
                }
                gyok = 0;
                gyok2 = 0;
                while(Math.Abs(szam - gyok2) > (tol * szam))
                {
                    gyok = (also + felso) / 2;
                    gyok2 = gyok * gyok;
                    if(gyok2 < szam)
                    {
                        also = gyok;
                    }
                    else if(gyok2 > szam)
                    {
                        felso = gyok;
                    }
                    else
                    {
                        break;
                    }
                }
                Console.WriteLine("Az eredmény: sqrt({0:G}) = {1:G} .", szam, gyok);
            }
        }
    }
}
