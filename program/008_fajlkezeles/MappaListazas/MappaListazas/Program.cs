﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DirList
{
    class Program
    {
        static void Main(string[] args)
        {
            string list = "";
            if (args.Length < 1)
            {
                list = DirList();
            }
            else
            {
                list = DirList(args[0]);
            }
            Console.WriteLine(list);
        }

        static string DirList(string root = "", string indent = "")
        {
            string output = "";
            // Default arguments
            if (string.IsNullOrEmpty(root))
            {
                root = Directory.GetCurrentDirectory();
            }
            // Check arguments
            if (!Directory.Exists(root))
            {
                return output;
            }
            // Create directory list
            string[] subDirs = Directory.GetDirectories(root);
            Array.Sort(subDirs, StringComparer.Ordinal);
            string[] files = Directory.GetFiles(root);
            Array.Sort(files, StringComparer.Ordinal);
            string[] children = subDirs.Concat(files).ToArray();
            for (int iChild = 0; iChild < children.Length; iChild++)
            {
                string currIndent = indent + "  ├ ";
                string newIndent = indent + "  │ ";
                if (iChild == children.Length - 1)
                {
                    currIndent = indent + "  └ ";
                    newIndent = indent + "    ";
                }
                if (iChild < subDirs.Length)
                {
                    output += currIndent + "[" + Path.GetFileName(children[iChild]) + "]" + Environment.NewLine;
                    output += DirList(subDirs[iChild], newIndent);
                }
                else
                {
                    output += currIndent + Path.GetFileName(children[iChild]) + Environment.NewLine;
                }
            }
            return output;
        }
    }
}
