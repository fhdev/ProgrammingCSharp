﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TombSzovegFajl
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Random számok írása szövegfájlba. Kérem adja meg, hány számot szeretne!");
            // Darabszám beolvasása.
            int n;
            do
            {
                Console.Write("n = ");
            } while (!int.TryParse(Console.ReadLine(), out n));
            string nev = "szamok.txt";
            // Számok kiírása.
            RandomSzamokSzovegFajlba(n, nev);
            Console.WriteLine("Számok beolvasása...");
            // Számok beolvasása.
            int[] szamok = SzamokSzovegFajlbol(nev);
            // Kiírás a konzolra.
            foreach(int szam in szamok)
            {
                Console.WriteLine(szam);
            }
        }

        static void RandomSzamokSzovegFajlba(int darab, string fajlNev)
        {
            // Fájl létrehozása írásra. Ha már létezik, kérdezés nélkül felülírjuk.
            StreamWriter fajl = new StreamWriter(fajlNev);
            // Ezzel az objektummal lehet véletlen számokat generálni. 
            Random rnd = new Random();
            // A kért darabszámú véletlenszámot írjuk a fájlba soronként.
            for (int i = 0; i < darab; i++)
            {
                fajl.WriteLine(rnd.Next());
            }
            // Bezárjuk a fájt, ezzel garantálva a mentést.
            fajl.Close();
            // Felszabadítjuk a fájlba íráshoz használt erőforrásokat (ciklikus puffer).
            fajl.Dispose();
        }

        static int[] SzamokSzovegFajlbol(string fajlNev)
        {
            // Fájl megnyitása olvasásra. A fájlnak léteznie kell!
            StreamReader fajl = new StreamReader(fajlNev);
            // Ebbe a változóba olvassuk fel a fájl sorait.
            string sor;
            // Számoljuk a felolvasott sorokat.
            int darab = 0;
            // Alapértelmezés szerint 10 számra foglalunk le helyet.
            int[] szamok = new int[10];
            // Felolvassuk a fájl összes sorát.
            while ((sor = fajl.ReadLine()) != null)
            {
                // Ha a már felolvastunk annyi számot, amennyi a tömbbe belefér, növeljük a tömb
                // méretét.
                if (darab >= szamok.Length)
                {
                    Array.Resize(ref szamok, darab + 10);
                }
                // Szám beolvasása.
                int.TryParse(sor, out szamok[darab]);
                // Darabszám növelése.
                darab++;
            }
            // Ha a végén a tömb hosszabb, mint amennyi számot felolvastunk, levágjuk a felesleget.
            if (darab < szamok.Length)
            {
                Array.Resize(ref szamok, darab);
            }
            // Fájl bezárása.
            fajl.Close();
            // Erőforrások felszabadítása.
            fajl.Dispose();
            return szamok;
        }
    }
}
