﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorPlus
{
    class Operator : Token
    {
        public static readonly List<Operator> List;

        static Operator()
        {
            // Create list of operators that we can handle.
            List = new List<Operator>();
            List.Add(new Operator("Addition", 
                                  "+",
                                  Precedences.Fourth,
                                  Associativities.LeftToRight,
                                  Types.Binary,
                                  (x, y) => x + y));
            List.Add(new Operator("PositiveSign",
                                  "+",
                                  Precedences.Second,
                                  Associativities.RightToLeft,
                                  Types.Unary,
                                  (x, y) => y));
            List.Add(new Operator("Subtraction",
                                  "-",
                                  Precedences.Fourth,
                                  Associativities.LeftToRight,
                                  Types.Binary,
                                  (x, y) => x - y));
            List.Add(new Operator("NegativeSign",
                                  "-",
                                  Precedences.Second,
                                  Associativities.RightToLeft,
                                  Types.Unary,
                                  (x, y) => -y));
            List.Add(new Operator("Multiplication",
                                  "*",
                                  Precedences.Third,
                                  Associativities.LeftToRight,
                                  Types.Binary,
                                  (x, y) => x * y));
            List.Add(new Operator("Division",
                                  "/",
                                  Precedences.Third,
                                  Associativities.LeftToRight,
                                  Types.Binary,
                                  (x, y) => x / y));
            List.Add(new Operator("Exponention",
                                  "^",
                                  Precedences.Second,
                                  Associativities.RightToLeft,
                                  Types.Binary,
                                  Math.Pow));
            List.Add(new Operator("Replus",
                                  "x",
                                  Precedences.Third,
                                  Associativities.LeftToRight,
                                  Types.Binary,
                                  (x, y) => x * y / (x + y)));
            List.Add(new Operator("Sine",
                                  "sin",
                                  Precedences.Third,
                                  Associativities.RightToLeft,
                                  Types.Unary,
                                  (x, y) => Math.Sin(y)));
            List.Add(new Operator("Cosine",
                                  "cos",
                                  Precedences.Third,
                                  Associativities.RightToLeft,
                                  Types.Unary,
                                  (x, y) => Math.Cos(y)));
            List.Add(new Operator("Tangent",
                                  "tan",
                                  Precedences.Third,
                                  Associativities.RightToLeft,
                                  Types.Unary,
                                  (x, y) => Math.Tan(y)));
        }
        public Operator(string name,
                        string symbol, 
                        Precedences precedence, 
                        Associativities associativity,
                        Types type, 
                        Func<double, double, double> evaluationFunction)
            : base(symbol)
        {
            Name = name;
            Precedence = precedence;
            Associativity = associativity;
            Type = type;
            EvaluationFunction = evaluationFunction;
        }
    
        public enum Precedences {None, Fourth, Third, Second, First };

        public enum Associativities {None, LeftToRight, RightToLeft };

        public enum Types {None, Unary, Binary };

        public string Name { get; private set; }

        public Precedences Precedence { get; private set; }

        public Associativities Associativity { get; private set; }

        public Types Type { get; private set; }

        public Func<double, double, double> EvaluationFunction {get; private set; }

        public static bool TryGet(string expression, Token previous, out Token foundOperator)
        {
            bool found = false;
            foundOperator = null;
            if (expression.Length > 0)
            {
                Operator validOperator = null;
                List<Operator> matchingOperators = 
                    List.FindAll(op => (expression.IndexOf(op.Symbol) == 0));
                // We search for a unary operator if previous token was an operator or we are at the
                // begining of the expression.
                if ((previous == null) || (previous is Operator))
                {
                    validOperator = matchingOperators.Find(op => (op.Type == Types.Unary));
                }
                // We search for a binary operator if previous token was an operand.
                else if (previous is Operand)
                {
                    validOperator = matchingOperators.Find(op => (op.Type == Types.Binary));
                }
                if(validOperator != null)
                {
                    foundOperator = validOperator;
                    found = true;
                }
                else if(matchingOperators.Count > 0)
                {
                    throw new ArgumentException("Expression contains unexpected operator \"" +
                        matchingOperators.First().Symbol + "\".");
                }
            }
            return found;
        }
        
    }
}
