﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorPlus
{
    abstract class Token
    {
        public Token(string symbol)
        {
            Symbol = symbol;
        }
        public string Symbol { get; private set; }


    }
}
