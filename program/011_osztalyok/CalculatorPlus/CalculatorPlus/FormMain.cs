﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using System.IO;

namespace CalculatorPlus
{
    public partial class FormMain : Form, INotifyPropertyChanged
    {
        private string expression;
        private string result;
        private string inFileName;
        private string outFileName;

        public string Expression
        {
            get
            {
                return expression;
            }
            set
            {
                if(value != expression)
                {
                    expression = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string Result
        {
            get
            {
                return result;
            }
            set
            {
                if (value != result)
                {
                    result = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public string InFileName
        {
            get
            {
                return inFileName;
            }
            set
            {
                if(value != inFileName)
                {
                    inFileName = value;
                    NotifyPropertyChanged();
                }
            }           
        }

        public string OutFileName
        {
            get
            {
                return outFileName;
            }
            set
            {
                if (value != outFileName)
                {
                    outFileName = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public FormMain()
        {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void buttonEvaluate_Click(object sender, EventArgs e)
        {
            if (Expression != null)
            {
                try
                {
                    double value = Parser.EvaluateExpression(Expression);
                    Result = value.ToString(System.Globalization.CultureInfo.InvariantCulture);
                }
                catch (Exception exception)
                {
                    Result = string.Empty;
                    MessageBox.Show(exception.Message, "Error during evaluation", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        private void textBoxExpression_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                buttonEvaluate_Click(sender, e);
                // Disabling the annoying notification sound which is played because we press Enter
                // in a single-line TextBox.
                e.SuppressKeyPress = true;
            }
        }

        private void textBoxInFileName_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if(openFileDialogIn.ShowDialog() == DialogResult.OK)
            {
                InFileName = openFileDialogIn.FileName;
            }
        }

        private void textBoxOutFileName_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if(saveFileDialogOut.ShowDialog() == DialogResult.OK)
            {
                OutFileName = saveFileDialogOut.FileName;
            }
        }

        private void buttonProcessFile_Click(object sender, EventArgs e)
        {
            if (File.Exists(InFileName) && (OutFileName != null))
            {
                Parser.EvaluateDlmTextFile(InFileName, OutFileName, ';');
                MessageBox.Show("Input file successfully processed.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void openInputFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(InFileName))
            {
                Process.Start(InFileName);
            }
        }

        private void openOutputFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(OutFileName))
            {
                Process.Start(OutFileName);
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
