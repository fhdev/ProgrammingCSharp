﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    class Person
    {
        static public int counter = 0;  // statikus adattag -> számolja az osztálypéldányok számát
        private string name;            
        public string Name              // tulajdonság (property), a public Name változtatásával lehet a private name-t változtatni
        {
            get { return this.name+" Úr"; }     // ha felhasználjuk a Name-t, akkor Name=name+" Úr"
            set { this.name = value+" Űr"; }    // ha értéket adunk a Name-nek, akkor name=Name+" Űr"
        }
        public void WriteOut()
        {
            Console.WriteLine(this.name);
        }
        public Person(string name)  // konstruktor
        {
            this.name = name;
            Person.counter++;  // osztálypéldányok számának növelése
        }
        ~Person()   // destruktor
        {
            Person.counter--;  // osztálypéldányok számának csökkentése
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person("Béla");      // p.name=Béla
            Console.WriteLine(Person.counter);  // >> 1, létrejött egy osztálypéldány
            Console.WriteLine(p.Name);          // p.Name=p.name+" Úr" >> Béla Úr
            p.Name = "Jenő";                    // p.name=p.Name+" Űr"="Jenő Űr"
            p.WriteOut();                       // >> Jenő Űr
            Console.WriteLine(p.Name);          // p.Name=p.name+" Úr" >> Jenő Űr Úr
            Console.ReadKey();
        }
    }
}
