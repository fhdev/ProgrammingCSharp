﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    // Absztrakt osztály: nem lehet példányosítani, az a lényege, hogy közös felületet nyújtson az utódainak
    // Metódus is lehet absztrakt, akkor nincs definíciója, csak deklarálva van a szülő osztályban, és majd a leszármazottak definiálják, muszáj is nekik!
    abstract class Animal
    {
        public Animal(string name)
        {
            this.Name = name;
            Console.WriteLine("Animal Constructor");
        }
        public string Name { get; set; }
        // Virtuális metódus, a leszármazott osztály az override kulcsszóval átírhatja!
        public virtual void Eat()
        {
            Console.WriteLine("An animal is eating");
        }
    }

    class Dog : Animal
    {
        // Konstruktor
        public Dog(string name)
            : base(name)    // A leszármazott osztály a szülő osztály konstruktorát hívja meg, majd utána amit még szeretnénk.
        {
            Console.WriteLine("Dog Constructor");
        }
        // A szülő osztály (Animal) virtuális metódusát írja felül. Automatikusan virtuális is, tehát a Dog leszármazottjai is átírhatják
        // Ha az override helyett new-t alkalmazunk, akkor a Dog leszármazottjai az eredeti Animal Eat metódusát már nem fogják látni, hanem
        // innen kezdődik újra a metódus öröklése
        public override void Eat()
        {
            Console.WriteLine("A dog is eating");
        }
    }

    class Yorkshire : Dog
    {
        public Yorkshire(string name)
            : base(name)
        {
            Console.WriteLine("Yorkshire Constructor");
        }
        // Lezárt metódus, ezt már nem lehet a további leszármazottaknak módosítani! Osztály is lehet sealed
        public sealed override void Eat()
        {
            Console.WriteLine("A yorkshire is eating");
        }
    }

    class Cat : Animal
    {
        public Cat(string name)
            : base(name)
        {
            Console.WriteLine("Cat Contructor");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Dog d = new Dog("Yorkshire Terrier");
            Cat c = new Cat("Main Coon");
            Dog y = new Yorkshire("Samumamu"); // Yorkshire y = new Yorkshire("Samumamu") is lehetne!!! Yorkshire is a dog!
            d.Eat();
            c.Eat();
            y.Eat();
            Console.ReadKey();
        }
    }
}
