﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    // Egy bizonyos minta, felület megvalósításáért felel. Ha alkalmazzuk, akkor az osztályunknak kötelezően tartalmazni kell az Interface metódusait
    interface IAnimal
    {
        void Eat();
    }

    class Dog : IAnimal
    {
        public Dog(string name)
        {
            this.Name = name;
        }
        public string Name { get; set; }
        public void Eat()
        {
            Console.WriteLine("A dog is eating");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Dog d = new Dog("Samu");
            if (d is IAnimal)
            {
                Console.WriteLine("Megvalósítjuk az IAnimal interfészt");
            }
            d.Eat();
            Console.ReadKey();
        }
    }
}
