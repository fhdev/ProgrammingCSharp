﻿using System;

namespace Complex
{
    class KomplexSzam
    {
        // A szám valós része. Privát elérésű, az osztályon kívülről nem látható. 
        private double valos;

        // A szám imaginárius része. Privát elérésű, az osztályon kívülről nem látható. 
        private double kepzetes;

        // Publikus tulajdonság (property) ami a szám valós részének osztályon kívülről történő 
        // elérésére szolgál. 
        public double Valos
        {
            // A get "függvény" akkor fut le, mikor a tulajdonság értékét fel akarjuk használni. 
            // Jelen programban mikor egy komplex szám objektum (cplxNum1, cplxNum2, cplxResult) 
            // valós részét valamilyen értékadás jobb  oldalán szeretnénk felhasználni. 
            // Pl.: textBoxNum1Re.Text = cplxNum1.Valos.ToString(); 
            get
            {
                // Itt nem végzünk semmilyen számítást, csak visszaadjuk az egyébként kívülről 
                // közvetlenül el nem érhető valos adattag értékét. 
                return valos;
            }
            // A set "függvény" akkor fut le, mikor a tulajdonság értékét módosítani akarjuk. 
            // Jelen programban mikor egy komplex szám objektum valós részét valamilyen értékre be
            // szeretnénk állítani.  Pl.: cplxNum1.Valos = num1Re;
            set
            {
                valos = value;
            }
        }

        // Publikus tulajdonság (property) ami a szám imaginárius részének osztályon kívülről 
        // történő elérésére szolgál.
        public double Kepzetes
        {
            get
            {
                return kepzetes;
            }
            set
            {
                kepzetes = value;
            }
        }
        // Publikus tulajdonság, ami a szám abszolút értékét tartalmazza. Csak olvasható tulajdonság 
        // (read-only property), mivel csak a get függvénye van meg. Értéket adni nem lehet neki, 
        // csak az értékét  felhasználni.
        public double AbszolutErtek
        {
            get
            {
                // A valós és imaginárius részek alapján kiszámítjuk az abszolút értéket, és 
                // visszatérünk vele. 
                return Math.Sqrt(valos * valos + kepzetes * kepzetes);
            }
        }
        // Publikus tulajdonság, ami a szám valós tengellyel bezárt szögének értékét tartalmazza. 
        // Csak olvasható.
        public double Szog
        {
            get
            {
                // Kiszámítjuk a valós tengellyel bezárt szöget. Figyelnünk kell, ha a valós rész
                //  értéke 0, hiszen a 0-val való osztás nem megengedett.
                double szog = 0.0;
                if (valos == 0.0)
                {
                    if (kepzetes > 0.0)
                    {
                        szog = Math.PI / 2.0;
                    }
                    if (kepzetes < 0.0)
                    {
                        szog = -Math.PI / 2.0;
                    }
                }
                else
                {
                    szog = Math.Atan(kepzetes / valos);
                }
                return szog;
            }
        }

        // Publikus tulajdonság, ami megadja az adott szám komplex konjugáltját. Csak olvasható. 
        public KomplexSzam Konjugalt
        {
            get
            {
                // Egy új komplex objektumot adunk vissza, melynek imaginárius része az adott szám
                // imaginárius részének -1 szerese. 
                return new KomplexSzam(valos, -kepzetes);
            }
        }

        // A Complex osztály egyik konstruktora. Ez a függvény akkor fut le, mikor az osztályból egy 
        // új objektumot hozunk létre a new kulcsszóval. Ez a konstruktor a neki megadott valós és 
        // imaginárius rész alapján hoz létre egy új komplex számot.
        public KomplexSzam(double valosResz, double kepzetesResz)
        {
            valos = valosResz;
            kepzetes = kepzetesResz;
        }

        // A Complex osztály másik konstruktora. Ez a konstruktor egy már létező komplex szám 
        // objektum másolatát hozza létre.
        public KomplexSzam(KomplexSzam masik)
        {
            valos = masik.valos;
            kepzetes = masik.kepzetes;
        }

        // Ez a metódus visszatér az komplex szám másolatával. 
        public KomplexSzam Masol()
        {
            // Készítünk egy új komplex számot ugyanazzal a valós és imaginárius résszel mint a 
            // sajátunk. 
            KomplexSzam masolat = new KomplexSzam(valos, kepzetes);
            // Visszatérünk a másolattal. 
            return masolat;
        }

        // Ez a metódus az argumentumában kapott másik komplex számot hozzáadja a komplex számhoz.
        public void OsszeadVele(KomplexSzam masik)
        {
            // A saját valós részünkhöz hozzáadjuk a másik szám valós részét. 
            valos += masik.valos;
            // A saját imaginárius részünkhöz hozzáadjuk a másik szám imaginárius részét. 
            kepzetes += masik.kepzetes;
        }

        // Ez a metódus visszatér ennek a komplex számnak és az argumentumában kapott másik komplex 
        // számnak az összegével.
        public KomplexSzam Osszead(KomplexSzam masik)
        {
            // Kiszámítjuk az összeg valós és imaginárius részeit. 
            double v = valos + masik.valos;
            double k = kepzetes + masik.kepzetes;
            // Visszatérünk egy új komplex számmal, amely az összeggel egyezik meg. 
            return new KomplexSzam(v, k);
        }

        // Ez a statikus metódus visszatér a neki átadott két komplex szám összegével. A statikus 
        // metódus azt jelenti, hogy ez a metódus nem kötődik egy konktért objektumhoz (konkrét 
        // komplex számhoz), hanem az osztály összes objektuma közösen használja. Emiatt hívni sem 
        // az adott komplex objektum nevével kell, hanem magának az osztálynak a nevével (Komplex).
        public static KomplexSzam Osszeadas(KomplexSzam bal, KomplexSzam jobb)
        {
            // Kiszámítjuk az összeg valós és imaginárius részét. 
            double v = bal.valos + jobb.valos;
            double k = bal.kepzetes + jobb.kepzetes;
            // Visszatérünk egy új komplex számmal, amely az összeggel egyezik meg. 
            return new KomplexSzam(v, k);
        }

        // Ez a metódus az argumentumában kapott másik komplex számot kivonja a komplex számból.
        public void KivonBelole(KomplexSzam masik)
        {
            // A saját valós részünkből kivonjuk a másik szám valós részét. 
            valos -= masik.valos;
            // A saját imaginárius részünkből kivonjuk a másik szám imaginárius részét. 
            kepzetes -= masik.kepzetes;
        }

        // Ez a metódus visszatér ennek a komplex számnak, és az argumentumában kapott másik komplex  
        // számnak a különbségével. 
        public KomplexSzam Kivon(KomplexSzam masik)
        {
            // Kiszámítjuk a különbség valós és imaginárius részét. 
            double v = valos - masik.valos;
            double k = kepzetes - masik.kepzetes;
            // Visszatérünk egy új komplex számmal, amely a különbséggel egyezik meg. 
            return new KomplexSzam(v, k);
        }

        // Ez a statikus metódus visszatér a neki átadott két komplex szám különbségével.
        public static KomplexSzam Kivonas(KomplexSzam bal, KomplexSzam jobb)
        {
            // Kiszámítjuk a különbség valós és imaginárius részét. 
            double v = bal.valos - jobb.valos;
            double k = bal.kepzetes - jobb.kepzetes;
            // Visszatérünk egy új komplex számmal, amely a különbséggel egyezik meg. 
            return new KomplexSzam(v, k);
        }

        // Ez a metótus az argumentumában átvett másik komplex számot hozzászorozza ehhez a komplex
        // számhoz.
        public void SzorozVele(KomplexSzam masik)
        {
            // Számítjuk a szorzat valós és imaginárius részeit. 
            double v = valos * masik.valos - kepzetes * masik.kepzetes;
            double k = valos * masik.kepzetes + kepzetes * masik.valos;
            // A saját valós és imaginárius részünket egyenlővé tesszük az eredményekkel. 
            valos = v;
            kepzetes = k;
        }

        // Ez a metódus a neki átadott másik komplex számnak és ennek a komplex számnak a 
        // szorzatával tér vissza.
        public KomplexSzam Szoroz(KomplexSzam masik)
        {
            // Számítjuk a szorzat valós és imaginárius részeit. 
            double v = valos * masik.valos - kepzetes * masik.kepzetes;
            double k = valos * masik.kepzetes + kepzetes * masik.valos;
            // Visszatérünk egy új komplex számmal, amely a szorzattal egyezik meg. 
            return new KomplexSzam(v, k);
        }

        // Ez a metódus visszatér a neki paraméterként átadott két komplex szám szorzatával.
        public static KomplexSzam Szorzas(KomplexSzam bal, KomplexSzam jobb)
        {
            // Számítjuk a szorzat valós és imaginárius részeit. 
            double v = bal.valos * jobb.valos - bal.kepzetes * jobb.kepzetes;
            double k = bal.valos * jobb.kepzetes + bal.kepzetes * jobb.valos;
            // Visszatérünk egy új komplex számmal, amely a szorzattal egyezik meg. 
            return new KomplexSzam(v, k);
        }

        // Ez a metódus elosztja ezt a komplex számot egy paraméterként átadott valós számmal.
        public void OsztVele(double valosOszto)
        {
            // Leosztjuk a saját valós és imaginárius részünket is a valós számmal. 
            valos /= valosOszto;
            kepzetes /= valosOszto;
        }

        // Ez a metódus visszatér ennek a komplex számmnak és egy paraméterként átvett valós
        // számnak a hányadosával.
        public KomplexSzam Oszt(double valosOszto)
        {
            // Számítjuk a hányados valós és imaginárius részeit. 
            double v = valos / valosOszto;
            double k = kepzetes / valosOszto;
            // Visszatérünk egy új komplex számmal, amely a hányadossal egyezik meg. 
            return new KomplexSzam(v, k);
        }

        // Ez a statikus metódus elosztja az argumentumában átvett komplex számot a szintén 
        // paraméterként átvett valós számmal.
        public static KomplexSzam Osztas(KomplexSzam komplex, double valosOszto)
        {
            // Számítjuk a hányados valós és imaginárius részeit. 
            double v = komplex.valos / valosOszto;
            double k = komplex.kepzetes / valosOszto;
            // Visszatérünk egy új komplex számmal, amely a hányadossal egyezik meg. 
            return new KomplexSzam(v, k);
        }

        // Ez a metódus elosztja a komplex számot egy a paraméterében átvett másik komplex számmal.
        public void OsztVele(KomplexSzam masik)
        {
            // Megszorozzuk magunkat a másik szám komplex konjugáltjával. 
            SzorozVele(masik.Konjugalt);
            // Kiszámítjuk a másik komplex szám abszolút értékének négyzetét, amely valós. 
            double nevezo = masik.valos * masik.valos + masik.kepzetes * masik.kepzetes;
            // Elosztjuk magunkat a valós abszolút érték négyzettel, hogy megkapjuk a végeredményt. 
            OsztVele(nevezo);
        }

        // Ez a metódus visszatér ennek a komplex számnak és egy a paraméterében átvett másik 
        // komplex számnak a  hányadosával.
        public KomplexSzam Oszt(KomplexSzam masik)
        {
            // Számítjuk a másik szám komplex konjugáltjával való szorzatot, ebból megvan a nevező. 
            KomplexSzam szamlalo = Szoroz(masik.Konjugalt);
            // A nevezőt elosztjuk a másik komplex szám abszolút értékének négyzetével, ami valós
            // szám. 
            double nevezo = masik.valos * masik.valos + masik.kepzetes * masik.kepzetes;
            // Számítjuk a komplex számláló és a valós nevező hányadosát. 
            return szamlalo.Oszt(nevezo);
        }

        // Ez a statikus metódus visszatér a paraméterként átvett két komplex szám hányadosával.
        public static KomplexSzam Osztas(KomplexSzam bal, KomplexSzam jobb)
        {
            // Összeszorozzuk az első számot a második komplex konjugáltjával. Ebből megvan a nevező. 
            KomplexSzam szamlalo = Szorzas(bal, jobb.Konjugalt);
            // Kiszámítjuk a második szám abszolútérték négyzetét. Ebből megvan a valós értékű 
            // számláló. 
            double nevezo = jobb.valos * jobb.valos + jobb.kepzetes * jobb.kepzetes;
            // A komplex nevezőt elosztjuk a valós számlálóval a végeredményhez 
            return Osztas(szamlalo, nevezo);
        }

        // Ez a metótus visszatér a komplex szám szöveges formájával
        public string Szovegge(bool algebrai)
        {
            string szoveg = string.Empty;
            if(algebrai)
            {
                szoveg = string.Format("{0:#0.000}.(cos({1:#0.000})+j.sin({1:#0.000}))",
                                       AbszolutErtek,
                                       Szog);
            }
            else
            {
                szoveg = string.Format("{0:+#0.000;-#0.000; #0.000}{1:+#0.000;-#0.000;+#0.000}j",
                                       valos,
                                       kepzetes);
            }
            return szoveg;
        }
    }
}
