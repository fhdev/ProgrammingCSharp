﻿using System;
using System.Windows.Forms;

namespace Complex
{
    public partial class FoAblak : Form
    {
        // Ebben a komplex változóban tároljuk az első operandust. 
        private KomplexSzam szam1;
        // Ebben a komplex változóban tároljuk a második operandust. 
        private KomplexSzam szam2;
        // Ebben a változóban tároljuk az eredményt. 
        private KomplexSzam eredmeny;

        public FoAblak()
        {
            InitializeComponent();
            // Létrehozzuk a három komplex számot, kezdeti értékük 0 + 0j lesz. 
            // Létrehozás a két paraméteres konstruktorral: megadjuk a kívánt valós és imaginárius 
            // részt. 
            szam1 = new KomplexSzam(0.0, 0.0);
            // Létrehozás egy másik komplex szám másolataként a copy konstruktorral: megadjuk hogy 
            // melyik másik komplex
            // számmal legyen egyenlő. 
            szam2 = new KomplexSzam(szam1);
            // Létrehozás egy másik komplex szám másolataként a Masol metódussal. Azon a másik
            // komplex számon hívjuk meg a Masol metódust, amivel a számunkat egyenlővé akarjuk 
            // tenni. 
            eredmeny = szam2.Masol();
        }

        // Ez a metódus akkor fog lefuni, amikor az ablakunk betöltődik.
        private void FoAblak_Load(object sender, EventArgs e)
        {
            // Minden TextBox tartalmát alapértelmezetten 0 értékre állítjuk be. 
            textBoxSzam1V.Text = "0";
            textBoxSzam1K.Text = "0";
            textBoxSzam2V.Text = "0";
            textBoxSzam2K.Text = "0";
        }

        // Ez a metódus akkor fog lefutni, ha az első komplex szám valós részét tartalmazó TextBox 
        // objektumban lévő szöveg megváltozik. 
        private void textBoxSzam1V_TextChanged(object sender, EventArgs e)
        {
            double szam1v;
            double.TryParse(textBoxSzam1V.Text, out szam1v);
            szam1.Valos = szam1v;
        }
        // Ez a metódus akkor fog lefutni, ha az első komplex szám imaginárius részét tartalmazó 
        // TextBox objektumban  lévő szöveg megváltozik.
        private void textBoxSzam1K_TextChanged(object sender, EventArgs e)
        {
            double szam1k;
            double.TryParse(textBoxSzam1K.Text, out szam1k);
            szam1.Kepzetes = szam1k;
        }

        // Ez a metódus akkor fog lefutni, ha a második komplex szám valós részét tartalmazó 
        // TextBox objektumban lévő szöveg megváltozik.
        private void textBoxSzam2V_TextChanged(object sender, EventArgs e)
        {
            double szam2v;
            double.TryParse(textBoxSzam2V.Text, out szam2v);
            szam2.Valos = szam2v;
        }

        // Ez a metódus akkor fog lefutni, ha a második komplex szám imaginárius részét tartalmazó 
        // TextBox objektumban lévő szöveg megváltozik.
        private void textBoxSzam2K_TextChanged(object sender, EventArgs e)
        {
            double szam2k;
            double.TryParse(textBoxSzam2K.Text, out szam2k);
            szam2.Kepzetes = szam2k;
        }

        // Ez a metódus az összeadás gombra való klikkeléskor fut le. 
        private void buttonOsszead_Click(object sender, EventArgs e)
        {
            // Az első komplex számhoz hozzáadjuk a második komplex számot, és az eredménybe töltjük 
            // az értéket. A művelethez az első komplex szám objektumán hívjuk meg az Add() 
            // példánymetódust (instance method). Ez a metódus annak a számnak az értékéhez adja az 
            // argumentumában szereplő számot, amelyik objektumon meghívtuk.  
            eredmeny = szam1.Osszead(szam2);
            // Frissítjük az eredmény kijelzésre használt TextBox-ot. 
            EredmenyFrissit();
        }

        // Ez a metódus a kivonás gombra való klikkeléskor fut le. 
        private void buttonKivon_Click(object sender, EventArgs e)
        {
            // Az első komplex számból kivonjuk a második komplex számot, és az eredménybe töltjük  
            // az értéket. A művelethez a komplex szám osztály statikus Subtract() metódusát 
            // használjuk (static method), ami  az első argumentumaként megadott számból kivonja a 
            // második argumentumaként megadott számot, és visszatér az eredményt tartalmazó új 
            // komplex számmal. 
            eredmeny = KomplexSzam.Kivonas(szam1, szam2);
            // Frissítjük az eredmény kijelzésre használt TextBox-ot. 
            EredmenyFrissit();
        }

        // Ez a metódus a szorzás gombra való klikkeléskor le. 
        private void buttonSzoroz_Click(object sender, EventArgs e)
        {
            // Az első komplex számmal összeszorozzuk a második komplex számot 
            eredmeny = szam1.Szoroz(szam2);
            // Frissítjük az eredmény kijelzésre használt TextBox-ot. 
            EredmenyFrissit();
        }
        
        // Ez a metódus az osztás gombra való klikkeléskor fut le. 
        private void buttonOszt_Click(object sender, EventArgs e)
        {
            // Az első komplex számot elosztjuk a második komplex számmal. 
            eredmeny = KomplexSzam.Osztas(szam1, szam2);
            // Frissítjük az eredmény kijelzésre használt TextBox-ot. 
            EredmenyFrissit();
        }

        // Ez a metódus az első számot a másodikba másoló gombra való klikkeléskor fut le. 
        private void buttonMasol12_Click(object sender, EventArgs e)
        {
            // A második számot az első másolataként létrehozott új komplex számmal tesszük 
            // egyenlővé, ehhez a Complex osztály szintén Complex paramétert átvevő konstruktorát
            // használjuk (copy constructor). 
            szam2 = new KomplexSzam(szam1);
            // Frissítjük a második szám valós és imaginárius részeihez tartozó TextBox-okat. 
            Szam2Frissit();
        }

        // Ez a metódus a második számot az elsőbe másoló gombra való klikkeléskor fut le. 
        private void buttonMasol21_Click(object sender, EventArgs e)
        {
            // Az első számot a második másolataként létrehozott új komplex számmal tesszük 
            // egyenlővé, ehhez a Complex objektumok Masol példánymetódusát használjuk, ami  
            // visszatér annak a számnak a másolatával, amin a metótust meghívtuk. Ez jelen esetben
            // a szam2 objektum, azaz a második komplex szám. 
            szam1 = szam2.Masol();
            // Frissítjük az első szám valós és imaginárius részeihez tartozó TextBox-okat. 
            Szam1Frissit();
        }

        // Ez a metódus az eredményt trigonometrikus alakba konvertáló gomra való klikkeléskor fut le.
        private void buttonEredmenyTrigo_Click(object sender, EventArgs e)
        {
            textBoxEredmeny.Text = eredmeny.Szovegge(true);
        }

        // Ez a metódus a második számot az elsőhöz hozzáadó gombra való klikkeléskor fut le.
        private void buttonOsszeadVele12_Click(object sender, EventArgs e)
        {
            // Az első számmot megnöveljük a második számmal. Ehhez a Complex objektumok AddTo nevű  
            // példánymetódusát használjuk. Ez a metódus ahhoz a komplex számhoz adja az 
            // argumentumában megadott másik komplex számot,  amin a metótust meghívtuk. Ez jelen 
            // eseten a cplxNum1 objektum, azaz az első komplex szám. 
            szam1.OsszeadVele(szam2);
            // Frissítjük az első szám valós és imaginárius részeihez tartozó TextBox-okat. 
            Szam1Frissit();
        }

        // Ez a metódus a második számot az elsőből kivonó gombra való klikkeléskor fut le.
        private void buttonKivonBelole12_Click(object sender, EventArgs e)
        {
            // Az első számból kivonjuk a második számot. 
            szam1.KivonBelole(szam2);
            // Frissítjük az első szám valós és imaginárius részeihez tartozó TextBox-okat. 
            Szam1Frissit();
        }

        // Ez a metódus a második számot az elsőhöz hozzászorzó gombra való klikkeléskor fut le.
        private void buttonSzorozVele12_Click(object sender, EventArgs e)
        {
            // Az első számhoz hozzászorozzuk a második számot. 
            szam1.SzorozVele(szam2);
            // Frissítjük az első szám valós és imaginárius részeihez tartozó TextBox-okat. 
            Szam1Frissit();
        }

        // Ez a metódus a második számmal az elsőt elosztó gombra való klikkeléskor fut le.
        private void buttonOsztVele12_Click(object sender, EventArgs e)
        {
            // Az első számot elosztjuk a második számmal. 
            szam1.OsztVele(szam2);
            // Frissítjük az első szám valós és imaginárius részeihez tartozó TextBox-okat. 
            Szam1Frissit();
        }

        // Ez a metódus beállítja az első komplex számhoz tartozó TextBox-ok szövegét a szám 
        // értékének megfelelően.
        private void Szam1Frissit()
        {
            // Az első szám valós részét betöltjük a neki megfelelő TextBox-ba. 
            textBoxSzam1V.Text = szam1.Valos.ToString();
            // Az első szám imaginárius részét betöltjük a neki megfelelő TextBox-ba. 
            textBoxSzam1K.Text = szam1.Kepzetes.ToString();
        }

        // Ez a metódus beállítja a második komplex számhoz tartozó TextBox-ok szövegét a szám
        // értékének megfelelően.
        private void Szam2Frissit()
        {
            // A második szám valós részét betöltjük a neki megfelelő TextBox-ba. 
            textBoxSzam2V.Text = szam2.Valos.ToString();
            // A második szám imaginárius részét betöltjük a neki megfelelő TextBox-ba. 
            textBoxSzam2K.Text = szam2.Kepzetes.ToString();
        }

        // Ez a metódus beállítja az eredményhez tartozó TextBox szövegét az eredményt tartalmazó 
        // komplex szám 
        private void EredmenyFrissit()
        {
            // A Complex objektumok WriteToString metódusa viszatér egy string-gel, amiben annak a 
            // számnak az algebrai alakja szerepel, amin a metódust meghívtuk. Ez jelen esetben az
            // eredmeny objektum, azaz az eredményeket tároló komplex szám. 
            textBoxEredmeny.Text = eredmeny.Szovegge(false);
        }


    }
}
