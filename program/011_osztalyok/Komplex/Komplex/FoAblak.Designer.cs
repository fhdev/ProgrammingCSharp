﻿namespace Complex
{
    partial class FoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOsszead = new System.Windows.Forms.Button();
            this.textBoxSzam1V = new System.Windows.Forms.TextBox();
            this.textBoxSzam2V = new System.Windows.Forms.TextBox();
            this.textBoxEredmeny = new System.Windows.Forms.TextBox();
            this.buttonKivon = new System.Windows.Forms.Button();
            this.labelSzam1 = new System.Windows.Forms.Label();
            this.labelNum2 = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.textBoxSzam1K = new System.Windows.Forms.TextBox();
            this.textBoxSzam2K = new System.Windows.Forms.TextBox();
            this.labelK1 = new System.Windows.Forms.Label();
            this.labelK2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonMasol12 = new System.Windows.Forms.Button();
            this.buttonSzoroz = new System.Windows.Forms.Button();
            this.buttonOszt = new System.Windows.Forms.Button();
            this.buttonMasol21 = new System.Windows.Forms.Button();
            this.buttonEredmenyTrigo = new System.Windows.Forms.Button();
            this.buttonOsszeadVele12 = new System.Windows.Forms.Button();
            this.buttonKivonBelole12 = new System.Windows.Forms.Button();
            this.buttonSzorozVele12 = new System.Windows.Forms.Button();
            this.buttonOsztVele12 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonOsszead
            // 
            this.buttonOsszead.Location = new System.Drawing.Point(15, 159);
            this.buttonOsszead.Name = "buttonOsszead";
            this.buttonOsszead.Size = new System.Drawing.Size(35, 22);
            this.buttonOsszead.TabIndex = 5;
            this.buttonOsszead.Text = "+";
            this.buttonOsszead.UseVisualStyleBackColor = true;
            this.buttonOsszead.Click += new System.EventHandler(this.buttonOsszead_Click);
            // 
            // textBoxSzam1V
            // 
            this.textBoxSzam1V.Location = new System.Drawing.Point(12, 25);
            this.textBoxSzam1V.Name = "textBoxSzam1V";
            this.textBoxSzam1V.Size = new System.Drawing.Size(75, 20);
            this.textBoxSzam1V.TabIndex = 1;
            this.textBoxSzam1V.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxSzam1V.TextChanged += new System.EventHandler(this.textBoxSzam1V_TextChanged);
            // 
            // textBoxSzam2V
            // 
            this.textBoxSzam2V.Location = new System.Drawing.Point(12, 74);
            this.textBoxSzam2V.Name = "textBoxSzam2V";
            this.textBoxSzam2V.Size = new System.Drawing.Size(75, 20);
            this.textBoxSzam2V.TabIndex = 3;
            this.textBoxSzam2V.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxSzam2V.TextChanged += new System.EventHandler(this.textBoxSzam2V_TextChanged);
            // 
            // textBoxEredmeny
            // 
            this.textBoxEredmeny.Location = new System.Drawing.Point(13, 123);
            this.textBoxEredmeny.Name = "textBoxEredmeny";
            this.textBoxEredmeny.Size = new System.Drawing.Size(176, 20);
            this.textBoxEredmeny.TabIndex = 16;
            this.textBoxEredmeny.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonKivon
            // 
            this.buttonKivon.Location = new System.Drawing.Point(61, 159);
            this.buttonKivon.Name = "buttonKivon";
            this.buttonKivon.Size = new System.Drawing.Size(35, 22);
            this.buttonKivon.TabIndex = 6;
            this.buttonKivon.Text = "-";
            this.buttonKivon.UseVisualStyleBackColor = true;
            this.buttonKivon.Click += new System.EventHandler(this.buttonKivon_Click);
            // 
            // labelSzam1
            // 
            this.labelSzam1.AutoSize = true;
            this.labelSzam1.Location = new System.Drawing.Point(11, 9);
            this.labelSzam1.Name = "labelSzam1";
            this.labelSzam1.Size = new System.Drawing.Size(65, 13);
            this.labelSzam1.TabIndex = 5;
            this.labelSzam1.Text = "Szám 1 (bal)";
            // 
            // labelNum2
            // 
            this.labelNum2.AutoSize = true;
            this.labelNum2.Location = new System.Drawing.Point(11, 58);
            this.labelNum2.Name = "labelNum2";
            this.labelNum2.Size = new System.Drawing.Size(71, 13);
            this.labelNum2.TabIndex = 6;
            this.labelNum2.Text = "Szám 2 (jobb)";
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(10, 107);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(54, 13);
            this.labelResult.TabIndex = 6;
            this.labelResult.Text = "Eredmeny";
            // 
            // textBoxSzam1K
            // 
            this.textBoxSzam1K.Location = new System.Drawing.Point(113, 25);
            this.textBoxSzam1K.Name = "textBoxSzam1K";
            this.textBoxSzam1K.Size = new System.Drawing.Size(75, 20);
            this.textBoxSzam1K.TabIndex = 2;
            this.textBoxSzam1K.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxSzam1K.TextChanged += new System.EventHandler(this.textBoxSzam1K_TextChanged);
            // 
            // textBoxSzam2K
            // 
            this.textBoxSzam2K.Location = new System.Drawing.Point(113, 74);
            this.textBoxSzam2K.Name = "textBoxSzam2K";
            this.textBoxSzam2K.Size = new System.Drawing.Size(75, 20);
            this.textBoxSzam2K.TabIndex = 4;
            this.textBoxSzam2K.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxSzam2K.TextChanged += new System.EventHandler(this.textBoxSzam2K_TextChanged);
            // 
            // labelK1
            // 
            this.labelK1.AutoSize = true;
            this.labelK1.Location = new System.Drawing.Point(194, 28);
            this.labelK1.Name = "labelK1";
            this.labelK1.Size = new System.Drawing.Size(9, 13);
            this.labelK1.TabIndex = 8;
            this.labelK1.Text = "j";
            // 
            // labelK2
            // 
            this.labelK2.AutoSize = true;
            this.labelK2.Location = new System.Drawing.Point(194, 77);
            this.labelK2.Name = "labelK2";
            this.labelK2.Size = new System.Drawing.Size(9, 13);
            this.labelK2.TabIndex = 8;
            this.labelK2.Text = "j";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(93, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "+";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(94, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "+";
            // 
            // buttonMasol12
            // 
            this.buttonMasol12.Location = new System.Drawing.Point(14, 187);
            this.buttonMasol12.Name = "buttonMasol12";
            this.buttonMasol12.Size = new System.Drawing.Size(82, 22);
            this.buttonMasol12.TabIndex = 9;
            this.buttonMasol12.Text = "Másol 1 -> 2";
            this.buttonMasol12.UseVisualStyleBackColor = true;
            this.buttonMasol12.Click += new System.EventHandler(this.buttonMasol12_Click);
            // 
            // buttonSzoroz
            // 
            this.buttonSzoroz.Location = new System.Drawing.Point(109, 159);
            this.buttonSzoroz.Name = "buttonSzoroz";
            this.buttonSzoroz.Size = new System.Drawing.Size(35, 22);
            this.buttonSzoroz.TabIndex = 7;
            this.buttonSzoroz.Text = "*";
            this.buttonSzoroz.UseVisualStyleBackColor = true;
            this.buttonSzoroz.Click += new System.EventHandler(this.buttonSzoroz_Click);
            // 
            // buttonOszt
            // 
            this.buttonOszt.Location = new System.Drawing.Point(154, 159);
            this.buttonOszt.Name = "buttonOszt";
            this.buttonOszt.Size = new System.Drawing.Size(35, 22);
            this.buttonOszt.TabIndex = 8;
            this.buttonOszt.Text = "/";
            this.buttonOszt.UseVisualStyleBackColor = true;
            this.buttonOszt.Click += new System.EventHandler(this.buttonOszt_Click);
            // 
            // buttonMasol21
            // 
            this.buttonMasol21.Location = new System.Drawing.Point(108, 187);
            this.buttonMasol21.Name = "buttonMasol21";
            this.buttonMasol21.Size = new System.Drawing.Size(81, 22);
            this.buttonMasol21.TabIndex = 10;
            this.buttonMasol21.Text = "Másol 2 -> 1";
            this.buttonMasol21.UseVisualStyleBackColor = true;
            this.buttonMasol21.Click += new System.EventHandler(this.buttonMasol21_Click);
            // 
            // buttonEredmenyTrigo
            // 
            this.buttonEredmenyTrigo.Location = new System.Drawing.Point(13, 215);
            this.buttonEredmenyTrigo.Name = "buttonEredmenyTrigo";
            this.buttonEredmenyTrigo.Size = new System.Drawing.Size(176, 22);
            this.buttonEredmenyTrigo.TabIndex = 11;
            this.buttonEredmenyTrigo.Text = "Eredmény trigonometrikus alak";
            this.buttonEredmenyTrigo.UseVisualStyleBackColor = true;
            this.buttonEredmenyTrigo.Click += new System.EventHandler(this.buttonEredmenyTrigo_Click);
            // 
            // buttonOsszeadVele12
            // 
            this.buttonOsszeadVele12.Location = new System.Drawing.Point(12, 242);
            this.buttonOsszeadVele12.Name = "buttonOsszeadVele12";
            this.buttonOsszeadVele12.Size = new System.Drawing.Size(177, 22);
            this.buttonOsszeadVele12.TabIndex = 12;
            this.buttonOsszeadVele12.Text = "Szám 1 += Szám 2";
            this.buttonOsszeadVele12.UseVisualStyleBackColor = true;
            this.buttonOsszeadVele12.Click += new System.EventHandler(this.buttonOsszeadVele12_Click);
            // 
            // buttonKivonBelole12
            // 
            this.buttonKivonBelole12.Location = new System.Drawing.Point(11, 268);
            this.buttonKivonBelole12.Name = "buttonKivonBelole12";
            this.buttonKivonBelole12.Size = new System.Drawing.Size(177, 22);
            this.buttonKivonBelole12.TabIndex = 13;
            this.buttonKivonBelole12.Text = "Szám 1 -= Szám 2";
            this.buttonKivonBelole12.UseVisualStyleBackColor = true;
            this.buttonKivonBelole12.Click += new System.EventHandler(this.buttonKivonBelole12_Click);
            // 
            // buttonSzorozVele12
            // 
            this.buttonSzorozVele12.Location = new System.Drawing.Point(11, 296);
            this.buttonSzorozVele12.Name = "buttonSzorozVele12";
            this.buttonSzorozVele12.Size = new System.Drawing.Size(177, 22);
            this.buttonSzorozVele12.TabIndex = 14;
            this.buttonSzorozVele12.Text = "Szám 1 *= Szám 2";
            this.buttonSzorozVele12.UseVisualStyleBackColor = true;
            this.buttonSzorozVele12.Click += new System.EventHandler(this.buttonSzorozVele12_Click);
            // 
            // buttonOsztVele12
            // 
            this.buttonOsztVele12.Location = new System.Drawing.Point(11, 324);
            this.buttonOsztVele12.Name = "buttonOsztVele12";
            this.buttonOsztVele12.Size = new System.Drawing.Size(177, 22);
            this.buttonOsztVele12.TabIndex = 15;
            this.buttonOsztVele12.Text = "Szám 1 /= Szám 2";
            this.buttonOsztVele12.UseVisualStyleBackColor = true;
            this.buttonOsztVele12.Click += new System.EventHandler(this.buttonOsztVele12_Click);
            // 
            // FoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(204, 367);
            this.Controls.Add(this.buttonOsztVele12);
            this.Controls.Add(this.buttonSzorozVele12);
            this.Controls.Add(this.buttonKivonBelole12);
            this.Controls.Add(this.buttonOsszeadVele12);
            this.Controls.Add(this.buttonEredmenyTrigo);
            this.Controls.Add(this.buttonMasol21);
            this.Controls.Add(this.buttonMasol12);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelK2);
            this.Controls.Add(this.labelK1);
            this.Controls.Add(this.textBoxSzam2K);
            this.Controls.Add(this.textBoxSzam1K);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.labelNum2);
            this.Controls.Add(this.labelSzam1);
            this.Controls.Add(this.buttonOszt);
            this.Controls.Add(this.buttonKivon);
            this.Controls.Add(this.textBoxEredmeny);
            this.Controls.Add(this.textBoxSzam2V);
            this.Controls.Add(this.textBoxSzam1V);
            this.Controls.Add(this.buttonSzoroz);
            this.Controls.Add(this.buttonOsszead);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FoAblak";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CplxCalc";
            this.Load += new System.EventHandler(this.FoAblak_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOsszead;
        private System.Windows.Forms.TextBox textBoxSzam1V;
        private System.Windows.Forms.TextBox textBoxSzam2V;
        private System.Windows.Forms.TextBox textBoxEredmeny;
        private System.Windows.Forms.Button buttonKivon;
        private System.Windows.Forms.Label labelSzam1;
        private System.Windows.Forms.Label labelNum2;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.TextBox textBoxSzam1K;
        private System.Windows.Forms.TextBox textBoxSzam2K;
        private System.Windows.Forms.Label labelK1;
        private System.Windows.Forms.Label labelK2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonMasol12;
        private System.Windows.Forms.Button buttonSzoroz;
        private System.Windows.Forms.Button buttonOszt;
        private System.Windows.Forms.Button buttonMasol21;
        private System.Windows.Forms.Button buttonEredmenyTrigo;
        private System.Windows.Forms.Button buttonOsszeadVele12;
        private System.Windows.Forms.Button buttonKivonBelole12;
        private System.Windows.Forms.Button buttonSzorozVele12;
        private System.Windows.Forms.Button buttonOsztVele12;
    }
}

