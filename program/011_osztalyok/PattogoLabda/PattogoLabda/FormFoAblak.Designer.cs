﻿namespace PattogoLabda
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerMozgat = new System.Windows.Forms.Timer(this.components);
            this.menuStripFoMenu = new System.Windows.Forms.MenuStrip();
            this.játékToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ujJatekToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.szunetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folytatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kilepToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelRajz = new System.Windows.Forms.Panel();
            this.buttonSzunet = new System.Windows.Forms.Button();
            this.buttonFolytat = new System.Windows.Forms.Button();
            this.buttonUjJatek = new System.Windows.Forms.Button();
            this.buttonLepes = new System.Windows.Forms.Button();
            this.checkBoxGravitacio = new System.Windows.Forms.CheckBox();
            this.labelIdo = new System.Windows.Forms.Label();
            this.menuStripFoMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerMozgat
            // 
            this.timerMozgat.Interval = 17;
            this.timerMozgat.Tick += new System.EventHandler(this.timerMozgat_Tick);
            // 
            // menuStripFoMenu
            // 
            this.menuStripFoMenu.BackColor = System.Drawing.SystemColors.ControlDark;
            this.menuStripFoMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.játékToolStripMenuItem});
            this.menuStripFoMenu.Location = new System.Drawing.Point(0, 0);
            this.menuStripFoMenu.Name = "menuStripFoMenu";
            this.menuStripFoMenu.Size = new System.Drawing.Size(610, 24);
            this.menuStripFoMenu.TabIndex = 0;
            this.menuStripFoMenu.Text = "menuStrip1";
            // 
            // játékToolStripMenuItem
            // 
            this.játékToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ujJatekToolStripMenuItem,
            this.szunetToolStripMenuItem,
            this.folytatToolStripMenuItem,
            this.kilepToolStripMenuItem});
            this.játékToolStripMenuItem.Name = "játékToolStripMenuItem";
            this.játékToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.játékToolStripMenuItem.Text = "Játék";
            // 
            // ujJatekToolStripMenuItem
            // 
            this.ujJatekToolStripMenuItem.Name = "ujJatekToolStripMenuItem";
            this.ujJatekToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ujJatekToolStripMenuItem.Text = "Új játék";
            this.ujJatekToolStripMenuItem.Click += new System.EventHandler(this.ujJatekToolStripMenuItem_Click);
            // 
            // szunetToolStripMenuItem
            // 
            this.szunetToolStripMenuItem.Name = "szunetToolStripMenuItem";
            this.szunetToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.szunetToolStripMenuItem.Text = "Szünet";
            this.szunetToolStripMenuItem.Click += new System.EventHandler(this.szunetToolStripMenuItem_Click);
            // 
            // folytatToolStripMenuItem
            // 
            this.folytatToolStripMenuItem.Name = "folytatToolStripMenuItem";
            this.folytatToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.folytatToolStripMenuItem.Text = "Folytatás";
            this.folytatToolStripMenuItem.Click += new System.EventHandler(this.folytatasToolStripMenuItem_Click);
            // 
            // kilepToolStripMenuItem
            // 
            this.kilepToolStripMenuItem.Name = "kilepToolStripMenuItem";
            this.kilepToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.kilepToolStripMenuItem.Text = "Kilépés";
            this.kilepToolStripMenuItem.Click += new System.EventHandler(this.kilepesToolStripMenuItem_Click);
            // 
            // panelRajz
            // 
            this.panelRajz.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelRajz.BackColor = System.Drawing.Color.White;
            this.panelRajz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelRajz.Location = new System.Drawing.Point(5, 60);
            this.panelRajz.Name = "panelRajz";
            this.panelRajz.Size = new System.Drawing.Size(600, 600);
            this.panelRajz.TabIndex = 2;
            this.panelRajz.Paint += new System.Windows.Forms.PaintEventHandler(this.panelRajz_Paint);
            this.panelRajz.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelRajz_MouseDown);
            // 
            // buttonSzunet
            // 
            this.buttonSzunet.Location = new System.Drawing.Point(66, 29);
            this.buttonSzunet.Name = "buttonSzunet";
            this.buttonSzunet.Size = new System.Drawing.Size(30, 25);
            this.buttonSzunet.TabIndex = 2;
            this.buttonSzunet.Text = "❚❚";
            this.buttonSzunet.UseVisualStyleBackColor = true;
            this.buttonSzunet.Click += new System.EventHandler(this.szunetToolStripMenuItem_Click);
            // 
            // buttonFolytat
            // 
            this.buttonFolytat.Location = new System.Drawing.Point(102, 29);
            this.buttonFolytat.Name = "buttonFolytat";
            this.buttonFolytat.Size = new System.Drawing.Size(30, 25);
            this.buttonFolytat.TabIndex = 3;
            this.buttonFolytat.Text = "▶";
            this.buttonFolytat.UseVisualStyleBackColor = true;
            this.buttonFolytat.Click += new System.EventHandler(this.folytatasToolStripMenuItem_Click);
            // 
            // buttonUjJatek
            // 
            this.buttonUjJatek.Location = new System.Drawing.Point(5, 29);
            this.buttonUjJatek.Name = "buttonUjJatek";
            this.buttonUjJatek.Size = new System.Drawing.Size(30, 25);
            this.buttonUjJatek.TabIndex = 1;
            this.buttonUjJatek.Text = "🔃";
            this.buttonUjJatek.UseVisualStyleBackColor = true;
            this.buttonUjJatek.Click += new System.EventHandler(this.ujJatekToolStripMenuItem_Click);
            // 
            // buttonLepes
            // 
            this.buttonLepes.Location = new System.Drawing.Point(138, 29);
            this.buttonLepes.Name = "buttonLepes";
            this.buttonLepes.Size = new System.Drawing.Size(30, 25);
            this.buttonLepes.TabIndex = 4;
            this.buttonLepes.Text = "❚▶";
            this.buttonLepes.UseVisualStyleBackColor = true;
            this.buttonLepes.Click += new System.EventHandler(this.buttonLepes_Click);
            // 
            // checkBoxGravitacio
            // 
            this.checkBoxGravitacio.AutoSize = true;
            this.checkBoxGravitacio.Location = new System.Drawing.Point(253, 34);
            this.checkBoxGravitacio.Name = "checkBoxGravitacio";
            this.checkBoxGravitacio.Size = new System.Drawing.Size(74, 17);
            this.checkBoxGravitacio.TabIndex = 5;
            this.checkBoxGravitacio.Text = "Gravitáció";
            this.checkBoxGravitacio.UseVisualStyleBackColor = true;
            // 
            // labelIdo
            // 
            this.labelIdo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelIdo.AutoSize = true;
            this.labelIdo.Location = new System.Drawing.Point(563, 35);
            this.labelIdo.Name = "labelIdo";
            this.labelIdo.Size = new System.Drawing.Size(0, 13);
            this.labelIdo.TabIndex = 0;
            this.labelIdo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(610, 664);
            this.Controls.Add(this.labelIdo);
            this.Controls.Add(this.checkBoxGravitacio);
            this.Controls.Add(this.buttonLepes);
            this.Controls.Add(this.buttonUjJatek);
            this.Controls.Add(this.buttonFolytat);
            this.Controls.Add(this.buttonSzunet);
            this.Controls.Add(this.panelRajz);
            this.Controls.Add(this.menuStripFoMenu);
            this.MainMenuStrip = this.menuStripFoMenu;
            this.Name = "FormFoAblak";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pattogó labdák";
            this.menuStripFoMenu.ResumeLayout(false);
            this.menuStripFoMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerMozgat;
        private System.Windows.Forms.MenuStrip menuStripFoMenu;
        private System.Windows.Forms.ToolStripMenuItem játékToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ujJatekToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem szunetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folytatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kilepToolStripMenuItem;
        private System.Windows.Forms.Panel panelRajz;
        private System.Windows.Forms.Button buttonSzunet;
        private System.Windows.Forms.Button buttonFolytat;
        private System.Windows.Forms.Button buttonUjJatek;
        private System.Windows.Forms.Button buttonLepes;
        private System.Windows.Forms.CheckBox checkBoxGravitacio;
        private System.Windows.Forms.Label labelIdo;
    }
}

