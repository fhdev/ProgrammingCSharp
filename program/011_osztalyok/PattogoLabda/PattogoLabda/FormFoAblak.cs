﻿using System;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;

namespace PattogoLabda
{
    public partial class FormFoAblak : Form
    {
        private const int labdakSzama = 10;
        private const double meter2Pixel = 50;

        private double elteltIdo;
        private LabdaCsoport labdaCsoport;
        private Stopwatch stopper;

        public FormFoAblak()
        {
            InitializeComponent();
            // Vibrálás elkerülése. A DoubleBuffered tulajdonság alapvetően protected, ezért kell
            // így, kerülőúton elérni. 
            typeof(Panel).InvokeMember("DoubleBuffered",
                                       BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                                       null,
                                       panelRajz,
                                       new object[] { true });
            labdaCsoport = new LabdaCsoport();
            stopper = new Stopwatch();
        }
        private void panelRajz_Paint(object sender, PaintEventArgs e)
        {
            labdaCsoport.Rajzol(panelRajz, e.Graphics);
        }

        private void timerMozgat_Tick(object sender, EventArgs e)
        {
            labdaCsoport.Pattog(panelRajz, (stopper.ElapsedMilliseconds - elteltIdo) / 1000.0, checkBoxGravitacio.Checked);
            elteltIdo = stopper.ElapsedMilliseconds;
            panelRajz.Invalidate();
            labelIdo.Text = string.Format("{0:F3} s", elteltIdo / 1000.0);
        }

        private void ujJatekToolStripMenuItem_Click(object sender, EventArgs e)
        {
            labdaCsoport = new LabdaCsoport(labdakSzama, meter2Pixel, panelRajz);
            stopper.Restart();
            elteltIdo = 0.0;
            timerMozgat.Start();
        }

        public void szunetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timerMozgat.Stop();
            stopper.Stop();
        }

        public void folytatasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timerMozgat.Start();
            stopper.Start();
        }

        private void kilepesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonLepes_Click(object sender, EventArgs e)
        {
            if (!timerMozgat.Enabled)
            {
                labdaCsoport.Pattog(panelRajz, 10.0 / 1000.0, checkBoxGravitacio.Checked);
                panelRajz.Invalidate();
            }
        }

        private void panelRajz_MouseDown(object sender, MouseEventArgs e)
        {
            if (timerMozgat.Enabled)
            {
                labdaCsoport.RaKattint(meter2Pixel, panelRajz, new Vektor2D(e.X, e.Y));
                if (labdaCsoport.Szam == 0)
                {
                    timerMozgat.Stop();
                    stopper.Stop();
                    MessageBox.Show("Gratulálunk, Ön nyert!", "Győzelem", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
