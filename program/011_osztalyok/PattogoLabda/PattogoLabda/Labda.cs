﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PattogoLabda
{
    class Labda
    {
        private const double minAtmeroM = 0.3;
        private const double maxAtmeroM = 1.2;
        private const double maxSebessegMps = 2.0;

        private const double gravitacio = 9.8065; // [m/s^2]

        private double meter2Pixel;
        private double suruseg;
        private double atmeroM;     // [m]
        private Vektor2D pozicioM;  // [m]
        private Vektor2D sebessegMps; // [m/s]
        private Color szin;

        private static Random veletlenSzamGenerator = new Random();
        private static SolidBrush ecset = new SolidBrush(Color.Black);
        private static Pen toll = new Pen(Color.Black);

        private double atmeroP
        {
            get { return atmeroM * meter2Pixel; }
            set { atmeroM = value / meter2Pixel; }
        }

        private Vektor2D pozicioP
        {
            get { return pozicioM * meter2Pixel; }
            set { pozicioM = value / meter2Pixel; }
        }

        private Vektor2D sebessegPps
        {
            get { return sebessegMps * meter2Pixel; }
            set { sebessegMps = value / meter2Pixel; }
        }

        private double sugarM
        {
            get { return atmeroM / 2.0; }
            set { atmeroM = 2.0 * value; }
        }

        private double sugarP
        {
            get { return atmeroM / 2.0 * meter2Pixel; }
            set { atmeroM = 2.0 * value / meter2Pixel; }
        }

        private double tomeg
        {
            get { return 4.0 / 3.0 * Math.Pow(sugarM, 3.0) * Math.PI * suruseg; }
        }

        private static double veletlenSzam(double alsoHatar, double felsoHatar)
        {
            return veletlenSzamGenerator.NextDouble() * (felsoHatar - alsoHatar) + alsoHatar;
        }

        private static Color veletlenSzin()
        {
            int piros = veletlenSzamGenerator.Next(0, 256);
            int zold = veletlenSzamGenerator.Next(0, 256);
            int kek = veletlenSzamGenerator.Next(0, 256);
            return Color.FromArgb(piros, zold, kek);
        }

        public Labda(double meter2Pixel, Panel tabla)
        {
            this.meter2Pixel = meter2Pixel;
            atmeroM = veletlenSzam(minAtmeroM, maxAtmeroM);
            pozicioP = new Vektor2D(veletlenSzam(sugarP, tabla.ClientSize.Width - sugarP), 
                veletlenSzam(sugarP, tabla.ClientSize.Height - sugarP));
            sebessegMps = new Vektor2D(veletlenSzam(-maxSebessegMps, maxSebessegMps), 
                veletlenSzam(-maxSebessegMps, maxSebessegMps));
            szin = veletlenSzin();
            suruseg = 1;
        }

        public Labda(double meter2Pixel, double atmero, Vektor2D pozicio, Vektor2D sebesseg, Color szin)
        {
            this.meter2Pixel = meter2Pixel;
            this.atmeroM = atmero;
            this.pozicioM = pozicio;
            this.sebessegMps = sebesseg;
            this.szin = szin;
            suruseg = 1;
        }
 
        public Rectangle BefoglaloTeglalap(Panel tabla)
        {
            return new Rectangle(Convert.ToInt32((pozicioP.X - sugarP)),
                tabla.ClientSize.Height - Convert.ToInt32(pozicioP.Y + sugarP),
                Convert.ToInt32(atmeroP),
                Convert.ToInt32(atmeroP));
        }
        public void Rajzol(Panel tabla, Graphics eszkoz)
        {
            ecset.Color = szin;
            Rectangle befoglalo = BefoglaloTeglalap(tabla);
            eszkoz.FillEllipse(ecset, befoglalo);
            eszkoz.DrawEllipse(toll, befoglalo);
        }

        public void Mozog(double ido, bool grav)
        {
            pozicioM += sebessegMps * ido;
            if (grav)
            {
                sebessegMps.Y -= gravitacio * ido;
            }
        }

        public void VisszapattanTablaSzelerol(Panel tabla)
        {
            // Ütközés akkor történik, ha a labda valamelyik oldalon kilóg. 
            // Az ütközésnek mefelelő új sebességeket viszont csak akkor akarjuk kiszámítani, ha
            // a labda még mindig az ablakból kifelé halad. Ezzel elkerülhető a labdák falhoz
            // ragadása.
            bool jobbraKint = ((pozicioP.X + sugarP) > tabla.ClientSize.Width) && (sebessegMps.X > 0.0);
            bool balraKint = ((pozicioP.X - sugarP) < 0.0) && (sebessegMps.X < 0.0);
            if (jobbraKint || balraKint) sebessegMps.X *= -1.0;
            bool fentKint = ((pozicioP.Y + sugarP) > tabla.ClientSize.Height) && (sebessegMps.Y > 0.0);
            bool lentKint = ((pozicioP.Y - sugarP) < 0.0) && (sebessegMps.Y < 0.0);
            if (lentKint || fentKint) sebessegMps.Y *= -1.0;
        }

        public bool UtkozikLabdaval(Labda l2)
        {
            // Az önmagával való ütközésnek nincs értelme
            if (this == l2) return false;
            // A képletek egységesítéséhez
            Labda l1 = this;
            // A két labdát összekötő egyenesre vetített sebességek meghatározása
            Vektor2D osszekoto = l2.pozicioM - l1.pozicioM;
            // Összeér-e a két labda
            return osszekoto.Hossz <= l1.sugarM + l2.sugarM;
        }

        public void VisszapattanLabdarol(Labda l2)
        {
            // Az önmagával való ütközésnek nincs értelme
            if (this == l2) return;
            // A képletek egységesítéséhez
            Labda l1 = this;
            // A két labdát összekötő egyenesre vetített sebességek meghatározása
            Vektor2D osszekoto = l2.pozicioM - l1.pozicioM;
            Vektor2D sebessegElotte1 = Vektor2D.Forgatva(l1.sebessegMps, osszekoto.Szog);
            Vektor2D sebessegElotte2 = Vektor2D.Forgatva(l2.sebessegMps, osszekoto.Szog);
            // Ütközik-e a két labda
            // Ütköznek
            if ((osszekoto.Hossz <= l1.sugarM + l2.sugarM))
            {
                Vektor2D tavolsag = Vektor2D.Forgatva(osszekoto, osszekoto.Szog);
                // Egymás felé haladnak
                if (tavolsag.X / (sebessegElotte1.X - sebessegElotte2.X) > 0.0)
                {
                    // Ütközés utáni sebességek az ütközés síkjában
                    Vektor2D sebessegUtana1 = new Vektor2D(sebessegElotte1);
                    Vektor2D sebessegUtana2 = new Vektor2D(sebessegElotte2);
                    sebessegUtana1.X = (2 * l2.tomeg * sebessegElotte2.X + (l1.tomeg - l2.tomeg) * sebessegElotte1.X) / (l1.tomeg + l2.tomeg);
                    sebessegUtana2.X = (2 * l1.tomeg * sebessegElotte1.X + (l2.tomeg - l1.tomeg) * sebessegElotte2.X) / (l1.tomeg + l2.tomeg);
                    // Ütközés utáni sebességek az eredeti koordinátarendszerben
                    l1.sebessegMps = Vektor2D.Forgatva(sebessegUtana1, -osszekoto.Szog);
                    l2.sebessegMps = Vektor2D.Forgatva(sebessegUtana2, -osszekoto.Szog);
                }
            }
        }

        public bool RaKattint(Panel tabla, Vektor2D egerPozicioP)
        {
            Vektor2D osszekoto = egerPozicioP - new Vektor2D(pozicioP.X, tabla.ClientSize.Height - pozicioP.Y);
            return osszekoto.Hossz <= sugarP;
        }

    }
}
