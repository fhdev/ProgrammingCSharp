﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace PattogoLabda
{
    class LabdaCsoport
    {
        private List<Labda> labdak;

        // A listában található labdák száma.
        public int Szam
        {
            get { return labdak.Count; }
        }

        public LabdaCsoport()
        {
            labdak = new List<Labda>();
        }

        public LabdaCsoport(int labdakSzama, double meter2Pixel, Panel tabla)
        {
            labdak = new List<Labda>();
            //labdaLista.Add(new Labda(50, new Vektor2D(100, 500), new Vektor2D(200, -200), Color.Red));
            //labdaLista.Add(new Labda(20, new Vektor2D(500, 100), new Vektor2D(-200, 200), Color.Blue));
            for (int iUj = 0; iUj < labdakSzama; iUj++)
            {
                LabdaHozzaad(meter2Pixel, tabla);
            }
        }

        public void LabdaHozzaad(double meter2Pixel, Panel tabla)
        {
            // Nem szabad olyan labdát hozzáadni a listához, amelyik ütközik valamelyik már
            // meglévő labdával.
            bool utkozik = false;
            Labda ujLabda = null;
            do
            {
                ujLabda = new Labda(meter2Pixel, tabla);
                // Minden már meglévő labda esetén ellenőrizzük az ütközést.
                for (int iMeglevo = 0; iMeglevo < labdak.Count; iMeglevo++)
                {
                    utkozik = ujLabda.UtkozikLabdaval(labdak[iMeglevo]);
                    // Ha bármelyik labdával ütközik a jelenleg létrehozott labda, akkor már
                    // nem vizsgáljuk meg a többi esetben, hiszen mindenképpen új kell.
                    if (utkozik) break;
                }
            } while (utkozik);
            labdak.Add(ujLabda);
        }

        public void Rajzol(Panel tabla, Graphics eszkoz)
        {
            // Minden egyes labdát lerajzolnunk.
            for (int iLabda = 0; iLabda < labdak.Count; iLabda++)
            {
                labdak[iLabda].Rajzol(tabla, eszkoz);
            }
        }

        public void Pattog(Panel tabla, double ido, bool grav)
        {
            for (int iLabda = 0; iLabda < labdak.Count; iLabda++)
            {
                // A labdákat az ablakon belül tartjuk
                labdak[iLabda].VisszapattanTablaSzelerol(tabla);
            }
            for (int iLabda = 0; iLabda < labdak.Count; iLabda++)
            {
                // Minden lehetséges ütközést megvizsgálunk.
                for (int jLabda = iLabda + 1; jLabda < labdak.Count; jLabda++)
                {
                    labdak[iLabda].VisszapattanLabdarol(labdak[jLabda]);
                }
            }
            for (int iLabda = 0; iLabda < labdak.Count; iLabda++)
            {
                // Minden egyes labdát mozgatunk
                labdak[iLabda].Mozog(ido, grav);
            }
        }

        public void RaKattint(double meter2Pixel, Panel tabla, Vektor2D egerPozicioP)
        {
            bool talalt = false;
            for(int iLabda = 0; iLabda < labdak.Count; iLabda++)
            {
                if (labdak[iLabda].RaKattint(tabla, egerPozicioP))
                {
                    labdak.RemoveAt(iLabda--);
                    talalt = true;
                }
            }
            if (!talalt)
            {
                LabdaHozzaad(meter2Pixel, tabla);
            }
        }
    }
}
