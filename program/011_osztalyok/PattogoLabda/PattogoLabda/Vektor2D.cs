﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PattogoLabda
{
    class Vektor2D
    {
        public double X { get; set;}
        public double Y { get; set; }

        public double Hossz
        {
            get { return Math.Sqrt(X * X + Y * Y); }
        }

        public double Szog
        {
            get { return Math.Atan(Y / X); }
        }

        public static Vektor2D operator +(Vektor2D v1, Vektor2D v2)
        {
            return new Vektor2D(v1.X + v2.X, v1.Y + v2.Y);
        }

        public static Vektor2D operator -(Vektor2D v1, Vektor2D v2)
        {
            return new Vektor2D(v1.X - v2.X, v1.Y - v2.Y);
        }

        public static Vektor2D operator *(Vektor2D v1, double k)
        {
            return new Vektor2D(v1.X * k, v1.Y * k);
        }
        public static Vektor2D operator /(Vektor2D v1, double k)
        {
            return new Vektor2D(v1.X / k, v1.Y / k);
        }

        public static double Tavolsag(Vektor2D v1, Vektor2D v2)
        {
            return (v1 - v2).Hossz;
        }

        public static Vektor2D Forgatva(Vektor2D v1, double szog)
        {
            return new Vektor2D(Math.Cos(szog) * v1.X + Math.Sin(szog) * v1.Y,
                -Math.Sin(szog) * v1.X + Math.Cos(szog) * v1.Y);
        }

        public Vektor2D(double x, double y)
        {
            X = x;
            Y = y;
        }

        public Vektor2D(Vektor2D v)
        {
            X = v.X;
            Y = v.Y;
        }

        public override string ToString()
        {
            return string.Format("[{0:G}, {1:G}]", X, Y);
        }

    }
}
