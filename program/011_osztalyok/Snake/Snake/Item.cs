﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Snake
{
    class Item : Wall
    {
        private const int colorOffset = 40;
        protected int value;

        public int Value
        {
            get { return value; }
        }

        public static Item Factory(Panel panel, Wall wall, Snake snake)
        {
            Item item;
            bool colliding;
            do
            {
                // Random location
                Point location = randomPointInside(panel, wall.SideLength, 0);
                item = new Item(wall.SideLength, location, 1);
                colliding = wall.Collides(item) || snake.Collides(item);
            } while (colliding);
            return item;
        }

        public Item(int sideLength, Point location, int value)
            :base(sideLength)
        {
            brush.Color = randomColor();
            pen.Color = darkerColor(brush.Color);
            points.Add(location);
            this.value = value;
        }

        public Item(Item other)
            :base(other.sideLength)
        {
            brush.Color = other.brush.Color;
            pen.Color = other.pen.Color;
            points.Add(other.points[0]);
            value = other.value;
        }

        private static Color randomColor()
        {
            return Color.FromArgb(rnd.Next(colorOffset, 256), rnd.Next(colorOffset, 256), rnd.Next(colorOffset, 256));
        }

        private static Color darkerColor(Color color)
        {
            return Color.FromArgb(Math.Max(color.R - colorOffset, 0), 
                Math.Max(color.G - colorOffset, 0), 
                Math.Max(color.B - colorOffset, 0));
        }

        new public void Draw(Panel panel, Graphics graphics)
        {
            for (int iPoint = 0; iPoint < points.Count; iPoint++)
            {
                Rectangle bounding = boundingRectangle(panel, points[iPoint]);
                graphics.FillRectangle(brush, bounding);
                graphics.DrawRectangle(pen, bounding);
            }
        }
    }
}
