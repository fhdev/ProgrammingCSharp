﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Snake
{
    class ExtraItem : Item
    {
        new public static ExtraItem Factory(Panel panel, Wall wall, Snake snake)
        {
            return new ExtraItem(Item.Factory(panel, wall, snake));
        }

        public ExtraItem(int sideLength, Point location, int value)
            :base(sideLength, location, value)
        {
            
        }

        public ExtraItem(Item item)
            :base(item)
        {
            value = 10;
        }

        public void Step()
        {
            if(value > 0) value--;
        }

        new public void Draw(Panel panel, Graphics graphics)
        {
            for (int iPoint = 0; iPoint < points.Count; iPoint++)
            {
                Rectangle bounding = boundingRectangle(panel, points[iPoint]);
                graphics.FillRectangle(brush, bounding);
                graphics.DrawRectangle(pen, bounding);
                graphics.DrawEllipse(pen, bounding);
            }
        }
    }
}
