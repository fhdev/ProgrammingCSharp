﻿namespace Snake
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelDrawing = new System.Windows.Forms.Panel();
            this.timerMozgat = new System.Windows.Forms.Timer(this.components);
            this.labelScore = new System.Windows.Forms.Label();
            this.labelScoreTitle = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelTimeTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panelDrawing
            // 
            this.panelDrawing.BackColor = System.Drawing.Color.White;
            this.panelDrawing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDrawing.Location = new System.Drawing.Point(11, 10);
            this.panelDrawing.Name = "panelDrawing";
            this.panelDrawing.Size = new System.Drawing.Size(602, 602);
            this.panelDrawing.TabIndex = 1;
            this.panelDrawing.Paint += new System.Windows.Forms.PaintEventHandler(this.panelDrawing_Paint);
            // 
            // timerMozgat
            // 
            this.timerMozgat.Interval = 200;
            this.timerMozgat.Tick += new System.EventHandler(this.timerMozgat_Tick);
            // 
            // labelScore
            // 
            this.labelScore.BackColor = System.Drawing.Color.White;
            this.labelScore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelScore.Location = new System.Drawing.Point(639, 47);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(137, 36);
            this.labelScore.TabIndex = 0;
            this.labelScore.Text = "0";
            this.labelScore.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelScoreTitle
            // 
            this.labelScoreTitle.BackColor = System.Drawing.SystemColors.Control;
            this.labelScoreTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelScoreTitle.Location = new System.Drawing.Point(665, 10);
            this.labelScoreTitle.Name = "labelScoreTitle";
            this.labelScoreTitle.Size = new System.Drawing.Size(85, 36);
            this.labelScoreTitle.TabIndex = 2;
            this.labelScoreTitle.Text = "Score";
            this.labelScoreTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTime
            // 
            this.labelTime.BackColor = System.Drawing.Color.White;
            this.labelTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTime.Location = new System.Drawing.Point(639, 158);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(137, 36);
            this.labelTime.TabIndex = 3;
            this.labelTime.Text = "0 s";
            this.labelTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTimeTitle
            // 
            this.labelTimeTitle.BackColor = System.Drawing.SystemColors.Control;
            this.labelTimeTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTimeTitle.Location = new System.Drawing.Point(665, 122);
            this.labelTimeTitle.Name = "labelTimeTitle";
            this.labelTimeTitle.Size = new System.Drawing.Size(85, 36);
            this.labelTimeTitle.TabIndex = 4;
            this.labelTimeTitle.Text = "Time";
            this.labelTimeTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 622);
            this.Controls.Add(this.labelTimeTitle);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelScoreTitle);
            this.Controls.Add(this.labelScore);
            this.Controls.Add(this.panelDrawing);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FormFoAblak";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Snake";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormFoAblak_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelDrawing;
        private System.Windows.Forms.Timer timerMozgat;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.Label labelScoreTitle;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelTimeTitle;
    }
}

