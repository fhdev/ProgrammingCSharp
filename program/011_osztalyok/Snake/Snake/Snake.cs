﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Snake
{
    class Snake : Wall
    {
        private Orientation orientation;

        private SolidBrush headBrush;

        public enum Orientation { Right = 0, Left = 1, Up = 2, Down = 3};

        public static Snake Factory(Panel panel, int sideLength)
        {
            int length = 3;
            // Random starting point
            Point startPoint = randomPointInside(panel, sideLength, length - 1);
            // Random orientation
            Orientation startOrientation = (Orientation)rnd.Next(0, 4);
            // New snake
            return new Snake(sideLength, length, startPoint, startOrientation);
        }

        public static Snake Factory(Panel panel, Wall wall)
        {
            Snake snake;
            bool colliding;
            do
            {
                snake = Factory(panel, wall.SideLength);
                colliding = snake.Collides(wall);
            } while (colliding);
            return snake;
        }

        public Snake(int sideLength, int length, Point startPoint, Orientation startOrientation)
            :base(sideLength)
        {
            brush.Color = Color.FromArgb(46, 204, 113);
            pen.Color = Color.FromArgb(22, 160, 133);
            points.Add(startPoint);
            for (int iPoint = 1; iPoint < length; iPoint++)
            {
                switch (startOrientation)
                {
                    case Orientation.Up:
                    {
                        points.Add(new Point(startPoint.X, startPoint.Y - iPoint * sideLength));
                        break;
                    }
                    case Orientation.Down:
                    {
                        points.Add(new Point(startPoint.X, startPoint.Y + iPoint * sideLength));
                        break;
                    }
                    case Orientation.Right:
                    {
                        points.Add(new Point(startPoint.X - iPoint * sideLength, startPoint.Y));
                        break;
                    }
                    case Orientation.Left:
                    {
                        points.Add(new Point(startPoint.X + iPoint * sideLength, startPoint.Y));
                        break;
                    }
                }
            }
            orientation = startOrientation;
            headBrush = new SolidBrush(Color.FromArgb(106, 176, 76));
        }

        new public void Draw(Panel panel, Graphics graphics)
        {
            for (int iPoint = 0; iPoint < points.Count; iPoint++)
            {
                Rectangle bounding = boundingRectangle(panel, points[iPoint]);
                if(iPoint == 0)
                {
                    graphics.FillEllipse(headBrush, bounding);
                }
                else
                {
                    graphics.FillEllipse(brush, bounding);
                }
                graphics.DrawEllipse(pen, bounding);
            }
        }

        public void Step(Panel panel, bool increment)
        {
            // Increment snake if necessary
            if (increment)
            {
                points.Add(new Point());
            }
            // Move tail
            for (int iPoint = points.Count - 1; iPoint > 0; iPoint--)
            {
                points[iPoint] = points[iPoint - 1];
            }
            // Move head according to current orientation
            switch (orientation)
            {
                case Orientation.Up:
                {
                    if (points[0].Y + sideLength <= panel.ClientSize.Height)
                        points[0] = new Point(points[0].X, points[0].Y + sideLength);
                    else
                        points[0] = new Point(points[0].X, sideLength);
                    break;
                }
                case Orientation.Down:
                {
                    if (points[0].Y - sideLength >= sideLength)
                        points[0] = new Point(points[0].X, points[0].Y - sideLength);
                    else
                        points[0] = new Point(points[0].X, panel.ClientSize.Height);
                    break;
                }
                case Orientation.Right:
                {
                    if (points[0].X + sideLength <= panel.ClientSize.Width - sideLength)
                        points[0] = new Point(points[0].X + sideLength, points[0].Y);
                    else
                        points[0] = new Point(0, points[0].Y);
                    break;
                }
                case Orientation.Left:
                {
                    if (points[0].X - sideLength >= 0)
                        points[0] = new Point(points[0].X - sideLength, points[0].Y);
                    else
                        points[0] = new Point(panel.ClientSize.Width - sideLength, points[0].Y);
                    break;
                }
            }
        }

        public void SetOrientation(Keys key)
        {
            switch (key)
            {
                case Keys.Up:
                {
                    if (orientation == Orientation.Left || orientation == Orientation.Right)
                        orientation = Orientation.Up;
                    break;
                }
                case Keys.Down:
                {
                    if (orientation == Orientation.Left || orientation == Orientation.Right)
                        orientation = Orientation.Down;
                    break;
                }
                case Keys.Right:
                {
                    if (orientation == Orientation.Up || orientation == Orientation.Down)
                        orientation = Orientation.Right;
                    break;
                }
                case Keys.Left:
                {
                    if (orientation == Orientation.Up || orientation == Orientation.Down)
                        orientation = Orientation.Left;
                    break;
                }
            }
        }

        public bool Collides()
        {
            for (int iPoint = 1; iPoint < points.Count; iPoint++)
            {
                if (points[0] == points[iPoint]) return true;
            }
            return false;
        }
    }
}
