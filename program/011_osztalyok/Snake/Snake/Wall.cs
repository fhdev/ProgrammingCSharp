﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Snake
{
    class Wall
    {
        protected SolidBrush brush;
        protected Pen pen;
        protected int sideLength;
        protected List<Point> points;

        protected static Random rnd = new Random();

        public enum Type { Left = 0, Right = 1, Top = 2, Bottom = 3, Around = 4}

        public int SideLength
        {
            get { return sideLength; }
        }

        protected static Point randomPointInside(Panel panel, int sideLength, int offset)
        {
            // Random point inside the panel considering the panel is discretized into squares
            // with a side length of sideLength.
            int width = panel.ClientSize.Width / sideLength;
            int height = panel.ClientSize.Height / sideLength;
            int x = rnd.Next(offset, width - offset) * sideLength;
            int y = rnd.Next(offset + 1, height - offset + 1) * sideLength;
            return new Point(x, y);
        }

        public Wall(int sideLength)
        {
            brush = new SolidBrush(Color.FromArgb(189, 195, 199));
            pen = new Pen(Color.FromArgb(127, 140, 141), 3);
            this.sideLength = sideLength;
            points = new List<Point>();
        }

        public Wall(int sideLength, Panel panel, Type type)
            :this(sideLength)
        { 
            switch (type)
            {
                case Type.Left:
                {
                    int length = panel.ClientSize.Height / sideLength;
                    for (int iPoint = 0; iPoint < length; iPoint++)
                    {
                        points.Add(new Point(0, (iPoint + 1) * sideLength));
                    }
                    break;
                }
            }
        }

        protected Point panelPoint(Panel panel, Point point)
        {
            return new Point(point.X, panel.ClientSize.Height - point.Y);
        }
        protected Rectangle boundingRectangle(Panel panel, Point point)
        { 
            Size size = new Size(sideLength, sideLength);
            return new Rectangle(panelPoint(panel, point), size);
        }

        public void Draw(Panel panel, Graphics graphics)
        {
            for (int iPoint = 0; iPoint < points.Count; iPoint++)
            {
                Rectangle bounding = boundingRectangle(panel, points[iPoint]);
                graphics.FillRectangle(brush, bounding);
                graphics.DrawRectangle(pen, bounding);
                graphics.DrawLine(pen,
                    panelPoint(panel, points[iPoint]),
                    panelPoint(panel, new Point(points[iPoint].X + sideLength, points[iPoint].Y - sideLength)));
                graphics.DrawLine(pen,
                    panelPoint(panel, new Point(points[iPoint].X + sideLength, points[iPoint].Y)),
                    panelPoint(panel, new Point(points[iPoint].X, points[iPoint].Y - sideLength)));
            }
        }

        public bool Collides(Wall other)
        {
            for (int iOwn = 0; iOwn < points.Count; iOwn++)
            {
                for (int iOther = 0; iOther < other.points.Count; iOther++)
                {
                    if (points[iOwn] == other.points[iOther]) return true;
                }
            }
            return false;
        }
    }
}
