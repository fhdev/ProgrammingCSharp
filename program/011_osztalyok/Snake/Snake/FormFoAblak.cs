﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;

namespace Snake
{
    public partial class FormFoAblak : Form
    {
        private Game game;
        private Stopwatch stopwatch;

        public FormFoAblak()
        {
            InitializeComponent();
            typeof(Panel).InvokeMember("DoubleBuffered",
                           BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                           null,
                           panelDrawing,
                           new object[] { true });
            stopwatch = new Stopwatch();
        }

        private void panelDrawing_Paint(object sender, PaintEventArgs e)
        {
            if (game != null)
            {
                game.Draw(panelDrawing, e.Graphics);
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Left: case Keys.Right: case Keys.Up: case Keys.Down:
                {
                    if (game != null)
                    {
                        game.LastKey = keyData;
                    }
                    // Block subsequent events
                    return true;
                }
                case Keys.Enter:
                {
                    // Start new game
                    timerMozgat.Interval = 200;
                    game = new Game(30, panelDrawing);
                    panelDrawing.Invalidate();
                    timerMozgat.Start();
                    stopwatch.Restart();
                    return true;
                }
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        private void FormFoAblak_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.P:
                {
                    timerMozgat.Stop();
                    stopwatch.Stop();
                    DialogResult result = MessageBox.Show("Press OK to continue!", "PAUSED", MessageBoxButtons.OK, MessageBoxIcon.None);
                    if (result == DialogResult.OK)
                    {
                        timerMozgat.Start();
                        stopwatch.Start();
                    }
                    break;
                }
            }
            e.Handled = true;
        }

        private void timerMozgat_Tick(object sender, EventArgs e)
        {
            if (game != null)
            {
                game.Step(panelDrawing);
                labelScore.Text = string.Format("{0:D}", game.Score);
                labelTime.Text = string.Format("{0:F2} s", stopwatch.ElapsedMilliseconds / 1000.0);
                timerMozgat.Interval = Math.Max(100, 200 - game.Score);
                if (game.Lost)
                {
                    timerMozgat.Stop();
                    MessageBox.Show("Unfortunately you lost the game.", "GAME OVER", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    panelDrawing.Invalidate();
                }
            }
        }

    }
}
