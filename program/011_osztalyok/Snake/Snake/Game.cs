﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;

namespace Snake
{
    class Game
    {
        private Wall wall;
        private Snake snake;
        private Item item;
        private ExtraItem extraItem;
        private bool lost;
        private int score;
        private int extraScore;
        private bool lastScored;
        private int steps;

        public Keys LastKey { get; set; }

        public bool Lost
        {
            get { return lost; }
        }

        public int Score
        {
            get { return score + extraScore; }
        }

        public int Steps
        {
            get { return steps; }
        }

        public Game(int sideLength, Panel panel)
        {
            wall = new Wall(sideLength, panel, Wall.Type.Left);
            snake = Snake.Factory(panel, wall);
            item = Item.Factory(panel, wall, snake);
            lost = false;
            score = 0;
            extraScore = 0;
            lastScored = false;
            steps = 0;
        }

        public void Draw(Panel panel, Graphics graphics)
        {
            wall.Draw(panel, graphics);
            snake.Draw(panel, graphics);
            item.Draw(panel, graphics);
            extraItem?.Draw(panel, graphics);
        }
        
        public void Step(Panel panel)
        {
            steps++;
            snake.SetOrientation(LastKey);
            snake.Step(panel, lastScored);
            lastScored = false;
            if (snake.Collides(wall) || snake.Collides())
            {
                lost = true;
                return;
            }
            if (snake.Collides(item))
            {
                score += item.Value;
                lastScored = true;
                item = Item.Factory(panel, wall, snake);
            }
            if ((extraItem != null))
            {
                if (extraItem.Value > 0)
                {
                    if (snake.Collides(extraItem))
                    {
                        extraScore += extraItem.Value;
                        lastScored = true;
                        extraItem = null;
                    }
                    else
                    {
                        extraItem.Step();
                    }
                }
                else
                {
                    extraItem = null;
                }
            }
            else if (lastScored && ((score % 5) == 0))
            {
                extraItem = ExtraItem.Factory(panel, wall, snake);
            }
        }
    }
}