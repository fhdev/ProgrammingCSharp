﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Tetris
{
    // A Tetris játékot megvalósító osztály. 
    class Jatek
    {
        // A pálya székessége. 
        private const int palyaSzelesseg = 10;
        // A pálya magassága. 
        private const int palyaMagassag = 20;
        // A pálya kirajzolásakor a négyzetek oldalhossza. 
        private const int negyzetOldal = 20;

        // Az aktuális alakzat. 
        private Alakzat aktualisAlakzat;
        // A következő alakzat (előre megmutatjuk majd). 
        private Alakzat kovetkezoAlakzat;
        // A pálya. 
        private Alakzat.Tipusok [,] palya;
        // A játékos pontszáma. 
        private int pontszam;
        // Csak olvasható tulajdonság. Arra használjuk, hogy a játékos pontszámát kívülről is el 
        // lehessen érni, de ne lehessen módosítani. 
        public int Pontszam
        {
            get
            {
                return pontszam;
            }
        }

        // A játék osztály konstruktora. Paramétere nincs, a játék létrehozásához szükséges 
        // lépéseket tartalamzza.
        public Jatek()
        {
            // Létrehozzuk az üres pályát. 
            palya = new Alakzat.Tipusok[palyaSzelesseg, palyaMagassag];
            // Létrehozzuk az aktuális és a következő alakzatot. 
            aktualisAlakzat = new Alakzat(negyzetOldal);
            kovetkezoAlakzat = new Alakzat(negyzetOldal);
        }

        // Ez a metótus a neki átadott grafikus objektum, toll objektum, és ecset objektum 
        // segítsévégel felrajzolja az aktuális pályát.
        private void RajzolPalyat(Graphics rajzTabla, Pen toll, SolidBrush ecset)
        {
            // A pálya minden egyes pontján (négyzetén) végigmegyünk. 
            for (int oszlop = 0; oszlop < palyaSzelesseg; oszlop++)
            {
                for (int sor = 0; sor < palyaMagassag; sor++)
                {
                    // Ha az aktuális mező értéke 0, akkor az üres, nem kell rajzolnunk, ugrunk a 
                    // következőre.
                    if (palya[oszlop, sor] == Alakzat.Tipusok.Ures)
                    {
                        continue;
                    }
                    // Beállítjuk az ecset színét a pálya aktuális négyzetének megfelleően. 
                    Color aktualisNegyzetSzine;
                    Alakzat.Szinek.TryGetValue(palya[oszlop, sor], out aktualisNegyzetSzine);
                    ecset.Color = aktualisNegyzetSzine;
                    // Meghatározzuk a rajzolandó négyzet bal felső sarkának koordinátáit, valamint
                    //  magasságát és szélességét. 
                    Rectangle aktualisNegyzet = new Rectangle(oszlop * negyzetOldal,
                                                              sor * negyzetOldal,
                                                              negyzetOldal,
                                                              negyzetOldal);
                    // Az ecsettel megrajzoljuk a teli négyzetet. 
                    rajzTabla.FillRectangle(ecset, aktualisNegyzet);
                    // A tollal megrajzoljuk a négyzet körvonalát. 
                    rajzTabla.DrawRectangle(toll, aktualisNegyzet);
                }
            }
        }

        // Ez a metódus a neki átadott grafikus objektum segítsévégel felrajzolja a már kisorsolt 
        // új alakzatot.
        public void RajzolUjAlakzatot(Graphics rajzTabla)
        {
            // Először töröljük a hátteret, világosszürke színnel. 
            rajzTabla.Clear(Color.LightGray);
            // Létrehozzuk a rajzoláshoz szükséges objektumokat. 
            SolidBrush ecset = new SolidBrush(Color.White);
            Pen toll = new Pen(Color.Black);
            // Használjuk az előzőekben megírt alakzatrajzoló függvényünket az alakzat 
            // megrajzolásához.
            kovetkezoAlakzat.Rajzol(rajzTabla, toll, ecset);
            // Felszabadítju ka rajzoláshoz használt objektumok által felhasznált erőforrásokat. 
            ecset.Dispose();
            toll.Dispose();
        }

        // Ez a metódus a neki átadott grafikus objektum segítségével felrajzolja a játékot.
        public void RajzolJatekot(Graphics rajzTabla)
        {
            // Először töröljük a hátteret, világosszürke színnel. 
            rajzTabla.Clear(Color.LightGray);
            // Létrehozzuk a rajzoláshoz szükséges objektumokat. 
            SolidBrush ecset = new SolidBrush(Color.White);
            Pen toll = new Pen(Color.Black);
            // A pálya közepére felrajzoljuk a TETRIS feliratot. 
            Font betu = new Font("Consolas", negyzetOldal);
            Point szovegHelye = new Point(2 * negyzetOldal, 9 * negyzetOldal);
            rajzTabla.DrawString("TETRIS", betu, ecset, szovegHelye);
            RajzolPalyat(rajzTabla, toll, ecset);
            // Használjuk az előzőekben megírt alakzatrajzoló függvényünket az alakzat 
            // megrajzolásához.
            aktualisAlakzat.Rajzol(rajzTabla, toll, ecset);
            // Felszabadítju ka rajzoláshoz használt objektumok által felhasznált erőforrásokat. 
            ecset.Dispose();
            toll.Dispose();
        }

        // Ez a metódus lefelé lépteti az alakzatot, ha ez lehetséges. Ha nem, akkor belemásolja az 
        // alakzatot a pályába, majd ezután a következő alakzatot kijelöli aktuálisnak. Ha ez az új 
        // aktuális alakzat már nem fér el a pályán, akkor igaz visszatérési értékkel tér vissza. 
        // Ezek után a metótus a pályán található teljes sorokat lebontja, és növeli a játékos
        // pontszámát.
        public bool Esik()
        {
            // Ha sikerül ütközés nélkül egyel lefelé léptetni az alakzatot, akkor visszatérünk 
            // hamis értékkel, a játék folytatódik. 
            if (aktualisAlakzat.Leptet(palya, 0, 1))
            {
                return false;
            }
            // Ha a lefelé léptetés már ütközéshez vezetett volna, akkor az elemet belemásoljuk a
            // pályába. 
            aktualisAlakzat.PalyabaMasol(palya);
            // Az aktuális elembe átrakjuk az előzőleg már kisorsolt következő elemet. 
            aktualisAlakzat = new Alakzat(kovetkezoAlakzat);
            // Ha ez a következő elem már nem fér el a pályán, akkor a játéknak vége ezért igaz 
            // értékkel térünk vissza.
            if (!aktualisAlakzat.Leptet(palya, 0, 0))
            {
                return true;
            }
            // Ha az új alakzat elfért a pályán, akkor sorsolunk következő alakzatot. 
            kovetkezoAlakzat = new Alakzat(negyzetOldal);
            // Lebontjuk a pálya teli sorait. Ehhez a pálya összes során végigmegyünk, mégpedig  
            // alulról kezdve. Nem felejtjük el, hogy a rajzolás koordinátarendszerének origoja az
            // elemek bal felső sarkában van, így a pálya alsó sorai a nagy sorindexeknél vannak!
            for (int sor = palyaMagassag - 1; sor >= 0; sor--)
            {
                // Feltesszük, hogy az aktuálisan vizsgált sor tele van. 
                bool teljesSor = true;
                // Végigmegyünk az adott sorban az összes oszlopon. 
                for (int oszlop = 0; oszlop < palyaSzelesseg; oszlop++)
                {
                    // Ha bármelyik elem az adott sorban 0, akkor az adott sor nem teli. 
                    if(palya[oszlop,sor] == Alakzat.Tipusok.Ures)
                    {
                        teljesSor = false;
                        break;
                    }
                }
                // Ha az adott sor teli, akkor az összes felette lévő sort egyel lejjebb helyezzük.
                if (teljesSor)
                {
                    for (int fSor = sor; fSor > 0; fSor--)
                    {
                        for (int fOszlop = 0; fOszlop < palyaSzelesseg; fOszlop++)
                        {
                            palya[fOszlop, fSor] = palya[fOszlop, fSor - 1];
                        }
                    }
                    // Ha egy sort sikerült lebontani, akkor a játékos pontszámát növeljük. 
                    pontszam++;
                    // Ha egy sort lebontottunk, akkor ismét meg kell vizsgálnunk, hiszen a tartalma
                    // megváltozott.
                    sor++;
                }
            }
            // Hamis értékkel térünk vissza, hiszen a játéknak nincsen vége. 
            return false;
        }

        // Ez a metódus a billentyűlenyománsnak megfelelően mozgatja az alakzatot. 
        public void Lep(KeyEventArgs lenyomottGomb)
        {
            // Attól függően cselekszünk, hogy mi volt a lenyomott billentyű. 
            switch(lenyomottGomb.KeyCode)
            {
                // Balra léptetés 
                case Keys.Left:
                case Keys.A:
                    {
                        aktualisAlakzat.Leptet(palya, -1, 0);
                        break;
                    }
                // Jobbra léptetés
                case Keys.Right:
                case Keys.D:
                    {
                        aktualisAlakzat.Leptet(palya, 1, 0);
                        break;
                    }
                // Lefelé léptetés
                case Keys.Down:
                case Keys.S:
                    {
                        aktualisAlakzat.Leptet(palya, 0, 1);
                        break;
                    }
                // Forgatás 
                case Keys.Up:
                case Keys.W:
                    {
                        aktualisAlakzat.Forgat(palya);
                        break;
                    }
            }
        }
    }
}
