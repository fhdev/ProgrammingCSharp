﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Tetris
{
    // A Tetris játék egy alakzatát megvalósító ostály. 
    class Alakzat
    {
        // Az alakzatot alkotó egy négyzet oldalhossza. 
        private int negyzetOldal;
        // Ebben a változóban tároljuk el az alakzat fajtáját. 
        private Tipusok tipus;
        // Ebben a pont tömbben tároljuk el az alakzat pontjainak (négyzeteinek) koordinátáit. 
        private Point[] pontok;
        // Véletlenszám generáló objektum 
        private static Random veletlenSzam;
        // Szótár objektum, amelyben alakzat típusokhoz színeket rendelhetünk hozzá. 
        public static readonly Dictionary<Tipusok, Color> Szinek;

        // Csak olvasható tulajdonség. Arra való, hogy az alakzatok típusát kívülről is fel 
        // lehessen használni, de ne lehessen módosítani. 
        public Tipusok Tipus
        {
            get
            {
                return tipus;
            }
        }

        // Csak olvasható tulajdonság, amivel az adott típusnak megfelelő színt megkaphatjuk.
        public Color Szin
        {
            get
            {
                Color alakzatSzine;
                Szinek.TryGetValue(tipus, out alakzatSzine);
                return alakzatSzine;
            }
        }

        // Felsorolásos típus, a lehetséges alakzatok felsorolását adja meg. 
        public enum Tipusok { Ures, I, L, J, Z, S, T, O };

        // Az alakzatok statikus konstruktora. A statikus mezők inicializálására szolgál. 
        static Alakzat()
        {
            veletlenSzam = new Random();
            Szinek = new Dictionary<Tipusok, Color>();
            Szinek.Add(Tipusok.Ures, Color.White);
            Szinek.Add(Tipusok.I, Color.Red);
            Szinek.Add(Tipusok.J, Color.Purple);
            Szinek.Add(Tipusok.L, Color.Orange);
            Szinek.Add(Tipusok.O, Color.Blue);
            Szinek.Add(Tipusok.S, Color.Yellow);
            Szinek.Add(Tipusok.T, Color.Green);
            Szinek.Add(Tipusok.Z, Color.Magenta);
        }

        // Az alakzatok konstruktora. Az aktuális pályának a megadásával elkészít egy új 
        // véletlenszerű alakzatot.
        public Alakzat(int oldalHossz)
            : this(RandomTipus(),  oldalHossz)
        {
        }

        public Alakzat(Alakzat masik) 
            : this(masik.tipus, masik.negyzetOldal)
        {
        }

        // Az alakzatok konstruktora. Az alakzat típusának és az aktuális pályának a megadásával 
        // elkészít egy új alakzatot. 
        public Alakzat(Tipusok ujTipus, int oldalHossz)
        {
            tipus = ujTipus;
            negyzetOldal = oldalHossz;
            pontok = new Point[4];
            // A típusnak megfelelően meghatározzuk az alakzat pontjainak kezdeti koordinátáit. 
            switch(tipus)
            {
                case Tipusok.I:
                    {
                        // - - - - 
                        // X X X X
                        // - - - -
                        // - - - -
                        pontok[0] = new Point(0, 2);
                        pontok[1] = new Point(1, 2);
                        pontok[2] = new Point(2, 2);
                        pontok[3] = new Point(3, 2);
                        break;
                    }
                case Tipusok.L:
                    {
                        // - - - -
                        // X X X -
                        // - - X -
                        // - - - -
                        pontok[0] = new Point(0, 2);
                        pontok[1] = new Point(1, 2);
                        pontok[2] = new Point(2, 2);
                        pontok[3] = new Point(2, 1);
                        break;
                    }
                case Tipusok.J:
                    {
                        // - - - -
                        // X X X -
                        // X - - -
                        // - - - -
                        pontok[0] = new Point(0, 2);
                        pontok[3] = new Point(0, 1);
                        pontok[1] = new Point(1, 2);
                        pontok[2] = new Point(2, 2);
                        break;
                    }
                case Tipusok.Z:
                    {
                        // - - - -
                        // - X X -
                        // X X - -
                        // - - - -
                        pontok[0] = new Point(0, 1);
                        pontok[1] = new Point(1, 1);
                        pontok[2] = new Point(1, 2);
                        pontok[3] = new Point(2, 2);
                        break;
                    }
                case Tipusok.S:
                    {
                        // - - - -
                        // - X X -
                        // X X - -
                        // - - - -
                        pontok[0] = new Point(0, 2);
                        pontok[1] = new Point(1, 2);
                        pontok[2] = new Point(1, 1);
                        pontok[3] = new Point(2, 1);
                        break;
                    }
                case Tipusok.T:
                    {
                        // - - - -
                        // X X X -
                        // - X - -
                        // - - - -
                        pontok[0] = new Point(0, 2);
                        pontok[1] = new Point(1, 1);
                        pontok[2] = new Point(1, 2);
                        pontok[3] = new Point(2, 2);
                        break;
                    }
                case Tipusok.O:
                    {
                        // - - - -
                        // - X X -
                        // - X X -
                        // - - - -
                        pontok[0] = new Point(1, 1);
                        pontok[1] = new Point(1, 2);
                        pontok[2] = new Point(2, 1);
                        pontok[3] = new Point(2, 2);
                        break;
                    }
            }
        }

        // Ez a metódus véletlenszerűen választ egy alakzattípust. 
        private static Tipusok RandomTipus()
        {
            int utolsoErtek = Enum.GetValues(typeof(Tipusok)).Cast<int>().Max();
            return (Tipusok)veletlenSzam.Next((int)Tipusok.I, (int)Tipusok.O + 1);
        }

        // Ez a metórus ellenőrzi, hogy a neki paraméterként átadott pont koordinátái a pályán  
        // kívülre, vagy már elfoglalt helyre esnek e. 
        private bool kilogVagyUtkozik(Tipusok[,] palya, Point pont)
        {
            // Oldalról kilóg
            return (pont.X < 0) || (pont.X >= palya.GetLength(0)) || 
                   // Felül vagy alul kilóg
                   (pont.Y < 0) || (pont.Y >= palya.GetLength(1)) ||  
                   // Már foglalt
                   (palya[pont.X, pont.Y] != Tipusok.Ures);
        }

        // Ez a metódus a megadott számú léptetést végez az alakzaton x és y irányban, ha ez nem 
        // vezet a pályán kívülre, vagy a pályán már elfoglalt helyre.
        public bool Leptet(Tipusok[,] palya, int xIrany, int yIrany)
        {
            // Ebbe a pont tömbbe számítjuk ki az új pozíciókat. 
            Point[] ujPontok = new Point[4];
            // Az alakzat mind a négy pontja esetén: 
            for (int pontIndex = 0; pontIndex < ujPontok.Length; pontIndex++)
            {
                // Kiszmítjuk az aktuális pont új koordinátáit. 
                ujPontok[pontIndex] = new Point(pontok[pontIndex].X + xIrany,
                                                pontok[pontIndex].Y + yIrany);
                // Ennelőrizzük, hogy az új koordináták nem mutatnak-e a pályán kívülre, 
                // vagy már foglalt helyre. 
                if (kilogVagyUtkozik(palya, ujPontok[pontIndex]))
                {
                    // Ha igen, akkor hamis értékkel térünk vissza. 
                    return false;
                }
            }
            // Ha minden pozíció megfelelő, akkor az alakzat pontjainak koordinátáit egynlővé 
            // tesszük az újonnan számított koordinátákkal. 
            pontok = ujPontok;
            // Igaz értékkel térünk vissza. 
            return true;
        }

        // Ez a metódus 90 fokkal elfordítja az alakzatot az óramutató járásával ellentétesen, ha 
        // ez nem vezet a pályán kívülre, vagy a pályán már elfoglalt helyre.
        public bool Forgat(Tipusok[,] palya)
        {
            // Ebbe a pont tömbbe számítjuk ki az új pozíciókat. 
            Point[] ujPoziciok = new Point[4];
            // Az alakzat mind a négy pontja esetén: 
            for (int i = 0; i < ujPoziciok.Length; i++)
            {
                // Kiszmítjuk az aktuális pont új koordinátáit. 
                ujPoziciok[i] = new Point(pontok[1].X + pontok[i].Y - pontok[1].Y, 
                                          pontok[1].Y - pontok[i].X + pontok[1].X);
                // Ennelőrizzük, hogy az új koordináták nem mutatnak-e a pályán kívülre, vagy már 
                // foglalt helyre. 
                if (kilogVagyUtkozik(palya, ujPoziciok[i]))
                {
                    // Ha igen, akkor hamis értékkel térünk vissza. 
                    return false;
                }
            }
            // Ha minden pozíció megfelelő, akkor az alakzat pontjainak koordinátáit egynlővé 
            // tesszük az újonnan számított koordinátákkal. 
            pontok = ujPoziciok;
            // Igaz értékkel térünk vissza. 
            return true;
        }

        // Ez a metódus kirajzolja az alakzat összes pontját a megadott grafikus objektumokkal. 
        public void Rajzol(Graphics rajzTabla, Pen toll, SolidBrush ecset)
        {
            // Az ecset színét beállítjuk az adott alakzat színének megfelelően. 
            ecset.Color = Szin;
            // Felrajzoljuk az alakzat öszes pontját (négyzetét). 
            for (int i = 0; i < pontok.Length; i++)
            {
                Rectangle aktualisNegyzet = new Rectangle(pontok[i].X * negyzetOldal, 
                                                          pontok[i].Y * negyzetOldal,
                                                          negyzetOldal,
                                                          negyzetOldal);
                rajzTabla.FillRectangle(ecset, aktualisNegyzet);
                rajzTabla.DrawRectangle(toll, aktualisNegyzet);
            }
        }

        // Ez a metódus a pályába másolja az alakzat összes pontját. 
        public void PalyabaMasol(Tipusok[,] aktualisPalya)
        {
            // Minden egyes pontot belemásolunk a pálya megfelelő helyére. 
            for (int pontIndex = 0; pontIndex < pontok.Length; pontIndex++)
            {
                aktualisPalya[pontok[pontIndex].X, pontok[pontIndex].Y] = tipus;
            }
        }

    }
}
