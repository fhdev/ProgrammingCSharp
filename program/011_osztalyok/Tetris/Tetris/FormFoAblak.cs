﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Tetris
{
    public partial class FormFoAblak : Form
    {
        // Alapértelmezett timer intervallum. 
        private const int timerIntervallum = 1000;
        // Ennyivel csökkenthük az intervallumot egy újabb pont megszerzése után. 
        private const int pontszamIntervallum = 25;
        // A játék objektuma. 
        private Jatek tetrisJatek;

        public FormFoAblak()
        {
            InitializeComponent();
            // A játék paneljének DoubleBuffered tulajdonságát csak ilyen csúnyán tudjuk true
            //  értékre állítani a vibrálás elkerülésének érdekében. Nem tananyag.
            typeof(Panel).InvokeMember("DoubleBuffered",
                                       BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                                       null,
                                       panelJatek,
                                       new object[] { true });
            typeof(Panel).InvokeMember("DoubleBuffered",
                                       BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                                       null,
                                       panelUjAlakzat,
                                       new object[] { true });
        }

        // Ez a metódus akkor fog lefutni, ha a játék paneljét (amire a pályát rajzoljuk fel) újra 
        // kell rajzolni. 
        private void panelJatek_Paint(object sender, PaintEventArgs e)
        {
            // Ha a játék már inicializálva van, akkor rajzoljuk ki. 
            if (tetrisJatek != null)
            {
                tetrisJatek.RajzolJatekot(e.Graphics);
            }
        }

        // Ez a metódus a játék timer kattanásaira fog lefutni. 
        private void timerMozgat_Tick(object sender, EventArgs e)
        {
            // A játékban az alakzatot lefelé ejtjük. Ha a Fall metódus igaz értékkel tér vissza, az 
            // azt jelenti, hogy az új alakzatot már nem tudjuk a pályán elhelyezni, ezért a 
            // játéknak vége. 
            if (tetrisJatek.Esik())
            {
                // Játék végén megállítjuk a mozgatást végző Timer-t. 
                timerMozgat.Stop();
                // Értesítjük a felhasználót hogy a játéknak vége. 
                string uzenet = string.Format("Az Ön pontszáma: {0:D}!", tetrisJatek.Pontszam);
                MessageBox.Show(uzenet, "Játék vége", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            // Frissítjük a pontszám kiírását. 
            labelPontszam.Text = string.Format("Pontszám: {0:D}", tetrisJatek.Pontszam);
            // Gyorsítjuk a játékot. 
            timerMozgat.Interval = timerIntervallum - tetrisJatek.Pontszam * pontszamIntervallum;
            // Érvénytelenítjük a pálya és az új alakzat panelét, hogy újrarajzolódjanak. 
            panelJatek.Invalidate();
            panelUjAlakzat.Invalidate();
        }

        // Ez a metódus egy gomb lenyomásakor fog lefutni. 
        private void FormFoAblak_KeyDown(object sender, KeyEventArgs e)
        {
            // P gomb lenyomása esetén szüneteltetjük a játékot. 
            if (e.KeyCode == Keys.P)
            {
                if (timerMozgat.Enabled)
                {
                    timerMozgat.Stop();
                }
                else
                {
                    timerMozgat.Start();
                }
            }
            // Mozgatjuk az alakzatot a lenyomott billentyűnek megfelelően. 
            tetrisJatek.Lep(e);
            // Újrarajzoltatjuk a játékot, hiszen lehet hogy elmozgattuk az alakzatot. 
            panelJatek.Invalidate();
        }

        // Ez a metótus akkor fog lefutni, ha az új alakzatot mutató panelt újra kell rajzolni. 
        private void panelUjAlakzat_Paint(object sender, PaintEventArgs e)
        {
            // Ha a játék már létre lett hozva, kirajzoljuk a panelre az új alakzatot. 
            if (tetrisJatek != null)
            {
                tetrisJatek.RajzolUjAlakzatot(e.Graphics);
            }
        }

        // Játék kezdés. 
        private void kezdesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Inicializáljuk a játékot. 
            tetrisJatek = new Jatek();
            // Elindítjuk a Timer-t. 
            timerMozgat.Start();
        }

        // Szünet. 
        private void szunetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timerMozgat.Stop();
        }

        // Folytatás. 
        private void folytatasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timerMozgat.Start();
        }
    }
}
