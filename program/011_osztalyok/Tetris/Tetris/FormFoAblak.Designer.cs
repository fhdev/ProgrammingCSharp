﻿namespace Tetris
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerMozgat = new System.Windows.Forms.Timer(this.components);
            this.panelUjAlakzat = new System.Windows.Forms.Panel();
            this.panelJatek = new System.Windows.Forms.Panel();
            this.labelPontszam = new System.Windows.Forms.Label();
            this.menuStripFoMenu = new System.Windows.Forms.MenuStrip();
            this.jatekToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kezdesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.szunetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folytatasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripFoMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerMozgat
            // 
            this.timerMozgat.Interval = 1000;
            this.timerMozgat.Tick += new System.EventHandler(this.timerMozgat_Tick);
            // 
            // panelUjAlakzat
            // 
            this.panelUjAlakzat.BackColor = System.Drawing.Color.LightGray;
            this.panelUjAlakzat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUjAlakzat.Location = new System.Drawing.Point(240, 40);
            this.panelUjAlakzat.Margin = new System.Windows.Forms.Padding(10);
            this.panelUjAlakzat.Name = "panelUjAlakzat";
            this.panelUjAlakzat.Size = new System.Drawing.Size(80, 80);
            this.panelUjAlakzat.TabIndex = 1;
            this.panelUjAlakzat.Paint += new System.Windows.Forms.PaintEventHandler(this.panelUjAlakzat_Paint);
            // 
            // panelJatek
            // 
            this.panelJatek.BackColor = System.Drawing.Color.LightGray;
            this.panelJatek.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelJatek.Location = new System.Drawing.Point(20, 40);
            this.panelJatek.Margin = new System.Windows.Forms.Padding(10);
            this.panelJatek.Name = "panelJatek";
            this.panelJatek.Size = new System.Drawing.Size(200, 400);
            this.panelJatek.TabIndex = 0;
            this.panelJatek.Paint += new System.Windows.Forms.PaintEventHandler(this.panelJatek_Paint);
            // 
            // labelPontszam
            // 
            this.labelPontszam.AutoSize = true;
            this.labelPontszam.Location = new System.Drawing.Point(237, 130);
            this.labelPontszam.Name = "labelPontszam";
            this.labelPontszam.Size = new System.Drawing.Size(65, 13);
            this.labelPontszam.TabIndex = 2;
            this.labelPontszam.Text = "Pontszám: 0";
            // 
            // menuStripFoMenu
            // 
            this.menuStripFoMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jatekToolStripMenuItem});
            this.menuStripFoMenu.Location = new System.Drawing.Point(0, 0);
            this.menuStripFoMenu.Name = "menuStripFoMenu";
            this.menuStripFoMenu.Size = new System.Drawing.Size(339, 24);
            this.menuStripFoMenu.TabIndex = 3;
            this.menuStripFoMenu.Text = "menuStrip1";
            // 
            // jatekToolStripMenuItem
            // 
            this.jatekToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kezdesToolStripMenuItem,
            this.szunetToolStripMenuItem,
            this.folytatasToolStripMenuItem});
            this.jatekToolStripMenuItem.Name = "jatekToolStripMenuItem";
            this.jatekToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.jatekToolStripMenuItem.Text = "Játék";
            // 
            // kezdesToolStripMenuItem
            // 
            this.kezdesToolStripMenuItem.Name = "kezdesToolStripMenuItem";
            this.kezdesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.kezdesToolStripMenuItem.Text = "Kezdés";
            this.kezdesToolStripMenuItem.Click += new System.EventHandler(this.kezdesToolStripMenuItem_Click);
            // 
            // szunetToolStripMenuItem
            // 
            this.szunetToolStripMenuItem.Name = "szunetToolStripMenuItem";
            this.szunetToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.szunetToolStripMenuItem.Text = "Szünet";
            this.szunetToolStripMenuItem.Click += new System.EventHandler(this.szunetToolStripMenuItem_Click);
            // 
            // folytatasToolStripMenuItem
            // 
            this.folytatasToolStripMenuItem.Name = "folytatasToolStripMenuItem";
            this.folytatasToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.folytatasToolStripMenuItem.Text = "Folytatás";
            this.folytatasToolStripMenuItem.Click += new System.EventHandler(this.folytatasToolStripMenuItem_Click);
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(339, 461);
            this.Controls.Add(this.labelPontszam);
            this.Controls.Add(this.panelUjAlakzat);
            this.Controls.Add(this.panelJatek);
            this.Controls.Add(this.menuStripFoMenu);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStripFoMenu;
            this.MaximizeBox = false;
            this.Name = "FormFoAblak";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tetris";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormFoAblak_KeyDown);
            this.menuStripFoMenu.ResumeLayout(false);
            this.menuStripFoMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerMozgat;
        private System.Windows.Forms.Panel panelUjAlakzat;
        private System.Windows.Forms.Panel panelJatek;
        private System.Windows.Forms.Label labelPontszam;
        private System.Windows.Forms.MenuStrip menuStripFoMenu;
        private System.Windows.Forms.ToolStripMenuItem jatekToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kezdesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem szunetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folytatasToolStripMenuItem;
    }
}

