﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Akasztofa
{
    class Program
    {
        const int eletekSzama = 5;

        static void Main(string[] args)
        {
            Console.WriteLine("Akasztófa játék.");
            Console.WriteLine("Kérem adjon meg egy szót (csak a tisztán betűkből álló bemenet érvényes)!");
            string szo = SzoBeolvasas();
            KonzolTorles();
            Console.WriteLine("Kérem kezdje el a játékot!");
            bool nyert = Jatek(szo);
            if(nyert)
            {
                Console.WriteLine("Gratulálunk, ön nyert!");
            }
            else
            {
                Console.WriteLine("Sajnos ön vesztett, próbálja meg újra!");
            }
            Console.WriteLine("A helyes megfejtés: " + szo);
        }

        // Csak betűket tartalmazó szöveg beolvasása a konzolról.
        static string SzoBeolvasas()
        {
            bool ervenyesSzo;
            string szo;
            /* A beolvasást addig ismételjük, amíg megfelelő bemenetet nem kapunk. */
            do
            {
                ervenyesSzo = true;
                Console.Write(">> ");
                szo = Console.ReadLine();
                /* A bevitt szövegben található minden karakter esetén lefut. */
                foreach(char katekter in szo)
                {
                    /* Ha bármelyik karakter nem betű. */
                    if(!char.IsLetter(katekter))
                    {
                        /* A bevitt szoveg nem lehet érvényes szó. */
                        ervenyesSzo = false;
                        /* Nem vizsgálódunk tovább, hiszen a bemenet már úgysem megfelelő. */
                        break;
                    }
                }
            } while (!ervenyesSzo);
            szo = szo.ToLower();
            return szo;
        }

        // A konzol tartalmának törlése billentyűlenyomás után.
        static void KonzolTorles()
        {
            Console.Write("Kérem nyomjon meg egy billentyűt a konzol letörléséhez!");
            Console.ReadKey();
            Console.Clear();
        }

        // Betű beolvasása a konzolról.
        static char BetuBeolvasas()
        {
            bool ervenyesBetu;
            char betu;
            /* A beolvasást addig ismételjük, amíg megfelelő bemenetet nem kapunk. */
            do
            {
                Console.Write("> ");
                string bemenet = Console.ReadLine();
                /* A bemenet csak akkor érvényes, ha sikerült karakterré konvertálni, ÉS a karakter egy betű. */
                ervenyesBetu = char.TryParse(bemenet, out betu);
                ervenyesBetu &= char.IsLetter(betu);
            } while (!ervenyesBetu);
            betu = char.ToLower(betu);
            return betu;
        }

        // Akasztófa játék.
        static bool Jatek(string szo)
        {
            int megtalaltBetukSzama = 0;
            int rosszBetukSzama = 0;
            /* A szó minden egyes betűjének helyére kezdetben egy "_ " szöveget helyettesítünk */
            string behelyettesitettBetuk = string.Concat(Enumerable.Repeat("_ ", szo.Length));
            /* Addig kell a játkot folytatni, amíg nem találják ki a szót, vagy 5-nél kevesebbszer
             * tippelnek rosszul.
             */
            while ((megtalaltBetukSzama < szo.Length) && (rosszBetukSzama < eletekSzama)) 
            {
                /* Kiírjuk a már kitalált betűket tartalmazó és a még ki nem talált betűket "-" jellel
                 * elfedő szöveget. */
                Console.WriteLine(behelyettesitettBetuk);
                /* Beolvassuk a következő tippet. */
                Console.WriteLine("Kérem adja meg a következő betűt.");
                char tippeltBetu = BetuBeolvasas();
                /* Ebben a változóban számoljuk, hogy az aktuális körben hány betűt találnak ki. */
                int talalatokSzama = 0;
                /* A szó minden egyes betűjén végigmegyünk. */
                for (int iBetu = 0; iBetu < szo.Length; iBetu++)
                {
                    /* Ha találatunk van. */
                    if(tippeltBetu == szo[iBetu])
                    {
                        if (behelyettesitettBetuk[2 * iBetu] != tippeltBetu)
                        {
                            /* A helyettesítő szövegben a "-" helyére beírjuk a betűt. */
                            behelyettesitettBetuk = behelyettesitettBetuk.Remove(2 * iBetu, 1);
                            behelyettesitettBetuk = behelyettesitettBetuk.Insert(2 * iBetu, tippeltBetu.ToString());
                            /* Növeljük a találatok számát. */
                            talalatokSzama++;
                        }
                        else
                        {
                            Console.WriteLine("Ezt a betűt már kitalálta!");
                            break;
                        }
                    }
                }
                if(talalatokSzama > 0)
                {
                    megtalaltBetukSzama += talalatokSzama;
                    Console.WriteLine("Talált!");
                }
                else
                {
                    rosszBetukSzama++;
                    Console.WriteLine("Nem talált! Még {0:D} lehetősége van.", eletekSzama - rosszBetukSzama);
                }
            } 
            /* Ha a szóban található összes betűt megtalálják, nyernek. */
            return megtalaltBetukSzama == szo.Length;
        }
    }
}
