﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetukSzama
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Az egyes betűk előfordulási számának meghatározása egy mondatban.");
            Console.WriteLine("Kérem adjon meg egy mondatot. A mondatban csak betűk, írásjelek, és a szóköz karakter elfogadott.");
            string mondat = MondatBeolvasas();
            Console.WriteLine("ASz egyes karakterek előfordulásainak száma: ");
            string betuk = "abcdefghijklmnopqrstuvwxyz ,.?!\"";
            BetuElofordulas(mondat, betuk);
        }

        // Ez a metódus beolvas egy mondatot, melyben csak betűk, írásjelek, és a szóköz karakter szerepel.
        static string MondatBeolvasas()
        {
            string mondat;
            bool ervenyesMondat;
            do
            {
                Console.Write(">> ");
                // Feltesszük hogy a megadott mondat érvényes lesz.
                ervenyesMondat = true;
                mondat = Console.ReadLine();
                // Ha a beolvasott szöveg üres, akkor a mondat érvénytelen.
                if (string.IsNullOrEmpty(mondat))
                {
                    ervenyesMondat = false;
                }
                else
                {
                    // A mondat minden egyes karakterét ellenőrizzük.
                    foreach (char karakter in mondat)
                    {
                        if (!char.IsLetter(karakter) && !(karakter == ' ') && !char.IsPunctuation(karakter))
                        {
                            ervenyesMondat = false;
                            break;
                        }
                    }
                }
            } while (!ervenyesMondat);
            return mondat;
        }

        // Ez a metódus meghatározza, hogy a betuk paraméterében felsorolt betűk közül a mondat  
        // paraméterében lévő szöveg hányat tartalmaz.
        static int[] BetuElofordulas(string mondat, string betuk)
        {
            // A betuk string minden egyes karakterének előfordulásait megszámoljuk, és ebben a
            // tömbben tároljuk. 
            int[] elofordulasok = new int[betuk.Length];
            // Végigmegyünk a mondat minden egyes karakterén. 
            for (int i = 0; i < mondat.Length; i++)
            {
                // A mondat minden egyes karaktere esetében végigmegyünk azokon a betűkön és 
                // írásjeleken, amiknek az előfordulásait meg akarjuk számolni.                  
                for (int j = 0; j < betuk.Length; j++)
                {
                    // Ha a mondat aktuális karaktere megegyezik valamelyik betűvel, növeljük az
                    // előfordulsi számot. 
                    if(mondat[i] == betuk[j])
                    {
                        elofordulasok[j]++;
                        break;
                    }
                }
            }
            // Kiírjuk az eredményeket.  
            for (int i = 0; i < elofordulasok.Length; i++)
            {
                Console.WriteLine("A(z) '" + betuk[i] + "' karakterek száma: {0:D}", elofordulasok[i]);
            }
            // Visszaadjuk az előfordulások számát tartalmazó tömböt. 
            return elofordulasok; 
        }
    }
}
