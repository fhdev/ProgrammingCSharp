﻿using System;
using System.IO;

namespace LinearisRegresszio
{
    class Program
    {
        static void Main(string[] args)
        {
            // Teszt fájl neve
            string fajlNev = "teszt.txt";
            // Ebben a változóban tároljuk a leütött billentyűt. 
            ConsoleKeyInfo billentyu = new ConsoleKeyInfo();
            // Menü mutatása és feladat végrehajtása kilépésig.
            do
            {
                // Menü mutatása.
                Menu();
                // Választott menüpont beolvasása.
                billentyu = Console.ReadKey(false);
                Console.WriteLine();
                // Megfelelő művelet végrehajtása.
                switch (billentyu.Key)
                {
                    case ConsoleKey.C:
                    {
                        TesztFajl(fajlNev);
                        break;
                    }
                    case ConsoleKey.L:
                    {
                        Listaz(fajlNev);
                        break;
                    }
                    case ConsoleKey.R:
                    {
                        Regresszio(fajlNev);
                        break;
                    }
                }
            }
            while (billentyu.Key != ConsoleKey.X);
        }

        // Double típusú változó beolvasása.
        static double SzamBeolvas(string nev)
        {
            double szam = 0.0;
            do
                Console.Write(nev + " = ");
            while (!double.TryParse(Console.ReadLine(), out szam));
            return szam;
        }

        // Menü kiírása
        static void Menu()
        {
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine("[C] - Tesztfájl létrehozása. ");
            Console.WriteLine("[L] - Tesztfájl tartalmának listázása. ");
            Console.WriteLine("[R] - Lineáris egyenes regresszió számítása.");
            Console.WriteLine("[X] - Kilépés.");
            Console.WriteLine("--------------------------------------------");
            Console.Write("Választott opció: ");
        }

        // Tesztfájl létrehozása
        static void TesztFajl(string fajlNev)
        {
            Console.WriteLine("Kérem adja meg az y = a.x + b egyenes paramétereit!");
            double a = SzamBeolvas("a");
            double b = SzamBeolvas("b");
            bool success = NumerikusFuggveny.TesztFajl(fajlNev, a, b);
            if (success)
            {
                Console.WriteLine("A " + fajlNev + " fájl sikeresen létrejött.");
            }
            else
            {
                Console.WriteLine("A 'test.txt' fájlt nem sikerült létrehozni. Lehet, hogy a fájl már létezik, és rejtett, vagy írásvédett.");
            }
        }

        // Teszt fájl tartalmának listázása.
        static void Listaz(string fajlNev)
        {
            // Beolvasás sikeressége
            bool siker;
            NumerikusFuggveny nf = NumerikusFuggveny.Fajlbol(fajlNev, out siker);
            if (siker)
            {
                nf.Konzolra();
            }
            else
            {
                Console.WriteLine("A " + fajlNev + " fájl nem létezik.");
            }
        }
        // Lineáris regresszió számítása a teszt fájl alapján.
        static void Regresszio(string fajlNev)
        {
            // Fájl olvasás sikeressége
            bool siker;
            NumerikusFuggveny nf = NumerikusFuggveny.Fajlbol(fajlNev, out siker);
            if (siker)
            {
                double a, b;
                EgyenesIlleszto.Szamol(nf, out a, out b);
                Console.WriteLine("Lineáris egyenes regresszió eredményei:");
                Console.WriteLine("y = {0:F5}x + {1:F5}", a, b);
            }
            else
            {
                Console.WriteLine("A " + fajlNev + " fájl nem létezik.");
            }
        }
    }

    // Struktúra lineáris regressziós egyenes illesztés számításához.
    struct EgyenesIlleszto
    {
        // Egyenes illesztése lineáris regresszióval
        public static void Szamol(NumerikusFuggveny fuggveny, out double a, out double b)
        {
            double xxOsszeg = 0.0;
            foreach (FuggvenyPont point in fuggveny.pontok)
            {
                xxOsszeg += point.x * point.x;
            }
            double xxAtlag = xxOsszeg / fuggveny.PontokSzama;
            double xyOsszeg = 0.0;
            foreach (FuggvenyPont point in fuggveny.pontok)
            {
                xyOsszeg += point.x * point.y;
            }
            double xyAtlag = xyOsszeg / fuggveny.PontokSzama;
            double xAtlag = fuggveny.XAtlag;
            double yAtlag = fuggveny.YAtlag;
            a = (xAtlag * yAtlag - xyAtlag) / (xAtlag * xAtlag - xxAtlag);
            b = yAtlag - a * xAtlag;
        }
    }
    // Numerikusan, pontjaival megadott függvényt reprezentáló struktúra.
    public struct NumerikusFuggveny
    {
        // A numerikusan megadott függvényt függvénypontok alkotják
        public FuggvenyPont[] pontok;

        // Konstruktorok
        // Megadott X és Y vektorok alapján (összetartozó x és y értékek)
        public NumerikusFuggveny(double[] xErtekek, double[] yErtekek)
        {
            int hasznalhatoHossz = Math.Min(xErtekek.Length, yErtekek.Length);
            pontok = new FuggvenyPont[hasznalhatoHossz];
            for (int i = 0; i < hasznalhatoHossz; i++)
            {
                pontok[i].x = xErtekek[i];
                pontok[i].y = yErtekek[i];
            }
        }
        // Függvénypontok halmaza alapján
        public NumerikusFuggveny(FuggvenyPont[] pontok)
        {
            this.pontok = pontok;
        }

        // Tulajdonságok (property)
        // Pontok száma (csak olvasható)
        public int PontokSzama
        {
            get { return pontok.Length; }
        }
        // A független változó pontjainak átlaga (csak olvasható)
        public double XAtlag
        {
            get
            {
                double xOsszeg = 0.0;
                foreach (FuggvenyPont pont in pontok)
                {
                    xOsszeg += pont.x;
                }
                return xOsszeg / PontokSzama;
            }
        }
        // A függő változó pontjainak átlaga (csak olvasható) 
        public double YAtlag
        {
            get
            {
                double yOsszeg = 0.0;
                foreach (FuggvenyPont pont in pontok)
                {
                    yOsszeg += pont.y;
                }
                return yOsszeg / PontokSzama;
            }
        }
        // Metódusok 
        // Pontok listázása a konzolra 
        public void Konzolra()
        {
            foreach (FuggvenyPont pontok in pontok)
            {
                Console.WriteLine("x = {0:F5}\ty = {1:F5}", pontok.x, pontok.y);
            }
        }
        // Tesztfájl készítése, az y = a.x + b egyenestől véletlenszerűen eltérő pontokkal
        public static bool TesztFajl(string fajlNev, double a, double b)
        {
            bool iras = false;
            FileInfo fileInfo = new FileInfo(fajlNev);
            bool rejtett = ((fileInfo.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden);
            bool irasvedett = fileInfo.IsReadOnly;
            bool letezo = fileInfo.Exists;
            if (!letezo || (letezo && !irasvedett && !rejtett))
            {
                FileStream fajl = new FileStream(fajlNev, FileMode.Create, FileAccess.Write);
                StreamWriter fajlIro = new StreamWriter(fajl);
                for (int x = 0; x <= 100; x++)
                {
                    FuggvenyPont fp = FuggvenyPont.TesztPont(Convert.ToDouble(x), a, b);
                    fp.Fajlba(fajlIro, '|');
                }
                fajlIro.Close();
                fajl.Close();
                iras = true;
            }
            return iras;
        }
        // Function to load numeric points from text file. 
        public static NumerikusFuggveny Fajlbol(string fajlNev, out bool siker)
        {
            siker = false;
            NumerikusFuggveny eredmeny = new NumerikusFuggveny();
            if (File.Exists(fajlNev))
            {
                FileStream fajl = new FileStream(fajlNev, FileMode.Open, FileAccess.Read);
                StreamReader fajlOlvaso = new StreamReader(fajl);
                int meret = 0;
                FuggvenyPont[] pontok = new FuggvenyPont[meret];
                while (!fajlOlvaso.EndOfStream)
                {
                    Array.Resize(ref pontok, ++meret);
                    FuggvenyPont fp = FuggvenyPont.Fajlbol(fajlOlvaso, '|');
                    pontok[meret - 1] = fp;

                }
                fajlOlvaso.Close();
                fajl.Close();
                eredmeny = new NumerikusFuggveny(pontok);
                siker = true;
            }
            return eredmeny;
        }
    }

    // Függvény egy pontját reprezentáló struktúra.
    public struct FuggvenyPont
    {
        // Független változó
        public double x;
        // Függő változó
        public double y;

        // Véletlenszám-generátor
        static Random rnd = new Random();

        // Konstruktor
        public FuggvenyPont(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        //Metódusok
        // Pont fájlba írása.
        public void Fajlba(StreamWriter fajlIro, char elvalaszto)
        {
            fajlIro.WriteLine("{0:F5}" + elvalaszto.ToString() + "{1:F5}", x, y);
        }
        // Pont beolvasása fájlból.  
        public static FuggvenyPont Fajlbol(StreamReader fajlOlvaso, char elvalaszto)
        {
            FuggvenyPont pont = new FuggvenyPont();
            string sor = fajlOlvaso.ReadLine();
            string[] reszek = sor.Split(elvalaszto);
            double.TryParse(reszek[0], out pont.x);
            double.TryParse(reszek[1], out pont.y);
            return pont;
        }
        // Teszt pont létrehozása az y = a.x + b egyenestől való random eltéréssel.
        public static FuggvenyPont TesztPont(double x, double a, double b)
        {
            double y = a * x + b;
            // Maximum +-10 % eltérés
            double delta = (-0.1 + (0.2 * rnd.NextDouble())) * y;
            return new FuggvenyPont(x, y + delta);
        }
    }
}
