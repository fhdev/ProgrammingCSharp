﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Komplex
{
    // Komplex számokat kezelő struktúra 
    struct Komplex
    {
        // Adattagok
        // Valósrész
        private double valos;
        // Képzetes rész 
        private double kepzetes;

        // Konstruktorok
        // Valós és képzetes részek megadásával
        public Komplex(double valos, double kepzetes)
        {
            this.valos = valos;
            this.kepzetes = kepzetes;
        }

        // Másik komplex szám megadásával
        public Komplex(Komplex masik)
        {
            valos = masik.valos;
            kepzetes = masik.kepzetes;
        }

        // Tulajdonságok (property)
        // Komplex szám abszolútértékének kiszámítása (csak olvasható)
        public double AbszolutErtek
        {
            get { return Math.Sqrt(valos * valos + kepzetes * kepzetes); }
        }

        // Komplex szám fázisának kiszámítása (csak olvasható)
        public double Fazis
        {
            get { return Math.Atan(kepzetes / valos); }
        }

        // Komplex szám konjugáltjának kiszámítása (csak olvasható)
        public Komplex Konjugalt
        {
            get { return new Komplex(valos, -kepzetes); }
        }

        // Metódusok
        // Összeadás 
        public static Komplex Osszead(Komplex a, Komplex b)
        {
            return new Komplex(a.valos + b.valos, a.kepzetes + b.kepzetes);
        }

        // Kivonás
        public static Komplex Kivon(Komplex a, Komplex b)
        {
            return new Komplex(a.valos - b.valos, a.kepzetes - b.kepzetes);
        }

        // Szorzás
        public static Komplex Szoroz(Komplex a, Komplex b)
        {
            return new Komplex(a.valos * b.valos - a.kepzetes * b.kepzetes, a.valos * b.kepzetes + b.valos * a.kepzetes);
        }

        // Osztás
        public static Komplex Oszt(Komplex a, Komplex b)
        {
            Komplex c = new Komplex();
            if (b.AbszolutErtek != 0.0)
            {
                c = Szoroz(a, b.Konjugalt);
                c.valos /= Math.Pow(b.AbszolutErtek, 2.0);
                c.kepzetes /= Math.Pow(b.AbszolutErtek, 2.0);
            }
            return c;
        }

        // Beolvasás konzolról
        public static Komplex Beolvas(string nev)
        {
            Komplex variable = new Komplex();
            Console.WriteLine("Kérem adja meg a komplex szám értékét: " + nev + " .");
            do
                Console.Write("Re{" + nev + "} = ");
            while (!double.TryParse(Console.ReadLine(), out variable.valos));
            do
                Console.Write("Im{" + nev + "} = ");
            while (!double.TryParse(Console.ReadLine(), out variable.kepzetes));
            return variable;
        }

        // Konzolra írás
        public void Kiir(string nev)
        {
            Console.WriteLine(nev + " = {0:F5} + {1:F5}i", valos, kepzetes);
        }
        // Fájlba írás
        public void FajlbaIr(StreamWriter fajl, string nev)
        {
            if (fajl != null)
            {
                fajl.WriteLine(nev + " = {0:F5} + {1:F5}i", valos, kepzetes);
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            // Program céljának leírása 
            Console.WriteLine("Komplex algebrai műveletek elvégzése.");
            // Fájl megnyitása írásra, ha nem írásvédett
            string fajlNev = "log.txt";
            StreamWriter fajl = null;
            FileInfo fileInfo = new FileInfo(fajlNev);
            if (!fileInfo.Exists || !fileInfo.IsReadOnly)
            {
                fajl = new StreamWriter(fajlNev);
            }
            // Első szám beolvasása és fájlba írása
            Komplex a = Komplex.Beolvas("a");
            a.FajlbaIr(fajl, "a");
            // Második fájl beolvasása és fájlba írása
            Komplex b = Komplex.Beolvas("b");
            b.FajlbaIr(fajl, "b");
            Console.WriteLine("Results:");
            // Számítások elvégzése és eredmények kiírása a konolra és fájlba.
            Komplex c = Komplex.Osszead(a, b);
            c.Kiir("a + b");
            c.FajlbaIr(fajl, "a + b");
            c = Komplex.Kivon(a, b);
            c.Kiir("a - b");
            c.FajlbaIr(fajl, "a - b");
            c = Komplex.Szoroz(a, b);
            c.Kiir("a * b");
            c.FajlbaIr(fajl, "a * b");
            c = Komplex.Oszt(a, b);
            c.Kiir("a / b");
            c.FajlbaIr(fajl, "a / b");
            // Fájl bezárása, erőforrások felszabadítása.
            if (fajl != null)
            {
                fajl.Close();
                fajl.Dispose();
            }
        }


    }
}
