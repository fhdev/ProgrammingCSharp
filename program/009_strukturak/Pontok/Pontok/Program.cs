﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace points
{
    class Program
    {
        // Térbeli pontot leíró struktúra
        struct Pont
        {
            // X koordináta 
            public int X;
            // Y koordináta 
            public int Y;
            // Z koordináta 
            public int Z;
            // Csak olvasható tulajdonság
            public double Tavolsag
            {
                get { return Math.Sqrt(X * X + Y * Y + Z * Z); }
            }
        }

        static void Main(string[] args)
        {
            // Program céljának rövid leírása
            Console.WriteLine("Térbeli pontok generálása véletlenszerűen, " + 
                              "majd sorbarendezésük az origótól való távolság alapján.");
            // Véletlenszerűen generált pontok készítése
            Pont[] pontok = PontGeneralas(10);
            // A pontok megjelenítése a konzolon
            Console.WriteLine("Eredeti sorrend:");
            PontListazas(pontok);
            // Pontok sorbarendezése origótól való távolság szerint 
            PontRendezes(pontok);
            // A pontok megjelenítése a konzolon
            Console.WriteLine("Rendezett sorrend:");
            PontListazas(pontok);
        }

        // Térbeli pontok generálása véletlenszerűen 
        static Pont[] PontGeneralas(int n)
        {
            Pont[] result = new Pont[n];
            Random random = new Random();
            for (int i = 0; i < result.Length; i++)
            {
                result[i].X = random.Next(-100, 101);
                result[i].Y = random.Next(-100, 101);
                result[i].Z = random.Next(-100, 101);
            }
            return result;
        }

        // A térbeli pontok sorbarendezése origótól számított távolságuk alapján
        static void PontRendezes(Pont[] points)
        {
            for (int i = points.Length - 2; i >= 0; i--)
                for (int j = 0; j <= i; j++)
                {
                    if (points[j].Tavolsag > points[j + 1].Tavolsag)
                    {
                        Pont temp = points[j];
                        points[j] = points[j + 1];
                        points[j + 1] = temp;
                    }
                }
        }

        // A térbeli pontok kilisázása origótól számított távolságukkal együtt 
        static void PontListazas(Pont[] points)
        {
            for (int i = 0; i < points.Length; i++)
            {
                Console.WriteLine("[{0,4};{1,4};{2,4}]   D = {3,8:#0.000}", points[i].X, points[i].Y, points[i].Z, points[i].Tavolsag);
            }
        }
    }
}
