﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AutoNyilvantartas
{
    class Program
    {
        // Ebben a struktúrában egy autó adatait lehet eltárolni. Az adatok különböző típusúak is
        //  lehetnek, de logikailag összetartoznak.         
        struct Auto
        {
            // Az autó gyártója. 
            public string Gyarto;
            // Az autó típusa. 
            public string Tipus;
            // Az autó gyártási éve. 
            public int Ev;
            // Az autó ára. 
            public int Ar;
        }

        static void Main(string[] args)
        {
            // Ebben a Car típusú elemeket tartalmazó listában fogjuk majd tárolni a nyilvántartást. 
            List<Auto> Autok = new List<Auto>();
            // Ebben a változóban fogjuk tárolni a leütött billentyű adatait. 
            ConsoleKeyInfo billentyu = new ConsoleKeyInfo();
            // Ebben a változóban tároljuk el minden lépésben a felhasználó által választott
            // menüpontot.
            char valasztas = '\0';
            // A program addig fog futni, menüt kiírni és a menüből való választást kérni, amíg 
            // egyszer a kilépést nem választja a felhasználó. 
            do
            {
                // Kiírjuk a menüt. Minden menüpontnak egy-egy billentyű leütése fog megfelelni. 
                valasztas = Menu();
                switch (valasztas)
                {
                    // u - Új autó adatait olvassuk be a konzolról. 
                    case 'u':
                    {
                        Autok.Add(Beolvas());
                        break;
                    }
                    // m - Létező adatait módosítjuk
                    case 'm':
                    {
                        Modosit(Autok);
                        break;
                    }
                    // t - Létező autó adatait töröljük
                    case 't':
                    {
                        Torol(Autok);
                        break;
                    }
                    // l - A nyilvántartást kiírjuk a konzolra. 
                    case 'l':
                    {
                        Listaz(Autok);
                        break;
                    }
                    // r - Nyilvántartás rendezése 
                    case 'r':
                    {
                        Rendez(Autok);
                        break;
                    }
                    // b - Nyilvántartás betöltáse fájlból
                    case 'b':
                    {
                            FajlBetoltes("autok.csv", Autok);
                        break;
                    }
                    // f - Fájlba mentjük a nyilvántartást. 
                    case 'f':
                    {
                        FajlMentes("Autok.csv", Autok);
                        break;
                    }
                    // k - A program vége, kilépés. 
                    case 'k':
                    {
                        Console.Write("Kilépés...");
                        break;
                    }
                    // Ha a menüben megadottak közül egyik opciót sem választja a felhasználó, akkor 
                    // jelezzük számára, hogy választása érvénytelen. 
                    default:
                    {
                        Console.WriteLine("Nem létező opció. Kérem válasszon másikat!");
                        break;
                    }
                }
            // A ciklus az e azaz kilépés gomb megnyomásáig tart. 
            } while (valasztas != 'k'); 
        }

        // Ez a metódus egy adott feladat befejezése után, illetve a program kezdetekor kiírja a 
        // program menüjét.
        static char Menu()
        {
            Console.WriteLine("┌──────────────────────────────────────────────────────────────┐");
            Console.WriteLine("│                      AUTÓ NYILVÁNTARTÁS                      │");
            Console.WriteLine("│ OPCIÓK:                                                      │");
            Console.WriteLine("│         [U] Új autó adatainak megadása                       │");
            Console.WriteLine("│         [M] Létező autó adatainak módosítása                 │");
            Console.WriteLine("│         [T] Létező autó adatainak törlése                    │");
            Console.WriteLine("│         [L] Nyilvántartás képernyőre listázása               │");
            Console.WriteLine("│         [R] Nyilvántartás rendezése                          │");
            Console.WriteLine("│         [B] Mentett nyilvántartás betöltése fájlból          │");
            Console.WriteLine("│         [F] Aktuális nyilvántartás mentése fájlba            │");
            Console.WriteLine("│         [K] Kilépés                                          │");
            Console.WriteLine("└──────────────────────────────────────────────────────────────┘");
            // Ezt az esztétika kedvéért, hogy a betűk alatt villogjon a kurzor. 
            Console.Write("           ");
            // Beolvassuk a lenyomott billentyűt. 
            ConsoleKeyInfo billentyu = Console.ReadKey();
            // Beírunk két üres sort az esztétika kedvéért. 
            Console.WriteLine("\n");
            // A lenyomott billentyűtől függően fogunk a menünek megfelelő feladatokat 
            // végrehajtani. 
            return char.ToLower(billentyu.KeyChar);

        }

        // Ez a metódus beolvassa egy autó adatait a konzolról.
        static Auto Beolvas()
        {
            // Ebben a Car (autó struktúra) típusú változóban fogjuk tárolni a beolvasott adatokat. 
            Auto auto;
            Console.Write("Kérem adja meg az autó gyártóját!\n\tGyártó:      ");
            auto.Gyarto = Console.ReadLine();
            Console.Write("Kérem adja meg az autó típusát!\n\tTípus:       ");
            auto.Tipus = Console.ReadLine();
            Console.Write("Kérem adja meg az autó gyártási évét!\n\tGyártási év: ");
            int.TryParse(Console.ReadLine(), out auto.Ev);
            Console.Write("Kérem adja meg az autó árát!\n\tÁr:          ");
            int.TryParse(Console.ReadLine(), out auto.Ar);
            Console.WriteLine();
            return auto;
        }

        // Ez a metódus kiírja a konzolra a neki paraméterként átadott Auto (autó struktúra) típusú 
        // elemeket tartalmazó List (lista) minden autójához tartozó adatokat. 
        static void Listaz(List<Auto> autok)
        {
            // Ha az autókat tartalmazó listának egyetlen eleme sincsen, akkor ezt jelezzük a 
            // felhasználónak és visszatérünk a programból, hiszen nincs mit kiírni. 
            if (autok.Count == 0)
            {
                Console.WriteLine("A nyilvántartás jelenleg üres.\n");
            }
            else
            {
                Console.WriteLine("┌──────────────────────────────────────────────────────────────┐");
                Console.WriteLine("│ A nyilvántartás jelenlegi tartalma:                          │");
                Console.WriteLine("├──────────────────────────────────────────────────────────────┤");
                Console.WriteLine("│{0,-6}{1,-14}{2,-14}{3,-14}{4,-14}│", "Index", "Gyártó", "Típus", "Év", "Ár");
                Console.WriteLine("├──────────────────────────────────────────────────────────────┤");
                // A foreach ciklus végigmegy lista minden elemén. Az aktuális elem az auto
                // változóba kerül. 
                foreach (Auto auto in autok)
                {
                    // Az aktuális autó minden adatát kiírjuk a konzolra.
                    Console.WriteLine("│{0,-6}{1,-14}{2,-14}{3,-14}{4,-14}│",
                                      autok.IndexOf(auto),
                                      auto.Gyarto,
                                      auto.Tipus,
                                      auto.Ev,
                                      auto.Ar);
                    Console.WriteLine("├──────────────────────────────────────────────────────────────┤");
                }
                Console.WriteLine("│ Nyomjon le egy gombot a folytatáshoz.                        │");
                Console.WriteLine("└──────────────────────────────────────────────────────────────┘");
                Console.ReadKey();
                Console.WriteLine();
            }
        }

        // Ez a metódus a nyilvántartásban szereplő összes autót (értsd az Autok lista minden elemét) 
        // egy CSV formátumú fájlba menti. Ezt a formátumot pl. az Excel is képes megnyitni.
        static bool FajlMentes(string fajlNev, List<Auto> autok)
        {
            // Ebben a változóban tároljuk el, hogy történt-e ténylegesen fájlba írás. Ez a vissza-
            // térési érték is.
            bool iras = false;
            // Ha az autókat tartalmazó listának egyetlen eleme sincsen, akkor ezt jelezzük a 
            // felhasználónak.
            if (autok.Count == 0)
            {
                Console.WriteLine("A nyilvántartás jelenleg üres.");
                Console.WriteLine("A(z) \"" + fajlNev + "\" fájlt nem mentettük el.\n");
            }
            else
            {
                // Ellenőrizzük, hogy a megadott fájl már létezik-e. Ha létezik, akkor megkérdezzük 
                // a felhasználót, hogy szeretné-e felülírni azt. 
                iras = true;
                if (File.Exists(fajlNev))
                {
                    Console.WriteLine("A(z) \"" + fajlNev + "\" fájl már létezik. Felülírjuk?");
                    Console.Write("Nyomja meg az [I] billentyűt a megerősítéshez. ");
                    ConsoleKeyInfo billentyu = Console.ReadKey();
                    Console.WriteLine();
                    // Ha a felhasználó azt választotta, hogy nem írja felül a fájlt, akkor 
                    // tájékoztatjuk, hogy a fájl nem került mentésre, majd visszatérünk a 
                    // függvényből hamis értékkel. 
                    if (char.ToLower(billentyu.KeyChar) != 'i')
                    {
                        Console.WriteLine("A(z) \"" + fajlNev + "\" fájlt nem mentettük el.\n");
                        iras = false;
                    }
                }
                if (iras)
                {
                    // Ez az ún. StreamWriter objektum fogja számunkra elvégezni a fájlba írást. 
                    StreamWriter fajl = new StreamWriter(fajlNev);
                    // Ebbe a string változóba fogjuk belemásolni a szöveges fájl teljes tartalmát. 
                    string tartalom = "Index;Gyártó;Típus;Gyártási év;Ár\n";
                    // A foreach ciklus végigmegy a lista minden elemén.
                    foreach (Auto auto in autok)
                    {
                        // A string.Concat metódus összefűzi a neki paraméterként átadott stringeket 
                        tartalom = string.Concat(tartalom,
                                                 autok.IndexOf(auto).ToString(),
                                                 ";",
                                                 auto.Gyarto,
                                                 ";",
                                                 auto.Tipus,
                                                 ";",
                                                 auto.Ev.ToString(),
                                                 ";",
                                                 auto.Ar.ToString(),
                                                "\n");
                    }
                    // A teljes tartalmat a fájlba írjuk. 
                    fajl.Write(tartalom);
                    // Bezárjuk a megnyitott fájlt. Ez a művelet nagyon fontos, ennek elmulasztása 
                    // esetén a fájl nem jön létre!
                    fajl.Close();
                    // A fájl írásához használt gépi errőforrásokat is fel kell szabadítani, ezt itt
                    // tesszük meg. 
                    fajl.Dispose();
                    // Tájékoztatjuk a felhasnálót a fájlba írás sikerességéről. 
                    Console.WriteLine("A(z) \"" + fajlNev + "\" fájlt sikeresen elmentettük.\n");
                }
            }
            return iras;
        }

        // Ez a metódus egy korábbi mentésből betölti a nyilvántartást a memóriába, azaz a fájlből 
        // feltölti a listát.
        static bool FajlBetoltes(string fajlNev, List<Auto> autok)
        {
            bool olvasas = false;
            // Ha a megadott fájl nem létezik, akkor ezt jelezzük a felhasználónak és visszatérünk 
            // hamis értékkel. 
            if (!File.Exists(fajlNev))
            {
                Console.WriteLine("A(z) \"" + fajlNev + "\" fájl nem található.");
                Console.WriteLine("Nem történt beolvasás.\n");
            }
            else
            {

                // Ha a listában már vannak elemek, megkérdezzük a felhasználót, hogy szeretné-e felülírni azokat. 
                if (autok.Count != 0)
                {
                    Console.WriteLine("A nyilvántartás jelenleg tartalmaz elemeket.");
                    Console.WriteLine("Kérem nyomja meg az [F] billentyűt a felülíráshoz, vagy a [H]");
                    Console.WriteLine("billentyűt a fájl tartalmának a jelenlegi nyilvántatáshoz való");
                    Console.Write("hozzáfűzéséhez. ");
                    ConsoleKeyInfo keyInfo = Console.ReadKey();
                    Console.WriteLine();
                    // Ha a felhasználó a felülírást választotta, akkor a lista minden elemét
                    // töröljük. 
                    if (char.ToLower(keyInfo.KeyChar) == 'o')
                    {
                        autok.Clear();
                    }
                }
                // Ez az ún. StreamReader objektum fogja számunkra elvégezni a fájlból olvasást. 
                StreamReader fajl = new StreamReader(fajlNev);
                // A fájl teljes tartalmát beolvassuk a content nevű változóba. 
                string tartalom = fajl.ReadToEnd();
                // A fájl teljes tartalmát sorokra bontjuk fel a Split metódus segítségével. 
                string[] sorok = tartalom.Split('\n');
                // Emlékszünk, hogy a fájl első sora a fejléc volt. A fájl utolsó sora pedig minden 
                // esetben egy üres string. Az első hasznos adatokat tartalmazó sor tehát a tömb 
                // második, míg az utolsó a tömb utolsó előtti eleme. 
                for (int i = 1; i < sorok.Length - 1; i++)
                {
                    // A fájl adott sorát az előzőekhez hasonlóan szintén felbontjuk, hiszen a 
                    // hasznos adataink a ';' karakterek között voltak. 
                    string[] mezok = sorok[i].Split(';');
                    Auto ujAuto;
                    // Az első kiírt információ az autó indexe volt, erre most nincs szükségünk. 
                    // A gyártó információ volt a 2. kiírt információ, azaz a megfelelő index az 1. 
                    ujAuto.Gyarto = mezok[1];
                    ujAuto.Tipus = mezok[2];
                    // Az egész szám értékeket át is kell konvertálnunk. 
                    ujAuto.Ev = Convert.ToInt32(mezok[3]);
                    ujAuto.Ar = Convert.ToInt32(mezok[4]);
                    // A beolvasott autót hozzáadjuk a listához. 
                    autok.Add(ujAuto);
                }
                // Bezárjuk a megnyitott fájlt. Ez a művelet nagyon fontos, ennek elmulasztása 
                // esetén a fájl nem szabadul fel, és legközelebb már nem tudunk bele írni, és nem
                // tudjuk olvasni sem!
                fajl.Close();
                // A fájl írásához használt gépi errőforrásokat is fel kell szabadítani, ezt itt 
                // tesszük meg. 
                fajl.Dispose();
                // Tájokoztatjuk a felhasználót a sikeres beolvasásról. 
                Console.WriteLine("A(z) \"" + fajlNev + "\" fájl tartalmát sikeresen beolvastuk.\n");
                // Igaz értékkel térünk vissza, jelezve ezzel a művelet sikerességét. 
                olvasas = true;
            }
            return olvasas;
        }

        // Ez a metódus módosítja az autókat tartalmazó lista egy a felhasználó által megadott 
        // elemét.
        static bool Modosit(List<Auto> autok)
        {
            // Bekérjük a felhasználótól, hogy melyik indexű (hanyadik) autó adatait szeretné
            // módosítani. 
            Console.WriteLine("Kérem adja meg a módosítandó autó sorszámát.");
            int index;
            bool ervenyes = int.TryParse(Console.ReadLine(), out index);
            // Ebben a változóban tároljuk, hogy történik-e valójában módosítás.
            bool modositas = false;
            // Ha a megadott index negatív, vagy pedig nagyobb mint a létező legnagyobb index, akkor
            // jelezzük az érvénytelenségét, és hamis értékkel térünk vissza. 
            if ((index < 0) || (index >= autok.Count) || !ervenyes)
            {
                Console.WriteLine("A megadott index nem érvényes. Nem történt módosítás.\n");
            }
            else
            {
                // Az ujAuto változóba beolvassuk az új adatokat. 
                Auto ujAuto = Beolvas();
                // A listában szereplő régi autót az újra cseréljük. 
                autok[index] = ujAuto;
                // Jelezzük a felhasználónak a módosítás sikerességét. 
                Console.WriteLine("A(z) {0:D}. autó adatait sikeresen módosítottuk.\n", index);
                // Visszatérési értékben is jelezzük a módosítást.
                modositas = true;
            }
            // Viszatérünk a megfelelő logikai értékkel.
            return modositas;
        }

        // Ez a metódus törli az autókat tartalmazó lista egy a felhasználó által megadott elemét.
        static bool Torol(List<Auto> autok)
        {
            // Bekérjük a felhasználótól, hogy melyik indexű (hanyadik) autó adatait szeretné
            // módosítani. 
            Console.WriteLine("Kérem adja meg a módosítandó autó sorszámát.");
            int index;
            bool ervenyes = int.TryParse(Console.ReadLine(), out index);
            // Ebben a változóban tároljuk, hogy történik-e valójában törlés.
            bool torles = false;
            // Ha a megadott index negatív, vagy pedig nagyobb mint a létező legnagyobb index, akkor
            // jelezzük az érvénytelenségét, és hamis értékkel térünk vissza. 
            if ((index < 0) || (index >= autok.Count) || !ervenyes)
            {
                Console.WriteLine("A megadott index nem érvényes. Nem történt törlés.\n");
            }
            else
            {
                autok.RemoveAt(index);
                // Jelezzük a felhasználónak a módosítás sikerességét. 
                Console.WriteLine("A(z) {0:D}. autó adatait sikeresen töröltük.\n", index);
                // Visszatérési értékben is jelezzük a törlést.
                torles = true;
            }
            // Viszatérünk a megfelelő logikai értékkel.
            return torles;
        }

        // Ez a metódus növekvő sorrendbe rendezi a neki átadott autókat tartalmazó lista elemeit a 
        // felhasználó által megadott tulajdonság alapján.
        static bool Rendez(List<Auto> autok)
        {
            // Ha az autókat tartalmazó listának egyetlen eleme sincsen, akkor ezt jelezzük a 
            // felhasználónak és visszatérünk
            bool rendez = false;
            if (autok.Count == 0)
            {
                Console.WriteLine("A nyilvántartás jelenleg üres.");
                Console.WriteLine("Nem történt rendezés.\n");
            }
            else
            {
                // Feltesszük, hogy lesz rendezés.
                rendez = true;
                // Beolvassuk a konzolról, hogy a felhasználó melyik tulajdonság alapján kíván sorbarendezni. 
                Console.WriteLine("Kérem adja meg, melyik tulajdonság szerint szeretne rendezni.");
                Console.Write(">> ");
                // A megadott tulajdonság alapján elvégezzük a sorbarendezést. 
                switch (Console.ReadLine().ToLower())
                {
                    case "gyártó":
                        {
                            // A sort metódusnak egy olyan függvényt kell megadni, ami két autó közül eldönti, hogy melyik
                            // a "nagyobb", hogy sorba tudja rendezni őket. A CompareCarsByManufacturer metódus gyártó
                            // szerint hasonlítja össze az autókat. 
                            autok.Sort(OsszehasonlitGyartot);
                            break;
                        }
                    case "típus":
                        {
                            // A CompareCarsByManufacturer metódus típus szerint hasonlítja össze az autókat. 
                            autok.Sort(OsszehasonlitTipust);
                            break;
                        }
                    case "év":
                        {
                            // A CompareCarsByManufacturer gyártási év szerint hasonlítja össze az autókat. 
                            autok.Sort(OsszehasonlitEvet);
                            break;
                        }
                    case "ár":
                        {
                            // A CompareCarsByManufacturer ár szerint hasonlítja össze az autókat. 
                            autok.Sort(OsszehasonlitArat);
                            break;
                        }
                    // Ha a felhasználó nem egy létező tulajdonság nevét írta be, akkor erről tájékoztatjuk, és hamis
                    // értékkel térünk vissza, hiszen nem történt sorbarendezés. 
                    default:
                        {
                            Console.WriteLine("A megadott név nem egy valós autó tulajdonság.");
                            Console.WriteLine("Nem történt rendezés.\n");
                            rendez = false;
                            break;
                        }
                }
                // Tájékoztatjuk a felhasználót a sikeres rendezésről.
                if (rendez)
                {
                    Console.WriteLine("A rendezés sikeresen megtörtént.\n");
                }
            }            
            return rendez;
        }

        // Ez a metódus összehasonlít két autót a gyártója alapján.
        static int OsszehasonlitGyartot(Auto egyik, Auto masik)
        {
            // < 0, ha egyik.Gyarto előbb szerepel az ABC-ben mint masik.Gyarto
            //   0, ha egyik.Gyarto megegyezik masik.Gyarto-val
            // > 0, ha masik.Gyarto később szerepel az ABC-ben mint egyik.Gyarto
            return egyik.Gyarto.CompareTo(masik.Gyarto);
        }

        // Ez a metódus összehasonlít két autót a típusa alapján.
        static int OsszehasonlitTipust(Auto egyik, Auto masik)
        {
            // < 0, ha egyik.Tipus előbb szerepel az ABC-ben mint masik.Tipus
            //   0, ha egyik.Tipus megegyezik masik.Tipus-sal
            // > 0, ha masik.Tipus később szerepel az ABC-ben mint egyik.Tipus
            return egyik.Tipus.CompareTo(masik.Tipus);
        }

        // Ez a metódus összehasonlít két autót a gyártási éve alapján.
        static int OsszehasonlitEvet(Auto egyik, Auto masik)
        {
            // < 0, ha egyik.Ev < masik.Ev
            //   0, ha egyik.Ev == masik.Ev
            // > 0, ha masik.Ev < egyik.Ev
            return egyik.Ev.CompareTo(masik.Ev);
        }

        // Ez a metódus összehasonlít két autót az ára alapján.
        static int OsszehasonlitArat(Auto egyik, Auto masik)
        {
            // < 0, ha egyik.Ar < masik.Ar
            //   0, ha egyik.Ar == masik.Ar
            // > 0, ha masik.Ar < egyik.Ar
            return egyik.Ar.CompareTo(masik.Ar);
        }
    }
}
