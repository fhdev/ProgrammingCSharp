﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Tartaly
{
    class Tartaly
    {
        private double hosszusag;
        private double szelesseg;
        private double magassag;

        private double terfogat;

        public double Terfogat
        {
            get { return terfogat; }
        }

        public double MaxTerfogat
        {
            get { return hosszusag * szelesseg * magassag; }
        }

        public Tartaly(double hosszusag, double szelesseg, double magassag)
        {
            this.hosszusag = hosszusag;
            this.szelesseg = szelesseg;
            this.magassag = magassag;
            terfogat = 0.0;
        }

        public bool Tolt(double terfogatAram, double ido)
        {
            double ujTerfogat = terfogat + terfogatAram * ido;
            if (ujTerfogat <= MaxTerfogat)
            {
                terfogat = ujTerfogat;
                return false;
            }
            else
            {
                terfogat = MaxTerfogat;
                return true;
            }
        }

        public bool Urit(double terfogatAram, double ido)
        {
            double ujTerfogat = terfogat - terfogatAram * ido;
            if (ujTerfogat >= 0)
            {
                terfogat = ujTerfogat;
                return false;
            }
            else
            {
                terfogat = 0;
                return true;
            }
        }

        public void Rajzol(Panel panel, Graphics eszkoz, int meter2Pixel)
        {
            Brush ecset = new SolidBrush(Color.LightBlue);
            Pen toll = new Pen(Color.Black, 2.0f);
            int szelessegPixel = (int)(szelesseg * meter2Pixel);
            int magassagPixel = (int)(magassag * meter2Pixel);
            int tartalomMagassagPixel = (int)(terfogat / (szelesseg * hosszusag) * meter2Pixel);
            Rectangle folyadek = new Rectangle(1,
                panel.ClientSize.Height - tartalomMagassagPixel - 1,
                szelessegPixel,
                magassagPixel);
            eszkoz.FillRectangle(ecset, folyadek);
            Rectangle tartaly = new Rectangle(1,
                panel.ClientSize.Height - magassagPixel - 1,
               szelessegPixel,
               magassagPixel);
            eszkoz.DrawRectangle(toll, tartaly);
        }

    }
}
