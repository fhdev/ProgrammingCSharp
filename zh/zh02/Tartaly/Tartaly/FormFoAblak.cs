﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Reflection;

namespace Tartaly
{
    public partial class FormFoAblak : Form
    {
        private const int meter2Pixel = 50;

        private Tartaly tartaly;

        private Stopwatch stopper;

        long elteltIdo;

        public FormFoAblak()
        {
            InitializeComponent();
            typeof(Panel).InvokeMember("DoubleBuffered",
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.SetProperty,
                null,
                panelRajz,
                new object[] { true });
            tartaly = new Tartaly(8, 8, 6);
            stopper = new Stopwatch();
            timerTolt.Start();
            stopper.Start();
            elteltIdo = 0;
        }

        private void panelRajz_Paint(object sender, PaintEventArgs e)
        {
            tartaly.Rajzol(panelRajz, e.Graphics, meter2Pixel);
        }

        private void timerTolt_Tick(object sender, EventArgs e)
        {
            double ido = (stopper.ElapsedMilliseconds - elteltIdo) / 1000.0;
            if (buttonTolt.BackColor == Color.LawnGreen)
            {
                double terfogatAram;
                double.TryParse(textBoxTolt.Text, out terfogatAram);
                bool tele = tartaly.Tolt(terfogatAram, ido);
                if (tele)
                {
                    buttonTolt.BackColor = Color.Red;
                }
            }
            if (buttonUrit.BackColor == Color.LawnGreen)
            {
                double terfogatAram;
                double.TryParse(textBoxUrit.Text, out terfogatAram);
                bool ures = tartaly.Urit(terfogatAram, ido);
                if (ures)
                {
                    buttonUrit.BackColor = Color.Red;
                }
            }
            elteltIdo = stopper.ElapsedMilliseconds;
            panelRajz.Invalidate();
            textBoxTerfogat.Text = string.Format("{0:F3} m^3", tartaly.Terfogat);
            textBoxTerfogatSzazalek.Text = string.Format("{0:F3} %", tartaly.Terfogat / tartaly.MaxTerfogat * 100);
        }

        private void buttonToltUrit_Click(object sender, EventArgs e)
        {
            if((sender as Button).BackColor == Color.Red)
            {
                (sender as Button).BackColor = Color.LawnGreen;
            }
            else
            {
                (sender as Button).BackColor = Color.Red;
            }
        }
    }
}
