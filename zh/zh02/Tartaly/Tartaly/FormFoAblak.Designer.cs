﻿namespace Tartaly
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelRajz = new System.Windows.Forms.Panel();
            this.buttonTolt = new System.Windows.Forms.Button();
            this.buttonUrit = new System.Windows.Forms.Button();
            this.timerTolt = new System.Windows.Forms.Timer(this.components);
            this.textBoxTerfogat = new System.Windows.Forms.TextBox();
            this.textBoxTerfogatSzazalek = new System.Windows.Forms.TextBox();
            this.textBoxTolt = new System.Windows.Forms.TextBox();
            this.textBoxUrit = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // panelRajz
            // 
            this.panelRajz.BackColor = System.Drawing.Color.White;
            this.panelRajz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelRajz.Location = new System.Drawing.Point(10, 121);
            this.panelRajz.Name = "panelRajz";
            this.panelRajz.Size = new System.Drawing.Size(404, 304);
            this.panelRajz.TabIndex = 0;
            this.panelRajz.Paint += new System.Windows.Forms.PaintEventHandler(this.panelRajz_Paint);
            // 
            // buttonTolt
            // 
            this.buttonTolt.BackColor = System.Drawing.Color.Red;
            this.buttonTolt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonTolt.Location = new System.Drawing.Point(11, 12);
            this.buttonTolt.Name = "buttonTolt";
            this.buttonTolt.Size = new System.Drawing.Size(75, 41);
            this.buttonTolt.TabIndex = 1;
            this.buttonTolt.Text = "Tölt";
            this.buttonTolt.UseVisualStyleBackColor = false;
            this.buttonTolt.Click += new System.EventHandler(this.buttonToltUrit_Click);
            // 
            // buttonUrit
            // 
            this.buttonUrit.BackColor = System.Drawing.Color.Red;
            this.buttonUrit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonUrit.Location = new System.Drawing.Point(10, 66);
            this.buttonUrit.Name = "buttonUrit";
            this.buttonUrit.Size = new System.Drawing.Size(75, 41);
            this.buttonUrit.TabIndex = 2;
            this.buttonUrit.Text = "Ürít";
            this.buttonUrit.UseVisualStyleBackColor = false;
            this.buttonUrit.Click += new System.EventHandler(this.buttonToltUrit_Click);
            // 
            // timerTolt
            // 
            this.timerTolt.Interval = 20;
            this.timerTolt.Tick += new System.EventHandler(this.timerTolt_Tick);
            // 
            // textBoxTerfogat
            // 
            this.textBoxTerfogat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxTerfogat.Location = new System.Drawing.Point(312, 19);
            this.textBoxTerfogat.Name = "textBoxTerfogat";
            this.textBoxTerfogat.ReadOnly = true;
            this.textBoxTerfogat.Size = new System.Drawing.Size(100, 26);
            this.textBoxTerfogat.TabIndex = 3;
            this.textBoxTerfogat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxTerfogatSzazalek
            // 
            this.textBoxTerfogatSzazalek.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxTerfogatSzazalek.Location = new System.Drawing.Point(312, 73);
            this.textBoxTerfogatSzazalek.Name = "textBoxTerfogatSzazalek";
            this.textBoxTerfogatSzazalek.ReadOnly = true;
            this.textBoxTerfogatSzazalek.Size = new System.Drawing.Size(100, 26);
            this.textBoxTerfogatSzazalek.TabIndex = 4;
            this.textBoxTerfogatSzazalek.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxTolt
            // 
            this.textBoxTolt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxTolt.Location = new System.Drawing.Point(101, 19);
            this.textBoxTolt.Name = "textBoxTolt";
            this.textBoxTolt.Size = new System.Drawing.Size(100, 26);
            this.textBoxTolt.TabIndex = 5;
            this.textBoxTolt.Text = "50";
            this.textBoxTolt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxUrit
            // 
            this.textBoxUrit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxUrit.Location = new System.Drawing.Point(101, 73);
            this.textBoxUrit.Name = "textBoxUrit";
            this.textBoxUrit.Size = new System.Drawing.Size(100, 26);
            this.textBoxUrit.TabIndex = 6;
            this.textBoxUrit.Text = "30";
            this.textBoxUrit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 436);
            this.Controls.Add(this.textBoxUrit);
            this.Controls.Add(this.textBoxTolt);
            this.Controls.Add(this.textBoxTerfogatSzazalek);
            this.Controls.Add(this.textBoxTerfogat);
            this.Controls.Add(this.buttonUrit);
            this.Controls.Add(this.buttonTolt);
            this.Controls.Add(this.panelRajz);
            this.Name = "FormFoAblak";
            this.Text = "Tartály";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelRajz;
        private System.Windows.Forms.Button buttonTolt;
        private System.Windows.Forms.Button buttonUrit;
        private System.Windows.Forms.Timer timerTolt;
        private System.Windows.Forms.TextBox textBoxTerfogat;
        private System.Windows.Forms.TextBox textBoxTerfogatSzazalek;
        private System.Windows.Forms.TextBox textBoxTolt;
        private System.Windows.Forms.TextBox textBoxUrit;
    }
}

