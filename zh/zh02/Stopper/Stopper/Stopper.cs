﻿using System;
using System.Drawing;

namespace Stopper
{
    class Stopper
    {
        /* Milliszekundum - szekundum átváltás szorzója. */
        private const double millisecond2Second = 0.001;
        /* Szekundum - fok átváltás szorzója. */
        private const double second2Rad = Math.PI / 30.0;
        /* A mutató szélessége. */
        private const double pointerWidth = 3.0;
        /* A számlap sugarának és a panel oldalhosszúságának aránya. */
        private const double dialClientRatio = 0.475;

        private double time;
        /* A panel oldalhosszúsága, amire a stoppert rajzolni akarjuk. */
        private int clientSize;
        /* A számlap rajzolásához használt szín. */
        private Color backColor;
        /* A mutató rajzolásához használt szín. */
        private Color foreColor;
        /* Ez a Random objektum használatos a véletlenszám-generáláshoz. */
        private static Random randomGenerator = new Random();
        /* Ez a csak olvasható tulajdonság meghatározza a stopper számlapjának sugarát a panel oldalhosszának 
         * arányában. */
        private double dialRadius
        {
            get
            {
                return (double)clientSize * dialClientRatio;
            }
        }
        /// <summary>
        /// Ez a Stopper osztály konstruktora. Új példányt (objektumot) hoz létre az osztályból. 
        /// </summary>
        /// <param name="initialTime">Az idő kezdeti értéke.</param>
        /// <param name="panelSize">A panel oldalhosszúsága, amire a stoppert kirajzolni kívánjuk.</param>
        public Stopper(double initialTime, int panelSize)
        {
            time = initialTime;
            clientSize = panelSize;
            /* Háttérszín sorsolás a piros egy árnyalataként. */
            int R = randomGenerator.Next(150, 256);
            int GB = randomGenerator.Next(0, 30);
            backColor = Color.FromArgb(R, GB, GB);
            /* Előtérszín sorsolás a kék egy árnyalataként. */
            int RG = randomGenerator.Next(0, 30);
            int B = randomGenerator.Next(150, 256);
            foreColor = Color.FromArgb(RG, RG, B);
        }
        /// <summary>
        /// Ez a metódus a neki átadott időintervallum értékével növeli az eltelt idő értékét.
        /// </summary>
        /// <param name="tickIntervalms">Az eltelt időintervallum millisecundum mértékegységben.</param>
        public void Tick(int tickIntervalms)
        {
            double tickTime = (double)tickIntervalms * millisecond2Second;
            time += tickTime;
        }
        /// <summary>
        /// Ez a metódus visszaállítja az eltelt időt számláló változót.
        /// </summary>
        public void Reset()
        {
            time = 0.0;
        }
        /// <summary>
        /// Ez a metódus kiszámolja a másodpercmutató végpontjának koordinátáit.
        /// </summary>
        /// <returns>A másodpercmutató végpontjának koordinátái.</returns>
        private Point CalculateSecondhandPosition()
        {
            /* Az adott másodpercnek megfelelő szög radiánban. */
            double angle =  time * second2Rad;
            /* A másodpercmutató végpontjának koordinátái. */
            int posX = clientSize / 2 + (int)(dialRadius * Math.Sin(angle));
            int posY = clientSize / 2 - (int)(dialRadius * Math.Cos(angle));
            /* Visszatérés a végpont koordinátáit tartalmazó pont objektummal. */
            return new Point(posX, posY);
        }

        public void Draw(Graphics graphics)
        {  
            /* Előzőleg felrajzolt állás törlése fehér színnel. */
            graphics.Clear(Color.White);
            /* Számlap megrajzolása. */
            SolidBrush brush = new SolidBrush(backColor);
            Point dialPosition = new Point(clientSize / 2 - (int)dialRadius, clientSize / 2 - (int)dialRadius);
            Size dialSize = new Size(2 * (int)dialRadius, 2 * (int)dialRadius);
            Rectangle dial = new Rectangle(dialPosition, dialSize);
            graphics.FillEllipse(brush, dial);
            /* Mutató megrajzolása. */
            Pen pen = new Pen(foreColor, (float)pointerWidth);
            Point markerPoint = CalculateSecondhandPosition();
            Point centerPoint = new Point(clientSize / 2, clientSize / 2);
            graphics.DrawLine(pen, centerPoint, markerPoint);
            /* Rajzoláshoz használt erőforrások felszabadítása. */
            pen.Dispose();
            brush.Dispose();
        }

        public string TimeToString()
        {
            /* Az aktuális időt X:XX s formátumban szövegként adjuk vissza. */
            return string.Format("{0:F2} s", time);
        }
    }
}
