﻿using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace Stopper
{
    public partial class FormMain : Form
    {
        private Stopper stopperDrawer;
        public FormMain()
        {
            InitializeComponent();
            /* A panel kétszeres pufferelése a vibrálás elkerülésének érdekében. */
            typeof(Panel).InvokeMember("DoubleBuffered",
                           BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                           null,
                           panelMain,
                           new object[] { true });
            stopperDrawer = new Stopper(0.0, panelMain.Width);
        }
        /// <summary>
        /// Ez a metótus akkor fog lefutni, ha a rajzoláshoz használt Panel-t újra kell rajzolni.
        /// </summary>
        /// <param name="sender">A metódus küldő objektuma.</param>
        /// <param name="e">Az eseménnyel kapcsolatos argumentumok gyűjtő objektuma.</param>
        private void panelMain_Paint(object sender, PaintEventArgs e)
        {
            /* Felrajzoljuk a stoppert a metódus argumentumában kapott grafikus objektummal. */
            stopperDrawer.Draw(e.Graphics);
        }
        /// <summary>
        /// Ez a metódus az időt mérő Timer kattanásaira fog lefutni. 
        /// </summary>
        /// <param name="sender">A metódus küldő objektuma.</param>
        /// <param name="e">Az eseménnyel kapcsolatos argumentumok gyűjtő objektuma.</param>
        private void timerMain_Tick(object sender, System.EventArgs e)
        {
            /* Növeljük a stopperben az eltelt idő értékét a timer invervallumának megfelelő értékkel. */
            stopperDrawer.Tick(timerMain.Interval);
            /* Kiírjuk az erre létrehozott Label-re az aktuális időértéket. */
            labelTime.Text = stopperDrawer.TimeToString();
            /* Újrarajzoltatjuk a stoppert a Panel érvényteleítésével. */
            panelMain.Invalidate();
        }
        /// <summary>
        /// Ez a metódus a Start gombra kattintáskor fog lefutni. 
        /// </summary>
        /// <param name="sender">A metódus küldő objektuma.</param>
        /// <param name="e">Az eseménnyel kapcsolatos argumentumok gyűjtő objektuma.</param>
        private void buttonStart_Click(object sender, System.EventArgs e)
        {
            /* Elindítjuk a Timer-t. */
            timerMain.Start();
        }
        /// <summary>
        /// Ez a metódus a Stop gombra kattintáskor fog lefutni. 
        /// </summary>
        /// <param name="sender">A metódus küldő objektuma.</param>
        /// <param name="e">Az eseménnyel kapcsolatos argumentumok gyűjtő objektuma.</param>
        private void buttonStop_Click(object sender, System.EventArgs e)
        {
            /* Leállítjuk a Timer-t. */
            timerMain.Stop();
        }
        /// <summary>
        /// Ez a metódus a Reset gombra kattintáskor fog lefutni.
        /// </summary>
        /// <param name="sender">A metódus küldő objektuma.</param>
        /// <param name="e">Az eseménnyel kapcsolatos argumentumok gyűjtő objektuma.</param>
        private void buttonReset_Click(object sender, System.EventArgs e)
        {
            /* Visszaállítjuk a Stopper-ben az eltelt időt. */
            stopperDrawer.Reset();
            /* Frissítjük az idő kiírását a Label-en.*/
            labelTime.Text = stopperDrawer.TimeToString();
            /* Újrarajzoltatjuk a stoppert a rajzoláshoz használt Panel érvénytelenítésével. */
            panelMain.Invalidate();
        }
    }
}
