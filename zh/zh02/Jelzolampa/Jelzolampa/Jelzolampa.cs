﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jelzolampa
{
    class Jelzolampa
    {
        private const int lampaSzam = 3;

        private const int piros = 0;
        private const int pirosSarga = 1;
        private const int zold = 2;
        private const int sarga = 3;
        private const int sargaVillogo = 4;
        private const int nemVilagit = 5;

        private int meret;
        private int allapot;

        private Pen toll;
        private SolidBrush ecset;
        Rectangle[] lampak;
        Color[] szinek;

                        
        private Stopwatch stopper;

        public Jelzolampa(int panelSzelesseg)
        {
            meret = panelSzelesseg;
            allapot = piros;
            toll = new Pen(Color.Black, 5);
            ecset = new SolidBrush(Color.Gray);
            lampak = new Rectangle[lampaSzam];
            for (int i = 0; i < lampaSzam; i++)
                lampak[i] = new Rectangle(0, i * meret, meret, meret);
            szinek = new Color[lampaSzam];
            stopper = Stopwatch.StartNew();
        }

        public bool Uzemmod
        {
            set
            {
                if (value)
                    allapot = piros;
                else
                    allapot = sargaVillogo;
            }
            get
            {
                if (allapot == sargaVillogo || allapot == nemVilagit)
                    return false;
                else
                    return true;
            }
        }

        public void AllapotAtmenet()
        {
            switch (allapot)
            {
                case piros:
                {
                    if (stopper.Elapsed.TotalSeconds > 5)
                    {
                        allapot = pirosSarga;
                        stopper.Restart();
                    }
                    break;
                }
                case pirosSarga:
                {
                    if (stopper.Elapsed.TotalSeconds > 1)
                    {
                        allapot = zold;
                        stopper.Restart();
                    }
                    break;
                }
                case zold:
                {
                    if (stopper.Elapsed.TotalSeconds > 5)
                    {
                        allapot = sarga;
                        stopper.Restart();
                    }
                    break;
                }
                case sarga:
                {
                    if (stopper.Elapsed.TotalSeconds > 2)
                    {
                        allapot = piros;
                        stopper.Restart();
                    }
                    break;
                }
                case sargaVillogo:
                {
                    if (stopper.Elapsed.TotalSeconds > 0.5)
                    {
                        allapot = nemVilagit;
                        stopper.Restart();
                    }
                    break;
                }
                case nemVilagit:
                {
                    if (stopper.Elapsed.TotalSeconds > 0.5)
                    {
                        allapot = sargaVillogo;
                        stopper.Restart();
                    }
                    break;
                }
            }
        }

        public void Rajzol(Graphics eszkoz)
        {
            
            switch (allapot)
            {
                case piros:
                {
                    szinek[0] = Color.Red;
                    szinek[1] = Color.DimGray;
                    szinek[2] = Color.DimGray;
                    break;
                }
                case pirosSarga:
                {
                    szinek[0] = Color.Red;
                    szinek[1] = Color.Yellow;
                    szinek[2] = Color.DimGray;
                    break;
                }
                case zold:
                {
                    szinek[0] = Color.DimGray;
                    szinek[1] = Color.DimGray;
                    szinek[2] = Color.FromArgb(0, byte.MaxValue, 0);
                    break;
                }
                case sarga:
                case sargaVillogo:
                {
                    szinek[0] = Color.DimGray;
                    szinek[1] = Color.Yellow;
                    szinek[2] = Color.DimGray;
                    break;
                }
                case nemVilagit:
                {
                    szinek[0] = Color.DimGray;
                    szinek[1] = Color.DimGray;
                    szinek[2] = Color.DimGray;
                    break;
                }
            }
            for (int i = 0; i < lampaSzam; i++)
            {
                ecset.Color = szinek[i];
                eszkoz.FillEllipse(ecset, lampak[i]);
                eszkoz.DrawEllipse(toll, lampak[i]);
            }
        }
    }
}
