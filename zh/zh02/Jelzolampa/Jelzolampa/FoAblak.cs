﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows.Forms;

namespace Jelzolampa
{
    public partial class FoAblak : Form
    {
        Jelzolampa jelzolampa;
        public FoAblak()
        {
            InitializeComponent();
            typeof(Panel).InvokeMember("DoubleBuffered", 
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.SetProperty, 
                null,
                panelLampa, new object[] { true });
            jelzolampa = new Jelzolampa(panelLampa.ClientSize.Width);
            timerRajzol.Start();
        }

        private void timerRajzol_Tick(object sender, EventArgs e)
        {
            jelzolampa.AllapotAtmenet();
            panelLampa.Invalidate();
        }

        private void panelLampa_Paint(object sender, PaintEventArgs e)
        {
            jelzolampa.Rajzol(e.Graphics);
        }

        private void buttonKapcsoló_Click(object sender, EventArgs e)
        {
            jelzolampa.Uzemmod = !jelzolampa.Uzemmod;
        }
    }
}
