﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Reflection;

namespace Sebessegmero
{
    public partial class FormFoAblak : Form
    {
        public double maxEro = 270e3;
        Mozdony mozdony;
        Stopwatch stopper;
        long elteltIdo;

        public FormFoAblak()
        {
            InitializeComponent();
            typeof(Panel).InvokeMember("DoubleBuffered",
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.SetProperty,
                null,
                panelRajz,
                new object[] { true });
            mozdony = new Mozdony(80e3, 6);
            timerMozgat.Start();
            stopper = new Stopwatch();
            stopper.Start();
            elteltIdo = 0;
        }

        private void timerMozgat_Tick(object sender, EventArgs e)
        {
            double ido = (stopper.ElapsedMilliseconds - elteltIdo) / 1000.0;
            double ero = maxEro * (trackBarEro.Value - 50) / 50.0;
            mozdony.Mozog(ero, ido);
            elteltIdo = stopper.ElapsedMilliseconds;
            panelRajz.Invalidate();
            textBoxEro.Text = string.Format("{0:F0} N", ero);
            textBoxSebesseg.Text = string.Format("{0:F0} km/h", mozdony.Sebesseg * 3.6);
        }

        private void panelRajz_Paint(object sender, PaintEventArgs e)
        {
            mozdony.Rajzol(panelRajz, e.Graphics);
        }
    }
}
