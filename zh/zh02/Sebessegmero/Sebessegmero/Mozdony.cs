﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Sebessegmero
{
    class Mozdony
    {
        private const double gravitacio = 9.8065; // [m/s^2]
        // V43: 1/2 * Cd * rho * A = 0.5 * 1 * 1.2 * 4.54 * 3.1 = 8.44
        private const double legellenallas = 8.44; // [1]
        // Vasúti kerék - sín: 0.001 - 0.002
        private const double gordulesiEllenallas = 0.0015; // [1]
        // ~ 160 km/h
        private const double maxSebesseg = 45; // [m/s]
        private const double minSebesseg = 0.1; // [m/s]

        private double tomeg; // [kg]
        private double gyorsulas; // [m/s^2]
        private double sebesseg; // [m/s]
        private int kocsikSzama;

        public double Sebesseg
        {
            get { return sebesseg; }
        }

        public Mozdony(double tomeg, int kocsikSzama)
        {
            this.tomeg = tomeg;
            this.kocsikSzama = kocsikSzama;
            gyorsulas = 0;
            sebesseg = 0;
        }

        public void Mozog(double ero, double ido)
        {
            if ((sebesseg <= minSebesseg) && (ero < 0.0))
                ero = 0.0;
            double legellenallasEro = (1.0 + 0.5 * kocsikSzama) * legellenallas * sebesseg * sebesseg;
            double gordulesiEllenallasEro = 0;
            if (sebesseg > minSebesseg)
                gordulesiEllenallasEro = ((1.0 + 0.5 * kocsikSzama) * tomeg) * gravitacio * gordulesiEllenallas * Math.Sign(sebesseg);
            gyorsulas = (ero - legellenallasEro - gordulesiEllenallasEro) / ((1.0 + 0.5 * kocsikSzama) * tomeg);
            sebesseg += gyorsulas * ido;
        }

        public void Rajzol(Panel panel, Graphics eszkoz)
        {
            SolidBrush ecset = new SolidBrush(Color.CadetBlue);
            Rectangle szamlap = new Rectangle(new Point(0, 0), panel.ClientSize);
            eszkoz.FillEllipse(ecset, szamlap);
            Pen toll = new Pen(Color.Black, 3.0f);
            double szog = (240.0 - 300.0 * (sebesseg / maxSebesseg)) / 180.0 * Math.PI;
            double mutatoHossz = panel.ClientSize.Height / 2.0;
            double xo = panel.ClientSize.Width / 2.0;
            double yo = panel.ClientSize.Height / 2.0;
            double x = xo + mutatoHossz * Math.Cos(szog);
            double y = yo + mutatoHossz * Math.Sin(szog);
            Point origo = new Point((int)xo, panel.ClientSize.Height - (int)yo);
            Point mutato = new Point((int)x, panel.ClientSize.Height - (int)y);
            eszkoz.DrawLine(toll, origo, mutato);
            toll.Dispose();
        }
    }
}
