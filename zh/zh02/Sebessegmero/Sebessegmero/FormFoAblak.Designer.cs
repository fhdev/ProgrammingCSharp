﻿namespace Sebessegmero
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelRajz = new System.Windows.Forms.Panel();
            this.trackBarEro = new System.Windows.Forms.TrackBar();
            this.textBoxEro = new System.Windows.Forms.TextBox();
            this.textBoxSebesseg = new System.Windows.Forms.TextBox();
            this.timerMozgat = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEro)).BeginInit();
            this.SuspendLayout();
            // 
            // panelRajz
            // 
            this.panelRajz.BackColor = System.Drawing.Color.White;
            this.panelRajz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelRajz.Location = new System.Drawing.Point(12, 40);
            this.panelRajz.Name = "panelRajz";
            this.panelRajz.Size = new System.Drawing.Size(300, 300);
            this.panelRajz.TabIndex = 0;
            this.panelRajz.Paint += new System.Windows.Forms.PaintEventHandler(this.panelRajz_Paint);
            // 
            // trackBarEro
            // 
            this.trackBarEro.LargeChange = 20;
            this.trackBarEro.Location = new System.Drawing.Point(322, 40);
            this.trackBarEro.Maximum = 100;
            this.trackBarEro.Name = "trackBarEro";
            this.trackBarEro.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarEro.Size = new System.Drawing.Size(45, 300);
            this.trackBarEro.SmallChange = 5;
            this.trackBarEro.TabIndex = 1;
            this.trackBarEro.Value = 50;
            // 
            // textBoxEro
            // 
            this.textBoxEro.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxEro.Location = new System.Drawing.Point(12, 8);
            this.textBoxEro.Name = "textBoxEro";
            this.textBoxEro.ReadOnly = true;
            this.textBoxEro.Size = new System.Drawing.Size(100, 26);
            this.textBoxEro.TabIndex = 2;
            this.textBoxEro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxSebesseg
            // 
            this.textBoxSebesseg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxSebesseg.Location = new System.Drawing.Point(212, 8);
            this.textBoxSebesseg.Name = "textBoxSebesseg";
            this.textBoxSebesseg.ReadOnly = true;
            this.textBoxSebesseg.Size = new System.Drawing.Size(100, 26);
            this.textBoxSebesseg.TabIndex = 3;
            this.textBoxSebesseg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // timerMozgat
            // 
            this.timerMozgat.Interval = 20;
            this.timerMozgat.Tick += new System.EventHandler(this.timerMozgat_Tick);
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 349);
            this.Controls.Add(this.textBoxSebesseg);
            this.Controls.Add(this.textBoxEro);
            this.Controls.Add(this.trackBarEro);
            this.Controls.Add(this.panelRajz);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFoAblak";
            this.Text = "Sebességkijelző";
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelRajz;
        private System.Windows.Forms.TrackBar trackBarEro;
        private System.Windows.Forms.TextBox textBoxEro;
        private System.Windows.Forms.TextBox textBoxSebesseg;
        private System.Windows.Forms.Timer timerMozgat;
    }
}

