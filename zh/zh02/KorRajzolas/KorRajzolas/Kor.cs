﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace KorRajzolas
{
    class Kor
    {
        // Véletlenszámgenerátor.
        private static Random veletlen = new Random();
        // Kör jellemzői.
        private double r;
        private double x;
        private double y;

        // Konstruktor.
        public Kor(double r, double x, double y)
        {
            this.r = r;
            this.x = x;
            this.y = y;
        }

        // Kör területének kiszámítása.
        public double Terulet
        {
            get
            {
                return Math.PI * r * r;
            }
        }

        // Kör kerületének kiszámítása.
        public double Kerulet
        {
            get
            {
                return 2 * r * Math.PI;
            }
        }

        // Befoglaló négyzet megadása.
        public Rectangle Befoglalo
        {
            get
            {
                return new Rectangle(Convert.ToInt32(x - r), Convert.ToInt32(y - r), 
                    Convert.ToInt32(2 * r), Convert.ToInt32(2 * r));
            }
        }

        // Annak eldöntése, hogy a kör és a masik kör metszik-e egymást.
        public bool Utkozik(Kor masik)
        {
            double d2 = Math.Pow(x - masik.x, 2.0) + Math.Pow(y - masik.y, 2.0);
            return d2 < Math.Pow(r + masik.r, 2.0);
        }

        // Körök generálása véletlenszerű jellemzőkkel, úgy, hogy ne metszhessék egymást, és ne 
        // lóghassanak le a rajzterületről. 
        public static Kor[] General(int darab, double szelesseg, double magassag)
        {
            // Maximális sugár a képátló 10%-a. 
            double kepAtlo = Math.Sqrt(Math.Pow(szelesseg, 2.0) +
                Math.Pow(magassag, 2.0));
            double rMax = 0.1 * kepAtlo;
            Kor[] korok = new Kor[darab];
            for (int i = 0; i < darab; i++)
            {
                bool utkozik;
                Kor kor;
                // Új kör generálása, amíg olyan nem adódik, ami semelyik másikkal sem ütközik. 
                do
                {
                    // Véletlen jellemzők generálása.
                    double r = veletlen.NextDouble() * rMax;
                    double x = r + veletlen.NextDouble() * (szelesseg - 2 * r);
                    double y = r + veletlen.NextDouble() * (magassag - 2 * r);
                    utkozik = false; 
                    kor = new Kor(r, x, y);
                    for (int j = 0; j < i; j++)
                    {
                        if (utkozik = kor.Utkozik(korok[j]))
                        {
                            break;
                        }

                    }
                } while (utkozik);
                korok[i] = kor;
            }
            return korok;
        }

        // Kör kirajzolása. A kör kitöltő színe a kerület és a koordináták függvénye.
        public void Rajzol(Graphics rajzEszkoz)
        {
            Pen toll = new Pen(Color.Black);
            SolidBrush ecset = new SolidBrush(Color.Black);
            if(Kerulet < 0.5 * (x + y))
            {
                ecset.Color = Color.Red;
            }
            else
            {
                ecset.Color = Color.Blue;
            }
            rajzEszkoz.FillEllipse(ecset, Befoglalo);
            rajzEszkoz.DrawEllipse(toll, Befoglalo);
        }
    }
}
