﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KorRajzolas
{
    public partial class FormFoAblak : Form
    {
        // Körök tömbje.
        private Kor[] korok;

        public FormFoAblak()
        {
            InitializeComponent();
            // Inicializálás üres tömbbel.
            korok = new Kor[0];
        }

        // A megadott számú új kör generálása. A körök által lefedett terület %-os arányának 
        // kijelzése. 
        private void buttonRajzol_Click(object sender, EventArgs e)
        {
            // Megadott számú kör generálása.
            int darab;
            int.TryParse(textBoxDarab.Text, out darab);
            korok = Kor.General(darab, ClientSize.Width, ClientSize.Height);
            // Területarány számítása.
            double terulet = 0.0;
            foreach(Kor kor in korok)
            {
                terulet += kor.Terulet;
            }
            terulet /= ClientSize.Height * ClientSize.Width;
            // Területarány kijelzése.
            labelTerulet.Text = string.Format("{0:F3} %", terulet);
            // Újrarajzolás.
            Invalidate();
        }

        private void FormFoAblak_Paint(object sender, PaintEventArgs e)
        {
            // Minden kör kirajzolása.
            foreach(Kor kor in korok)
            {
                kor.Rajzol(e.Graphics);
            }
        }
    }
}
