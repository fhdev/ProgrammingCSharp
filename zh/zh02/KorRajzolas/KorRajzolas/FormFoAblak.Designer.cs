﻿namespace KorRajzolas
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRajzol = new System.Windows.Forms.Button();
            this.textBoxDarab = new System.Windows.Forms.TextBox();
            this.labelTerulet = new System.Windows.Forms.Label();
            this.labelDarab = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonRajzol
            // 
            this.buttonRajzol.Location = new System.Drawing.Point(12, 12);
            this.buttonRajzol.Name = "buttonRajzol";
            this.buttonRajzol.Size = new System.Drawing.Size(75, 23);
            this.buttonRajzol.TabIndex = 0;
            this.buttonRajzol.Text = "Rajzol";
            this.buttonRajzol.UseVisualStyleBackColor = true;
            this.buttonRajzol.Click += new System.EventHandler(this.buttonRajzol_Click);
            // 
            // textBoxDarab
            // 
            this.textBoxDarab.Location = new System.Drawing.Point(93, 14);
            this.textBoxDarab.Name = "textBoxDarab";
            this.textBoxDarab.Size = new System.Drawing.Size(46, 20);
            this.textBoxDarab.TabIndex = 1;
            this.textBoxDarab.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelTerulet
            // 
            this.labelTerulet.AutoSize = true;
            this.labelTerulet.Location = new System.Drawing.Point(537, 539);
            this.labelTerulet.Name = "labelTerulet";
            this.labelTerulet.Size = new System.Drawing.Size(0, 13);
            this.labelTerulet.TabIndex = 2;
            // 
            // labelDarab
            // 
            this.labelDarab.AutoSize = true;
            this.labelDarab.Location = new System.Drawing.Point(142, 17);
            this.labelDarab.Name = "labelDarab";
            this.labelDarab.Size = new System.Drawing.Size(22, 13);
            this.labelDarab.TabIndex = 3;
            this.labelDarab.Text = "db.";
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.Controls.Add(this.labelDarab);
            this.Controls.Add(this.labelTerulet);
            this.Controls.Add(this.textBoxDarab);
            this.Controls.Add(this.buttonRajzol);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFoAblak";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KorRajzolas";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FormFoAblak_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonRajzol;
        private System.Windows.Forms.TextBox textBoxDarab;
        private System.Windows.Forms.Label labelTerulet;
        private System.Windows.Forms.Label labelDarab;
    }
}

