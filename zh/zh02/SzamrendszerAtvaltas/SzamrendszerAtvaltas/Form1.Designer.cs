﻿namespace SzamrendszerAtvaltas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_In = new System.Windows.Forms.TextBox();
            this.textBox_Out = new System.Windows.Forms.TextBox();
            this.textBox_Base = new System.Windows.Forms.TextBox();
            this.button_Convert = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox_In
            // 
            this.textBox_In.Location = new System.Drawing.Point(20, 40);
            this.textBox_In.Name = "textBox_In";
            this.textBox_In.Size = new System.Drawing.Size(260, 22);
            this.textBox_In.TabIndex = 0;
            this.textBox_In.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox_Out
            // 
            this.textBox_Out.Location = new System.Drawing.Point(20, 140);
            this.textBox_Out.Name = "textBox_Out";
            this.textBox_Out.Size = new System.Drawing.Size(260, 22);
            this.textBox_Out.TabIndex = 1;
            this.textBox_Out.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox_Base
            // 
            this.textBox_Base.Location = new System.Drawing.Point(20, 90);
            this.textBox_Base.Name = "textBox_Base";
            this.textBox_Base.Size = new System.Drawing.Size(100, 22);
            this.textBox_Base.TabIndex = 2;
            this.textBox_Base.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button_Convert
            // 
            this.button_Convert.Location = new System.Drawing.Point(170, 90);
            this.button_Convert.Name = "button_Convert";
            this.button_Convert.Size = new System.Drawing.Size(110, 23);
            this.button_Convert.TabIndex = 3;
            this.button_Convert.Text = "Convert";
            this.button_Convert.UseVisualStyleBackColor = true;
            this.button_Convert.Click += new System.EventHandler(this.button_Convert_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Original number:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "At base of:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Is this number:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(20, 180);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(260, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Write LOG";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 220);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_Convert);
            this.Controls.Add(this.textBox_Base);
            this.Controls.Add(this.textBox_Out);
            this.Controls.Add(this.textBox_In);
            this.Name = "Form1";
            this.Text = "BaseConverter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_In;
        private System.Windows.Forms.TextBox textBox_Out;
        private System.Windows.Forms.TextBox textBox_Base;
        private System.Windows.Forms.Button button_Convert;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
    }
}

