﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SzamrendszerAtvaltas
{
    public partial class Form1 : Form
    {
        BaseConverter BC = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Convert_Click(object sender, EventArgs e)
        {
            int B, In;
            if (!int.TryParse(textBox_Base.Text, out B) || !int.TryParse(textBox_In.Text, out In))
            {
                B = In = 0;
                MessageBox.Show("Incorrect input!");
                textBox_Out.Text = "ERROR";
            }
            else
            {
                BC = new BaseConverter(In, B);
                BC.ConvertBase();
                textBox_Out.Text = BC.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(BC != null)
            {
                StreamWriter SW = new StreamWriter("log.txt");
                SW.WriteLine(BC.Original + " decimal number at base of " + BC.Numbase + " is equal to " + BC.ToString() + ".");
                SW.Close();
                SW.Dispose();
            }
        }
    }
}
