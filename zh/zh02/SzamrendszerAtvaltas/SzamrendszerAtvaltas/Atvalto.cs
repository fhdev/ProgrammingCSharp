﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SzamrendszerAtvaltas
{
    class BaseConverter
    {
        private int numbase;
        public int Numbase
        {
            get { return numbase; }
        }
        private int original;
        public int Original
        {
            get { return original; }
        }
        private int[] converted;
        public int[] Converted
        {
            get { return converted; }
        }

        public BaseConverter(int Number, int InBase)
        {
            numbase = InBase;
            original = Number;
        }

        public void ConvertBase()
        {
            if (0 != numbase && 0 != original)
            {
                int dim = (int)Math.Ceiling(Math.Log(original) / Math.Log(numbase));
                converted = new int[dim];
                int temp = original;
                for (int i = 0; temp > 0; i++)
                {
                    converted[i] = temp % numbase;
                    temp /= numbase;
                }
            }
            else
            {
                MessageBox.Show("Converting without constructor call is not possible.");
            }
        }

        public override string ToString()
        {
            string Result = "|";
            for (int i = converted.GetLength(0) - 1; i >= 0; i--)
            {
                Result += converted[i].ToString() + "|";
            }
            return Result;
        }
    }
}
