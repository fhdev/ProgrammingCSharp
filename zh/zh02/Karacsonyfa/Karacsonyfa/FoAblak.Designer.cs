﻿namespace Karacsonyfa
{
    partial class FoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelRajz = new System.Windows.Forms.Panel();
            this.buttonValt = new System.Windows.Forms.Button();
            this.buttonTorles = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panelRajz
            // 
            this.panelRajz.BackColor = System.Drawing.Color.White;
            this.panelRajz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelRajz.Location = new System.Drawing.Point(14, 12);
            this.panelRajz.Name = "panelRajz";
            this.panelRajz.Size = new System.Drawing.Size(300, 400);
            this.panelRajz.TabIndex = 0;
            this.panelRajz.Paint += new System.Windows.Forms.PaintEventHandler(this.panelRajz_Paint);
            this.panelRajz.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelRajz_MouseClick);
            // 
            // buttonValt
            // 
            this.buttonValt.Location = new System.Drawing.Point(327, 12);
            this.buttonValt.Name = "buttonValt";
            this.buttonValt.Size = new System.Drawing.Size(75, 29);
            this.buttonValt.TabIndex = 1;
            this.buttonValt.Text = "Színváltó";
            this.buttonValt.UseVisualStyleBackColor = true;
            this.buttonValt.Click += new System.EventHandler(this.buttonValt_Click);
            // 
            // buttonTorles
            // 
            this.buttonTorles.Location = new System.Drawing.Point(327, 47);
            this.buttonTorles.Name = "buttonTorles";
            this.buttonTorles.Size = new System.Drawing.Size(75, 29);
            this.buttonTorles.TabIndex = 2;
            this.buttonTorles.Text = "Törlés";
            this.buttonTorles.UseVisualStyleBackColor = true;
            this.buttonTorles.Click += new System.EventHandler(this.buttonTorles_Click);
            // 
            // FoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(414, 422);
            this.Controls.Add(this.buttonTorles);
            this.Controls.Add(this.buttonValt);
            this.Controls.Add(this.panelRajz);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FoAblak";
            this.Text = "Karácsonyfa";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelRajz;
        private System.Windows.Forms.Button buttonValt;
        private System.Windows.Forms.Button buttonTorles;
    }
}

