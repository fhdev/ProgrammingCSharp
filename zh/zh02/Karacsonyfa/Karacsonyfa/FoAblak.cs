﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows.Forms;

namespace Karacsonyfa
{
    public partial class FoAblak : Form
    {
        private Karacsonyfa karacsonyfa;
        public FoAblak()
        {
            InitializeComponent();
            typeof(Panel).InvokeMember("DoubleBuffered",
                BindingFlags.SetProperty | BindingFlags.NonPublic | BindingFlags.Instance,
                null, 
                panelRajz,
                new object[] { true });
            karacsonyfa = new Karacsonyfa(panelRajz);
        }

        private void panelRajz_Paint(object sender, PaintEventArgs e)
        {
            karacsonyfa.Rajzol(e.Graphics);
        }

        private void panelRajz_MouseClick(object sender, MouseEventArgs e)
        {
            karacsonyfa.DiszHozzaad(e.X, e.Y);
            panelRajz.Invalidate();
        }

        private void buttonValt_Click(object sender, EventArgs e)
        {
            karacsonyfa.SzintValt();
            panelRajz.Invalidate();
        }

        private void buttonTorles_Click(object sender, EventArgs e)
        {
            karacsonyfa.Torol();
            panelRajz.Invalidate();
        }
    }
}
