﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karacsonyfa
{
    class Karacsonyfa
    {
        private int szelesseg;
        private int magassag;
        private int diszSugar;
        private Color diszSzin;
        private List<Point> diszek;
        private static Random veletlen = new Random();

        private SolidBrush ecset;
        private Pen toll;

        private List<Point> fa;

        public Karacsonyfa(Panel tabla)
        {
            szelesseg = tabla.ClientSize.Width;
            magassag = tabla.ClientSize.Height;
            diszSugar = (szelesseg + magassag) / 50;
            diszSzin = Color.OrangeRed;
            diszek = new List<Point>();            

            ecset = new SolidBrush(Color.White);
            toll = new Pen(Color.Black);

            fa = new List<Point>()
            {
                new Point(tabla.ClientSize.Width/2, 0),
                new Point(0, tabla.ClientSize.Height),
                new Point(tabla.ClientSize.Width, tabla.ClientSize.Height)
            };

        }

        public void Rajzol(Graphics eszkoz)
        {
            ecset.Color = Color.DarkGreen;
            eszkoz.FillPolygon(ecset, fa.ToArray());
            ecset.Color = diszSzin;
            diszek.ForEach(p => eszkoz.FillEllipse(ecset, 
                new Rectangle(new Point(p.X - diszSugar, p.Y - diszSugar) , 
                new Size(2 * diszSugar, 2 * diszSugar))));
        }

        public void DiszHozzaad(int x, int y)
        {
            // Rajta van-e a panelen
            if ((x - diszSugar) < 0 || (x + diszSugar) > szelesseg)
                return;
            if ((y - diszSugar) < 0 || (y + diszSugar) > magassag)
                return;

            // Rajta van-e a fán
            double k = (double)szelesseg/ 2.0 / (double)magassag;
            double bal = (double)szelesseg / 2.0 - y * k;
            double jobb = (double)szelesseg / 2.0 + y * k;
            if ((x < bal) || (x > jobb))
                return;

            // Másik disszel ütközik-e
            foreach (Point disz in diszek)
            {
                double tavolsag = Math.Sqrt(Math.Pow(disz.X + diszSugar - x, 2.0) +
                     Math.Pow(disz.Y + diszSugar - y, 2.0));
                if (tavolsag < 2 * diszSugar)
                    return;
            }
            diszek.Add(new Point(x, y));
        }

        public void SzintValt()
        {
            diszSzin = Color.FromArgb(veletlen.Next(0, byte.MaxValue + 1),
                veletlen.Next(0, byte.MaxValue + 1),
                veletlen.Next(0, byte.MaxValue + 1));
        }

        public void Torol()
        {
            diszek.Clear();
        }
    }
}
