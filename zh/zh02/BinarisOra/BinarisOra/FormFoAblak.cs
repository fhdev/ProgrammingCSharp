﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace BinarisOra
{
    public partial class FormFoAblak : Form
    {
        // a kirajzolás alap oldalhossza
        private const int oldalHossz = 20;
        // bináris óra objektum
        private BinarisOra ora;
        public FormFoAblak()
        {
            InitializeComponent();
            // panel kétszeres pufferelés
            typeof(Panel).InvokeMember("DoubleBuffered",
               BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
               null,
               panelOra,
               new object[] { true });
            // óra létrehozása
            ora = new BinarisOra(oldalHossz, Color.LawnGreen);
        }

        private void panelOra_Paint(object sender, PaintEventArgs e)
        {
            // óra kirajzolása
            DateTime most = DateTime.Now;
            ora.Rajzol(e.Graphics, most.Hour, most.Minute);
        }

        private void panelOra_Click(object sender, EventArgs e)
        {
            // óra színének változtatása
            if(ora.Szin == Color.Red)
            {
                ora.Szin = Color.LawnGreen;
            }
            else
            {
                ora.Szin = Color.Red;
            }
            panelOra.Invalidate();
        }

        private void timerFrissit_Tick(object sender, EventArgs e)
        {
            // óra paneljának újrarajzolása
            panelOra.Invalidate();
        }
    }
}
