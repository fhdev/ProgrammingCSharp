﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BinarisOra
{
    class BinarisOra
    {
        // a nem aktív kijelző bitek színe
        private static readonly Color sotet = Color.DimGray;

        // az időt kijelző biteket megjelenítő téglalap objektumok
        private Rectangle[] percek;
        private Rectangle[] orak;
        private Rectangle delutan;

        // az kijelző bitek színe
        private Color szin;

        // az óra színéhez hozzáférést biztosító tulajdonság
        public Color Szin
        {
            get { return szin; }
            set { szin = value; }
        }

        // A bináris óra konstruktora. A kirajzoláshoz használt alap oldalhossz és szín alapján
        // elkészíti az egyes biteket jelző négyzet objektumokat.
        public BinarisOra(int oldal, Color szin)
        {
            // szin beállítása
            this.szin = szin;
            // a négyzetek létrehozásához szükséges pont és méret objektumok
            Point bf;
            Size m = new Size(oldal, oldal);
            // a perceket mutató bitek négyzeteinek létrehozása
            percek = new Rectangle[6];
            for (int i = 0; i < percek.Length; i++)
            {
                bf = new Point((11 - 2 * i) * oldal, 5 * oldal);
                percek[i] = new Rectangle(bf, m);
            }
            // az órákat mutató bitek négyzeteinek létrehozása
            orak = new Rectangle[4];
            for(int i = 0; i < orak.Length; i++)
            {
                bf = new Point((11 - 2 * i) * oldal, 3 * oldal);
                orak[i] = new Rectangle(bf, m);
            }
            // a délelőtt/délután bit négyzetének létrehozása
            bf = new Point(11 * oldal, oldal);
            delutan = new Rectangle(bf, m);
        }

        public void Rajzol(Graphics rajzTabla, int ora24, int perc)
        {
            // rajz objektumok létrehozása
            Pen toll = new Pen(Color.Black);
            SolidBrush ecset = new SolidBrush(Color.White);
            // perceket mutató bitek kirajzolása
            for (int i = 0; i < percek.Length; i++)
            {
                ecset.Color = sotet;
                if (Vilagitson(perc, i))
                {
                    ecset.Color = szin;
                }
                rajzTabla.FillEllipse(ecset, percek[i]);
                rajzTabla.DrawEllipse(toll, percek[i]);
            }
            // óra érték átváltása 12 órás formátumra
            bool pm;
            int ora12 = Ora24bol12(ora24, out pm);
            // órákat mutató bitek kirajzolása
            for (int i = 0; i < orak.Length; i++)
            {
                ecset.Color = sotet;
                if (Vilagitson(ora12, i))
                {
                    ecset.Color = szin;
                }
                rajzTabla.FillEllipse(ecset, orak[i]);
                rajzTabla.DrawEllipse(toll, orak[i]);
            }
            // délután jelzőbit kirajzolása
            ecset.Color = sotet;
            if(pm)
            {
                ecset.Color = szin;
            }
            rajzTabla.FillEllipse(ecset, delutan);
            rajzTabla.DrawEllipse(toll, delutan);
            // rajz objektumok felszabadítása
            ecset.Dispose();
            toll.Dispose();
        }

        // Ez a metódus meghatározza, hogy az adott ertek mellett a meghatározott helyiertek-en
        // 1-es érték van-e.
        private bool Vilagitson(int ertek, int helyiertek)
        {
            return (ertek & (1 << helyiertek)) > 0;
        }

        // Ez a metóus a 24 órás formátumban megadott óra24 értéket 12 órás formátumba átváltja, 
        // és visszatér az értékkel. A metódus a pm változó értékét igaz-ra állítja be, ha délután
        // van.
        private int Ora24bol12(int ora24, out bool delutan)
        {
            delutan = ora24 > 11;
            if ((ora24 > 0) && (ora24 < 13))
            {
                return ora24;
            }
            return Math.Abs(ora24 - 12);
        }
    }
}
