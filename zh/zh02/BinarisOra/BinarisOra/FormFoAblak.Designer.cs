﻿namespace BinarisOra
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelOra = new System.Windows.Forms.Panel();
            this.timerFrissit = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // panelOra
            // 
            this.panelOra.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelOra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelOra.Location = new System.Drawing.Point(12, 13);
            this.panelOra.Name = "panelOra";
            this.panelOra.Size = new System.Drawing.Size(260, 140);
            this.panelOra.TabIndex = 0;
            this.panelOra.Click += new System.EventHandler(this.panelOra_Click);
            this.panelOra.Paint += new System.Windows.Forms.PaintEventHandler(this.panelOra_Paint);
            // 
            // timerFrissit
            // 
            this.timerFrissit.Enabled = true;
            this.timerFrissit.Interval = 1000;
            this.timerFrissit.Tick += new System.EventHandler(this.timerFrissit_Tick);
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(284, 166);
            this.Controls.Add(this.panelOra);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormFoAblak";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bináris óra";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelOra;
        private System.Windows.Forms.Timer timerFrissit;
    }
}

