﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;

namespace VonalAbra
{
    class VonalAbra
    {
        // Windows.Forms.Control felület mérete, amire rajzolni fogunk.
        private Size abraMeret;
        // Adatpontok.
        private double[] x;
        private double[] y;
        // Szín.
        private Color szin;
        // Konstans margó.
        private const int margo = 10;

        // Vonalábra objektum létrehozása megadott színnel, üres adatokkal.
        public VonalAbra(Size abraMeret, Color szin)
        {
            this.abraMeret = abraMeret;
            this.x = new double[0];
            this.y = new double[0];
            Szin = szin;
        }

        public Size AbraMeret
        {
            get
            {
                return abraMeret;
            }
            set
            {
                if(value != null)
                {
                    abraMeret = value;
                }
            }
        }
        // Tulajdonság a független változó adatsorának eléréséhez.
        public double[] X
        {
            get
            {
                return x;
            }
            set
            {
                // Ha a kapott érték nem null, akkor beállítjuk, és újrarajzoljuk az ábrát.
                if (value != null)
                {
                    x = value;
                }
            }
        }
        // Tulajdonság a függő változó adatsorának eléréséhez.
        public double[] Y
        {
            get
            {
                return y;
            }
            set
            {
                // Ha a kapott érték nem null, akkor beállítjuk, és újrarajzoljuk az ábrát.
                if (value != null)
                {
                    y = value;
                }
            }
        }
        // Tulajdonság a szín eléréséhez.
        public Color Szin
        {
            get
            {
                return szin;
            }
            set
            {
                // Ha a kapott érték nem null, akkor beállítjuk, és újrarajzoljuk az ábrát.
                if (value != null)
                {
                    szin = value;
                }
            }
        }
        // X adatsor tartományának lekérdezése.
        public double XTartomany
        {
            get
            {
                double xTartomany = 0.0;
                // Ha van eleme az adatsornak, akkor a tartomány a maximális és minimális érték 
                // különbsége.
                if (X.Length > 0)
                {
                    xTartomany = X.Max() - X.Min();
                }
                // Ha minden elem egyforma, akkor a tartomány alapértelmezett értéke 1.
                if(xTartomany <= 0.0)
                {
                    xTartomany = 1.0;
                }
                return xTartomany;
            }
        }
        // Y adatsor tartományának lekérdezése.
        public double YTartomany
        {
            get
            {
                double yTartomany = 0.0;
                // Ha van eleme az adatsornak, akkor a tartomány a maximális és minimális érték 
                // különbsége.
                if (Y.Length > 0)
                {
                    yTartomany = Y.Max() - Y.Min();
                }
                // Ha minden elem egyforma, akkor a tartomány alapértelmezett értéke 1.
                if (yTartomany <= 0.0)
                {
                    yTartomany = 1.0;
                }
                return yTartomany;
            }
        }
        // Az adatsorokból közös pontok számának meghatározása.
        public int PontokSzama
        {
            get
            {
                return Math.Min(X.Length, Y.Length);
            }
        }
        // Rajzolás.
        public void Rajzol(Graphics rajzEszkoz)
        {
            // Ábra törlése
            rajzEszkoz.Clear(Color.White);
            // Ha van annyi pont, amennyiből lehet vonalat rajzolni. 
            if (PontokSzama > 1)
            {
                // Legjobb kitöltés pixel / érték aránya X irányban.
                double kX = (abraMeret.Width - 2 * margo) / XTartomany;
                // Legjobb kitöltés pixel / érték aránya Y irányban.
                double kY = (abraMeret.Height - 2 * margo) / YTartomany;
                // Pixel pontok kiszámítása.
                Point[] pontok = new Point[PontokSzama];
                for (int i = 0; i < pontok.Length; i++)
                {
                    pontok[i] = new Point(Convert.ToInt32(margo + (X[i] - X.Min()) * kX),
                        Convert.ToInt32(abraMeret.Height - margo - (Y[i] - Y.Min()) * kY));
                }
                // Rajzolás
                Pen toll = new Pen(Szin, 1.0f);
                rajzEszkoz.DrawLines(toll, pontok);
                toll.Dispose();
            }
        }
        // Tömb feltöltése egymástól whitespace karakterekkel elválasztott számokat tartalmazó
        // string-ből.
        private static double[] SzovegbolTomb(string szoveg)
        {
            // A szöveg szétvágása részekre minden whitespace mentén, és az üres részek eltávolítása.
            string[] reszek = szoveg.Split().Where(x => !string.IsNullOrEmpty(x)).ToArray();
            // Az egymástól üres karakterekkel elválasztott részek számmá konvertálása.
            double[] ertekek = new double[reszek.Length];
            for (int i = 0; i < ertekek.Length; i++)
            {
                double.TryParse(reszek[i], out ertekek[i]);
            }
            return ertekek;
        }
        // X értékek feltöltése szövegből.
        public void SzovegbolX(string xSzoveg)
        {
            X = SzovegbolTomb(xSzoveg);
        }
        // Y értékek feltöltése szövegből.
        public void SzovegbolY(string ySzoveg)
        {
            Y = SzovegbolTomb(ySzoveg);
        }
    }
}
