﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace VonalAbra
{
    public partial class FormFoAblak : Form
    {
        // Vonalas ábra objektum.
        private VonalAbra abra;
        public FormFoAblak()
        {
            InitializeComponent();
            typeof(Control).InvokeMember("ResizeRedraw",
                BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic, null,
                this.panelAbra, new object[] { true });
            // Vonalas ábra létrehozása adat nélkül.
            abra = new VonalAbra(panelAbra.ClientSize, Color.Black);
        }

        private void buttonAbrazol_Click(object sender, EventArgs e)
        {
            // Adatok lekérése szövegként közvetlenül vagy fájlból.
            string xSzoveg = textBoxXErtekek.Text;
            if (File.Exists(xSzoveg))
            {
                xSzoveg = File.ReadAllLines(xSzoveg)[0];
            }
            string ySzoveg = textBoxYErtekek.Text;
            if(File.Exists(ySzoveg))
            {
                ySzoveg = File.ReadAllLines(ySzoveg)[0];
            }
            // Az ábra pontjainak feltöltése a szöveges adatokból.
            abra.SzovegbolX(xSzoveg);
            abra.SzovegbolY(ySzoveg);
            // Újrarajzolás.
            panelAbra.Invalidate();
        }

        private void buttonEgyenes_Click(object sender, EventArgs e)
        {
            // Minimálisan működő példa megmutatása - egyenes rajzolás.
            abra.X = new double[] { 1, 2 };
            abra.Y = new double[] { 1, 2 };
            // Újrarajzolás.
            panelAbra.Invalidate();
        }

        private void panelAbra_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            abra.Szin = Color.FromArgb(rnd.Next(0, 256), rnd.Next(0, 256), rnd.Next(0, 256));
            // Újrarajzolás.
            panelAbra.Invalidate();
        }

        private void panelAbra_Paint(object sender, PaintEventArgs e)
        {
            abra.Rajzol(e.Graphics);
        }

        private void panelAbra_SizeChanged(object sender, EventArgs e)
        {
            abra.AbraMeret = panelAbra.ClientSize;
            panelAbra.Invalidate();
        }
    }
}
