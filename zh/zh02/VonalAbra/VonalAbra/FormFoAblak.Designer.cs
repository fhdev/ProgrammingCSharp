﻿namespace VonalAbra
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelAbra = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxYErtekek = new System.Windows.Forms.TextBox();
            this.labelY = new System.Windows.Forms.Label();
            this.labelX = new System.Windows.Forms.Label();
            this.buttonAbrazol = new System.Windows.Forms.Button();
            this.textBoxXErtekek = new System.Windows.Forms.TextBox();
            this.buttonEgyenes = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelAbra
            // 
            this.panelAbra.BackColor = System.Drawing.Color.White;
            this.panelAbra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.panelAbra, 3);
            this.panelAbra.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAbra.Location = new System.Drawing.Point(3, 63);
            this.panelAbra.Name = "panelAbra";
            this.panelAbra.Size = new System.Drawing.Size(278, 195);
            this.panelAbra.TabIndex = 0;
            this.panelAbra.SizeChanged += new System.EventHandler(this.panelAbra_SizeChanged);
            this.panelAbra.Click += new System.EventHandler(this.panelAbra_Click);
            this.panelAbra.Paint += new System.Windows.Forms.PaintEventHandler(this.panelAbra_Paint);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.Controls.Add(this.textBoxYErtekek, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelY, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panelAbra, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelX, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonAbrazol, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBoxXErtekek, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonEgyenes, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(284, 261);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // textBoxYErtekek
            // 
            this.textBoxYErtekek.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxYErtekek.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxYErtekek.Location = new System.Drawing.Point(23, 35);
            this.textBoxYErtekek.Name = "textBoxYErtekek";
            this.textBoxYErtekek.Size = new System.Drawing.Size(178, 22);
            this.textBoxYErtekek.TabIndex = 5;
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelY.Location = new System.Drawing.Point(3, 30);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(14, 30);
            this.labelY.TabIndex = 2;
            this.labelY.Text = "Y";
            this.labelY.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelX
            // 
            this.labelX.AutoSize = true;
            this.labelX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelX.Location = new System.Drawing.Point(3, 0);
            this.labelX.Name = "labelX";
            this.labelX.Size = new System.Drawing.Size(14, 30);
            this.labelX.TabIndex = 1;
            this.labelX.Text = "X ";
            this.labelX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonAbrazol
            // 
            this.buttonAbrazol.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAbrazol.Location = new System.Drawing.Point(207, 3);
            this.buttonAbrazol.Name = "buttonAbrazol";
            this.buttonAbrazol.Size = new System.Drawing.Size(74, 24);
            this.buttonAbrazol.TabIndex = 3;
            this.buttonAbrazol.Text = "Ábrázol";
            this.buttonAbrazol.UseVisualStyleBackColor = true;
            this.buttonAbrazol.Click += new System.EventHandler(this.buttonAbrazol_Click);
            // 
            // textBoxXErtekek
            // 
            this.textBoxXErtekek.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxXErtekek.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxXErtekek.Location = new System.Drawing.Point(23, 5);
            this.textBoxXErtekek.Name = "textBoxXErtekek";
            this.textBoxXErtekek.Size = new System.Drawing.Size(178, 22);
            this.textBoxXErtekek.TabIndex = 4;
            // 
            // buttonEgyenes
            // 
            this.buttonEgyenes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonEgyenes.Location = new System.Drawing.Point(207, 33);
            this.buttonEgyenes.Name = "buttonEgyenes";
            this.buttonEgyenes.Size = new System.Drawing.Size(74, 24);
            this.buttonEgyenes.TabIndex = 6;
            this.buttonEgyenes.Text = "Egyenes";
            this.buttonEgyenes.UseVisualStyleBackColor = true;
            this.buttonEgyenes.Click += new System.EventHandler(this.buttonEgyenes_Click);
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FormFoAblak";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VonalÁbra";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelAbra;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelX;
        private System.Windows.Forms.Button buttonAbrazol;
        private System.Windows.Forms.TextBox textBoxYErtekek;
        private System.Windows.Forms.TextBox textBoxXErtekek;
        private System.Windows.Forms.Button buttonEgyenes;
    }
}

