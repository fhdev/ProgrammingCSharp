﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

namespace IkonSzerkeszto
{
    class BinarisKep
    {
        private int pixelMeret;
        private int[,] pixelMezo;
        public int[,] PixelMezo
        {
            get { return pixelMezo; }
        }

        public BinarisKep(int szelesseg, int magassag, bool feher, int oldalMeret)
        {
            pixelMezo = new int[szelesseg, magassag];
            for (int i = 0; i < szelesseg; i++)
            {
                for (int j = 0; j < magassag; j++)
                {
                    if (feher)
                    {
                        pixelMezo[i, j] = 1;
                    }
                    else
                    {
                        pixelMezo[i, j] = 0;
                    }
                }
            }
            pixelMeret = oldalMeret;
        }

        public void PixelBeallit(int X , int Y, bool feher)
        {
            if(feher)
            {
                pixelMezo[X, Y] = 1;
            }
            else
            {
                pixelMezo[X, Y] = 0;
            }
        }

        public void Rajzol(Graphics eszkoz)
        {
            SolidBrush ecset = new SolidBrush(Color.Black);
            Pen toll = new Pen(Color.Green);
            for (int i = 0; i < pixelMezo.GetLength(0); i++)
            {
                for (int j = 0; j < pixelMezo.GetLength(1); j++)
                {
                    if (pixelMezo[i, j] == 1)
                    {
                        ecset.Color = Color.White;
                    }
                    else
                    {
                        ecset.Color = Color.Black;
                    }
                    eszkoz.FillRectangle(ecset, i * pixelMeret, j * pixelMeret, pixelMeret, pixelMeret);
                    eszkoz.DrawRectangle(toll, i * pixelMeret, j * pixelMeret, pixelMeret, pixelMeret);
                }
            }
            ecset.Dispose();
            toll.Dispose();
        }

        public void Ment(string fajlNev)
        {
            StreamWriter SW = new StreamWriter(fajlNev);
            for (int i = 0; i < pixelMezo.GetLength(1); i++)
            {
                string sor = "";
                for (int j = 0; j < pixelMezo.GetLength(0); j++)
                {
                    sor += pixelMezo[j, i].ToString();
                }
                SW.WriteLine(sor);
            }
            SW.Close();
            SW.Dispose();
        }

        public void Beolvas(string fajlNev)
        {
            StreamReader SR = new StreamReader(fajlNev);
            for (int i = 0; i < pixelMezo.GetLength(1); i++)
            {
                string sor = SR.ReadLine();
                for (int j = 0; j < sor.Length; j++)
                {
                    pixelMezo[j, i] = int.Parse(sor[j].ToString());
                }
            }
            SR.Close();
            SR.Dispose();
        }
    }
}
