﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace IkonSzerkeszto
{
    public partial class FormFoAblak : Form
    {
        private BinarisKep kep;
        private const int pixelMeret = 30;
        public FormFoAblak()
        {
            InitializeComponent();
            typeof(Panel).InvokeMember("DoubleBuffered",
                           BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                           null,
                           panelRajz,
                           new object[] { true });
            kep = new BinarisKep(panelRajz.Width / pixelMeret, panelRajz.Height / pixelMeret, false, pixelMeret);
        }

        private void panelFeketeFeher_Click(object sender, EventArgs e)
        {
            panelAktualis.BackColor = (sender as Panel).BackColor;
        }

        private void panelRajz_Paint(object sender, PaintEventArgs e)
        {
            kep.Rajzol(e.Graphics);
        }

        private void panelRajz_MouseDown(object sender, MouseEventArgs e)
        {
            kep.PixelBeallit(e.X / pixelMeret, e.Y / pixelMeret, panelAktualis.BackColor == Color.White);
            panelRajz.Invalidate();
        }

        private void buttonMent_Click(object sender, EventArgs e)
        {
            kep.Ment("picture.txt");
        }

        private void buttonBeolvas_Click(object sender, EventArgs e)
        {
            kep.Beolvas("picture.txt");
            panelRajz.Invalidate();
        }


    }
}
