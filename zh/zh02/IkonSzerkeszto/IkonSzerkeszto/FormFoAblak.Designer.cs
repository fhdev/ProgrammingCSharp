﻿namespace IkonSzerkeszto
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelFekete = new System.Windows.Forms.Panel();
            this.panelFeher = new System.Windows.Forms.Panel();
            this.buttonMent = new System.Windows.Forms.Button();
            this.buttonBetolt = new System.Windows.Forms.Button();
            this.panelAktualis = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelRajz = new System.Windows.Forms.Panel();
            this.panelAktualis.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelFekete
            // 
            this.panelFekete.BackColor = System.Drawing.Color.Black;
            this.panelFekete.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFekete.Location = new System.Drawing.Point(15, 16);
            this.panelFekete.Margin = new System.Windows.Forms.Padding(2);
            this.panelFekete.Name = "panelFekete";
            this.panelFekete.Size = new System.Drawing.Size(38, 41);
            this.panelFekete.TabIndex = 1;
            this.panelFekete.Click += new System.EventHandler(this.panelFeketeFeher_Click);
            // 
            // panelFeher
            // 
            this.panelFeher.BackColor = System.Drawing.Color.White;
            this.panelFeher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFeher.Location = new System.Drawing.Point(75, 16);
            this.panelFeher.Margin = new System.Windows.Forms.Padding(2);
            this.panelFeher.Name = "panelFeher";
            this.panelFeher.Size = new System.Drawing.Size(38, 41);
            this.panelFeher.TabIndex = 2;
            this.panelFeher.Click += new System.EventHandler(this.panelFeketeFeher_Click);
            // 
            // buttonMent
            // 
            this.buttonMent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonMent.Location = new System.Drawing.Point(510, 73);
            this.buttonMent.Margin = new System.Windows.Forms.Padding(2);
            this.buttonMent.Name = "buttonMent";
            this.buttonMent.Size = new System.Drawing.Size(68, 41);
            this.buttonMent.TabIndex = 3;
            this.buttonMent.Text = "Ment";
            this.buttonMent.UseVisualStyleBackColor = true;
            this.buttonMent.Click += new System.EventHandler(this.buttonMent_Click);
            // 
            // buttonBetolt
            // 
            this.buttonBetolt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonBetolt.Location = new System.Drawing.Point(510, 130);
            this.buttonBetolt.Margin = new System.Windows.Forms.Padding(2);
            this.buttonBetolt.Name = "buttonBetolt";
            this.buttonBetolt.Size = new System.Drawing.Size(68, 43);
            this.buttonBetolt.TabIndex = 4;
            this.buttonBetolt.Text = "Betölt";
            this.buttonBetolt.UseVisualStyleBackColor = true;
            this.buttonBetolt.Click += new System.EventHandler(this.buttonBeolvas_Click);
            // 
            // panelAktualis
            // 
            this.panelAktualis.BackColor = System.Drawing.Color.Black;
            this.panelAktualis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAktualis.Controls.Add(this.panel1);
            this.panelAktualis.Location = new System.Drawing.Point(458, 16);
            this.panelAktualis.Margin = new System.Windows.Forms.Padding(2);
            this.panelAktualis.Name = "panelAktualis";
            this.panelAktualis.Size = new System.Drawing.Size(38, 41);
            this.panelAktualis.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 100);
            this.panel1.TabIndex = 0;
            // 
            // panelRajz
            // 
            this.panelRajz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelRajz.Location = new System.Drawing.Point(15, 73);
            this.panelRajz.Name = "panelRajz";
            this.panelRajz.Size = new System.Drawing.Size(483, 483);
            this.panelRajz.TabIndex = 6;
            this.panelRajz.Paint += new System.Windows.Forms.PaintEventHandler(this.panelRajz_Paint);
            this.panelRajz.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelRajz_MouseDown);
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 566);
            this.Controls.Add(this.panelRajz);
            this.Controls.Add(this.panelAktualis);
            this.Controls.Add(this.buttonBetolt);
            this.Controls.Add(this.buttonMent);
            this.Controls.Add(this.panelFeher);
            this.Controls.Add(this.panelFekete);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "FormFoAblak";
            this.Text = "IkonSzerkeszto";
            this.panelAktualis.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelFekete;
        private System.Windows.Forms.Panel panelFeher;
        private System.Windows.Forms.Button buttonMent;
        private System.Windows.Forms.Button buttonBetolt;
        private System.Windows.Forms.Panel panelAktualis;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelRajz;
    }
}

