﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VektorSzorzas
{
    class Vect
    {
        public int[] Vector;

        public Vect(int X, int Y, int Z)
        {
            Vector = new int[3];
            Vector[0] = X;
            Vector[1] = Y;
            Vector[2] = Z;
        }

        public Vect(int[] A)
        {
            Vector = new int[3];
            if (A.GetLength(0) < 3)
            {
                for (int i = 0; i < 3; i++)
                {
                    Vector[i] = 0;
                }
                MessageBox.Show("Vector dimensions not agree!");
            }
            for (int i = 0; i < 3; i++)
            {
                Vector[i] = A[i];
            }
        }

        public int DotProduct(Vect B)
        {
            int Result=0;
            for (int i = 0; i < 3; i++)
            {
                Result += Vector[i] * B.Vector[i];
            }
            return Result;
        }

        public Vect CrossProduct(Vect A, Vect B)
        {
            Vect Result = new Vect(0, 0, 0);
            Result.Vector[0] = A.Vector[1] * B.Vector[2] - A.Vector[2] * B.Vector[1];
            Result.Vector[1] = -(A.Vector[0] * B.Vector[2] - A.Vector[2] * B.Vector[0]);
            Result.Vector[2] = A.Vector[0] * B.Vector[1] - A.Vector[1] * B.Vector[0];
            return Result;
        }
    }
}
