﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VektorSzorzas
{
    public partial class Form1 : Form
    {
        Vect A, B, C;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            A = new Vect(0, 0, 0);
            B = new Vect(0, 0, 0);
            C = new Vect(0, 0, 0);
        }

        private bool GetVectors()
        {
            char[] Separator = { ';' };
            string[] VectAString = textBoxVectA.Text.Split(Separator);
            string[] VectBString = textBoxVectB.Text.Split(Separator);
            if (VectAString.GetLength(0) < 3 || VectBString.GetLength(0) < 3)
            {
                return false;
            }
            for (int i = 0; i < 3; i++)
            {
                A.Vector[i] = int.Parse(VectAString[i]);
                B.Vector[i] = int.Parse(VectBString[i]);
            }
            return true;
        }

        private void buttonDot_Click(object sender, EventArgs e)
        {
            textBoxResult.Text = "";
            GetVectors();
            textBoxResult.Text = A.DotProduct(B).ToString();
        }

        private void buttonCross_Click(object sender, EventArgs e)
        {
            textBoxResult.Text = "";
            GetVectors();
            C = A.CrossProduct(A, B);
            for (int i = 0; i < 3; i++)
            {
                textBoxResult.Text += C.Vector[i].ToString();
                if (i < 2)
                {
                    textBoxResult.Text += ";";
                }
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
