﻿namespace VektorSzorzas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.labelVectA = new System.Windows.Forms.Label();
            this.textBoxVectA = new System.Windows.Forms.TextBox();
            this.labelVectB = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.textBoxVectB = new System.Windows.Forms.TextBox();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.buttonDot = new System.Windows.Forms.Button();
            this.buttonCross = new System.Windows.Forms.Button();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculateDotProcuctToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculateCrossProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(382, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // labelVectA
            // 
            this.labelVectA.AutoSize = true;
            this.labelVectA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelVectA.Location = new System.Drawing.Point(20, 59);
            this.labelVectA.Name = "labelVectA";
            this.labelVectA.Size = new System.Drawing.Size(71, 25);
            this.labelVectA.TabIndex = 1;
            this.labelVectA.Text = "VectA";
            // 
            // textBoxVectA
            // 
            this.textBoxVectA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxVectA.Location = new System.Drawing.Point(114, 56);
            this.textBoxVectA.Name = "textBoxVectA";
            this.textBoxVectA.Size = new System.Drawing.Size(100, 30);
            this.textBoxVectA.TabIndex = 2;
            // 
            // labelVectB
            // 
            this.labelVectB.AutoSize = true;
            this.labelVectB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelVectB.Location = new System.Drawing.Point(21, 101);
            this.labelVectB.Name = "labelVectB";
            this.labelVectB.Size = new System.Drawing.Size(70, 25);
            this.labelVectB.TabIndex = 3;
            this.labelVectB.Text = "VectB";
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelResult.Location = new System.Drawing.Point(21, 143);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(72, 25);
            this.labelResult.TabIndex = 4;
            this.labelResult.Text = "Result";
            // 
            // textBoxVectB
            // 
            this.textBoxVectB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxVectB.Location = new System.Drawing.Point(114, 101);
            this.textBoxVectB.Name = "textBoxVectB";
            this.textBoxVectB.Size = new System.Drawing.Size(100, 30);
            this.textBoxVectB.TabIndex = 5;
            // 
            // textBoxResult
            // 
            this.textBoxResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxResult.Location = new System.Drawing.Point(114, 143);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.Size = new System.Drawing.Size(100, 30);
            this.textBoxResult.TabIndex = 6;
            // 
            // buttonDot
            // 
            this.buttonDot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDot.Location = new System.Drawing.Point(239, 56);
            this.buttonDot.Name = "buttonDot";
            this.buttonDot.Size = new System.Drawing.Size(126, 30);
            this.buttonDot.TabIndex = 7;
            this.buttonDot.Text = "Dot [.]";
            this.buttonDot.UseVisualStyleBackColor = true;
            this.buttonDot.Click += new System.EventHandler(this.buttonDot_Click);
            // 
            // buttonCross
            // 
            this.buttonCross.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCross.Location = new System.Drawing.Point(239, 101);
            this.buttonCross.Name = "buttonCross";
            this.buttonCross.Size = new System.Drawing.Size(126, 30);
            this.buttonCross.TabIndex = 8;
            this.buttonCross.Text = "Cross [x]";
            this.buttonCross.UseVisualStyleBackColor = true;
            this.buttonCross.Click += new System.EventHandler(this.buttonCross_Click);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculateDotProcuctToolStripMenuItem,
            this.calculateCrossProductToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // calculateDotProcuctToolStripMenuItem
            // 
            this.calculateDotProcuctToolStripMenuItem.Name = "calculateDotProcuctToolStripMenuItem";
            this.calculateDotProcuctToolStripMenuItem.Size = new System.Drawing.Size(233, 24);
            this.calculateDotProcuctToolStripMenuItem.Text = "Calculate Dot Procuct";
            this.calculateDotProcuctToolStripMenuItem.Click += new System.EventHandler(this.buttonDot_Click);
            // 
            // calculateCrossProductToolStripMenuItem
            // 
            this.calculateCrossProductToolStripMenuItem.Name = "calculateCrossProductToolStripMenuItem";
            this.calculateCrossProductToolStripMenuItem.Size = new System.Drawing.Size(233, 24);
            this.calculateCrossProductToolStripMenuItem.Text = "Calculate Cross Product";
            this.calculateCrossProductToolStripMenuItem.Click += new System.EventHandler(this.buttonCross_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 203);
            this.Controls.Add(this.buttonCross);
            this.Controls.Add(this.buttonDot);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.textBoxVectB);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.labelVectB);
            this.Controls.Add(this.textBoxVectA);
            this.Controls.Add(this.labelVectA);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label labelVectA;
        private System.Windows.Forms.TextBox textBoxVectA;
        private System.Windows.Forms.Label labelVectB;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.TextBox textBoxVectB;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.Button buttonDot;
        private System.Windows.Forms.Button buttonCross;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculateDotProcuctToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculateCrossProductToolStripMenuItem;
    }
}

