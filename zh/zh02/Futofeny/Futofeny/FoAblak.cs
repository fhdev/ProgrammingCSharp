﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Futofeny
{
    public partial class FoAblak : Form
    {
        private Futofeny futofeny;
        public FoAblak()
        {
            InitializeComponent();
            typeof(Panel).InvokeMember("DoubleBuffered",
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.SetProperty,
                null,
                panelLampa, new object[] { true });
            futofeny = new Futofeny(panelLampa.ClientSize.Width, panelLampa.ClientSize.Height);
            timerRajzol.Start();
        }

        private void buttonValt_Click(object sender, EventArgs e)
        {
            futofeny.Irany = -futofeny.Irany;
        }

        private void panelLampa_Paint(object sender, PaintEventArgs e)
        {
            futofeny.Rajzol(e.Graphics);
        }

        private void timerRajzol_Tick(object sender, EventArgs e)
        {
            futofeny.Leptet();
            panelLampa.Invalidate();
        }
    }
}
