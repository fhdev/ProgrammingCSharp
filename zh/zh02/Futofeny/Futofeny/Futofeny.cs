﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Futofeny
{
    class Futofeny
    {
        private const int darab = 10;
        private const int ido = 50;

        private int szelesseg;
        private int magassag;
        private int irany;
        private int index;

        private Rectangle[] lampak;

        private SolidBrush ecset;

        private Stopwatch stopper;

        public int Irany
        {
            get
            {
                return irany;
            }
            set
            {
                irany = (value >= 0) ? 1 : -1;
            }
        }

        public Futofeny(int panelSzelesseg, int panelMagassag)
        {
            szelesseg = panelSzelesseg / (darab * 2 - 1);
            magassag = panelMagassag;
            irany = 1;
            index = 0;
            lampak = new Rectangle[darab];
            for (int i = 0; i < darab; i++)
            {
                lampak[i] = new Rectangle(i * 2 * szelesseg, 0, szelesseg, magassag);
            }
            ecset = new SolidBrush(Color.LightGreen);
            stopper = Stopwatch.StartNew();
        }

        public void Rajzol(Graphics eszkoz)
        {
            for (int i = 0; i < darab; i++)
            {
                if (i == index)
                    ecset.Color = Color.FromArgb(0, byte.MaxValue, 0);
                else
                    ecset.Color = Color.DimGray;
                eszkoz.FillRectangle(ecset, lampak[i]);
            }
        }

        public void Leptet()
        {
            if (stopper.ElapsedMilliseconds < ido)
                return;
            index += irany;
            if (index < 0)
                index = darab - 1;
            if (index > darab - 1)
                index = 0;
            stopper.Restart();
        }
    }
}
