﻿namespace Futofeny
{
    partial class FoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelLampa = new System.Windows.Forms.Panel();
            this.timerRajzol = new System.Windows.Forms.Timer(this.components);
            this.buttonValt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panelLampa
            // 
            this.panelLampa.BackColor = System.Drawing.Color.Black;
            this.panelLampa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLampa.Location = new System.Drawing.Point(12, 12);
            this.panelLampa.Name = "panelLampa";
            this.panelLampa.Size = new System.Drawing.Size(572, 50);
            this.panelLampa.TabIndex = 0;
            this.panelLampa.Paint += new System.Windows.Forms.PaintEventHandler(this.panelLampa_Paint);
            // 
            // timerRajzol
            // 
            this.timerRajzol.Interval = 17;
            this.timerRajzol.Tick += new System.EventHandler(this.timerRajzol_Tick);
            // 
            // buttonValt
            // 
            this.buttonValt.Location = new System.Drawing.Point(596, 12);
            this.buttonValt.Name = "buttonValt";
            this.buttonValt.Size = new System.Drawing.Size(75, 23);
            this.buttonValt.TabIndex = 1;
            this.buttonValt.Text = "Irányváltás";
            this.buttonValt.UseVisualStyleBackColor = true;
            this.buttonValt.Click += new System.EventHandler(this.buttonValt_Click);
            // 
            // FoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 74);
            this.Controls.Add(this.buttonValt);
            this.Controls.Add(this.panelLampa);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FoAblak";
            this.Text = "Futófény";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLampa;
        private System.Windows.Forms.Timer timerRajzol;
        private System.Windows.Forms.Button buttonValt;
    }
}

