﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace SzinesNegyzetek
{
    class PanelDrawing
    {
        private Color[,] Field;
        private int[,] Clicks;
        private Color PrevColor;
        public int PixelSize;

        public PanelDrawing(int X, int Y)
        {
            Random Rnd = new Random();
            Field = new Color[X, Y];
            Clicks = new int[X, Y];
            for (int i = 0; i < X; i++)
            {
                for (int j = 0; j < Y; j++)
                {
                    Field[i,j]=new Color();
                    Field[i, j] = Color.White;
                    Clicks[i, j] = 0;
                }
            }
            PixelSize = 50;
            PrevColor = Color.FromArgb(Rnd.Next(255), Rnd.Next(255), Rnd.Next(255));
        }

        public PanelDrawing(int X, int Y, int PS)
        {
            Random Rnd = new Random();
            Field = new Color[X, Y];
            Clicks = new int[X, Y];
            for (int i = 0; i < X; i++)
            {
                for (int j = 0; j < Y; j++)
                {
                    Field[i, j] = new Color();
                    Field[i, j] = Color.White;
                    Clicks[i, j] = 0;
                }
            }
            PixelSize = PS;
            PrevColor = Color.FromArgb(Rnd.Next(255), Rnd.Next(255), Rnd.Next(255));
        }

        public void GenerateRndField()
        {
            Random Rnd = new Random();
            for (int i = 0; i < Field.GetLength(0); i++)
            {
                for (int j = 0; j < Field.GetLength(1); j++)
                {
                    Field[i, j] = Color.FromArgb(Rnd.Next(255), Rnd.Next(255), Rnd.Next(255));
                }
            }
        }

        public void PaintField(Panel Board, PaintEventArgs E)
        {
            Board.Width = Field.GetLength(0) * PixelSize + 1;
            Board.Height = Field.GetLength(1) * PixelSize + 1;
            Pen P = new Pen(Color.Black);
            Font F = new Font("Arial", PixelSize/2);
            for (int i = 0; i < Field.GetLength(0); i++)
            {
                for (int j = 0; j < Field.GetLength(1); j++)
                {
                    SolidBrush B = new SolidBrush(Field[i,j]); 
                    Rectangle Square=new Rectangle(i*PixelSize, j*PixelSize, PixelSize, PixelSize);
                    E.Graphics.FillRectangle(B, Square);
                    E.Graphics.DrawRectangle(P, Square);
                    B = new SolidBrush(Color.Black);
                    E.Graphics.DrawString(Clicks[i, j].ToString(), F, B, new Point(i * PixelSize, j * PixelSize));
                }
            }
        }

        public void ClickOnCoordinate(int PX, int PY)
        {
            int X = PX / PixelSize;
            int Y = PY / PixelSize;
            Color Temp = PrevColor;
            PrevColor = Field[X, Y];
            Field[X, Y] = Temp;
            Clicks[X, Y]++;
        }

    }
}
