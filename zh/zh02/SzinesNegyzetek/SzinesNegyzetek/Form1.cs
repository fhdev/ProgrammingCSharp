﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SzinesNegyzetek
{
    public partial class Form1 : Form
    {
        PanelDrawing PD;

        public Form1()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            PD.PaintField(sender as Panel, e);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PD = new PanelDrawing(10, 10);
            PD.GenerateRndField();
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            PD.ClickOnCoordinate(e.X, e.Y);
            panel1.Invalidate();
        }
    }
}
