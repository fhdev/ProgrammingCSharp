﻿using System;
using System.IO;

namespace Titkositas
{
    class Crypto
    {
        /* A betűk száma az angol ábécében, így a helyettesítési tábla mérete is. */
        private const int numberOfLetters = 26;

        /* A helyettesítési tábla. Az első eleme az A betű helyett használatos betűt tartalmazza, a második eleme a B
         * betű helyett használatos betűt, és így tovább. 
         */
        private char[] substitutionTable;

        /* Random objektum a random szám generáláshoz. */
        private static Random randomGenerator = new Random();

        /// <summary>
        /// A titkosító osztály konstruktora.
        /// </summary>
        public Crypto()
        {
            /* Feltöltjük a helyettesítő táblát véletlenszerűen. */
            substitutionTable = new char[numberOfLetters];
            for(int i = 0; i < numberOfLetters; i++)
            {
                /* Választunk egy random betűt az adott betű helyettesítésére. Az i=0 index tartozik a helyettesítési
                 * táblában az A, az i=1 index a B betűkhöz és így tovább. 
                 */
                substitutionTable[i] = (char)randomGenerator.Next((int)'A', (int)'A' + numberOfLetters);
                /* Leellenőrizzük, hogy amit most választottunk helyettesítő betű szerepel-e már a táblázatban, mert
                 * akkor másikat kell helyette választanunk. Végigmegyünk a helyettesítési tábla korábban generált 
                 * elemein.
                 */
                for (int j = 0; j < i; j++)
                {
                    /* Ha a most generált betű megegyezik egy korábbi betűvel a helyettesítési táblában, akkor újra
                     * kell generálni. 
                     */
                    if(substitutionTable[i] == substitutionTable[j])
                    {
                        /* Újra kell generálni a mostani betűt, ezért az i indexet eggyel csökkentjük, így nem lépünk 
                         * tovább a következő elemre, a külső for ciklus lefutásakor, hanem újra generáljuk a mostanit.
                         */
                        i--;
                        /* Nem folytatjuk az ellenőrzést, hiszen már biztosan nem jó a most generált betű. */
                        break;
                    }
                }
            }
        }
        /// <summary>
        /// Ez a metódus titkosítja a neki paraméterként átadott szöveget.
        /// </summary>
        /// <param name="text">A titkosítandó eredeti szöveg.</param>
        /// <returns>A titkosított szöveg.</returns>
        public string Encrypt(string text)
        {
            /* Ebbe a változóba töltjük be a titkosított szöveget. */
            string code = string.Empty;
            /* Nagybetűsre konvertáljuk a kapott szöveget. */
            string textUpper = text.ToUpper();
            /* Végigmegyünk a kapott szövegen. */
            for (int i = 0; i < textUpper.Length; i++)
            {
                /* Kiszámoljuk hogy a helyettesítési tábla melyik indexe tartozik az adott betűhöz. Az A betű  
                 * karakterkódja benne van a charCodeOfA konstansban. Így A betű esetén az index 0 lesz, B betű esetén
                 * 1, és így tovább. 
                 */
                int substituteIndex = (int)(textUpper[i] - 'A');
                /* Ha az index a helyettesítési táblán kívülre mutat, az azt jelenti, hogy az aktuális karakter nem
                 * egy nagybetű. Ebben az esetben visszaadjuk az eddig kódolt szöveget, és kilépünk. 
                 */
                if((substituteIndex < 0 ) || (substituteIndex >= numberOfLetters))
                {
                    return code;
                }
                /* A titkosított szöveghez hozzáadjuk az eredeti szöveg aktuális betűjének megfelelő titkosított 
                 * karaktert. 
                 */
                code += substitutionTable[substituteIndex];
            }
            /* Visszatérünk a teljes titkosított szöveggel. */
            return code;
        }
        /// <summary>
        /// Ez a metódus dekódolja a neki paraméterként átadott titkosított szöveget.
        /// </summary>
        /// <param name="code">A dekódolandó titkosított szöveg.</param>
        /// <returns>A dekódolt eredeti szöveg.</returns>
        public string Decrypt(string code)
        {
            /* Ebbe a változóba töltjük be a dekódolt szöveget. */
            string text = string.Empty;
            /* Nagybetűssé konvertáljuk a titkosított szöveget. */
            string codeUpper = code.ToUpper();
            /* Végigmegyünk a titkosított szövegen. */
            for(int i = 0; i < codeUpper.Length; i++)
            {
                /* Megkeressük, hogy az adott betű milyen indexhez tartozik a helyettesítési táblában. */
                int substituteIndex = Array.IndexOf(substitutionTable, codeUpper[i]);
                /* Ha nem található meg a helyettesítési táblában az adott betű, akkor visszaadjuk az eddig dekódolt
                 * szöveget. 
                 */
                if (substituteIndex < 0)
                {
                    return text;
                }
                /* A szöveghez hozzáadjuk a dekódolt karaktert. A 0 index tartozik az A betűhöz, így a 0 index esetén
                 * a 65 kódú karaktert kell a szöveghez adni. 
                 */
                text += (char)((int)'A' + substituteIndex);
            }
            return text;
        }
        /// <summary>
        /// Ez a metódus a neki átadott nevű fájlba írja ki a helyettesítési táblát.
        /// </summary>
        /// <param name="fileName">A kívánt fájlnév.</param>
        public void WriteTableToFile(string fileName)
        {
            /* Létrehozzuk a fájlt. */
            StreamWriter file = new StreamWriter(fileName);
            /* A fájl első sorába beírjuk a fejlécet. */
            file.WriteLine("Letter;Code");
            /* Végigmegyünk az helyettesítési tábla összes elemén. */
            for(int i = 0; i < numberOfLetters; i++)
            {
                /* Kiírjuk az eredeti karaktert és mellé a helyettesített karaktert pontosvesszővel elválasztva. */
                char originalChar = (char)((int)'A' + i);
                string line = originalChar + ";" + substitutionTable[i];
                file.WriteLine(line);
            }
            /* Bezárjuk a fájlt és felszabadítjuk az erőforrásokat. */
            file.Close();
            file.Dispose();
        }
    }
}
