﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Titkositas
{
    public partial class FormMain : Form
    {
        /* Titkosító objektum. */
        private Crypto crypto;
        public FormMain()
        {
            InitializeComponent();
            /* Létrehozzuk a titkosító objektumot. */
            crypto = new Crypto();
        }
        /// <summary>
        /// Ez a metódus a titkosítás menüpontkra klikkeléskor fog lefutni. 
        /// </summary>
        /// <param name="sender">Az esemény küldő objektuma.</param>
        /// <param name="e">Az eseményre jellemző argumentumokat összefoglaló objektum.</param>
        private void encryptOriginalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /* Titkosítjuk a szöveget. */
            string encryptedText = crypto.Encrypt(textBoxOriginal.Text);
            /* Ha a titkosított szöveg nem ugyanolyan hosszú, mint a titkosítatlan, akkor az eredeti szövegben
             * valamilyen meg nem engedett karakter volt, így a titkosítás csak részlegesen sikerült. 
             * Tájékoztatjuk a felhasználót. 
             */
            if(encryptedText.Length != textBoxOriginal.Text.Length)
            {
                MessageBox.Show("The original text contains invalid characters. Encryption ended with partial success.",
                                "Invalid character in original text",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
            /* Kiírjuk a titkosított szöveget. */
            textBoxEncrypted.Text = encryptedText;
        }
        /// <summary>
        /// Ez a metódus a dekódolás menüpontra klikkeléskor fog lefutni.
        /// </summary>
        /// <param name="sender">Az esemény küldő objektuma.</param>
        /// <param name="e">Az eseményre jellemző argumentumokat összefoglaló objektum.</param>
        private void decryptEncryptedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /* Dekódoljuk a szöveget. */
            string decryptedText = crypto.Decrypt(textBoxEncrypted.Text);
            /* Ha a dekódolt szöveg nem ugyanolyan hosszú, mint a titkosított, akkor az eredeti szövegben
             * valamilyen meg nem engedett karakter volt, így a dekódolás csak részlegesen sikerült. 
             * Tájékoztatjuk a felhasználót. 
             */
            if (decryptedText.Length != textBoxEncrypted.Text.Length)
            {
                MessageBox.Show("The encrypted text contains invalid characters. Decryption ended with partial success.",
                                "Invalid character in encrypted text",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
            /* Kiírjuk a dekódolt szöveget. */
            textBoxDecrypted.Text = decryptedText;
        }
        /// <summary>
        /// Ez a metódus a helyettesítési tábla fájlba írása menüpontra való klikkeléskor fog lefutni. 
        /// </summary>
        /// <param name="sender">Az esemény küldő objektuma.</param>
        /// <param name="e">Az eseményre jellemző argumentumokat összefoglaló objektum.</param>
        private void saveEncyptionTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /* Elmentjük a helyettesítési táblát a crypto.csv fájlba. */
            crypto.WriteTableToFile("crypto.csv");
        }
    }
}
