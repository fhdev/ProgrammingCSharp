﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlakzatRajzolas
{
    public partial class FormFoAblak : Form
    {
        private List<Alakzat> alakzatok;
        public FormFoAblak()
        {
            InitializeComponent();
            alakzatok = new List<Alakzat>();
        }

        private void buttonRajzol_Click(object sender, EventArgs e)
        {
            int p1X, p1Y, p2X, p2Y;
            int.TryParse(textBoxP1X.Text, out p1X);
            int.TryParse(textBoxP1Y.Text, out p1Y);
            int.TryParse(textBoxP2X.Text, out p2X);
            int.TryParse(textBoxP2Y.Text, out p2Y); 
            Point pt1 = new Point(p1X, p1Y);
            Point pt2 = new Point(p2X, p2Y);
            if(!panelRajz.ClientRectangle.Contains(pt1))
            {
                MessageBox.Show("Az első pont nincs a rajzoláshoz használt területen belül.",
                                "Érvénytelen első pont",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return;
            }
            if (!panelRajz.ClientRectangle.Contains(pt2))
            {
                MessageBox.Show("A második pont nincs a rajzoláshoz használt területen belül.",
                                "Érvénytelen második pont",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return;
            }
            Alakzat.Tipusok tipus;
            switch(textBoxTipus.Text.ToLower())
            {
                case "vonal":
                    {
                        tipus = Alakzat.Tipusok.Vonal;
                        break;
                    }
                case "teglalap":
                    {
                        tipus = Alakzat.Tipusok.Teglalap;
                        break;
                    }
                case "ellipszis":
                    {
                        tipus = Alakzat.Tipusok.Ellipszis;
                        break;
                    }
                default:
                    {
                        MessageBox.Show("A megadott alakzattípus \"" + textBoxTipus.Text + "\" nem létezik.",
                                        "Érvénytelen alakzattípus", 
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                        return;
                    }
            }
            alakzatok.Add(new Alakzat(pt1, pt2, tipus));
            panelRajz.Invalidate();
        }

        private void panelRajzol_Paint(object sender, PaintEventArgs e)
        {
            foreach (Alakzat alakzat in alakzatok)
            {
                alakzat.Rajzol(e.Graphics);
            }
        }
    }
}
