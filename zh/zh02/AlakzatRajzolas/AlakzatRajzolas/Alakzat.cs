﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace AlakzatRajzolas
{
    class Alakzat
    {
        public enum Tipusok { Vonal, Teglalap, Ellipszis };

        private Point pont1;
        private Point pont2;
        private Tipusok tipus;
        private Color szin;

        private static Random veletlen = new Random();

        public Alakzat(Point pt1, Point pt2, Tipusok tip)
        {
            pont1 = pt1;
            pont2 = pt2;
            tipus = tip;
            szin = Color.FromArgb(veletlen.Next(0, 256), veletlen.Next(0, 256), veletlen.Next(0, 256));
        }

        public void Rajzol(Graphics graphics)
        {
            SolidBrush ecset = new SolidBrush(szin);
            Pen toll = new Pen(szin);
            if(tipus == Tipusok.Vonal)
            {
                graphics.DrawLine(toll, pont1, pont2);
            }
            else
            {
                Point balFelso = new Point(Math.Min(pont1.X, pont2.X), Math.Min(pont1.Y, pont2.Y));
                Size meret = new Size(Math.Abs(pont1.X - pont2.X), Math.Abs(pont1.Y - pont2.Y));
                Rectangle befoglalo = new Rectangle(balFelso, meret);
                if(tipus == Tipusok.Ellipszis)
                {
                    graphics.FillEllipse(ecset, befoglalo);
                }
                if(tipus == Tipusok.Teglalap)
                {
                    graphics.FillRectangle(ecset, befoglalo);
                }
            }
        }

    }
}
