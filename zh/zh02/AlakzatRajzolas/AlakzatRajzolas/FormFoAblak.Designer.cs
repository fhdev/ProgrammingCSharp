﻿namespace AlakzatRajzolas
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxP1X = new System.Windows.Forms.TextBox();
            this.textBoxP1Y = new System.Windows.Forms.TextBox();
            this.textBoxP2X = new System.Windows.Forms.TextBox();
            this.textBoxP2Y = new System.Windows.Forms.TextBox();
            this.labelP1X = new System.Windows.Forms.Label();
            this.labelP2X = new System.Windows.Forms.Label();
            this.labelP1Y = new System.Windows.Forms.Label();
            this.labelP2Y = new System.Windows.Forms.Label();
            this.buttonRajzol = new System.Windows.Forms.Button();
            this.textBoxTipus = new System.Windows.Forms.TextBox();
            this.labelTipus = new System.Windows.Forms.Label();
            this.panelRajz = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // textBoxP1X
            // 
            this.textBoxP1X.Location = new System.Drawing.Point(29, 518);
            this.textBoxP1X.Name = "textBoxP1X";
            this.textBoxP1X.Size = new System.Drawing.Size(40, 20);
            this.textBoxP1X.TabIndex = 1;
            // 
            // textBoxP1Y
            // 
            this.textBoxP1Y.Location = new System.Drawing.Point(100, 518);
            this.textBoxP1Y.Name = "textBoxP1Y";
            this.textBoxP1Y.Size = new System.Drawing.Size(40, 20);
            this.textBoxP1Y.TabIndex = 2;
            // 
            // textBoxP2X
            // 
            this.textBoxP2X.Location = new System.Drawing.Point(28, 544);
            this.textBoxP2X.Name = "textBoxP2X";
            this.textBoxP2X.Size = new System.Drawing.Size(40, 20);
            this.textBoxP2X.TabIndex = 3;
            // 
            // textBoxP2Y
            // 
            this.textBoxP2Y.Location = new System.Drawing.Point(100, 544);
            this.textBoxP2Y.Name = "textBoxP2Y";
            this.textBoxP2Y.Size = new System.Drawing.Size(40, 20);
            this.textBoxP2Y.TabIndex = 4;
            // 
            // labelP1X
            // 
            this.labelP1X.AutoSize = true;
            this.labelP1X.Location = new System.Drawing.Point(9, 521);
            this.labelP1X.Name = "labelP1X";
            this.labelP1X.Size = new System.Drawing.Size(14, 13);
            this.labelP1X.TabIndex = 5;
            this.labelP1X.Text = "X";
            // 
            // labelP2X
            // 
            this.labelP2X.AutoSize = true;
            this.labelP2X.Location = new System.Drawing.Point(9, 547);
            this.labelP2X.Name = "labelP2X";
            this.labelP2X.Size = new System.Drawing.Size(14, 13);
            this.labelP2X.TabIndex = 6;
            this.labelP2X.Text = "X";
            // 
            // labelP1Y
            // 
            this.labelP1Y.AutoSize = true;
            this.labelP1Y.Location = new System.Drawing.Point(80, 521);
            this.labelP1Y.Name = "labelP1Y";
            this.labelP1Y.Size = new System.Drawing.Size(14, 13);
            this.labelP1Y.TabIndex = 7;
            this.labelP1Y.Text = "Y";
            // 
            // labelP2Y
            // 
            this.labelP2Y.AutoSize = true;
            this.labelP2Y.Location = new System.Drawing.Point(80, 547);
            this.labelP2Y.Name = "labelP2Y";
            this.labelP2Y.Size = new System.Drawing.Size(14, 13);
            this.labelP2Y.TabIndex = 8;
            this.labelP2Y.Text = "Y";
            // 
            // buttonRajzol
            // 
            this.buttonRajzol.Location = new System.Drawing.Point(437, 516);
            this.buttonRajzol.Name = "buttonRajzol";
            this.buttonRajzol.Size = new System.Drawing.Size(75, 23);
            this.buttonRajzol.TabIndex = 9;
            this.buttonRajzol.Text = "Rajzol";
            this.buttonRajzol.UseVisualStyleBackColor = true;
            this.buttonRajzol.Click += new System.EventHandler(this.buttonRajzol_Click);
            // 
            // textBoxTipus
            // 
            this.textBoxTipus.Location = new System.Drawing.Point(188, 518);
            this.textBoxTipus.Name = "textBoxTipus";
            this.textBoxTipus.Size = new System.Drawing.Size(100, 20);
            this.textBoxTipus.TabIndex = 10;
            // 
            // labelTipus
            // 
            this.labelTipus.AutoSize = true;
            this.labelTipus.Location = new System.Drawing.Point(151, 521);
            this.labelTipus.Name = "labelTipus";
            this.labelTipus.Size = new System.Drawing.Size(35, 13);
            this.labelTipus.TabIndex = 11;
            this.labelTipus.Text = "Típus";
            // 
            // panelRajz
            // 
            this.panelRajz.BackColor = System.Drawing.Color.White;
            this.panelRajz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelRajz.Location = new System.Drawing.Point(12, 12);
            this.panelRajz.Name = "panelRajz";
            this.panelRajz.Size = new System.Drawing.Size(500, 500);
            this.panelRajz.TabIndex = 12;
            this.panelRajz.Paint += new System.Windows.Forms.PaintEventHandler(this.panelRajzol_Paint);
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 612);
            this.Controls.Add(this.panelRajz);
            this.Controls.Add(this.labelTipus);
            this.Controls.Add(this.textBoxTipus);
            this.Controls.Add(this.buttonRajzol);
            this.Controls.Add(this.labelP2Y);
            this.Controls.Add(this.labelP1Y);
            this.Controls.Add(this.labelP2X);
            this.Controls.Add(this.labelP1X);
            this.Controls.Add(this.textBoxP2Y);
            this.Controls.Add(this.textBoxP2X);
            this.Controls.Add(this.textBoxP1Y);
            this.Controls.Add(this.textBoxP1X);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormFoAblak";
            this.Text = "AlakzatRajzolas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxP1X;
        private System.Windows.Forms.TextBox textBoxP1Y;
        private System.Windows.Forms.TextBox textBoxP2X;
        private System.Windows.Forms.TextBox textBoxP2Y;
        private System.Windows.Forms.Label labelP1X;
        private System.Windows.Forms.Label labelP2X;
        private System.Windows.Forms.Label labelP1Y;
        private System.Windows.Forms.Label labelP2Y;
        private System.Windows.Forms.Button buttonRajzol;
        private System.Windows.Forms.Label labelTipus;
        private System.Windows.Forms.TextBox textBoxTipus;
        private System.Windows.Forms.Panel panelRajz;
    }
}

