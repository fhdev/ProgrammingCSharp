﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kijelzo7
{
    public partial class FormFoAblak : Form
    {
        // a kijelző méretét meghatározó négyzet oldalhossza
        private const int negyzetOldal = 50;
        // a kijelző objektum
        private Kijelzo7 kijelzo;

        // az ablak konstruktora
        public FormFoAblak()
        {
            InitializeComponent();
            // létrehozzuk a kijelzőt
            kijelzo = new Kijelzo7(negyzetOldal, Color.Red);
        }

        // a kijelző paneljára rajzoljuk a kijelzőt
        private void panelKijelzo_Paint(object sender, PaintEventArgs e)
        {
            kijelzo.Rajzol(e.Graphics);
        }

        // a timer egyel lépteti a kijelzőn található értéket, ha elért a megjeleníthető
        // értékek végére, akkor újraindítja azt
        private void timerLeptet_Tick(object sender, EventArgs e)
        {
            kijelzo.Ertek = kijelzo.Ertek + 1;
            // panel újrarajzolása, hogy lássuk a frissített kijelzőt
            panelKijelzo.Invalidate();
        }

        // számláló indítása
        private void buttonSzamlalo_Click(object sender, EventArgs e)
        {
            timerLeptet.Start();
        }

        // kijelző átszínezése
        private void buttonSzin_Click(object sender, EventArgs e)
        {
            kijelzo.Szinez();
            // panel újrarajzolása, hogy lássuk a frissített kijelzőt
            panelKijelzo.Invalidate();
        }

        // kijelző értékének visszaállítása
        private void buttonVissza_Click(object sender, EventArgs e)
        {
            kijelzo.Ertek = 0;
            // panel újrarajzolása, hogy lássuk a frissített kijelzőt
            panelKijelzo.Invalidate();
        }

        // számláló megállítása
        private void buttonMegallit_Click(object sender, EventArgs e)
        {
            timerLeptet.Stop();
        }
    }
}
