﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace kijelzo7
{
    class Kijelzo7
    {
        // szegmensek száma
        private const int szegmensSzam  = 7;
        // a szegély szélesség hányszorosa legyen az alap négyzet oldalhosszának
        private const int szSzegely     = 1;
        // a rövidebbik oldal oldalhossza hányszorosa legyen az alap négyzet oldalhosszának
        private const int szRovid       = 1;
        // a hosszabbik oldal oldalhossza hányszorosa legyen az alap négyzet oldalhosszának
        private const int szHosszu      = 3;

        // a szegmenseket téglalapokkal reprezentáljuk
        private Rectangle[] szegmensek;
        // a szegmensek színe
        private Color szin;
        // az aktuálisan kirajzolni kívánt érték
        private int ertek;
        
        // az egyes szegmensek bal felső sarkának koordinátái, szélessége és magassága
        // -------
        // --000--
        // -5---1-
        // -5---1-
        // -5---1-
        // --666--
        // -4---2-
        // -4---2-
        // -4---3-
        // --333--
        // -------
        private static readonly int[,] szegmensPoziciok=
        {
        //    bal felső sarok x koordináta    bal felső sarok y koordináta          szélesség magasság
            { szSzegely + szRovid,            szSzegely,                            szHosszu, szRovid  },
            { szSzegely + szRovid + szHosszu, szSzegely + szRovid,                  szRovid,  szHosszu },
            { szSzegely + szRovid + szHosszu, szSzegely + 2 * szRovid + szHosszu,   szRovid,  szHosszu },
            { szSzegely + szRovid,            szSzegely + 2 * (szRovid + szHosszu), szHosszu, szRovid  },
            { szSzegely,                      szSzegely + 2 * szRovid + szHosszu,   szRovid,  szHosszu },
            { szSzegely,                      szSzegely + szRovid,                  szRovid,  szHosszu },
            { szSzegely + szRovid,            szSzegely + szRovid + szHosszu,       szHosszu, szRovid  }

        };

        // az egyes értékekhez melyik szegmenseket kell be- és kikapcsolni
        private static readonly bool[,] ertekTablaBool =
        {
        //    Szegmens sorszám                                                  Kijelzett érték
        //    0        1        2        3        4        5        6
            { true,    true,    true,    true,    true,    true,    false }, // 0
            { false,   true,    true,    false,   false,   false,   false }, // 1
            { true,    true,    false,   true,    true,    false,   true  }, // 2
            { true,    true,    true,    true,    false,   false,   true  }, // 3
            { false,   true,    true,    false,   false,   true,    true  }, // 4
            { true,    false,   true,    true,    false,   true,    true  }, // 5
            { true,    false,   true,    true,    true,    true,    true  }, // 6
            { true,    true,    true,    false,   false,   false,   false }, // 7
            { true,    true,    true,    true,    true,    true,    true  }, // 8
            { true,    true,    true,    true,    false,   true,    true  }  // 9
        };

        //// az egyes értékekhez melyik szegmenseket kell be- és kikapcsolni
        //private static readonly byte[] ertekTablaBajt =
        //{ 
        ////           Szegmens sorszám (0 - sötét, 1 - világít)
        ////           7    6    5    4    3    2    1    0
        //// Érték
        ////   0       X    0    1    1    1    1    1    1         =    63
        ////   1       X    0    0    0    0    1    1    0         =     6
        ////   2       X    1    0    1    1    0    1    1         =    91
        ////   3       X    1    0    0    1    1    1    1         =    79
        ////   4       X    1    1    0    0    1    1    0         =   102
        ////   5       X    1    1    0    1    1    0    1         =   109
        ////   6       X    1    1    1    1    1    0    1         =   125
        ////   7       X    0    0    0    0    1    1    1         =     7
        ////   8       X    1    1    1    1    1    1    1         =   127
        ////   9       X    1    1    0    1    1    1    1         =   111
        //    63,
        //    6,
        //    91,
        //    79,
        //    102,
        //    109,
        //    125,
        //    7,
        //    127,
        //    111
        //};

        // véletlenszám generátor
        private static Random veletlen = new Random();

        // csak olvasható tulajdonság, hogy tudjuk a kijelzőn lévő aktuális éréket
        public int Ertek
        {
            get
            {
                return ertek;
            }
            set
            {
                if ((value >= 0) && (value < ertekTablaBool.GetLength(0)))
                    ertek = value;
                else
                    ertek = 0;
            }
        }

        // a kijelző konstruktora
        public Kijelzo7(int oHossz, Color ujSzin)
        {
            // alapértelmezetten 0 értékről indulunk
            ertek = 0;
            szin = ujSzin;

            // az egyes szegmensek létrehozása
            szegmensek = new Rectangle[szegmensSzam];
            for (int i = 0; i < szegmensek.Length; i++)
            {
                Point balFelso = new Point(szegmensPoziciok[i, 0] * oHossz, 
                                           szegmensPoziciok[i, 1] * oHossz);
                Size meret = new Size(szegmensPoziciok[i, 2] * oHossz, 
                                      szegmensPoziciok[i, 3] * oHossz);
                szegmensek[i] = new Rectangle(balFelso, meret);
            }
        }

        // kijelző kirajzolása
        public void Rajzol(Graphics rTabla)
        {
            SolidBrush ecset = new SolidBrush(szin);
            for (int szIndex = 0; szIndex < szegmensek.Length; szIndex++)
            {
                // ha az adott értékhez tartozóan az adott szegmens aktív, akkor azt kirajzoljuk
                if(ertekTablaBool[ertek, szIndex])
                {
                    rTabla.FillRectangle(ecset, szegmensek[szIndex]);
                }
                //if ((ertekTablaBajt[ertek] & (byte)(1 << szIndex)) > 0)
                //{
                //    rTabla.FillRectangle(ecset, szegmensek[szIndex]);
                //}
            }
            ecset.Dispose();
        }

        // számláló átszínezése véletlen színűre
        public void Szinez()
        {
            szin = Color.FromArgb(veletlen.Next(0, 256),
                                  veletlen.Next(0, 256),
                                  veletlen.Next(0, 256));
        }

    }
}
