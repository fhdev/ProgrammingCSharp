﻿namespace kijelzo7
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelKijelzo = new System.Windows.Forms.Panel();
            this.timerLeptet = new System.Windows.Forms.Timer(this.components);
            this.buttonSzamlalo = new System.Windows.Forms.Button();
            this.buttonSzin = new System.Windows.Forms.Button();
            this.buttonMegallit = new System.Windows.Forms.Button();
            this.buttonVissza = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panelKijelzo
            // 
            this.panelKijelzo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelKijelzo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelKijelzo.Location = new System.Drawing.Point(17, 95);
            this.panelKijelzo.Name = "panelKijelzo";
            this.panelKijelzo.Size = new System.Drawing.Size(350, 550);
            this.panelKijelzo.TabIndex = 0;
            this.panelKijelzo.Paint += new System.Windows.Forms.PaintEventHandler(this.panelKijelzo_Paint);
            // 
            // timerLeptet
            // 
            this.timerLeptet.Interval = 1000;
            this.timerLeptet.Tick += new System.EventHandler(this.timerLeptet_Tick);
            // 
            // buttonSzamlalo
            // 
            this.buttonSzamlalo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSzamlalo.Location = new System.Drawing.Point(17, 22);
            this.buttonSzamlalo.Name = "buttonSzamlalo";
            this.buttonSzamlalo.Size = new System.Drawing.Size(75, 50);
            this.buttonSzamlalo.TabIndex = 1;
            this.buttonSzamlalo.Text = "Indít";
            this.buttonSzamlalo.UseVisualStyleBackColor = true;
            this.buttonSzamlalo.Click += new System.EventHandler(this.buttonSzamlalo_Click);
            // 
            // buttonSzin
            // 
            this.buttonSzin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSzin.Location = new System.Drawing.Point(292, 22);
            this.buttonSzin.Name = "buttonSzin";
            this.buttonSzin.Size = new System.Drawing.Size(75, 50);
            this.buttonSzin.TabIndex = 4;
            this.buttonSzin.Text = "Színez";
            this.buttonSzin.UseVisualStyleBackColor = true;
            this.buttonSzin.Click += new System.EventHandler(this.buttonSzin_Click);
            // 
            // buttonMegallit
            // 
            this.buttonMegallit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonMegallit.Location = new System.Drawing.Point(108, 22);
            this.buttonMegallit.Name = "buttonMegallit";
            this.buttonMegallit.Size = new System.Drawing.Size(75, 50);
            this.buttonMegallit.TabIndex = 2;
            this.buttonMegallit.Text = "Megállít";
            this.buttonMegallit.UseVisualStyleBackColor = true;
            this.buttonMegallit.Click += new System.EventHandler(this.buttonMegallit_Click);
            // 
            // buttonVissza
            // 
            this.buttonVissza.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonVissza.Location = new System.Drawing.Point(200, 22);
            this.buttonVissza.Name = "buttonVissza";
            this.buttonVissza.Size = new System.Drawing.Size(75, 50);
            this.buttonVissza.TabIndex = 3;
            this.buttonVissza.Text = "Vissza";
            this.buttonVissza.UseVisualStyleBackColor = true;
            this.buttonVissza.Click += new System.EventHandler(this.buttonVissza_Click);
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(384, 661);
            this.Controls.Add(this.buttonSzin);
            this.Controls.Add(this.buttonVissza);
            this.Controls.Add(this.buttonMegallit);
            this.Controls.Add(this.buttonSzamlalo);
            this.Controls.Add(this.panelKijelzo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormFoAblak";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "7 szegmenses kijelző";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelKijelzo;
        private System.Windows.Forms.Timer timerLeptet;
        private System.Windows.Forms.Button buttonSzamlalo;
        private System.Windows.Forms.Button buttonSzin;
        private System.Windows.Forms.Button buttonMegallit;
        private System.Windows.Forms.Button buttonVissza;
    }
}

