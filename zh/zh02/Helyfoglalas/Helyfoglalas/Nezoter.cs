﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Helyfoglalas
{
    class Nezoter
    {
        // a nézőtéren lévő helyek
        private Foglaltsag[,] helyek;

        // rajzeszközök
        Pen toll;
        SolidBrush ecset;

        // az egyes foglaltsági állapotokhoz tartozó színek
        private static readonly Color[] szinek = { Color.Green, Color.Yellow, Color.Red, Color.Black };

        // a lehetséges foglaltsági állapotok
        public enum Foglaltsag { Szabad = 0, Kivalasztott = 1, Foglalt = 2, NemHasznalhato = 3};

        // Konstruktor, a nézőtér méretének beállítása és az üres helyek létrehozása.
        public Nezoter(int szelesseg, int hosszusag)
        {
            helyek = new Foglaltsag[szelesseg, hosszusag];
            toll = new Pen(Color.Black);
            ecset = new SolidBrush(Color.White);
        }

        // Ez a metódus a neki megadott oszlopban és sorban lévő helyet nem használható állapotba
        // állítja.
        public void NemHasznalhato(int oszlop, int sor)
        {
            helyek[oszlop, sor] = Foglaltsag.NemHasznalhato;
        }

        // Ez a metódus a neki átadott grafikus objektum, hely oldalméret és helyek közti rés méret
        // segítségével megrajzolja a nézőteret.
        public void Rajzol(Graphics rajzTabla, int oldal, int elvalasztas)
        {
            for (int sor = 0; sor < helyek.GetLength(1); sor++)
            {
                for (int oszlop = 0; oszlop < helyek.GetLength(0); oszlop++)
                {
                    Point balFelso = new Point(elvalasztas + oszlop * (elvalasztas + oldal),
                                               elvalasztas + sor * (elvalasztas + oldal));
                    Size meret = new Size(oldal, oldal);
                    Rectangle hely = new Rectangle(balFelso, meret);
                    ecset.Color = szinek[(int)helyek[oszlop, sor]];
                    rajzTabla.FillRectangle(ecset, hely);
                    rajzTabla.DrawRectangle(toll, hely);
                }
            }
        }

        // Ez a metódus a neki átadott pont, hely oldalméret és helyek közti rés méret segítségével
        // kiválasztott állapotba állítja azt a helyet, amelyiken a pont rajta van.
        // A visszatérési érték igaz, ha a kiválasztás sikeres, hamis, ha a kiválasztás nem
        // lehetséges. 
        public bool Kivalaszt(Point pont, int oldal, int elvalasztas)
        {
            // kiszámítjuk, hogy a megadott pont a helyek melyik oszlopának és sorának felel meg
            int oszlop = (pont.X - elvalasztas) / (oldal + elvalasztas);
            int sor = (pont.Y - elvalasztas) / (oldal + elvalasztas);
            // ha a hely nem szabad, akkor nem választható ki
            if ((helyek[oszlop, sor] != Foglaltsag.Szabad))
            {
                return false;
            }
            // hely kiválasztása
            helyek[oszlop, sor] = Foglaltsag.Kivalasztott;
            return true;
        }

        // Ez a metódus véglegesíti a foglalásokat, ha a neki átadott foglal változó igaz értékű,
        // és elveti a kiválasztott helyeket, ha a neki átadott foglal változó hamis értékű.
        public void Foglal(bool foglal)
        {
            for (int sor = 0; sor < helyek.GetLength(1); sor++)
            {
                for (int oszlop = 0; oszlop < helyek.GetLength(0); oszlop++)
                {
                    if(helyek[oszlop, sor] == Foglaltsag.Kivalasztott)
                    {
                        // foglalás vagy szabaddá tétel
                        if (foglal)
                        {
                            helyek[oszlop, sor] = Foglaltsag.Foglalt;              
                        }
                        else
                        {
                            helyek[oszlop, sor] = Foglaltsag.Szabad;
                        }
                    }
                }
            }
        }

    }
}
