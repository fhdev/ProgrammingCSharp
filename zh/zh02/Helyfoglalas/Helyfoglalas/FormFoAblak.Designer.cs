﻿namespace Helyfoglalas
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelNezoter = new System.Windows.Forms.Panel();
            this.buttonFoglal = new System.Windows.Forms.Button();
            this.buttonValaszt = new System.Windows.Forms.Button();
            this.textBoxDarab = new System.Windows.Forms.TextBox();
            this.buttonMegse = new System.Windows.Forms.Button();
            this.labelHelyek = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panelNezoter
            // 
            this.panelNezoter.BackColor = System.Drawing.Color.Gray;
            this.panelNezoter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelNezoter.Enabled = false;
            this.panelNezoter.Location = new System.Drawing.Point(12, 75);
            this.panelNezoter.Name = "panelNezoter";
            this.panelNezoter.Size = new System.Drawing.Size(508, 258);
            this.panelNezoter.TabIndex = 0;
            this.panelNezoter.Paint += new System.Windows.Forms.PaintEventHandler(this.panelNezoter_Paint);
            this.panelNezoter.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelNezoter_MouseClick);
            // 
            // buttonFoglal
            // 
            this.buttonFoglal.Enabled = false;
            this.buttonFoglal.Location = new System.Drawing.Point(97, 39);
            this.buttonFoglal.Margin = new System.Windows.Forms.Padding(5);
            this.buttonFoglal.Name = "buttonFoglal";
            this.buttonFoglal.Size = new System.Drawing.Size(75, 23);
            this.buttonFoglal.TabIndex = 2;
            this.buttonFoglal.Text = "Foglal";
            this.buttonFoglal.UseVisualStyleBackColor = true;
            this.buttonFoglal.Click += new System.EventHandler(this.buttonFoglal_Click);
            // 
            // buttonValaszt
            // 
            this.buttonValaszt.Location = new System.Drawing.Point(12, 39);
            this.buttonValaszt.Margin = new System.Windows.Forms.Padding(5);
            this.buttonValaszt.Name = "buttonValaszt";
            this.buttonValaszt.Size = new System.Drawing.Size(75, 23);
            this.buttonValaszt.TabIndex = 1;
            this.buttonValaszt.Text = "Választ";
            this.buttonValaszt.UseVisualStyleBackColor = true;
            this.buttonValaszt.Click += new System.EventHandler(this.buttonValaszt_Click);
            // 
            // textBoxDarab
            // 
            this.textBoxDarab.Location = new System.Drawing.Point(98, 13);
            this.textBoxDarab.Name = "textBoxDarab";
            this.textBoxDarab.Size = new System.Drawing.Size(74, 20);
            this.textBoxDarab.TabIndex = 0;
            this.textBoxDarab.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonMegse
            // 
            this.buttonMegse.Location = new System.Drawing.Point(182, 39);
            this.buttonMegse.Margin = new System.Windows.Forms.Padding(5);
            this.buttonMegse.Name = "buttonMegse";
            this.buttonMegse.Size = new System.Drawing.Size(75, 23);
            this.buttonMegse.TabIndex = 3;
            this.buttonMegse.Text = "Mégse";
            this.buttonMegse.UseVisualStyleBackColor = true;
            this.buttonMegse.Click += new System.EventHandler(this.buttonMegse_Click);
            // 
            // labelHelyek
            // 
            this.labelHelyek.AutoSize = true;
            this.labelHelyek.Location = new System.Drawing.Point(14, 16);
            this.labelHelyek.Name = "labelHelyek";
            this.labelHelyek.Size = new System.Drawing.Size(76, 13);
            this.labelHelyek.TabIndex = 4;
            this.labelHelyek.Text = "Helyek száma:";
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 341);
            this.Controls.Add(this.labelHelyek);
            this.Controls.Add(this.buttonMegse);
            this.Controls.Add(this.textBoxDarab);
            this.Controls.Add(this.buttonValaszt);
            this.Controls.Add(this.buttonFoglal);
            this.Controls.Add(this.panelNezoter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormFoAblak";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Helyfoglalás";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelNezoter;
        private System.Windows.Forms.Button buttonFoglal;
        private System.Windows.Forms.Button buttonValaszt;
        private System.Windows.Forms.TextBox textBoxDarab;
        private System.Windows.Forms.Button buttonMegse;
        private System.Windows.Forms.Label labelHelyek;
    }
}

