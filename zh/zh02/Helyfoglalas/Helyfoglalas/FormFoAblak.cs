﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Helyfoglalas
{
    public partial class FormFoAblak : Form
    {
        // nézőtér mérete
        private const int sorok = 10;
        private const int oszlopok = 20;

       // a rajzoláshoz használatos méretek
        private const int elvalaszto = 5;
        private const int oldal = 20;
        // nézőtér objektum 
        private Nezoter nezoter;
        // választott és választandó helyek száma
        private int valasztott;
        private int valasztando;
        public FormFoAblak()
        {
            InitializeComponent();
            // a kirajzoláshoz használt panel kettős pufferelése
            typeof(Panel).InvokeMember("DoubleBuffered",
                           BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                           null,
                           panelNezoter,
                           new object[] { true });
            // nézőtér létrehozása
            nezoter = new Nezoter(oszlopok, sorok);
            // nem használható helyek beállítása
            nezoter.NemHasznalhato(0, 0);
            nezoter.NemHasznalhato(1, 0);
        }

        // nézőtér kirajzolása
        private void panelNezoter_Paint(object sender, PaintEventArgs e)
        {
            nezoter.Rajzol(e.Graphics, oldal, elvalaszto);
        }

        // nézőtérre való klikkelés
        private void panelNezoter_MouseClick(object sender, MouseEventArgs e)
        {
            // figyelmeztetés ha a választani kívánt hely nem szabad
            if(!nezoter.Kivalaszt(e.Location, oldal, elvalaszto))
            {
                MessageBox.Show("Ez a hely nem választható!",
                                "Hiba", 
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return;
            }
            // kiválasztás után a nézőtér újrarajzolása
            panelNezoter.Invalidate();
            // kiválasztott helyek számának növelése
            valasztott++; 
            // ha minden helyet kiválasztottunk, a vezérlők megfelelő engedélyezése
            if (valasztott == valasztando)
            {
                buttonValaszt.Enabled = false;
                buttonFoglal.Enabled = true;
                panelNezoter.Enabled = false;
            }
        }

        // a Választ gombra történő klikkelés
        private void buttonValaszt_Click(object sender, EventArgs e)
        {
            // figyelmeztetés, ha a választandó helyek megadása száma nem megfelelő
            if(!int.TryParse(textBoxDarab.Text, out valasztando))
            {
                MessageBox.Show("Nem adta meg a helyek számát!",
                                "Hiba",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return;
            }
            // kiválasztott helyek nullázása
            valasztott = 0;
            // vezérlők beállítása
            buttonValaszt.Enabled = false;
            buttonFoglal.Enabled = false;
            panelNezoter.Enabled = true;
        }

        private void buttonFoglal_Click(object sender, EventArgs e)
        {
            // foglalás véglegesítése
            nezoter.Foglal(true);
            // vezérlők beállítása
            buttonValaszt.Enabled = true;
            buttonFoglal.Enabled = false;
            panelNezoter.Enabled = false;
            // nézőtér rajzának frissítése
            panelNezoter.Invalidate();
        }

        private void buttonMegse_Click(object sender, EventArgs e)
        {
            // foglalás visszavonása
            nezoter.Foglal(false);
            // vezérlők beállítása
            buttonValaszt.Enabled = true;
            buttonFoglal.Enabled = false;
            panelNezoter.Enabled = false;
            // nézőtér rajzának frissítése
            panelNezoter.Invalidate();
        }
    }
}
