﻿namespace KapjEl
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerMozgat = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // timerMozgat
            // 
            this.timerMozgat.Interval = 16;
            this.timerMozgat.Tick += new System.EventHandler(this.timerMozgat_Tick);
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.KeyPreview = true;
            this.Name = "FormFoAblak";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KapjEl";
            this.Click += new System.EventHandler(this.FormFoAblak_Click);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FormFoAblak_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormFoAblak_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerMozgat;
    }
}

