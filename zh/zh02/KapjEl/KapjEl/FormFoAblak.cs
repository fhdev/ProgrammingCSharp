﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KapjEl
{
    public partial class FormFoAblak : Form
    {
        // Lenyomott billentyű
        private Keys billentyu;
        // Játékos alakzata és célpont alakzat.
        private Alakzat jatekos;
        private Alakzat celpont;
        // Elért pontszám (elkapott célpontok száma).
        private int pontszam;

        public FormFoAblak()
        {
            InitializeComponent();
            // Vibrálás elkerülése.
            DoubleBuffered = true;
        }

        // Nyíl billentyű lenyomás esetén a változó beállítása.
        private void FormFoAblak_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Up) || (e.KeyCode == Keys.Down) || 
                (e.KeyCode == Keys.Left) || (e.KeyCode == Keys.Right))
            {
                billentyu = e.KeyCode;
            }
        }

        // Alakzatok mozgatása és játék vezérlése.
        private void timerMozgat_Tick(object sender, EventArgs e)
        {
            // Ha a játékos nekimegy a falnak, a játéknak vége.
            if (jatekos.Mozgat(billentyu, timerMozgat.Interval))
            {
                timerMozgat.Enabled = false;
                MessageBox.Show("Játék vége! Pontszáma: " + pontszam + ".");
            }
            // Ha a játékos elkapta a célpontot, növeljük a pontszámát és új célpontot kap.
            if(jatekos.Utkozik(celpont))
            {
                pontszam++;
                celpont = new Alakzat(this.ClientSize, Alakzat.Tipusok.Celpont);
            }
            // Újrarajzolás.
            this.Invalidate();
        }

        // Játék indítása.
        private void FormFoAblak_Click(object sender, EventArgs e)
        {
            jatekos = new Alakzat(this.ClientSize, Alakzat.Tipusok.Jatekos);
            celpont = new Alakzat(this.ClientSize, Alakzat.Tipusok.Celpont);
            billentyu = Keys.Up;
            pontszam = 0;
            timerMozgat.Enabled = true;
        }

        // Alakzatok megrajzolása.
        private void FormFoAblak_Paint(object sender, PaintEventArgs e)
        {
            if (jatekos != null)
            {
                jatekos.Rajzol(e.Graphics);
            }
            if (celpont != null)
            {
                celpont.Rajzol(e.Graphics);
            }
        }
    }
}
