﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace KapjEl
{
    class Alakzat
    {
        // Véletlenszám generátor.
        private static Random rnd = new Random();
        // Kiterjedés és sebesség aránya a képátlóhoz képest.
        private const double meretArany = 0.05;
        private const double sebessegArany = 0.5;

        // A Form vagy Panel mérete, amire rajzolni fogunk.
        private Size abraMeret;
        // Alakzat jellemzői.
        private Tipusok tipus;
        private double x;
        private double y;
        private double kiterjedes;
        private double sebesseg;

        // Konstruktor
        public Alakzat(Size abraMeret, Tipusok tipus)
        {
            this.abraMeret = abraMeret;
            this.tipus = tipus;
            // Elhelyezés a képátló arányában úgy, hogy az alakzat ne tudjon lelógni a rendelkezésre
            // álló területről.
            double atlo = Math.Sqrt(Math.Pow(abraMeret.Width, 2.0) + Math.Pow(abraMeret.Height, 2.0));
            kiterjedes = Convert.ToInt32(meretArany * atlo);
            x = rnd.Next((int)kiterjedes, (int)(abraMeret.Width - kiterjedes + 1));
            y = rnd.Next((int)kiterjedes, (int)(abraMeret.Height - kiterjedes + 1));
            // Sebesség választása az átló arányában.
            sebesseg = sebessegArany * atlo;
        }

        // Lehetséges típusok.
        public enum Tipusok { Jatekos, Celpont };

        // Alakzat oldalait megadó csak olvasható tulajdonságo.
        public double Bal
        {
            get
            {
                return x - kiterjedes / 2;
            }
        }

        public double Jobb
        {
            get
            {
                return x + kiterjedes / 2;
            }
        }

        public double Teto
        {
            get
            {
                return y + kiterjedes / 2;
            }
        }

        public double Alj
        {
            get
            {
                return y - kiterjedes / 2;
            }
        }

        // Mozgatás a megadott billentyűnek megfelelően a timer intervallum figyelembe vételével. 
        public bool Mozgat(Keys billentyu, int intervallum)
        {
            double pixelSebesseg = sebesseg * intervallum / 1000;
            double ujX = x;
            double ujY = y;
            switch (billentyu)
            {
                case Keys.Up:
                    {
                        ujY = y + pixelSebesseg;
                        break;
                    }
                case Keys.Down:
                    {
                        ujY = y - pixelSebesseg;
                        break;
                    }
                case Keys.Left:
                    {
                        ujX = x - pixelSebesseg;
                        break;
                    }
                case Keys.Right:
                    {
                        ujX = x + pixelSebesseg;
                        break;
                    }
            }
            bool kint = ((Jobb > abraMeret.Width) || (Bal < 0) || 
                (Teto > abraMeret.Height) || (Alj < 0));
            if(!kint)
            {
                x = ujX;
                y = ujY;
            }
            return kint;
        }

        // Ütközés vizsgálata másik alakzattal.
        public bool Utkozik(Alakzat masik)
        {
            return (Jobb > masik.Bal) && (Bal < masik.Jobb) && 
                (Teto > masik.Alj) && (Alj < masik.Teto);
        }

        // Kirajzolás.
        public void Rajzol(Graphics rajzEszkoz)
        {
            // A befoglaló négyszög mindkét típus esetén azonos.
            Rectangle befoglalo = new Rectangle((int)(Bal),
                (int)(abraMeret.Height - Teto),
                (int)(kiterjedes),
                (int)(kiterjedes));
            SolidBrush ecset = new SolidBrush(Color.Black);
            Pen toll = new Pen(Color.Black);
            if (tipus == Tipusok.Celpont)
            {
                ecset.Color = Color.Red;
                rajzEszkoz.FillEllipse(ecset, befoglalo);
                rajzEszkoz.DrawEllipse(toll, befoglalo);
            }
            else if(tipus == Tipusok.Jatekos)
            {
                ecset.Color = Color.Blue;
                rajzEszkoz.FillRectangle(ecset, befoglalo);
                rajzEszkoz.DrawRectangle(toll, befoglalo);
            }
            toll.Dispose();
            ecset.Dispose();
        }

    }
}
