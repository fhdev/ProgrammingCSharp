﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KomplexSzamologep
{
    /* Class for handling Complex variables */
    public class Komplex
    {
        private double valos;

        private double kepzetes;

        public double Valos
        {
            get { return valos; }
            set { valos = value; }
        }

        public double Kepzetes
        {
            get { return kepzetes; }
            set { kepzetes = value; }
        }

        public double Abszolut
        {
            get 
            { 
                return Math.Sqrt(valos * valos + kepzetes * kepzetes); 
            }
        }

        public double Szog
        {
            get
            {
                return Math.Atan(kepzetes / valos);
            }
        }

        public Komplex Konjugalt
        {
            get
            {
                return new Komplex(valos, -kepzetes);
            }
        }

        public Komplex()
        {
            valos = 0.0;
            kepzetes = 0.0;
        }

        public Komplex(double valos, double kepzetes)
        {
            this.valos = valos;
            this.kepzetes = kepzetes;
        }

        public Komplex(string valos, string kepzetes)
        {
            double.TryParse(valos, out this.valos);
            double.TryParse(kepzetes, out this.kepzetes);
        }

        public static Komplex operator +(Komplex a, Komplex b)
        {
            return new Komplex(a.valos + b.valos, a.kepzetes + b.kepzetes);
        }

        public static Komplex operator +(Komplex a, double b)
        {
            return new Komplex(a.valos + b, a.kepzetes);
        }

        public static Komplex operator -(Komplex a, Komplex b)
        {
            return new Komplex(a.valos - b.valos, a.kepzetes - b.kepzetes);
        }

        public static Komplex operator -(Komplex a, double b)
        {
            return new Komplex(a.valos - b, a.kepzetes);
        }

        public static Komplex operator *(Komplex a, Komplex b)
        {
            return new Komplex(a.valos * b.valos - a.kepzetes * b.kepzetes, a.valos * b.kepzetes + b.valos * a.kepzetes);
        }

        public static Komplex operator *(Komplex a, double b)
        {
            return new Komplex(a.valos * b, a.kepzetes * b);
        }

        public static Komplex operator /(Komplex a, Komplex b)
        {
            return (a * b.Konjugalt) / (b * b.Konjugalt).Valos;
        }

        public static Komplex operator /(Komplex a, double b)
        {
            return new Komplex(a.valos / b, a.kepzetes / b);
        }
        
    }
}
