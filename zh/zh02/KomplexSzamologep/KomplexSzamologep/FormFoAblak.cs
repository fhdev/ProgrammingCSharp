﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KomplexSzamologep
{
    public partial class FormFoAblak : Form
    {
        private Komplex szam1;
        private Komplex szam2;
        private Komplex eredmeny;

        public FormFoAblak()
        {
            InitializeComponent();
            szam1 = new Komplex();
            szam2 = new Komplex();
        }

        private void buttonOsszead_Click(object sender, EventArgs e)
        {
            szam1 = new Komplex(textBoxSzam1Valos.Text, textBoxSzam1Kepzetes.Text);
            szam2 = new Komplex(textBoxSzam2Valos.Text, textBoxSzam2Kepzetes.Text);
            switch((sender as Button).Name)
            {
                case "buttonOsszead":
                {
                    eredmeny = szam1 + szam2;
                    break;
                }
                case "buttonKivon":
                {
                    eredmeny = szam1 - szam2;
                    break;
                }
                case "buttonSzoroz":
                {
                    eredmeny = szam1 * szam2;
                    break;
                }
                case "buttonOszt":
                {
                    eredmeny = szam1 / szam2;
                    break;
                }
            }
            textBoxEredemenyValos.Text = eredmeny.Valos.ToString();
            textBoxEredmenyKepzetes.Text = eredmeny.Kepzetes.ToString();
        }
        
    }
}
