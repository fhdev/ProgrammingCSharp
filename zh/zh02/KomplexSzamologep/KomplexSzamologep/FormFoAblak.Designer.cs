﻿namespace KomplexSzamologep
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSzam1Valos = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxSzam1Kepzetes = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxSzam2Valos = new System.Windows.Forms.TextBox();
            this.textBoxSzam2Kepzetes = new System.Windows.Forms.TextBox();
            this.textBoxEredemenyValos = new System.Windows.Forms.TextBox();
            this.buttonOsszead = new System.Windows.Forms.Button();
            this.buttonKivon = new System.Windows.Forms.Button();
            this.buttonSzoroz = new System.Windows.Forms.Button();
            this.buttonOszt = new System.Windows.Forms.Button();
            this.textBoxEredmenyKepzetes = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 41);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Szám 1 (a)";
            // 
            // textBoxSzam1Valos
            // 
            this.textBoxSzam1Valos.Location = new System.Drawing.Point(82, 38);
            this.textBoxSzam1Valos.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSzam1Valos.Name = "textBoxSzam1Valos";
            this.textBoxSzam1Valos.Size = new System.Drawing.Size(76, 20);
            this.textBoxSzam1Valos.TabIndex = 1;
            this.textBoxSzam1Valos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 84);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Szám 2 (b)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 127);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Eredmeny";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(104, 20);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Valós";
            // 
            // textBoxSzam1Kepzetes
            // 
            this.textBoxSzam1Kepzetes.Location = new System.Drawing.Point(168, 38);
            this.textBoxSzam1Kepzetes.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSzam1Kepzetes.Name = "textBoxSzam1Kepzetes";
            this.textBoxSzam1Kepzetes.Size = new System.Drawing.Size(76, 20);
            this.textBoxSzam1Kepzetes.TabIndex = 5;
            this.textBoxSzam1Kepzetes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(181, 21);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Képzetes";
            // 
            // textBoxSzam2Valos
            // 
            this.textBoxSzam2Valos.Location = new System.Drawing.Point(82, 81);
            this.textBoxSzam2Valos.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSzam2Valos.Name = "textBoxSzam2Valos";
            this.textBoxSzam2Valos.Size = new System.Drawing.Size(76, 20);
            this.textBoxSzam2Valos.TabIndex = 7;
            this.textBoxSzam2Valos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxSzam2Kepzetes
            // 
            this.textBoxSzam2Kepzetes.Location = new System.Drawing.Point(168, 81);
            this.textBoxSzam2Kepzetes.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSzam2Kepzetes.Name = "textBoxSzam2Kepzetes";
            this.textBoxSzam2Kepzetes.Size = new System.Drawing.Size(76, 20);
            this.textBoxSzam2Kepzetes.TabIndex = 8;
            this.textBoxSzam2Kepzetes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxEredemenyValos
            // 
            this.textBoxEredemenyValos.Location = new System.Drawing.Point(82, 124);
            this.textBoxEredemenyValos.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxEredemenyValos.Name = "textBoxEredemenyValos";
            this.textBoxEredemenyValos.Size = new System.Drawing.Size(76, 20);
            this.textBoxEredemenyValos.TabIndex = 9;
            this.textBoxEredemenyValos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonOsszead
            // 
            this.buttonOsszead.Location = new System.Drawing.Point(261, 35);
            this.buttonOsszead.Margin = new System.Windows.Forms.Padding(2);
            this.buttonOsszead.Name = "buttonOsszead";
            this.buttonOsszead.Size = new System.Drawing.Size(50, 25);
            this.buttonOsszead.TabIndex = 11;
            this.buttonOsszead.Text = "+";
            this.buttonOsszead.UseVisualStyleBackColor = true;
            this.buttonOsszead.Click += new System.EventHandler(this.buttonOsszead_Click);
            // 
            // buttonKivon
            // 
            this.buttonKivon.Location = new System.Drawing.Point(261, 63);
            this.buttonKivon.Margin = new System.Windows.Forms.Padding(2);
            this.buttonKivon.Name = "buttonKivon";
            this.buttonKivon.Size = new System.Drawing.Size(50, 25);
            this.buttonKivon.TabIndex = 12;
            this.buttonKivon.Text = "-";
            this.buttonKivon.UseVisualStyleBackColor = true;
            this.buttonKivon.Click += new System.EventHandler(this.buttonOsszead_Click);
            // 
            // buttonSzoroz
            // 
            this.buttonSzoroz.Location = new System.Drawing.Point(261, 92);
            this.buttonSzoroz.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSzoroz.Name = "buttonSzoroz";
            this.buttonSzoroz.Size = new System.Drawing.Size(50, 25);
            this.buttonSzoroz.TabIndex = 13;
            this.buttonSzoroz.Text = "*";
            this.buttonSzoroz.UseVisualStyleBackColor = true;
            this.buttonSzoroz.Click += new System.EventHandler(this.buttonOsszead_Click);
            // 
            // buttonOszt
            // 
            this.buttonOszt.Location = new System.Drawing.Point(261, 121);
            this.buttonOszt.Margin = new System.Windows.Forms.Padding(2);
            this.buttonOszt.Name = "buttonOszt";
            this.buttonOszt.Size = new System.Drawing.Size(50, 25);
            this.buttonOszt.TabIndex = 14;
            this.buttonOszt.Text = "/";
            this.buttonOszt.UseVisualStyleBackColor = true;
            this.buttonOszt.Click += new System.EventHandler(this.buttonOsszead_Click);
            // 
            // textBoxEredmenyKepzetes
            // 
            this.textBoxEredmenyKepzetes.Location = new System.Drawing.Point(168, 124);
            this.textBoxEredmenyKepzetes.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxEredmenyKepzetes.Name = "textBoxEredmenyKepzetes";
            this.textBoxEredmenyKepzetes.Size = new System.Drawing.Size(76, 20);
            this.textBoxEredmenyKepzetes.TabIndex = 15;
            this.textBoxEredmenyKepzetes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(331, 160);
            this.Controls.Add(this.textBoxEredmenyKepzetes);
            this.Controls.Add(this.buttonOszt);
            this.Controls.Add(this.buttonSzoroz);
            this.Controls.Add(this.buttonKivon);
            this.Controls.Add(this.buttonOsszead);
            this.Controls.Add(this.textBoxEredemenyValos);
            this.Controls.Add(this.textBoxSzam2Kepzetes);
            this.Controls.Add(this.textBoxSzam2Valos);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxSzam1Kepzetes);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxSzam1Valos);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormFoAblak";
            this.Text = "Komplex számológép";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxSzam1Valos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxSzam1Kepzetes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxSzam2Valos;
        private System.Windows.Forms.TextBox textBoxSzam2Kepzetes;
        private System.Windows.Forms.TextBox textBoxEredemenyValos;
        private System.Windows.Forms.Button buttonOsszead;
        private System.Windows.Forms.Button buttonKivon;
        private System.Windows.Forms.Button buttonSzoroz;
        private System.Windows.Forms.Button buttonOszt;
        private System.Windows.Forms.TextBox textBoxEredmenyKepzetes;
    }
}

