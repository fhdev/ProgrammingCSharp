﻿namespace EletJatek
{
    partial class FormFoAblak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerLeptet = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // timerLeptet
            // 
            this.timerLeptet.Tick += new System.EventHandler(this.timerLeptet_Tick);
            // 
            // FormFoAblak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(500, 500);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFoAblak";
            this.Text = "ÉletJáték";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FormFoAblak_Paint);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormFoAblak_KeyUp);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FormFoAblak_MouseClick);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerLeptet;
    }
}

