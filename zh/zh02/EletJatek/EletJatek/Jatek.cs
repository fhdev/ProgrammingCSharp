﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace EletJatek
{
    class Jatek
    {
        private const int meret = 50;

        private static Random veletlen = new Random();

        private bool[,] cellak;
        private int oldalHossz;
        private Color szin;

        // Új játék létrehozása az ablakméretnek megfelelően.
        public Jatek(Size ablakMeret)
        {
            cellak = new bool[meret, meret];
            oldalHossz = Math.Min(ablakMeret.Height, ablakMeret.Width) / meret;
            this.szin = Color.FromArgb(veletlen.Next(0, 40), veletlen.Next(0, 40),
                veletlen.Next(50, 256));

        }
        // Új sejt hozzáadása az egérrel való kattintás helyén. 
        public void HozzaAd(MouseEventArgs egerEsemeny)
        {
            cellak[egerEsemeny.X / oldalHossz, egerEsemeny.Y / oldalHossz] ^= true;
        }
        // Összes élő sejt eltávolítása.
        public void Urit()
        {
            cellak = new bool[meret, meret];
        }
        // Környező sejtek számának meghatározása.
        private int KornyezokSzama(int oszlop, int sor)
        {
            int kornyezokSzama = 0;
            // A szélső celláknak nem minden irányban vannak szomszédaik. Csak a valós szomszédokat
            // lehet vizsgálni. 
            int oszlopMin = Math.Max(0, oszlop - 1);
            int oszlopMax = Math.Min(oszlop + 1, meret - 1);
            int sorMin = Math.Max(0, sor - 1);
            int sorMax = Math.Min(sor + 1, meret - 1);
            // Szomszédok vizsgálata.
            for (int kOszlop = oszlopMin; kOszlop <= oszlopMax; kOszlop++)
            {
                for (int kSor = sorMin; kSor <= sorMax; kSor++)
                {
                    // Az aktuális sejtet nem számoljuk bele a szomszédok közé.
                    if ((kOszlop == oszlop) && (kSor == sor))
                    {
                        continue;
                    }
                    else if (cellak[kOszlop, kSor])
                    {
                        kornyezokSzama++;
                    }
                }
            }
            return kornyezokSzama;
        }
        // A játék egy köre. A 2 és 3 szomszéddal rendelkező sejtek élik túl a kört. 
        // A 3 szomszéddal rendelkező üres cellákban új sejtek jönnek létre.
        public bool Lep()
        {
            // Ebben a változóban tároljuk a sejtek számát.
            int sejtekSzama = 0;
            bool[,] cellakUj = new bool[meret, meret];
            for (int oszlop = 0; oszlop < meret; oszlop++)
            {
                for (int sor = 0; sor < meret; sor++)
                {
                    int kornyezokSzama = KornyezokSzama(oszlop, sor);
                    if((kornyezokSzama == 3) || ((kornyezokSzama == 2) && cellak[oszlop, sor]))
                    {
                        cellakUj[oszlop, sor] = true;
                        sejtekSzama++;
                    }
                }
            }
            cellak = cellakUj;
            return (sejtekSzama == 0);
        }

        public void Rajzol(Graphics rajzEszkoz)
        {
            rajzEszkoz.Clear(Color.White);
            Pen toll = new Pen(Color.Black);
            SolidBrush ecset = new SolidBrush(szin);
            for (int oszlop = 0; oszlop < meret; oszlop++)
            {
                for (int sor = 0; sor < meret; sor++)
                {
                    Rectangle cella = new Rectangle(oszlop * oldalHossz, sor * oldalHossz,
                        oldalHossz, oldalHossz);
                    // Élő sejtek kirajzolása.
                    if (cellak[oszlop, sor])
                    {
                        rajzEszkoz.FillRectangle(ecset, cella);                      
                    }
                    rajzEszkoz.DrawRectangle(toll, cella);
                }
            }
            ecset.Dispose();
            toll.Dispose();
        }
    }
}
