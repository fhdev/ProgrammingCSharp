﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EletJatek
{
    public partial class FormFoAblak : Form
    {
        // Játék objektum.
        private Jatek eletJatek;
        public FormFoAblak()
        {
            InitializeComponent();
            // Játék létrehozása
            eletJatek = new Jatek(ClientSize);
        }
        // Új sejtek hozzáadása, ha a játék még nem fut.
        private void FormFoAblak_MouseClick(object sender, MouseEventArgs e)
        {
            if (!timerLeptet.Enabled)
            {
                eletJatek.HozzaAd(e);
                Invalidate();
            }
        }
        // Játék megrajzolása.
        private void FormFoAblak_Paint(object sender, PaintEventArgs e)
        {
            eletJatek.Rajzol(e.Graphics);
        }
        // Játék léptetése, játék vége esetén információ.
        private void timerLeptet_Tick(object sender, EventArgs e)
        {
            if(eletJatek.Lep())
            {
                timerLeptet.Stop();
                MessageBox.Show("Minden sejt meghalt. :(", "Játék vége", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            Invalidate();
        }
        // Enter gombra az álló játék futni kezd, a futó játék pedig leáll, és a 
        // cellákat nullázzuk.
        private void FormFoAblak_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if (timerLeptet.Enabled)
                {
                    eletJatek.Urit();
                    Invalidate();
                }
                timerLeptet.Enabled = !timerLeptet.Enabled;
            }
        }
    }
}
