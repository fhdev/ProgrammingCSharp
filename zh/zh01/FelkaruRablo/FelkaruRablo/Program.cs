﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FelkaruRablo
{
    class Program
    {
        static void Main(string[] args)
        {
            // kezdő pontszám beállítása
            int kezdo = 100;
            int pontszam = kezdo;
            // számláló a sorsolásoknak
            int jatekokSzama = 0;
            // logikai változó annak jelzésére, hogy a felhasználó még játékban van
            bool jatekban = true;
            // logikai változó annak jelzésére, hogy a felhasználó sorsolni szeretne
            bool sorsolna = true;
            Console.WriteLine("Félkarú rabló játék");
            do
            {
                Console.WriteLine("──────────────────────────");
                // megkérdezzük a felhasználót hogy akar-e sorsolni
                sorsolna = AkarSorsolni();
                // ha a játékos akar sorsolni
                if (sorsolna)
                {
                    // sorsolás
                    int[] jatek = Sorsol();
                    // sorsolt számok kiírása
                    Console.WriteLine(Szovegkent(jatek));
                    // sorsolás eredményének számítása
                    pontszam += Eredmeny(jatek);
                    // eredmény kiírása
                    Console.WriteLine("Az aktuális pontszám: " + pontszam);
                    jatekban = (pontszam >= 10);
                    // játékok számának kövelése
                    jatekokSzama++;
                }
            } while (sorsolna && jatekban);
            // a játék végeredménye
            if (jatekokSzama > 3)
            {
                if ((pontszam > kezdo))
                {
                    Console.WriteLine("Gratulálunk, Ön nyert. Nyereménye " + pontszam + ".");
                }
                else
                {
                    Console.WriteLine("Sajnos Ön a kezdeti pontszámának egy részét elveszítette.");
                }
            }
            else
            {
                Console.WriteLine("Sajnos Ön a teljes kezdeti pontszámát elveszítette. Legközelebb legyen több szerencséje.");
            }
            ProgramVege();
        }

        static int[] Sorsol()
        {
            int[] sorsolas = new int[3];
            Random veletlen = new Random();
            for (int i = 0; i < sorsolas.Length; i++)
            {
                sorsolas[i] = veletlen.Next(1, 10);
            }
            return sorsolas;
        }

        static string Szovegkent(int[] sorsolas)
        {
            string szoveg = "    ┌─┬─┬─┐\n    │";
            for (int i = 0; i < sorsolas.Length; i++)
            {
                szoveg += sorsolas[i] + "│";
            }
            szoveg += "\n    └─┴─┴─┘";
            return szoveg;
        }

        static int Eredmeny(int[] sorsolas)
        {
            if((sorsolas[0] == sorsolas[1]) && (sorsolas[1] == sorsolas[2]))
            {
                return 3 * sorsolas[0];
            }
            if((sorsolas[0] == sorsolas[1]) || 
               (sorsolas[0] == sorsolas[2]) || 
               (sorsolas[1] == sorsolas[2]))
            {
                return 2 * sorsolas[0];
            }
            return -10;
        }

        static bool AkarSorsolni()
        {
            Console.Write("Kíván sorsolni (I/N)? ");
            string valasz = Console.ReadLine();
            if (valasz.ToUpper() == "I")
            {
                return true;
            }
            return false;
        }

        static void ProgramVege()
        {
            Console.Write("Nyomjon meg egy billentyűt a kilépéshez...");
            Console.ReadKey();
        }

    }
}
