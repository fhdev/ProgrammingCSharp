﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lotto
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Egyéni lottó játék.");
            Console.WriteLine("Kérem adja meg, hogy 1 és mekkora legnagyobb szám között szeretne sorsolni.");
            Console.WriteLine("A legnagyobb szám 40 és 100 között lehet.");
            int max = BeolvasInt("Legnagyobb szám", 40, 100);
            Console.WriteLine("Kérem adja meg, hogy hány számot szeretne sorsolni.");
            Console.WriteLine("A sorsolt számok mennyisége 3 és 10 között lehet.");
            int darab = BeolvasInt("Számok mennyisége", 3, 10);
            Console.WriteLine("A nyerési esély: 1:{0:F0} .", 1.0 / NyeresiEsely(darab, max));
            int[] szamok = LottoSzamok(darab, max);
            Console.WriteLine("A nyerő számok:");
            KiirSzamok(szamok);
        }
        // Egész szám beolvasása also és felso között. A felhasználónak a beolvasás előtt a nev 
        // nevet írja ki. 
        static int BeolvasInt(string nev, int also, int felso)
        {
            int ertek;
            bool ervenyes;
            do
            {
                Console.Write(nev + " = ");
                ervenyes = int.TryParse(Console.ReadLine(), out ertek);
                if ((ertek < also) || (ertek > felso))
                {
                    ervenyes = false;
                }
            } while (!ervenyes);
            return ertek;
        }

        // Lottószámok generálása. A metódus darab mennyiségű számot generál 1 és max között.
        // A sorsolt számok közül egy csak egyszer szerepelhez.
        static int[] LottoSzamok(int darab, int max)
        {
            Random veletlen = new Random();
            int[] szamok = new int[darab];
            for (int i = 0; i < darab; i++)
            {
                bool uj;
                int szam;
                do
                {
                    uj = true;
                    szam = veletlen.Next(1, max + 1);
                    for (int j = 0; j < i; j++)
                    {
                        if(szamok[i] == szam)
                        {
                            uj = false;
                            break;
                        }
                    }
                } while (!uj);
                szamok[i] = szam;
            }
            return szamok;
        }
        // Nyerési esély kiszámítása, ha 1 és max közötti számokból kell darab mennyiséget
        // ismétlés nélkül kiválasztani. 
        static double NyeresiEsely(int darab, int max)
        {
            double szamlalo = 1;
            for (int i = max; i > max - darab; i--)
            {
                szamlalo *= i;
            }
            double nevezo = 1;
            for(int i = 1; i < darab + 1; i++)
            {
                nevezo *= i;
            }
            double NalattK = szamlalo / nevezo;
            return 1.0 / NalattK;
        }
        // Nyerőszámok kiírása. A szamok tömb elemeit írja ki egymás mellé, zárójelek között.
        static void KiirSzamok(int[] szamok)
        {
            foreach (int szam in szamok)
            {
                Console.Write("({0:D}) ", szam);
            }
            Console.WriteLine();
        }
    }
}
