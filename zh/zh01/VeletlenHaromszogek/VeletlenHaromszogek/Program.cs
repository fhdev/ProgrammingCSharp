﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeletlenHaromszogek
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ZH2A 2015.12.10. Hegedüs Ferenc");
            Triangle[] triangles = GetRandomTriangles(10);
            Console.WriteLine("Random generated triangles before sorting:");
            TrianglesToConsole(triangles);
            SortTriangles(triangles);
            Console.WriteLine("Random generated triangles after sorting:");
            TrianglesToConsole(triangles);
            WaitForExit();
        }

        static Triangle[] GetRandomTriangles(int n)
        {
            Triangle[] triangles = new Triangle[n];
            for (int i = 0; i < triangles.Length; i++)
                triangles[i] = new Triangle();
            return triangles;
        }

        static void TrianglesToConsole(Triangle[] triangles)
        {
            for (int i = 0; i < triangles.Length; i++)
            {
                Console.Write("Triangle {0:D}: ", i);
                triangles[i].ToConsole();
            }
        }

        static void SortTriangles(Triangle[] triangles)
        {
            for(int i = triangles.Length - 2; i >= 0; i--)
                for(int j = 0; j <= i; j++)
                    if(triangles[j].GetArea() < triangles[j + 1].GetArea())
                    {
                        Triangle temp = triangles[j];
                        triangles[j] = triangles[j + 1];
                        triangles[j + 1] = temp;
                    }
        }

        static void WaitForExit()
        {
            Console.Write("Press any keys to exit...");
            Console.ReadKey();
        }
    }

    class Triangle
    {
        public double SideA;
        public double SideB;
        public double Angle;
        private static readonly Random Rand = new Random();
        private const int maxSide = 100;

        public Triangle()
        {
            SideA = Rand.NextDouble() * maxSide;
            SideB = Rand.NextDouble() * maxSide;
            Angle = Rand.NextDouble() * Math.PI;
        }

        public Triangle(double sideA, double sideB, double angle)
        {
            SideA = sideA;
            SideB = sideB;
            Angle = angle;
        }

        public double GetAngleDegree()
        {
            return Angle / Math.PI * 180.0;
        }

        public double GetArea()
        {
            return SideA * SideB * Math.Sin(Angle) / 2.0;
        }

        public void ToConsole()
        {
            Console.WriteLine("A = {0:F2} mm, B = {1:F2} mm, α = {2:F2} °, Area = {3:F2} mm^2", SideA, SideB, GetAngleDegree(), GetArea());
        }

    }
}
