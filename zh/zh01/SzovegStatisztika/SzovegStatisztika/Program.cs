﻿using System;
using System.IO;

namespace SzovegStatisztika
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = GetText();
            int[] letterCounts = GetLetterCounts(text);
            DisplayLetterCounts(letterCounts);
            SaveLetterCounts("letters.txt", text, letterCounts);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        static string GetText()
        {
            Console.WriteLine("Please type in a text to analyze.");
            string text = Console.ReadLine();
            text = text.ToUpper();
            Console.WriteLine("Text with capital letters is:");
            Console.WriteLine(text);
            return text;
        }

        static int[] GetLetterCounts(string text)
        {
            int numLetters = 26;
            int codeOfA = 65;
            Console.WriteLine("Length of text is: {0:D}.", text.Length);
            int[] letterCounts = new int[numLetters + 1];
            for (int i = 0; i < text.Length; i++)
            {
                int indexInLetterCounts = (int)text[i] - codeOfA;
                if((indexInLetterCounts >= 0) && (indexInLetterCounts < numLetters))
                {
                    letterCounts[indexInLetterCounts]++;
                }
                else
                {
                    letterCounts[numLetters]++;
                }
            }
            return letterCounts;
        }

        static void DisplayLetterCounts(int[] letterCounts)
        {
            int numLetters = 26;
            int codeOfA = 65;
            Console.WriteLine("Writing results to console...");
            for (int i = 0; i < numLetters; i++)
            {
                string line = "The number of " + (char)(i + codeOfA) + " characters is " + letterCounts[i].ToString();
                Console.WriteLine(line);
            }
            Console.WriteLine("The number of other characters = " + letterCounts[numLetters].ToString());
        }

        static void SaveLetterCounts(string fileName, string text, int[] letterCounts)
        {
            int numLetters = 26;
            int codeOfA = 65;
            StreamWriter file = new StreamWriter(fileName);
            Console.WriteLine("Writing results to file...");
            file.WriteLine(text);
            for (int i = 0; i < numLetters; i++)
            {
                string line = (char)(i + codeOfA) + ";" + letterCounts[i].ToString();
                file.WriteLine(line);
            }
            file.WriteLine("OTHER;" + letterCounts[numLetters].ToString());
            file.Close();
            file.Dispose();
        }
    }
}
