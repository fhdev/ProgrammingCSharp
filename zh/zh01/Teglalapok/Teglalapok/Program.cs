﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teglalapok
{
    class Program
    {
        static Random veletlen = new Random();

        struct Teglalap
        {
            // Bal alsó pont koordinátái
            public double X;
            public double Y;
            // Méret
            public double Sz;
            public double M;
        }

        const int maxKoordinata = 20;
        const int maxMeret = 50;

        static void Main(string[] args)
        {
            Console.WriteLine("Véletlen négyszögek generálása.");

            Console.Write("Kérem adja meg, hány négyszöget szeretne! ");
            int darab;
            int.TryParse(Console.ReadLine(), out darab);

            Console.WriteLine("A négyszögek előállítási sorrendben:");
            Teglalap[] teglalapok = new Teglalap[darab];
            for(int iTeglalap = 0; iTeglalap < darab; iTeglalap++)
            {
                teglalapok[iTeglalap] = VeletlenTeglalap(maxKoordinata, maxMeret);
                TeglalapKiir(teglalapok[iTeglalap]);
                bool[] tesztEredmeny = Teszt(teglalapok[iTeglalap]);
                if (tesztEredmeny[0]) Console.Write("[O]    ");
                if (tesztEredmeny[1]) Console.Write("[M]    ");
                Console.WriteLine();
            }

            Console.WriteLine("A téglalapok terület szerinti csökkenő sorrendben:");
            Array.Sort(teglalapok, TeruletOszehasonlit);
            for(int iTeglalap = 0; iTeglalap < darab; iTeglalap++)
            {
                TeglalapKiir(teglalapok[iTeglalap]);
                Console.WriteLine();
            }
        }

        static Teglalap VeletlenTeglalap(double maxKoordinata, double maxMeret)
        {
            Teglalap teglalap = new Teglalap();
            teglalap.X = -maxKoordinata + 2 * veletlen.NextDouble() * maxKoordinata;
            teglalap.Y = -maxKoordinata + 2 * veletlen.NextDouble() * maxKoordinata;
            teglalap.Sz = veletlen.NextDouble() * maxMeret;
            teglalap.M = veletlen.NextDouble() * maxMeret;
            return teglalap;
        }

        static int TeruletOszehasonlit(Teglalap teglalap1, Teglalap teglalap2)
        {
            return (int)Math.Sign((teglalap2.Sz * teglalap2.M) - (teglalap1.Sz * teglalap1.M));
        }

        static bool[] Teszt(Teglalap teglalap)
        {
            bool[] teszt = new bool[2];
            // Tartalmazza-e az origót?
            teszt[0] = (teglalap.X <= 0) && ((teglalap.X + teglalap.Sz) >= 0) && 
                (teglalap.Y <= 0) && ((teglalap.Y + teglalap.M) >= 0);
            // A kerület nagyobb-e mint a terület?
            teszt[1] = teglalap.M > teglalap.Sz;
            return teszt;
        }

        static void TeglalapKiir(Teglalap teglalap)
        {
            Console.Write("X={0,8:+##0.00;-##0.00};    " + 
                          "Y={1,8:+##0.00;-##0.00};    " + 
                          "Sz={2,8:+##0.00;-##0.00};    " + 
                          "M={3,8:+##0.00;-##0.00};    ", 
                          teglalap.X, 
                          teglalap.Y, 
                          teglalap.Sz, 
                          teglalap.M);
        }
    }
}
