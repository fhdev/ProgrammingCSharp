﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EgyezoKarakterek
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Egyező karakterek keresése.");
            Console.WriteLine("Kérem adja meg az első szót!");
            string szo1 = SzoBeolvas("első szó");
            Console.WriteLine("Kérem adja meg a második szót!");
            string szo2 = SzoBeolvas("második szó");
            string egyezoHelyek;
            int egyezokSzama = EgyezoBetuk(szo1, szo2, out egyezoHelyek);
            Console.WriteLine("Az egyező karakterek száma: " + egyezokSzama);
            Console.WriteLine("Az egyezések helyei: " + egyezoHelyek);
            if(EredmenyFajlba("eredmeny.txt", szo1, szo2, egyezoHelyek, egyezokSzama))
            {
                Console.WriteLine("Az eredményfájl sikeresen frissítve.");
            }
            else
            {
                Console.WriteLine("Az eredményfájl sikeresen mentve.");
            }
            ProgramVege();
        }

        static void ProgramVege()
        {
            Console.Write("Kérem nyomjon meg egy billentyűt a kilépéshez... ");
            Console.ReadKey();
        }

        static string SzoBeolvas(string nev)
        {
            string szo;
            bool ervenytelen;
            do
            {
                ervenytelen = false;
                Console.Write(">> ");
                szo = Console.ReadLine();
                ervenytelen = string.IsNullOrEmpty(szo);
                if (ervenytelen)
                {
                    Console.WriteLine("A megadott bemenet üres. A szó csak betűket tartalmazhat.");
                    continue;
                }
                foreach (char karakter in szo)
                {
                    if (!char.IsLetter(karakter))
                    {
                        ervenytelen = true;
                        break;
                    }
                }
                if(ervenytelen)
                {
                    Console.WriteLine("A megadott bemenet érvénytelen. A szó csak betűket tartalmazhat.");
                }
            } while (ervenytelen);
            return szo.ToLower();
        }

        static int EgyezoBetuk(string szo1, string szo2, out string egyezoHelyek)
        {
            int rovidebbikHossza = Math.Min(szo1.Length, szo2.Length);
            egyezoHelyek = string.Empty;
            int egyezokSzama = 0;
            for (int karakterIndex = 0; karakterIndex < rovidebbikHossza; karakterIndex++)
            {
                if(szo1[karakterIndex] == szo2[karakterIndex])
                {
                    egyezokSzama++;
                    egyezoHelyek += "*";
                }
                else
                {
                    egyezoHelyek += ".";
                }
            }
            return egyezokSzama;
        }

        static bool EredmenyFajlba(string fajlNev, string szo1, string szo2, string egyezoHelyek, int egyezokSzama)
        {
            bool feluliras = false;
            if(File.Exists(fajlNev))
            {
                feluliras = true;
            }
            using (StreamWriter fajl = new StreamWriter(fajlNev))
            {
                fajl.WriteLine(szo1);
                fajl.WriteLine(szo2);
                fajl.WriteLine(egyezoHelyek);
                fajl.WriteLine("Az egyező karakterek száma: " + egyezokSzama);
                fajl.Close();
            }
            return feluliras;
        }
    }
}
