﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolinomKiertekeles
{
    class Program
    {
        static void Main(string[] args)    // 6 pont
        {
            // Program céljának leírása.
            Console.WriteLine("Polinom kiértékelése adott pontokban.");
            // Polinom fokszámának beolvasása.
            Console.Write("Kérem adja meg, hanyadfokú polinomot kíván kiértékelni.\nn = ");
            int n = int.Parse(Console.ReadLine());
            // Polinom együtthatóinak beolvasása.
            Console.WriteLine("Kérem adja meg a polinom együtthatóit, kezdve a nulladik hatvány együtthatójával.");
            double[] p = EgyutthatoBeolvas(n);
            #region Nem része a ZH-nak
            // Polinom képletének kiírása. 
            //Console.Write("A megadott polinom:\nP(x) = ");
            //Console.WriteLine(PolinomSzoveg(p, "x"));
            #endregion
            // Kiértékelési intervallum adatainak beolvasása.
            Console.WriteLine("Kérem adja meg, hogy milyen intervallumon kívánja a polinomot kiértékelni.");
            Console.Write("Alsó határ:\nx1 = ");
            double x1 = double.Parse(Console.ReadLine());
            Console.Write("Felső határ:\nx2 = ");
            double x2 = double.Parse(Console.ReadLine());
            Console.Write("Intervallumok száma:\nN = ");
            int N = int.Parse(Console.ReadLine());
            // Felosztás generálása.
            double[] X = Felosztas(N, x1, x2);
            // Értékek számítása.
            Console.WriteLine("A polinom értéke a kívánt pontokban.");
            // Értékek kiírása.
            double[] Y = PolinomErtekek(p, X);
            ErtekekKiir(X, Y);
        }

        // Ez a metódus beolvassa és visszaadja az n-edfokú polinom együtthatóit.
        static double[] EgyutthatoBeolvas(int n)    // 5 pont
        {
            // Együtthatóvektor létrehozása (n-ed fokú polinomnak n+1 együtthatója van).
            double[] X = new double[n + 1];
            // Együtthatók beolvasása.
            for (int i = 0; i < n + 1; i++)
            {
                Console.Write("p{0:0} = ", i);
                double.TryParse(Console.ReadLine(), out X[i]);
            }
            // Visszatérés az együtthatóvektorral.
            return X;
        }

        #region Nem része a ZH-nak
        //// Ez a metódus a p paramétervektorral adott polinom képletének szövegét állítja elő.
        //static string PolinomSzoveg(double[] p, string nev)
        //{
        //    // Ebben a változóban tároljuk a képletet.
        //    string szoveg = "";
        //    // Polinom képlete:    y = p[0] + p[1].x + p[2].x^2 + p[3].x^3 + ... + p[n].x^n
        //    for (int i = 0; i < p.Length; i++)
        //    {
        //        szoveg += p[i] + "." + nev + "^" + i;
        //        // Az utolsó kivételével minden tag után egy + jelet fűzünk. 
        //        if (i < (p.Length - 1))
        //        {
        //            szoveg += " + ";
        //        }
        //    }
        //    return szoveg;
        //}
        #endregion

        // Ez a metódus egyenletesen n részre osztja az [x1; x2] intervallumot, és visszatér
        // a felosztással.
        static double[] Felosztas(int n, double x1, double x2)    // 7 pont
        {
            // Vektor létrehozása a pontok tárolásához.
            double[] X = new double[n + 1];
            // Lépésköz kiszámítása.
            double dx = (x2 - x1) / n;
            // Pontok generálása.
            for (int i = 0; i < n + 1; i++)
            {
                X[i] = x1 + i * dx;
            }
            // Visszatérés az egyenletesen mintavételezett pontokkal.
            return X;
        }

        // Ez a metódus kiértékeli a p paraméterekkel adott polinomot az x pontban.
        static double PolinomErtek(double[] p, double x)
        {
            // Polinom kiértékelése:    y = p[0] + p[1].x + p[2].x^2 + p[3].x^3 + ... + p[n].x^n
            double y = 0.0;
            for (int i = 0; i < p.Length; i++)
            {
                y += p[i] * Math.Pow(x, i);
            }
            // Eredmény visszaadása.
            return y;
        }

        // Ez a metódus kiértékeli a p paraméterekkel adott polinomot az X pontokban.
        static double[] PolinomErtekek(double[] p, double[] X)    // 8 pont
        {
            // Bemeneti pontokkal megegyező nagyságú eredményvektor létrehozása.
            double[] Y = new double[X.Length];
            // Minden egyes bemeneti ponthoz a polinom értékének meghatározása.
            for (int i = 0; i < X.Length; i++)
            {
                Y[i] = PolinomErtek(p, X[i]);
            }
            // Eredményvektor visszaadása.
            return Y;
        }

        // Ez a metódus kiírja a megfelelő értékpárokat az X és Y vektorból a konzolra.
        static void ErtekekKiir(double[] X, double[] Y)    // 4 pont
        {
            // Eredmények kiírása P(x) = y formátumban.
            for (int i = 0; i < X.Length; i++)
            {
                Console.WriteLine("P({0,10:#0.000}) = {1,10:#0.000}", X[i], Y[i]);
            }
        }
    }
}
