﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integralas
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Numerikus integrálás trapéz szabállyal.");
            Console.WriteLine("f(x) = sin(x), x E [0, pi]");
            Console.WriteLine("Kérem adja meg, hány egyenlő részre kívánja osztani az intervallumot");
            int n = Beolvas("n");
            double[] x = LinearisFelosztas(0, Math.PI, n);
            double[] y = Sin(x);
            double s = 0.0;
            Trapez(x, y, out s);
            Console.WriteLine("Az eredmény: {0:F18}", s);
            Console.WriteLine("A pontos eredmény: -cos(pi) - (-cos(0)) = -(-1) - (-1) = 2.0");
            ProgramVege();
        }

        static int Beolvas(string nev)
        {
            int szam;
            bool ervenyes;
            do
            {
                Console.Write(nev + " = ");
                ervenyes = int.TryParse(Console.ReadLine(), out szam);
                if (szam <= 0)
                {
                    ervenyes = false;
                }
            } while (!ervenyes);
            return szam;
        }

        static bool Trapez(double[] x, double[] y, out double s)
        {
            s = 0.0;
            if (x.Length != y.Length)
            {
                return false;
            }
            for (int i = 0; i < x.Length - 1; i++)
            {
                s += 0.5 * (y[i + 1] + y[i]) * (x[i + 1] - x[i]);
            }
            return true;
        }

        static double[] Sin(double[] x)
        {
            double[] sin = new double[x.Length];
            for (int i = 0; i < x.Length; i++)
            {
                sin[i] = Math.Sin(x[i]);
            }
            return sin;
        }

        static double[] LinearisFelosztas(double a, double b, int n)
        {
            if(n < 1)
            {
                return null;
            }
            double[] x = new double[n + 1];
            double felosztas = (b - a) / n;
            for (int i = 0; i < n + 1; i++)
            {
                x[i] = a + i * felosztas;
            }
            return x;
        }

        static void ProgramVege()
        {
            Console.Write("Nyomjon meg egy billentyűt a kilépéshez... ");
            Console.ReadKey();
        }
    }
}
