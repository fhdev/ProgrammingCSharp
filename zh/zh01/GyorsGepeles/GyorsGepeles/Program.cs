﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace ReflexJatek
{
    class Program
    {
        static void Main(string[] args)
        {
            // Játék és eredmények kiírása.
            Random veletlen = new Random();
            Console.WriteLine("Gyorsgépelés játék.");
            Console.WriteLine("Gépelje be a képernyőn megjelenő szöveget, amilyen gyorsan csak tudja.");
            Console.WriteLine("Kérem adja meg a nevét!");
            string jatekos = Beolvas();
            Console.WriteLine("Nyomjon meg egy billentyűt a kezdéshez.");
            Console.ReadKey();
            Console.WriteLine();
            double masodperc = Jatek(jatekos, veletlen);
            Console.WriteLine();
            Console.WriteLine("Az Ön ideje: {0:F5} sec.", masodperc);
        }
        // Csak betűket és számjegyeket tartalmazó szöveg beolvasása.
        static string Beolvas()
        {
            string bemenet;
            bool ervenytelen;
            do
            {
                ervenytelen = false;
                Console.Write(">> ");
                bemenet = Console.ReadLine();
                // Minden karakterre ellenőrizzük, hogy betű, vagy számjegy-e.
                foreach (char karakter in bemenet)
                {
                    if (ervenytelen = !char.IsLetterOrDigit(karakter))
                    {
                        break;
                    }
                }
            } while (ervenytelen || string.IsNullOrEmpty(bemenet));
            return bemenet;
        }
        // Véletlen szöveg generálása a veletlen objektum segítségével, hossz hosszúsággal.
        static string General(Random veletlen, int hossz)
        {
            string sorozat = string.Empty;
            for (int i = 0; i < hossz; i++)
            {
                char uj;
                double esely = veletlen.NextDouble();
                // 50 % eséllyel kisbetű.
                if (esely < 0.5)
                {
                    uj = (char)veletlen.Next(97, 123);
                }
                // 30 % eséllyel nagybetű.
                else if (esely < 0.8)
                {
                    uj = (char)veletlen.Next(65, 91);
                }
                // 20 % eséllyel számjegy.
                else
                {
                    uj = (char)veletlen.Next(48, 58);
                }
                sorozat += uj;
            }
            return sorozat;
        }
        // Eredmények fájlba írása.
        static void EredmenyFajlba(string fajlNev, string jatekos, long ms)
        {
            using (StreamWriter fajl = new StreamWriter(fajlNev))
            {
                fajl.WriteLine(jatekos + " " + ms);
            }
        }
        // Játék a véletlen random objektum segítségével. A felhasználónak be kell írnia a random
        // szöveget helyesen. Mérjük, hogy ez mennyi idő alatt sikerül. 
        static double Jatek(string jatekos, Random veletlen)
        {
            Stopwatch stopper = new Stopwatch();
            // Generált szöveg kiírása
            string sorozat = General(veletlen, 10);
            Console.WriteLine(sorozat);
            // Stopper indul
            stopper.Start();
            // Addig kérjük a beírást, amíg a felhasználó pontosan a megadott szöveget írja be.
            string bemenet;
            do
            {
                Console.WriteLine();
                bemenet = Beolvas();
            } while (bemenet != sorozat);
            // Időmérés leállítása.
            stopper.Stop();
            // Eredmények kiírása.
            EredmenyFajlba("eredmeny.txt", jatekos, stopper.ElapsedMilliseconds);
            // Eltelt idő másodpercekben.
            return stopper.ElapsedMilliseconds / 1000.0;
        }
    }
}
