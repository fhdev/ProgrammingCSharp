﻿using System;
using System.IO;

namespace BinarisAtvaltas
{
    class Program
    {
        static void Main(string[] args)
        {
            /* A program céljának rövid leírása. */
            Console.WriteLine("Convert a decimal number to binary representation.");
            Console.WriteLine("Please specify the number you want to convert.");
            /* Beolvasunk egy pozitív egész számot. */
            int number = ReadPositiveInt("a");
            /* A számot átkonvertáljuk kettes számrendszerbe. */
            int[] binaryNumber = ConvertToBinary(number);
            /* A kettes számrendszerbeli számot 0bXXX formátumban szöveges változóvá konvertáljuk. */
            string binaryString = WriteBinaryToString(binaryNumber);
            /* A konverzió eredményét a konzolra írjuk. */
            Console.WriteLine("{0:D} = " + binaryString, number);
            /* A konverzió eredményét elmentjük a binary.txt nevű fájlba. */
            bool saveSuccess = WriteBinaryToFile(binaryNumber, "binary.txt");
            /* A fájlba írást végző függvény visszatérési értékéből tudjuk, hogy sikeres volt-e a fájlba írás. Ezt 
             * a felhasználóval is közöljük. */
            if(saveSuccess)
            {
                Console.WriteLine("File has been saved successfully.");
            }
            else
            {
                Console.WriteLine("File already exists. No data has been saved.");
            }
            /* A kilépéshez megvárjuk a felhasználó billentyűleütését. */
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }/// <summary>
        /// Ez a függvény beolvassa a neki átadott nevű pozitív egész változó értékét a konzolról.
        /// </summary>
        /// <param name="name">A változó neve, amit a függvény a felhasználó számára kiír.</param>
        /// <returns>A beolvasott pozitív egész szám.</returns>
        static int ReadPositiveInt(string name)
        {
            /* Ebbe a változóba olvassuk be a konzolról a számot. */
            int number = 0;
            /* Ez a változó a bevitt szám érvénytelenségét jelzi. Alap esetben feltesszük, hogy érvényes lesz a bemenet. 
             */
            bool invalidInput = false;
            do
            {
                Console.Write("\t" + name + " = ");
                string input = Console.ReadLine();
                invalidInput = !int.TryParse(input, out number);
                /* Nem sikeres a konverzió, erről tájékoztatjuk a felhasználót. */
                if (invalidInput)
                {
                    Console.WriteLine("This is not an integer value. Please type in a positive integer value.");
                    continue;
                }
                invalidInput = (number <= 0);
                /* Ha a bemenet nem pozitív, akkor erről a felhasználót is tájékoztatjuk. */
                if (invalidInput)
                {
                    Console.WriteLine("This value is not a positive. Please type in a positive integer value.");
                }
            } while (invalidInput); /* A beolvasást addig ismételjük, amíg megfelelő bemenetet nem kapunk. */
            return number;
        }
        /// <summary>
        /// Ez a függvény kettes számrendszerbe váltja át a neki paraméterként megadott pozitív egész számot.
        /// </summary>
        /// <param name="number">Az átváltani kívánt pozitív egész szám.</param>
        /// <returns>Az átváltott kettes számrendszerbeli szám számjegyeit tartalmazó tömb.</returns>
        static int[] ConvertToBinary(int number)
        {
            /* Ez a változó megadja az átváltás számrendszerét. */
            int numberBase = 2;
            /* Kiszámítjuk, hogy hány számjegye lesz az adott számjegynek a kettes számrendszerben. Egy szám kettes
             * alapú logaritmusa megadja, hogy kettőt hanyadik hatványra kell emelni, hogy megkapjuk az adott számot.
             * Pl.: log_2(4)=2, log_2(8)=3. A számok ábrázolásához ennél egyel több számjegy szükséges, hiszen az első
             * helyiértéken a 2^0=1 áll. Tetszőleges alapú logaritmust nem tudunk számolni, ezért használjuk az alábbi 
             * azonosságot: log_a(b) = log_c(b) / log_c(a).
             */

            int numberOfDigits = 1;
            if (number > 0)
                numberOfDigits = Convert.ToInt32(Math.Floor(Math.Log(number) / Math.Log(Convert.ToDouble(numberBase))) + 1.0);
            /* Ebben a tömbben tároljuk majd el a kettes számrendszerbeli szám számjegyeit. */
            int[] binary = new int[numberOfDigits];
            /* Kiszámoljuk az egyes számjegyeket. */
            for(int i = 0; number > 0; i++)
            {
                /* Az adott helyiértéken lévő számjegy mindig a kettővel való egészosztás maradéka. */
                binary[i] = number % numberBase;
                /* A átváltandó számot minden lépésben kettővel el kell osztanunk. */
                number /= numberBase;
            }
            /* Visszaadjuk a számjegyeket tartalmazó tömböt. */
            return binary;
        }
        /// <summary>
        /// Ez a függvény szöveges változóba konvertálja a számjegyeinek tömbjével megadott kettes számrendszerbeli számot.
        /// </summary>
        /// <param name="binary">A kettes számrendszerbeli szám számjegyeit tartalmazó tömb.</param>
        /// <returns>A kettes számrendszerbeli szám szöveges megfelelője. </returns>
        static string WriteBinaryToString(int[] binary)
        {
            /* A bináris számot a 0b karakterekkel kezdjük el. */
            string binaryAsString = "0b";
            /* Mivel a tömb első eleme jelöli a legkisebb helyiértéket, utolsó eleme pedig a legnagyobb helyiértéket,
             * a számjegyek leírását a tömb utolsó elemétől kezdjük. 
             */
            for(int i = binary.Length - 1; i >= 0; i--)
            {
                /* A szöveghez hozzáadjuk az aktuális számjegyet. */
                binaryAsString += string.Format("{0}", binary[i]);
            }
            /* Visszatérünk az eredménnyel. */
            return binaryAsString;
        }
        /// <summary>
        /// Ez a függvény szöveges fájlba írja a a számjegyeinek tömbjével megadott kettes számrendszerbeli számot.
        /// </summary>
        /// <param name="binary">A kettes számrendszerbeli szám számjegyeit tartalmazó tömb.</param>
        /// <param name="path">A szöveges fájl elérési útvonala.</param>
        /// <returns>Logikai változó. Értéke igaz, ha a fájlbaírás sikeresen megtörtént, és hamis egyébként.</returns>
        static bool WriteBinaryToFile(int[] binary, string path)
        {  
            /* Ha a fájl már létezik, akkor nem írjuk felül, és hamis értékkel térünk vissza. */
            if(File.Exists(path))
            {
                return false;
            }
            /* Ha a fájl nem létezett, akkor létrehozzuk, és beleírjuk a bináris szám szöveges megfelelőjét, amit
             * az előző függvény segítségével állítunk elő. */
            using (StreamWriter file = new StreamWriter(path))
            {
                string content = WriteBinaryToString(binary);
                file.Write(content);
                /* A fájt az írás után bezárjuk, ezzel elmentjük. */
                file.Close();
            } /* Felszabadítjuk a fájlba íráshoz felhasznált erőforrásokat. */
            /* Igaz logikai értékkel térünk vissza a sikeres fájlbaírást követően. */
            return true;
        }
    }
}
