﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonzolKep
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Elkészítjük a képet. */
            char[,] image = CreateImage(20, 30);
            /* Ebbe a változóba fogjuk beolvasni a kilépés felkínálásakor beírt opciót. */
            string exitString;
            /* Amíg a felhasználó a kilépést nem választja. */
            do
            {
                /* Kiírjuk a képet a konzolra. */
                WriteImage(image);
                /* Elkérjük az átrajzolandó koordinátákat. */
                int[] coordinates = GetCoordinates();
                /* Ha a koordináták beolvasása sikeres, átállítjuk az adott karaktert. */
                if (coordinates != null)
                {
                    SetPixel(image, coordinates);
                }
                /* Felkínáljuk a felhasználónak a kilépés lehetőségét, ha begépeli az "exit" string-et. */
                Console.Write("Type 'exit' to end program: ");
                exitString = Console.ReadLine();
            } while (exitString != "exit");
        }
        /// <summary>
        /// Ez a metódus egy konzolra kirajzolható teljesen kitöltött képet, azaz egy minden elemében téglalap karaktereket tartalmazó 
        /// kétdimenziós karaktertömböt készít, és visszatér azzal. A függvény két paramétere a készítendő kép magassága és szélessége.
        /// </summary>
        /// <param name="height">A karaktertömb magassága, azaz sorainak száma.</param>
        /// <param name="width">A karaktertömb szélessége, azaz oszlopainak száma.</param>
        /// <returns></returns>
        private static char[,] CreateImage(int height, int width)
        {
            /* Egy konzol karakter helyét teljesen kitöltő téglalap █ */
            int box = 9608;
            /* A képünk egy kétdimenziós karakter tömb lesz, hiszen a konzolra csak karaktereket tudunk rajzolni */
            char[,] image = new char[height, width];
            /* A kép minden pontját, azaz minden karaktert feltöltünk a teli téglalap elemmel. */
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    image[i, j] = ((char)box);
                }
            }
            /* Visszatérünk a teljes kitöltött karakter tömbbel, azaz képpel. */
            return image;
        }
        /// <summary>
        /// Ez a metódus a neki átadott image kétdimenziós karaktertömbben (képben) a vect argumentuma által specifikált 
        /// helyen lévő karakter értékét teli téglalapról üresre, vagy üres téglalapról telire változtatja.
        /// </summary>
        /// <param name="image">A kétdimenziós karaktertömb, azaz kép.</param>
        /// <param name="vect">A megváltoztatandó karakter sor és oszlopindexét tartalmazó vektor.</param>
        /// <returns>Igaz, ha történt változtatás, hamis, ha az átadott indexek érvénytelensége maitt nem történt változtatás.</returns>
        private static bool SetPixel(char[,] image, int[] vect)
        {
            /* Egy konzol karakter helyét teljesen kitöltő téglalap █ */
            int box = 9608;
            /* Az ellentettjére változtatni kívánt karakter sora. */
            int row = vect[0];
            /* Az ellentettjére változtatni kívánt karakter oszlopa. */
            int col = vect[1];
            /* A karaktertömb (kép) méretei. */
            int height = image.GetLength(0);
            int width = image.GetLength(1);
            /* A megadott sorindex érvényességének ellenőrzése. */
            if ((row < 0) || (row >= height))
            {
                Console.WriteLine("Invalid row index.");
                /* A függvény nem folytatható, hamis értékkel lépünk ki. */
                return false;
            }
            /* A megadott oszlopindex érvényességének ellenőrzése. */
            if ((col < 0) || (col >= width))
            {
                Console.WriteLine("Invalid column index.");
                /* A függvény nem folytatható, hamis értékkel lépünk ki. */
                return false;
            }
            /* Az adott pixel értékéntek megváltoztatása az ellentettjére. */
            if ((int)image[row, col] == 0)
            {
                image[row, col] = (char)box;
            }
            else
            {
                image[row, col] = (char)0;
            }
            /* Sikeres változtatás esetén visszatérés igaz értékkel. */
            return true;
        }
        /// <summary>
        /// Ez a metódus a neki átadott kétdimenziós karaktertömböt a konzolra írja. 
        /// </summary>
        /// <param name="image">A konzolra írandó karaktertömb. </param>
        private static void WriteImage(char[,] image)
        {
            /* A kép előtt egy sor kihagyása. */
            Console.WriteLine();
            /* A karaktertömb (kép) méretei. */
            int height = image.GetLength(0);
            int width = image.GetLength(1);
            /* Az összes karakter kiiratása. */
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Console.Write(image[i, j]);
                }
                /* Egy adott sor befejezése után új sor kezdése. */
                Console.WriteLine();
            }
            /* A kép után egy sor kihagyása. */
            Console.WriteLine();
        }
        /// <summary>
        /// Ez a metódus a felhasználótól bekér egy X, Y koordináta párt, és azt sikeres beolvasás esetén egy
        /// egész tömbben adja vissza.
        /// </summary>
        /// <returns>A koordinátákat tartalmazó egész tömb, vagy sikertelen beolvasás esetén null.</returns>
        private static int[] GetCoordinates()
        {
            /* A felhasználó tájékoztatása. */
            Console.WriteLine("Please type in coordinate pairs in 'X1,Y1'");
            Console.WriteLine("format where X1 and Y1 are integers.");
            /* Bemenet beolvasása. */
            string input = Console.ReadLine();
            /* A koordinátákat vesszővel elválasztva kérnük, a vessző mentén szétválasztjuk a bemeneti szöveget. */
            string[] values = input.Split(',');
            /* Ha a bemenet vessző karakter mentén nem választható szét, akkor a bemenet nem tartalmazza a 
             * koordinátákat. 
             */
            if (values == null)
            {
                /* Felhasznló tájékoztatása és kilépés a függvényből. */
                Console.WriteLine("The input contains no coordinates. Returning...");
                return null;
            }
            /* Ha a bemenet nem csak két darab egymástól vesszővel elválasztott részt tartalmazott, akkor tartalma
             * biztosan érvénytelen. */
            if (values.Length != 2)
            {
                /* Felhasznló tájékoztatása és kilépés a függvényből. */
                Console.WriteLine("The number of coordinates is invalid. Returning...");
                return null;
            }
            /* A koordinátákat tartalmazó vektor létrehozása és feltöltése. */
            int[] vect = new int[2];
            for (int i = 0; i < values.Length; i++)
            {
                /* Ha a két koordináta közül bármelyik nem konvertálható át egész értékké, hibajelzés utéán null 
                 * értékkel lépünk ki a függvényből. */
                if (!int.TryParse(values[i], out vect[i]))
                {
                    /* Felhasznló tájékoztatása és kilépés a függvényből. */
                    Console.WriteLine("The coordinate {0:D} is not integer.", i);
                    return null;
                }
            }
            /* Visszatérünk a sikeresen beolvasott koordinátákkal. */
            return vect;
        }
    }
}
