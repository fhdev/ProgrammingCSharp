﻿using System;
using System.IO;

namespace VeletlenVektorok
{
    class Program
    {
        /* Egy térbeli pont tárolására alkalmas struktúra. */
        struct Vector
        {
            public double x;
            public double y;
            public double z;
        }

        static void Main(string[] args)
        {
            /* Röviden leírjuk a program célját. */
            Console.WriteLine("Renerating 10 random vectors with coordinates from interval [-100 -10] U [10 100] inside the R=100 sphere.");
            /* Ebben a konstansban tároljuk a generálni kívánt vektorok számát. */
            const int vectorCount = 10;
            /* Az újonnan létrehozott randomVectors változóba generálunk 10 darab véletlen vektort. */
            Vector[] randomVectors = GenerateVectors(vectorCount);
            /* A random vektorokat megjeleníthető szöveges formátumra konvertáljuk. */
            string stringOfVectors = WriteVectorsToString(randomVectors);
            /* Megjelenítjük a generált vektorokat. */
            Console.Write(stringOfVectors);
            /* A vectors.txt fájlba írjuk a generált vektorokat. */
            bool fileWriteSuccess = WriteVectorsToFile(randomVectors, "vectors.txt");
            /* A fájlba írást végző függvény visszatérési értékétől függően tájékoztatjuk a felhasználót a fájlba írás
             * eredményéről. */
            if (fileWriteSuccess)
            {
                Console.WriteLine("The file has been saved successfully.");
            }
            else
            {
                Console.WriteLine("No data has been saved.");
            }
            /* Kilépés előtt megvárjuk, míg a felhasználó leüt egy billentyűt. */
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
        /// <summary>
        /// Ez a függvény generál a neki paraméterként átadott számú véletlen koordinátákkal rendelkező vektort.
        /// Az egyes koordináták a [-100;-10] U [10;100] intervallumból kerülnek ki, és mindegyik vektor beleesik a
        /// 100.0 egység sugarú gömbbe. 
        /// </summary>
        /// <param name="numberOfVectors">A generálni kívánt véletlen vektorok száma.</param>
        /// <returns>A generált véletlen vektorok tömbje.</returns>
        static Vector[] GenerateVectors(int numberOfVectors)
        {
            /* Ebben a konstans változóban eltároljuk a gömb sugarát. */
            const double radiusOfSphere = 100.0;
            /* Ebben a kosntans változóban eltároljuk az intervallum másik határát. */
            const double offset = 10.0;
            /* Létrehozzuk a vektorok töbjét. */
            Vector[] vectors = new Vector[numberOfVectors];
            /* Ezzel a random objektummal végezzük majd a véletlenszám generálást. */
            Random randomGenerator = new Random();
            /* Végigmegyünk a vektorokat tartalmazó tömb minden elemén. */
            for (int i = 0; i < numberOfVectors; i++)
            {
                /* Ebben a változóban fogjuk tárolni az aktuálisan generált koordinátákkal rendelkező vektor hosszát. */
                double lengthOfVector = 0.0;
                do
                {
                    /* Generáljuk a vektor koordinátáit. */
                    vectors[i].x = GenerateRandomSign(randomGenerator) * (offset + (radiusOfSphere - offset) * randomGenerator.NextDouble());
                    vectors[i].y = GenerateRandomSign(randomGenerator) * (offset + (radiusOfSphere - offset) * randomGenerator.NextDouble()); 
                    vectors[i].z = GenerateRandomSign(randomGenerator) * (offset + (radiusOfSphere - offset) * randomGenerator.NextDouble());
                    /* Kiszámítjuk a vektor hosszát. */
                    lengthOfVector = Math.Sqrt(vectors[i].x * vectors[i].x + vectors[i].y * vectors[i].y + vectors[i].z * vectors[i].z);
                } while (lengthOfVector >= radiusOfSphere); /* A generálást újra és újra elvégezzük, amíg a vektor bele 
                                                             * nem esik a megadott sugarú gömbbe. */
            }
            /* Visszaétrünk a generált vektorok tömbjével. */
            return vectors;
        }
        /// <summary>
        /// Ez a függvény a neki átadott Random objektum segíségével véletlenszerű előjelet generál.
        /// </summary>
        /// <param name="randomGenerator">Random objektum a véletlenszám generáláshoz.</param>
        /// <returns>Véletlen előjel. 1.0 vagy -1.0 véletlenszerűen.</returns>
        static double GenerateRandomSign(Random randomGenerator)
        {
            /* Generálunk egy számot 0 és int.MaxValue közé. Ha a szám az intervallum első felébe esik, 1.0 lesz az
             * előjel, egyébként -1.0 
             */
            if (randomGenerator.Next() < (int.MaxValue / 2))
            {
                return 1.0;
            }
            return -1.0;
        }
        /// <summary>
        /// Ez a függvény vektorok tömbjét egy a konzolon megjeleníthető szöveges változóvá konvertálja.
        /// </summary>
        /// <param name="vectors">A vektorok tömbje, melyeket szöveggé szeretnénk konvertálni.</param>
        /// <returns>A konzolon megjeleníthető szöveg.</returns>
        static string WriteVectorsToString(Vector[] vectors)
        {
            /* Ebbe a változóba fojuk beleírni a vektorokat. */
            string vectorsInStrig = string.Empty;
            /* Végigmegyünk az összes vektoron, és a koordinátákat a szövegbe írjuk. */
            for (int i = 0; i < vectors.Length; i++)
            {
                vectorsInStrig += string.Format("[{0,7:F3} {1,7:F3} {2,7:F3}]\n", vectors[i].x, vectors[i].y, vectors[i].z);
            }
            /* Visszatérünk a szöveges változóval. */
            return vectorsInStrig;
        }
        /// <summary>
        /// Ez a függvény vektorok tömbjét egy szöveges fájlba írja.
        /// </summary>
        /// <param name="vectors">A vektorok tömbje melyeket a fájlba szeretnénk menteni.</param>
        /// <param name="path">A fájl elérési útvonala.</param>
        /// <returns>Logikai változó. Igaz érték, ha a fájlba írás sikeres volt, egyébként hamis.</returns>
        static bool WriteVectorsToFile(Vector[] vectors, string path)
        {
            /* Ha a fájl már létezik, akkor csak felhasználói utasításra írjuk felül. */
            if (File.Exists(path))
            {
                /* Kiírjuk a felhasználónak, hogy a fájl már létezik, és megkérdezzük hogy felül kívánja-e írni azt. */
                Console.Write("The file already exists. Press [O] to overwrite it. ");
                ConsoleKeyInfo keyInfo = Console.ReadKey();
                Console.WriteLine();
                /* Ha a felhasználó nem a felülírást válaszja, visszatérünk a függvényből hamis logikai értékkel. */
                if(char.ToLower(keyInfo.KeyChar) != 'o')
                {
                    return false;
                }
            }
            /* Ha a fájl még nem létezett, vagy a felhasználó a felülírást választota, akkor az előző függvény
             * segítségével szöveggé konvertált vektorokat kiírjuk a fájlba. */
            StreamWriter file = new StreamWriter(path);
            string content = WriteVectorsToString(vectors);
            file.Write(content);
            /* Bezárjuk és ezzel elmentjük a fájlt. */
            file.Close();
            /* Felszabadítjuk a fájl írásához használt erőforrásokat. */
            file.Dispose();
            /* Igaz logikai értékkel jelezzük a fájlba írás sikerességét. */
            return true;
        }
    }
}
