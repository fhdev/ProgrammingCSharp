﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hisztogram
{
    class Program
    {
        static void Main(string[] args)
        {
            const double minimum = 0.0;
            const double maximum = 10.0;
            const int meresekSzama = 10;
            Console.WriteLine("Gyakoriság és kumulált gyakoriság hisztogramok számítása.");
            Console.WriteLine("Kérem adja meg a mérési pontokat (10 db., 0.0 és 10.0 között).");
            double[] meresek = TombBeolvasas(meresekSzama, "meresek", minimum, maximum);
            int[] gyakorisagok = Gyakorisag(meresek, minimum, maximum, meresekSzama);
            Console.WriteLine("A gyakoriság hisztogram (10 db. intervallum 0.0 és 10.0 között).");
            TombKonzolraIras("gyakorisag", gyakorisagok);
            //int[] kumulaltGyakorisagok = KumulaltGyakorisag(meresek, minimum, maximum, meresekSzama);
            //Console.WriteLine("A kumulált gyakoriság hisztogram (10 db. intervallum 0.0 és 10.0 között).");
            //TombKonzolraIras("kumulaltGyakorisag", kumulaltGyakorisagok);
            ProgramVege();
        }

        static void ProgramVege()
        {
            Console.Write("Kérem nyomjon meg egy gombot a kilépéshez...");
            Console.ReadKey();
        }

        static double ErtekBeolvasas(string nev, double min, double max)
        {
            double ertek;
            bool ervenytelen;
            do
            {
                Console.Write(nev + " >> ");
                ervenytelen = !double.TryParse(Console.ReadLine(), out ertek);
                if(ervenytelen || (ertek < min) || (ertek > max))
                {
                    Console.WriteLine("Az érték nem megfelelő. Kérem adjon meg {0:F3} és {1:F3} közötti értéket.", min, max);
                    ervenytelen = true;
                }
            } while (ervenytelen);
            return ertek;
        }

        static double[] TombBeolvasas(int ertekekSzama, string nev, double min, double max)
        {
            double[] ertekek = new double[ertekekSzama];
            for (int i = 0; i < ertekekSzama; i++)
            {
                string elemNev = string.Format(nev + "[{0:D}]", i);
                ertekek[i] = ErtekBeolvasas(elemNev, min, max);
            }
            return ertekek;
        }

        static void TombKonzolraIras(string nev, int[] ertekek)
        {
            for (int i = 0; i < ertekek.Length; i++)
            {
                Console.WriteLine(nev + "[{0:D}] = {1:D}", i, ertekek[i]);
            }
        }


        static int[] Gyakorisag(double[] meresek, double min, double max, int intervallumokSzama)
        {
            int[] gyakorisagok = new int[intervallumokSzama];
            double intervallum = (max - min) / intervallumokSzama;
            for(int gyakorisagIndex = 0; gyakorisagIndex < intervallumokSzama; gyakorisagIndex++)
            {
                double alsoHatar = min + gyakorisagIndex * intervallum;
                double felsoHatar = alsoHatar + intervallum;
                for (int meresIndex = 0; meresIndex < meresek.Length; meresIndex++)
                {
                    if ((meresek[meresIndex] > alsoHatar) && (meresek[meresIndex] <= felsoHatar))
                    {
                        gyakorisagok[gyakorisagIndex]++;
                    }  
                }

            }
            return gyakorisagok;
        }

        //static int[] KumulaltGyakorisag(double[] meresek, double min, double max, int intervallumokSzama)
        //{
        //    int[] kumulaltGyakurisagok = new int[intervallumokSzama];
        //    double intervallum = (max - min) / intervallumokSzama;
        //    for (int gyakorisagIndex = 0; gyakorisagIndex < intervallumokSzama; gyakorisagIndex++)
        //    {
        //        double felsoHatar = min + (gyakorisagIndex + 1) * intervallum;
        //        for (int meresIndex = 0; meresIndex < meresek.Length; meresIndex++)
        //        {
        //            if (meresek[meresIndex] <= felsoHatar)
        //            {
        //                kumulaltGyakurisagok[gyakorisagIndex]++;
        //            }
        //        }
        //    }
        //    return kumulaltGyakurisagok;
        //}
    }
}
