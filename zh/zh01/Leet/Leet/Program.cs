﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dummy
{
    class Program
    {
        static void Main(string[] args)    // 4 pont
        {
            // Program céljának leírása.
            Console.WriteLine("Szöveg alkalmas karaktereinek kicserélése számokkal. (Leet fordítás.)");
            // Fordítani kívánt szöveg beolvasása.
            Console.WriteLine("Kérem adja meg a lefordítani kívánt szöveget!");
            string szoveg = BeolvasTobbSort();
            // Fordítás leet szöveggé, melyben egyes betűk helyén számok jelennek meg. 
            int n;
            string leet = Leet(szoveg, out n);
            // Eredmények kiírása konzolra.
            Console.WriteLine("A lefordított szöveg:");
            Console.WriteLine(leet);
            Console.WriteLine("A kicserélt karakterek száma: {0:D} .", n);
            // Eredmények kiírása fájlba. 
            Console.WriteLine("Az eredmények fájlba írása.");
            FajlbaIras("forditas.txt", szoveg, leet);
        }

        // Ez a metódus több soros szöveget olvas fel a konzolról. Visszatérési értéke a beolvasott
        // szöveg.
        static string BeolvasTobbSort()    // 8 pont
        {
            // Ebbe a változóba olvassuk be a szöveget.
            string szoveg = "";
            // Ebbe a változóba olvassuk be a felhasználó válaszát, hogy akar-e további sorokat
            // megadni. 
            string valasz = "";
            do
            {
                // Sor beolvasása.
                Console.Write(">> ");
                szoveg += Console.ReadLine() + "\r\n";
                // Döntés a beolvasás folytatásáról. 
                Console.Write("Kíván további sorokat bevinni? (Igen/Nem)? ");
                valasz = Console.ReadLine().ToUpper();
            } while (valasz == "IGEN");
            return szoveg;
        }

        // Ez a metódus leet nyelvre fordítja az eredeti argumentumában megadott szöveget, és a
        // csereSzam argumentumában megadja az elvégzett karaktercserék számát. Visszatérési értéke
        // a lefordított szöveg.
        static string Leet(string eredeti, out int csereSzam)    // 10 pont
        {
            // Cserélendő betűk.
            string betuk = "ABEGIOQSTZ";
            // A cserélendő betűknek megfelelő számok.
            string szamok = "4836109572";
            // Ebbe a változóba kerül bele a lefordított szöveg.
            string leet = "";
            // Ebben a változóban számoljuk a cserék számát.
            csereSzam = 0;
            // Eredeti szöveg nagybetűssé alakítása.
            eredeti = eredeti.ToUpper();
            // Eredeti szövegben a cserélendő betűk kicserélése a megdott számokra.
            for (int i = 0; i < eredeti.Length; i++)
            {
                bool csere = false;
                for (int j = 0; j < betuk.Length; j++)
                {
                    if (eredeti[i] == betuk[j])
                    {
                        leet += szamok[j];
                        csereSzam++;
                        csere = true;
                        break;
                    }
                }
                if (!csere)
                {
                    leet += eredeti[i];
                }
            }
            // Leet nyelvű szöveg visszaadása. 
            return leet;
        }

        // Ez a metódus a fajlNev elérési útvonallal megadott szövegfájlba menti az eredeti és leet
        // argumentumaiban található szöveget. 
        static void FajlbaIras(string fajlNev, string eredeti, string leet)    // 6 pont
        {
            // Figyelmeztetés felülírásra.
            if (File.Exists(fajlNev))
            {
                Console.WriteLine("A " + fajlNev + " fájl már létezik. Felülírás...");
            }
            // Új fájl nyitása írásra.
            StreamWriter fajl = new StreamWriter(fajlNev);
            // Megfelelő sorok kiírása.
            fajl.WriteLine("EREDETI SZÖVEG  ------------------------");
            fajl.WriteLine(eredeti);
            fajl.WriteLine("FORDÍTÁS -------------------------------");
            fajl.WriteLine(leet);
            // Fájl bezárása (és kiírása) valamint az erőforrások felszabadítása. 
            fajl.Close();
            fajl.Dispose();
        }
    }
}
