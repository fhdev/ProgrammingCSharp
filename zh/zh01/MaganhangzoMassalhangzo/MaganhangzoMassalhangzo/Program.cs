﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaganhangzoMassalhangzo
{
    class Program
    {
        static void Main(string[] args)
        {
            string szo = SzovegBeolvas();
            int[] szamlalo = Elemzes(szo);
            Console.WriteLine("Szóközök száma:      {0,3}", szamlalo[0]);
            Console.WriteLine("Írásjelek száma:     {0,3}", szamlalo[1]);
            Console.WriteLine("Magánhangzók száma:  {0,3}", szamlalo[2]);
            Console.WriteLine("Mássalhangzók száma: {0,3}", szamlalo[3]);
            Console.WriteLine("A behelyettesített jelek:");
            Console.WriteLine(Behelyettesites(szo));
        }

        static string SzovegBeolvas()
        {
            string szo;
            bool ok;
            do
            {
                ok = true;
                Console.Write("Kérem adjon meg egy szöveget: ");
                szo = Console.ReadLine();
                foreach (char karakter in szo)
                {
                    if(!(char.IsLetter(karakter) || char.IsWhiteSpace(karakter) || char.IsPunctuation(karakter)))
                    {
                        ok = false;
                        break;
                    }
                }
                if(!ok) Console.WriteLine("A megadott bemenet tisztán szöveg!");
            } while (!ok);
            return szo;
        }

        static bool MaganhangzoE(char karakter)
        {
            string maganhangzok = "aáeéiíoóöőuúüű";
            foreach (char maganhangzo in maganhangzok)
            {
                if (char.ToLower(karakter) == maganhangzo) return true;
            }
            return false;
        }

        static int[] Elemzes(string szoveg)
        {
            int[] szamlalo = new int[4];
            foreach(char karakter in szoveg)
            {
                if (char.IsWhiteSpace(karakter)) szamlalo[0]++;
                else if (char.IsPunctuation(karakter)) szamlalo[1]++;
                else if (MaganhangzoE(karakter)) szamlalo[2]++;
                else szamlalo[3]++;
            }
            return szamlalo;
        }



        static string Behelyettesites(string szoveg)
        {
            string behelyettesites = "";
            foreach(char karakter in szoveg)
            {
                if(char.IsWhiteSpace(karakter)) behelyettesites += '_';
                else if(char.IsPunctuation(karakter)) behelyettesites += '#';
                else if (MaganhangzoE(karakter)) behelyettesites += '@';
                else behelyettesites += '*';
            }
            return behelyettesites;
        }
    }
}
