﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeletlenKupok
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ZH2B 2015.12.10. Hegedüs Ferenc");
            Cone[] cones = GetRandomCones();
            ConesToConsole(cones);
            int hit = HowMany(cones, 1000.0, 300.0);
            Console.WriteLine("Number of hits: {0:D}.", hit);
            WaitForExit();
        }

        static Cone[] GetRandomCones()
        {
            Cone[] cones = new Cone[10];
            for (int i = 0; i < cones.Length; i++)
                cones[i] = new Cone();
            return cones;
        }

        static void ConesToConsole(Cone[] cones)
        {
            for(int i = 0; i < cones.Length; i++)
            {
                Console.Write("Cone {0:D}: ", i);
                cones[i].ToConsole();
            }
        }

        static int HowMany(Cone[] cones, double maxVolume, double minSurfaceArea)
        {
            int counter = 0;
            for (int i = 0; i < cones.Length; i++)
                if ((cones[i].GetSurfaceArea() > minSurfaceArea) && (cones[i].GetVolume() < maxVolume))
                    counter++;
            return counter;
        }

        static void WaitForExit()
        {
            Console.Write("Press any keys to exit...");
            Console.ReadKey();
        }
    }

    class Cone
    {
        public double Radius;
        public double Height;
        private static readonly Random Rand = new Random();
        private const double maxRadius = 20;
        private const double maxHeight = 100;

        public Cone()
        {
            Radius = Rand.NextDouble() * maxRadius;
            Height = Rand.NextDouble() * maxHeight;
        }

        public Cone(double radius, double height)
        {
            Radius = radius;
            Height = height;
        }

        public double GetSurfaceArea()
        {
            return Math.PI * Radius * (Radius + Math.Sqrt(Math.Pow(Radius, 2.0) + Math.Pow(Height, 2.0)));
        }

        public double GetVolume()
        {
            return Math.Pow(Radius, 2.0) * Math.PI * Height / 3.0;
        }

        public void ToConsole()
        {
            Console.WriteLine("R = {0:F2} mm, H = {1:F2} mm, SA = {2:F2} mm^2, V = {3:F2} mm^3", Radius, Height, GetSurfaceArea(), GetVolume());
        }
    }
}
